package com.george.way.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutUsActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;

    @BindView(R.id.webViewAboutUs)
    WebView webViewAboutUs;

    @BindView(R.id.textViewHeader)
    TextView textViewHeader;

    @BindView(R.id.app_bar)
    Toolbar app_bar;

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_about_us);

        /**
         * init
         */
        ButterKnife.bind(this);

        /**
         * init
         */
        init();

        /**
         * set on click events
         */
        setOnClickEvents();


        /**
         *get about us screen data
         */
        //getAboutUsRequest();

    }



    /**
     * setting up onclick on views
     */
    private void setOnClickEvents() {

        imageViewBack.setOnClickListener(this);

     }


    /* * initialize class
    */
    private void init() {

        mActivity = this;
        textViewHeader.setText(getResources().getString(R.string.About_Us));

        /**
         * Initializing objects from inflating from xml to perform actions
         */
        webViewAboutUs.getSettings().setJavaScriptEnabled(true);
        webViewAboutUs.getSettings().setBuiltInZoomControls(true);
        webViewAboutUs.getSettings().setUseWideViewPort(true);
        webViewAboutUs.getSettings().setLoadWithOverviewMode(true);
        webViewAboutUs.loadUrl(ApiClient.BASE_URL+ApiClient.ABOUT_US_API);
        webViewAboutUs.getSettings().setDisplayZoomControls(true);
        webViewAboutUs.getSettings().setBuiltInZoomControls(true);
        webViewAboutUs.setWebViewClient(new SSLTolerentWebViewClient());
        WebSettings webSettings = webViewAboutUs.getSettings();
        webSettings.setTextZoom(webSettings.getTextZoom() + 90);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(mActivity);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imageViewBack:
                finish();
                ViewAnimUtils.activityExitTransitions(mActivity);
                break;

            case R.id.imageViewCalculator:

                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;

        }
    }

    /**
     *get about us screen data
     */
    public void getAboutUsRequest() {


        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.getAboutUsRequest();

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                         /*
                            JSONArray mJSONArray = new JSONArray(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJSONArray.toString());
                            JSONObject mmJSONObj = mJSONArray.getJSONObject(0);
*/
                            ResponseBody mResponseBody = response.body();
                            String about_us =new String(mResponseBody.bytes());


                            if (!TextUtils.isEmpty(about_us)) {

                                /**
                                 * show about us content
                                 */
                                updateDataInWebView(about_us);

                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack3);
        }
    }

    DialogInterface.OnClickListener networkCallBack3 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
           getAboutUsRequest();
        }
    };


    /**
     * update data into web view
     */
    private void updateDataInWebView( String dataHtml){

        String output = dataHtml.replaceAll("\"\"", "\'");
        output = output.replaceAll("&lt;","<");
        output = output.replaceAll("&gt;",">");
        output = output.replaceAll("&amp;nbsp;","<br/>");

        try {
            webViewAboutUs.loadData((URLEncoder.encode(output, "utf-8").replaceAll("\\+", " ")), "text/html; charset=UTF-8", null);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }

    // SSL Error Tolerant Web View Client
    private class SSLTolerentWebViewClient extends WebViewClient {


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            /**
             * dismiss dialog if open
             */
            GWProgressDialog.getProgressDialog(mActivity).showDialog1();

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            /**
             * dismiss dialog if open
             */
            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
        }
    }


}