package com.george.way.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.george.way.R;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.identity.intents.model.UserAddress;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.CardInfo;
import com.google.android.gms.wallet.CardRequirements;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentMethodTokenizationParameters;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.TransactionInfo;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.stripe.android.model.Token;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JAI on 4/15/2018.
 */

public class BuyAcitvity extends NaviBaseActivity {
    private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 7005;
    private Button btn30min;
    private Button btn150min;
    private Button btn300min;
    private Double amount = 0.0;
    private int totalSecond = 0;
    /**
     * pay pal configurations
     */
    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(AppConstant.KEY_CLIENT_ID)
            .merchantName("GeorgeWay");

    /**
     * declare objects and views of xml of the class
     */
    private Activity mActivity;
    private static final int REQUEST_CODE_PAYMENT = 1;
    private BottomSheetDialog paymentBottomSheetDialog;

    PaymentsClient paymentsClient;
    private Boolean isGooglePayReady=null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setTitle("Buy Minutes");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        paymentBottomSheetDialog = new BottomSheetDialog(BuyAcitvity.this);
        initView();

        paymentsClient =
                Wallet.getPaymentsClient(this,
                        new Wallet.WalletOptions.Builder().setEnvironment(WalletConstants.ENVIRONMENT_TEST)
                                .build());
        isReadyToPay();

        btn30min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                totalSecond = 60*30;
                amount = 15.00;
                openPaymetDialog();
            }
        });

        btn150min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                totalSecond = 60*150;
                amount = 75.00;
                openPaymetDialog();
            }
        });

        btn300min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                totalSecond = 60*300;
                amount = 150.00;
                openPaymetDialog();
            }
        });
    }

    @Override
    int getContentViewId() {
        return R.layout.buy_layout_activity;
    }

    @Override
        int getNavigationMenuItemId() {
            return R.id.navigation_payment;
    }

    private void initView() {
        btn30min = (Button) findViewById(R.id.btn30min);
        btn150min = (Button) findViewById(R.id.btn150min);
        btn300min = (Button) findViewById(R.id.btn300min);
    }

    @Override
    public boolean onNavigateUp() {
        onBackPressed();
        return super.onNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    TextView googlePayBtn;
    private void openPaymetDialog() {

        paymentBottomSheetDialog.setContentView(R.layout.caomplete_action);
        TextView paypalButton = (TextView) paymentBottomSheetDialog.findViewById(R.id.paypalButton);
        TextView stripButton = (TextView) paymentBottomSheetDialog.findViewById(R.id.stripButton);
        TextView cancelButton = (TextView) paymentBottomSheetDialog.findViewById(R.id.cancelButton);
        TextView payAmount_txt = (TextView)paymentBottomSheetDialog.findViewById(R.id.payAmount_txt);
        /*googlePayBtn = paymentBottomSheetDialog.findViewById(R.id.google_pay_btn);

        if (isGooglePayReady!=null){
            if (isGooglePayReady){
                googlePayBtn.setVisibility(View.VISIBLE);
                googlePayBtn.setEnabled(true);
            }else{
                googlePayBtn.setVisibility(View.GONE);
                googlePayBtn.setEnabled(false);
            }
        }

        googlePayBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PaymentDataRequest request = createPaymentDataRequest();
                        if (request != null) {
                            AutoResolveHelper.resolveTask(
                                    paymentsClient.loadPaymentData(request),
                                    BuyAcitvity.this,
                                    LOAD_PAYMENT_DATA_REQUEST_CODE);
                        }
                    }
                });*/
        payAmount_txt.setText("Pay $"+amount);


        paymentBottomSheetDialog.show();

        paypalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentBottomSheetDialog.cancel();
                 onBuyPressed();

            }
        });

        stripButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentBottomSheetDialog.cancel();

                startActivity(new Intent(BuyAcitvity.this, Stripe_Payment_Activity.class).putExtra("plan_type","Monthly").putExtra("plan_time","1")
                        .putExtra("id","1").putExtra("amount",amount*100).putExtra("time",totalSecond));
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentBottomSheetDialog.cancel();
            }
        });

    }

    public void onBuyPressed() {

        PayPalPayment thingToBuy = getStuffToBuy(PayPalPayment.PAYMENT_INTENT_ORDER,amount,"Monthly");
        Intent intent = new Intent(BuyAcitvity.this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);

    }

    /*
     * This method shows use of optional payment details and item list.
     */
    private PayPalPayment getStuffToBuy(String paymentIntent, Double aDouble, String plan_type) {
        Double amountTotal = 0.0;

        try {
            amountTotal = aDouble;
        } catch (Exception e) {
            e.printStackTrace();
        }

        PayPalItem[] items = {new PayPalItem(plan_type, 1, new BigDecimal(amountTotal), "USD",
                "")};
        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal("0");
        BigDecimal tax = new BigDecimal("0");
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        PayPalPayment payment = new PayPalPayment(amount, "USD", plan_type, paymentIntent);
        payment.items(items).paymentDetails(paymentDetails);
        payment.custom("");

        return payment;
    }


    protected void displayResultText(String result) {
        new SingleActionDialog().showDialogNormal(result,BuyAcitvity.this,false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i("tg", confirm.toJSONObject().toString(4));
                        Log.i("tg", confirm.getPayment().toJSONObject().toString(4));

                        updatePlanOnServerWebApi();


                    } catch (JSONException e) {

                        Toast.makeText(BuyAcitvity.this, "Sorry something went wrong.", Toast.LENGTH_SHORT).show();

                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {

                displayResultText("Payment process has been cancelled.");

            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {

                displayResultText("Invalid Payment details was submitted.");

            }
        } else if (requestCode == LOAD_PAYMENT_DATA_REQUEST_CODE) {

            switch (resultCode) {

                case Activity.RESULT_OK:
                    PaymentData paymentData = PaymentData.getFromIntent(data);
                    // You can get some data on the user's card, such as the brand and last 4 digits
                    CardInfo info = paymentData.getCardInfo();
                    // You can also pull the user address from the PaymentData object.
                    UserAddress address = paymentData.getShippingAddress();
                    // This is the raw JSON string version of your Stripe token.
                    String rawToken = paymentData.getPaymentMethodToken().getToken();

                    // Now that you have a Stripe token object, charge that by using the id
                    Token stripeToken = Token.fromString(rawToken);
                    if (stripeToken != null) {
                        // This chargeToken function is a call to your own server, which should then connect
                        // to Stripe's API to finish the charge.
                        //chargeToken(stripeToken.getId());
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    break;
                case AutoResolveHelper.RESULT_ERROR:
                    Status status = AutoResolveHelper.getStatusFromIntent(data);
                    // Log the status for debugging
                    // Generally there is no need to show an error to
                    // the user as the Google Payment API will do that
                    break;
                default:
                    // Do nothing.
                    break;

            }
        }
    }



    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    /**
     * call api
     */
    public void updatePlanOnServerWebApi() {

        if (NetworkUtil.isConnected(BuyAcitvity.this)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(BuyAcitvity.this);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(BuyAcitvity.this).showDialog1();
                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(BuyAcitvity.this);
                String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);
                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.subscribePlanRequest(
                        userId,
                        "1",
                        "1",""+amount,"Monthly","",""+totalSecond);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(BuyAcitvity.this).dismissDialog1();

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");


                            if (!TextUtils.isEmpty(status_code)) {

                                if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {

                                    // SingleActionDialog.getDialogObj(BuyAcitvity.this).showDialogNormal(success_message);
                                    displayResultText("Your Plan has been subscribed successfully.Thanks for subscribing the plan.");

                                    try {
                                        String success_message = mJsonObj.optString("success_message");
                                        JSONArray dataJsonArr = mJsonObj.getJSONArray("data");
                                        JSONObject dataJsonObj = dataJsonArr.getJSONObject(0);
//                                        String plan_start = dataJsonObj.optString("plan_start");
//                                        String plan_end = dataJsonObj.optString("plan_end");
//
//                                        purchasedPlanDatum.setPlanStartDate(plan_start);
//                                        purchasedPlanDatum.setPlanEndDate(plan_end);


                                        /**
                                         * updating plan data
                                         */
                                        getHomeDataWebApi();


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
//                                    purchasedPlanDatum.setPlanSubscribed(true);
//                                    mPlansListingAdapter.notifyDataSetChanged();

                                } else {

                                    String error_message = mJsonObj.optString(AppConstant.getObj().ERROR_MESSAGE);
                                    new SingleActionDialog().showDialogNormal(error_message,BuyAcitvity.this,false);

                                }
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(BuyAcitvity.this).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(BuyAcitvity.this, networkCallBack2);
        }
    }

    DialogInterface.OnClickListener networkCallBack2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            updatePlanOnServerWebApi();
        }
    };

    /**
     * call api
     */
    public void getHomeDataWebApi() {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {


                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.getHomeDataRequest(userId);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {

                            ResponseBody mResponseBody = response.body();


                            if (mResponseBody != null) {

                                JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                                Log.e("tg", "response from server = " + mJsonObj.toString());
                                String status_code = mJsonObj.optString("status_code");
                                if (!TextUtils.isEmpty(status_code)) {

                                    if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {
                                        try {
                                            JSONArray dataJsonArr = mJsonObj.getJSONArray("data");
                                            JSONObject dataJsonObj = dataJsonArr.getJSONObject(0);

                                            PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(mActivity);

                                            try {
                                                String is_plan_active = dataJsonObj.optString("is_plan_active");
                                                String plan_id = dataJsonObj.optString("plan_id");
                                                String plan_name = dataJsonObj.optString("plan_name");
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_PLAN_ID, plan_id);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_SUBSCRIPTION_PLAN, plan_name);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_IS_PLAN_ACTIVE, is_plan_active);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                String isMathClass = dataJsonObj.optString("isMathClass");
                                                String isTestPreparation = dataJsonObj.optString("isTestPreparation");
                                                String isTutoringService = dataJsonObj.optString("isTutoringService");
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_MATH_CLASS, isMathClass);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_TEST_PREPARATION, isTestPreparation);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_TUTORING_SERVICE, isTutoringService);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void isReadyToPay() {
        IsReadyToPayRequest request = IsReadyToPayRequest.newBuilder()
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                .build();
        Task<Boolean> task = paymentsClient.isReadyToPay(request);
        task.addOnCompleteListener(
                new OnCompleteListener<Boolean>() {
                    public void onComplete(Task<Boolean> task) {
                        try {
                            boolean result = task.getResult(ApiException.class);
                            if(result) {
                                isGooglePayReady = true;
                                if (googlePayBtn!=null){
                                    googlePayBtn.setVisibility(View.VISIBLE);
                                    googlePayBtn.setEnabled(true);
                                }
                            } else {
                                isGooglePayReady = false;
                                if (googlePayBtn!=null) {
                                    googlePayBtn.setVisibility(View.GONE);
                                    googlePayBtn.setEnabled(false);
                                }
                                //hide Google as payment option
                            }
                        } catch (ApiException exception) { }
                    }
                });
    }


    private PaymentMethodTokenizationParameters createTokenizationParameters() {
        return PaymentMethodTokenizationParameters.newBuilder()
                .setPaymentMethodTokenizationType(WalletConstants.PAYMENT_METHOD_TOKENIZATION_TYPE_PAYMENT_GATEWAY)
                .addParameter("gateway", "stripe")
                .addParameter("stripe:publishableKey", "pk_test_hEGUHsM2TDrZlUehpvuLyUR5")
                .addParameter("stripe:version", "2018-09-24")
                .build();
    }


    private PaymentDataRequest createPaymentDataRequest() {
        PaymentDataRequest.Builder request =
                PaymentDataRequest.newBuilder()
                        .setTransactionInfo(
                                TransactionInfo.newBuilder()
                                        .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
                                        .setTotalPrice(String.format(Locale.getDefault(),"%.2f",amount))
                                        .setCurrencyCode("USD")
                                        .build())
                        .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                        .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                        .setCardRequirements(
                                CardRequirements.newBuilder()
                                        .addAllowedCardNetworks(Arrays.asList(
                                                WalletConstants.CARD_NETWORK_AMEX,
                                                WalletConstants.CARD_NETWORK_DISCOVER,
                                                WalletConstants.CARD_NETWORK_VISA,
                                                WalletConstants.CARD_NETWORK_MASTERCARD))
                                        .build());

        request.setPaymentMethodTokenizationParameters(createTokenizationParameters());
        return request.build();
    }
}
