package com.george.way.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.adapter.Home_Adapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.prefrences.PreferenceUserUtil;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by JAI on 2/21/2018.
 */

public class Chat_History_Activity extends NaviBaseActivity
{
    private DatabaseReference databaseReference;
    private ArrayList<JSONObject> jsonObjects;
    private Home_Adapter home_adapter;
    private ListView listView;
    private TextView no_chat;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sessions");
        PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(Chat_History_Activity.this);
        String userId = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(Chat_History_Activity.this).KEY_USER_ID);
        databaseReference = FirebaseDatabase.getInstance().getReference("users/"+userId).child("chat_rooms");
        listView = (ListView)findViewById(R.id.chathistory_list);
        no_chat = (TextView)findViewById(R.id.no_chat);
        jsonObjects = new ArrayList<>();
        home_adapter = new Home_Adapter(Chat_History_Activity.this,jsonObjects);
        listView.setAdapter(home_adapter);

    }

    @Override
    int getContentViewId() {return R.layout.chat_history_activity;}

    @Override
    int getNavigationMenuItemId() {
        return R.id.navigation_session;
    }

    @Override
    protected void onResume() {
        super.onResume();
        jsonObjects.clear();
        databaseReference.orderByChild("timestamp").addChildEventListener(childEventListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        databaseReference.removeEventListener(childEventListener);
    }

    private ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            if (dataSnapshot!=null)
            {
                if (dataSnapshot.getValue()!=null) {
                    try {
                        HashMap hashMap = (HashMap)dataSnapshot.getValue();
                        hashMap.put("key",dataSnapshot.getKey());
                        home_adapter.updateList(new JSONObject(hashMap));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    listView.setVisibility(View.GONE);
                    no_chat.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
       // getMenuInflater().inflate(R.menu.history_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_done:
                startActivity(new Intent(Chat_History_Activity.this,SearchTutor_Activity.class));
                ViewAnimUtils.activityEnterTransitions(Chat_History_Activity.this);
                finish();
                break;

            case android.R.id.home:
                    ViewAnimUtils.activityExitTransitions(Chat_History_Activity.this);
                    finish();
                break;
        }
        return true;
    }
}
