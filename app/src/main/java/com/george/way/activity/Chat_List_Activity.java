package com.george.way.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.george.way.BuildConfig;
import com.george.way.R;
import com.george.way.adapter.Chat_Adapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.calling.BaseActivity;
import com.george.way.calling.CallScreenActivity;
import com.george.way.calling.SinchService;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.drawingBoard.ui.activity.Drawing_Activity;
import com.george.way.model.ChatModel;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.sinch.android.rtc.MissingPermissionException;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JAI on 2/19/2018.
 */

public class Chat_List_Activity extends BaseActivity implements SinchService.StartFailedListener{


    private ListView userChatRv;
    private View llBottom;
    private EditText msgEt;
    private ImageButton sendImageBtn;
    private ImageView textViewMore;
    private CircleImageView ivUser;
    private ImageButton sendBtn;
    public static String tutor_id = "";
    private String userId;
    private DatabaseReference chatDatabase, userChatFirebaseDatabase, tutorChatDatabase;
    private Toolbar toolbar;
    private BottomSheetDialog bottomSheetDialog, chooseMediaMenu;
    private TextView mailButton, calculatorButton, btnEndChatSession, cancelButton;
    private TextView textViewHeader, tvTimer;
    String admin_email1 = AppConstant.getObj().EMPTY_STRING,
            admin_email2 = AppConstant.getObj().EMPTY_STRING,
            admin_email3 = AppConstant.getObj().EMPTY_STRING,
            admin_email4 = AppConstant.getObj().EMPTY_STRING,
            admin_email5 = AppConstant.getObj().EMPTY_STRING;
    private Chat_Adapter chat_adapter;
    private ArrayList<JSONObject> jsonObjects;
    private TextView chatdeleteButton;
    private ImageView callBtn;
    private TextView endSessionBtn;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
    };
    private File filePathImageCamera = null;
    private String chatKey = "";
    private String TAG = "Chat_Actiivyt";
    private LinearLayout cameraLayout;
    private LinearLayout galleryLayout;
    private TextView cancelTxt;
    private String type = "";
    private String userImage = "";
    private ProgressDialog progressDialog;
    private LinearLayout dawring_layout;
    private ImageView drawingBoardbtn;
    Boolean firstStatus=null;
    private String fetchedTime="";
    private String capturedImageFilePath="";
    private AlertDialog OneMinuteDialog;
    boolean isLive=false;
    ProgressBar callBtnProgress;



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private DatabaseReference referenceTutor;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isLive = true;
        setContentView(R.layout.chat_list_activity);
        firstStatus = null;
        startService(new Intent (this,SinchService.class));
        progressDialog = new ProgressDialog(Chat_List_Activity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        preferenceUserUtil = PreferenceUserUtil.getInstance(Chat_List_Activity.this);
        initView();

        if (getIntent() != null) {
            if (getIntent().getStringExtra("key") != null) {
                jsonObjects = new ArrayList<>();
                chatKey = getIntent().getStringExtra("key");
                setSupportActionBar(toolbar);
                String nameOfOtherParty = getIntent().getStringExtra("name");
                SinchService.callerDisplayName = nameOfOtherParty;
                textViewHeader.setText(nameOfOtherParty);
                tutor_id = getIntent().getStringExtra("tutor_id");
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);

                if (getIntent().getStringExtra("session") != null) {
                    boolean sessionLive = getIntent().getStringExtra("session").equalsIgnoreCase("true");
                    if (sessionLive){
                        llBottom.setVisibility(View.VISIBLE);
                    }else{
                        llBottom.setVisibility(View.GONE);
                    }
                }
            }
        }

        PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(Chat_List_Activity.this);
        userId = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(Chat_List_Activity.this).KEY_USER_ID);

        chatDatabase = FirebaseDatabase.getInstance().getReference("chats/" + chatKey);
        referenceTutor = FirebaseDatabase.getInstance().getReference().child("tutors").child(tutor_id);
        tutorChatDatabase = FirebaseDatabase.getInstance().getReference("tutors/" + tutor_id + "/chat_rooms").child(chatKey);
        userChatFirebaseDatabase = FirebaseDatabase.getInstance().getReference("users/" + userId + "/chat_rooms").child(chatKey);
        userChatFirebaseDatabase.child("image").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String imageUrl = (String) dataSnapshot.getValue();
                if (imageUrl!=null && !imageUrl.isEmpty()){
                    String actualUrl="";
                    if (imageUrl.startsWith("http:")){
                        actualUrl = imageUrl.substring(0,4) + "s" + imageUrl.substring(4);
                    }else if (imageUrl.startsWith("https:")){
                        actualUrl = imageUrl;
                    }else{
                        actualUrl = ApiClient.BASE_URL + imageUrl;
                    }
                    ImageLoader.getInstance().displayImage(actualUrl, ivUser, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {}

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {}

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            ivUser.setImageBitmap(loadedImage);
                        }
                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {}
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        userChatFirebaseDatabase.child("is_active").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    String value = "" + dataSnapshot.getValue();
                    boolean sessionLive = value.equalsIgnoreCase("true");
                    if (firstStatus==null){
                        if (sessionLive){
                            llBottom.setVisibility(View.VISIBLE);
                            if (countDownTimer==null && !fetchedTime.isEmpty()){
                                initTimer(fetchedTime);
                            }
                        }else{
                            llBottom.setVisibility(View.GONE);
                        }
                    }else{
                        if (sessionLive){
                            llBottom.setVisibility(View.VISIBLE);
                        }else{
                            llBottom.setVisibility(View.GONE);
                            navigateToRatingScreen();
                        }
                    }
                    firstStatus = sessionLive;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });

    }


    @Override
    protected void onStop() {
        isLive = false;
        if (countDownTimer!=null) {
            countDownTimer.cancel();
            countDownTimer =null;
        }
        updateTimeCurrentPlane();
        super.onStop();
        chatDatabase.removeEventListener(newMessageListener);
    }

    @Override
    protected void onResume() {
        isLive = true;
        super.onResume();
        fetchCurrentPlane();
        jsonObjects.clear();
        if (chatDatabase != null) {
            chatDatabase.addChildEventListener(newMessageListener);
            chat_adapter = new Chat_Adapter(Chat_List_Activity.this, jsonObjects, userChatRv);
            userChatRv.setAdapter(chat_adapter);
        }
    }

    private void initView() {
        callBtnProgress = findViewById(R.id.sinch_progress);
        callBtn = (ImageView) findViewById(R.id.sinch_call);
        userChatRv = (ListView) findViewById(R.id.user_chat_rv);
        llBottom =  findViewById(R.id.ll_bottom);
        msgEt = (EditText) findViewById(R.id.msg_et);
        sendImageBtn = (ImageButton) findViewById(R.id.send_image_btn);
        sendBtn = (ImageButton) findViewById(R.id.send_btn);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivUser = findViewById(R.id.img_user);
        drawingBoardbtn = (ImageView)findViewById(R.id.drawingBoardbtn);
        chooseMediaMenu = new BottomSheetDialog(Chat_List_Activity.this);
        chooseMediaMenu.setContentView(R.layout.media_choose_layout);
        cameraLayout = (LinearLayout) chooseMediaMenu.findViewById(R.id.camera_layout);
        galleryLayout = (LinearLayout) chooseMediaMenu.findViewById(R.id.gallery_layout);
        dawring_layout = (LinearLayout) chooseMediaMenu.findViewById(R.id.dawring_layout);
        cancelTxt = (TextView) chooseMediaMenu.findViewById(R.id.cancel_txt);
        textViewMore = (ImageView) findViewById(R.id.textViewMore);
        bottomSheetDialog = new BottomSheetDialog(Chat_List_Activity.this);
        bottomSheetDialog.setContentView(R.layout.chat_more_option);
        cancelButton = (TextView) bottomSheetDialog.findViewById(R.id.cancelButton);
        mailButton = (TextView) bottomSheetDialog.findViewById(R.id.mailButton);
        calculatorButton = (TextView) bottomSheetDialog.findViewById(R.id.calculatorButton);
        btnEndChatSession = (TextView) bottomSheetDialog.findViewById(R.id.editButton);
        chatdeleteButton = (TextView) bottomSheetDialog.findViewById(R.id.chatdeleteButton);
        textViewHeader = (TextView) findViewById(R.id.textViewHeader);
        tvTimer = findViewById(R.id.tvTimer);
        endSessionBtn = (TextView) findViewById(R.id.tv_end_chat);


        textViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.show();
            }
        });

        drawingBoardbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Chat_List_Activity.this, Drawing_Activity.class),5001);
            }
        });

        sendImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(totalSec<1){
                    showNoMinuteLeftDialog();
                    return;
                }
                if (shouldAskPermissions()) {
                    verifyStoragePermissions();
                } else {
                    if (chooseMediaMenu.isShowing()) {
                        chooseMediaMenu.dismiss();
                    } else {
                        chooseMediaMenu.show();
                    }
                }
            }
        });

        cameraLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "camera";
                //photoCameraIntent();
                launchCamera();
                chooseMediaMenu.dismiss();
            }
        });

        galleryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "gallery";
                photoGalleryIntent();
                chooseMediaMenu.dismiss();
            }
        });

        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseMediaMenu.dismiss();
            }
        });

        dawring_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "drawring";
                photoDrawringIntent();
                chooseMediaMenu.dismiss();
            }
        });

        ivUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Chat_List_Activity.this, FullImage_Activity.class).putExtra("path",userImage));
            }
        });

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(totalSec<1){
                    showNoMinuteLeftDialog();
                    return;
                }
                String prefix = "";
                if (Chat_Adapter.selectedMessagePositionForReply>-1){
                    prefix = Chat_Adapter.replyPrefix;
                }
                if (!msgEt.getText().toString().isEmpty()) {
                    ChatModel chatModel = new ChatModel(prefix + msgEt.getText().toString().trim(),
                            userId, ServerValue.TIMESTAMP, "T", "", "U");
                    chatDatabase.push().setValue(chatModel);
                    tutorChatDatabase.child("last_msg").setValue(chatModel);
                    tutorChatDatabase.child("timestamp").setValue(ServerValue.TIMESTAMP);
                    tutorChatDatabase.child("is_read").setValue("false");

                    userChatFirebaseDatabase.child("last_msg").setValue(chatModel);
                    userChatFirebaseDatabase.child("timestamp").setValue(ServerValue.TIMESTAMP);
                    userChatFirebaseDatabase.child("is_read").setValue("true");
                    msgEt.setText("");
                    chat_adapter.markUnSelected();
                } else {
                    new SingleActionDialog().showDialogNormal("Please enter message", Chat_List_Activity.this, false);
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        mailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();

                PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(Chat_List_Activity.this);
                admin_email1 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_1);
                admin_email2 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_2);
                admin_email3 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_3);
                admin_email4 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_4);
                admin_email5 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_5);


                /**
                 * open email
                 */
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{
                        admin_email1,
                        admin_email2,
                        admin_email3,
                        admin_email4,
                        admin_email5});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contact GeorgeWay");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Hey i want to connect to GeorgeWay.");
                try {
                    startActivity(Intent.createChooser(emailIntent, "Send Mail"));
                } catch (android.content.ActivityNotFoundException ex) {
                }
            }
        });

        btnEndChatSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEndChatDialog();
            }
        });

        calculatorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();

                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(Chat_List_Activity.this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(Chat_List_Activity.this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(Chat_List_Activity.this);
            }
        });

        endSessionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEndChatDialog();
            }
        });

        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Object tag = view.getTag();
                if (tag==null){
                    showCallingOptions();
                }else{
                    callBtn.setVisibility(View.INVISIBLE);
                    callBtn.setTag(null);
                    callBtn.setImageResource(R.drawable.ic_call_white_24dp);
                    callBtnProgress.setVisibility(View.VISIBLE);

                    if (getSinchServiceInterface()!=null){
                        if (!getSinchServiceInterface().isStarted()){
                            getSinchServiceInterface().stopClient();
                            getSinchServiceInterface().startClient("U_"+preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID));
                        }
                    }
                }
            }
        });
    }

    private ChildEventListener newMessageListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            if (dataSnapshot != null) {
                HashMap hashMap = (HashMap) dataSnapshot.getValue();
                //jsonObjects.add(new JSONObject(hashMap));
                chat_adapter.updateChatList(new JSONObject(hashMap));
                userChatRv.smoothScrollToPosition(jsonObjects.size() - 1);
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    protected boolean shouldAskPermissions() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * Checks if the app has permission to write to device storage
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     */
    public void verifyStoragePermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(Chat_List_Activity.this, Manifest.permission.CAMERA);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(Chat_List_Activity.this, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE
            );
        } else {
            if (type.equalsIgnoreCase("video")) {
                callVideo();
            } else if (type.equalsIgnoreCase("audio")) {
                callAudio();
            } else if (type.equalsIgnoreCase("camera")) {
                chooseMediaMenu.show();
            } else if (type.equalsIgnoreCase("gallery")) {
                chooseMediaMenu.show();
            } else if (type.equalsIgnoreCase("drawring")) {
                chooseMediaMenu.show();
            } else {
                chooseMediaMenu.show();
            }
            // we already have permission, lets go ahead and call camera intent

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE:
                boolean doWeHaveAllPermissions =true;
                // If request is cancelled, the result arrays are empty.
                for (int s : grantResults){
                    if (s!=PackageManager.PERMISSION_GRANTED){
                        doWeHaveAllPermissions = false;
                        break;
                    }
                }
                if (doWeHaveAllPermissions) {
                    // permission was granted
                    if (type.equalsIgnoreCase("video")) {
                        callVideo();
                    }
                    if (type.equalsIgnoreCase("audio")) {
                        callAudio();
                    }
                    if (type.equalsIgnoreCase("camera")) {
                        launchCamera();
                    }
                    if (type.equalsIgnoreCase("gallery")) {
                        photoGalleryIntent();
                    }
                    if (type.equalsIgnoreCase("drawring")) {
                        photoDrawringIntent();
                    }
                }
                break;
        }
    }


    public void launchCamera(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(takePictureIntent.resolveActivity(getPackageManager()) != null){

            File photoObj = null;
            try {
                photoObj = createImageFile(this);
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            if (photoObj != null) {
                Uri photoURI = FileProvider.getUriForFile(this, getApplicationContext().getPackageName()+".MyProvider",photoObj);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION);

                } else {
                    List<ResolveInfo> resInfoList=
                            getPackageManager()
                                    .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, photoURI,
                                Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }

                startActivityForResult(takePictureIntent, IMAGE_CAMERA_REQUEST);
            }
        }
    }

    public File createImageFile(Context context) throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        capturedImageFilePath = image.getAbsolutePath();
        return image;
    }

    private void callAudio() {
        try {
            Call call = getSinchServiceInterface().callUser("T_" + tutor_id);
            if (call == null) {
                // Service failed for some reason, show a Toast and abort
                Toast.makeText(Chat_List_Activity.this, R.string.error_sinch_not_started, Toast.LENGTH_LONG).show();
                return;
            }
            String callId = call.getCallId();
            Intent callScreen = new Intent(Chat_List_Activity.this, CallScreenActivity.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            callScreen.putExtra("name", toolbar.getTitle().toString());
            startActivity(callScreen);
        } catch (MissingPermissionException e) {
            ActivityCompat.requestPermissions(Chat_List_Activity.this, new String[]{e.getRequiredPermission()}, 0);
        }
    }

    private void callVideo() {
        try {
            Call call = getSinchServiceInterface().callUserVideo("T_" + tutor_id);
            if (call == null) {
                // Service failed for some reason, show a Toast and abort
                Toast.makeText(Chat_List_Activity.this, R.string.error_sinch_not_started, Toast.LENGTH_LONG).show();
                return;
            }
            String callId = call.getCallId();
            Intent callScreen = new Intent(Chat_List_Activity.this, CallScreenActivity.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            callScreen.putExtra("name", toolbar.getTitle().toString());
            startActivity(callScreen);
        } catch (MissingPermissionException e) {
            ActivityCompat.requestPermissions(Chat_List_Activity.this, new String[]{e.getRequiredPermission()}, 0);
        }
    }

    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;

    /**
     * Enviar foto tirada pela camera
     */
    private void photoCameraIntent() {
        String nomeFoto = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
        filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), nomeFoto + "camera.jpg");
        Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri photoURI = FileProvider.getUriForFile(Chat_List_Activity.this,
                BuildConfig.APPLICATION_ID + ".provider", filePathImageCamera);
        it.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        startActivityForResult(it, IMAGE_CAMERA_REQUEST);
    }

    /**
     * Enviar foto pela galeria
     */
    private void photoGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Choose option"), IMAGE_GALLERY_REQUEST);
    }

    public static final String URL_STORAGE_REFERENCE = "gs://gw-tutor.appspot.com";
    public static final String FOLDER_STORAGE_IMG = "images";
    FirebaseStorage storage = FirebaseStorage.getInstance();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        StorageReference storageRef = storage.getReferenceFromUrl(URL_STORAGE_REFERENCE).child(chatKey).child(FOLDER_STORAGE_IMG);

        if (requestCode == IMAGE_GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null) {
                    sendFileFirebase(storageRef, selectedImageUri);
                } else {
                    //URI IS NULL
                }
            }
        } else if (requestCode == IMAGE_CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                filePathImageCamera = compress(this,capturedImageFilePath);
                StorageReference imageCameraRef = storageRef.child(filePathImageCamera.getName() /*+ "_camera"*/);
                sendFileFirebase(imageCameraRef, filePathImageCamera);
            }
        } else if (requestCode == 5001) {
            if (data != null) {
                Log.e("Chat ", " " + data.getStringExtra("path"));
                File file = new File(data.getStringExtra("path"));
                sendFileDrwaFirebase(storageRef, file);
            }
        }
    }

    public static File compress(Context context, String inputFilePath){
        File capturedFile = new File(inputFilePath);

        File CompressedFile=null;
        try {
            CompressedFile = new Compressor(context).compressToFile(capturedFile);
        } catch (IOException e){e.printStackTrace();}
        catch (Exception e){e.printStackTrace();}

        return CompressedFile;
    }

    private void photoDrawringIntent() {
        Intent intent = new Intent(Chat_List_Activity.this, Drawing_Activity.class);
        startActivityForResult(intent, 5001);
    }

    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileFirebase(StorageReference storageReference, final Uri file) {
        if (storageReference != null) {
            progressDialog.show();
            final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
            StorageReference imageGalleryRef = storageReference.child(name + "_gallery");
            UploadTask uploadTask = imageGalleryRef.putFile(file);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                    progressDialog.dismiss();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG, "onSuccess sendFileFirebase");
                    progressDialog.dismiss();
                    /*String downloadUrl = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                    sendMediaFile(downloadUrl);*/
                    imageGalleryRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(@NonNull Uri uri) {
                            sendMediaFile(uri.toString());
                            progressDialog.dismiss();
                        }
                    });
                }
            });
        } else {
            //IS NULL
        }

    }

    private void sendMediaFile(String image_path) {
        ChatModel chatModel = new ChatModel("Media File", userId, ServerValue.TIMESTAMP, "M", image_path, "U");
        chatDatabase.push().setValue(chatModel);
        tutorChatDatabase.child("last_msg").setValue(chatModel);
        tutorChatDatabase.child("timestamp").setValue(ServerValue.TIMESTAMP);
        tutorChatDatabase.child("is_read").setValue("false");

        userChatFirebaseDatabase.child("last_msg").setValue(chatModel);
        userChatFirebaseDatabase.child("timestamp").setValue(ServerValue.TIMESTAMP);
        userChatFirebaseDatabase.child("is_read").setValue("true");
    }

    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileDrwaFirebase(StorageReference storageReference, final File file) {
        if (storageReference != null) {
            progressDialog.show();
            try {
                InputStream stream = new FileInputStream(file);
                final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
                StorageReference imageGalleryRef = storageReference.child(name + "_gallery");
                UploadTask uploadTask = imageGalleryRef.putStream(stream);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                        progressDialog.dismiss();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Log.i(TAG, "onSuccess sendFileFirebase");
                        imageGalleryRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(@NonNull Uri uri) {
                                sendMediaFile(uri.toString());
                                progressDialog.dismiss();
                            }
                        });
                    }
                });
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            //IS NULL
        }

    }

    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileFirebase(StorageReference storageReference, final File file) {
        if (storageReference != null) {
            progressDialog.show();
            Uri photoURI = Uri.fromFile(file);

            UploadTask uploadTask = storageReference.putFile(photoURI);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                    progressDialog.dismiss();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG, "onSuccess sendFileFirebase");
                    storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(@NonNull Uri uri) {
                            sendMediaFile(uri.toString());
                            progressDialog.dismiss();
                        }
                    });
                }
            });
        }
    }

    private boolean isKeyboardShown(){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm!=null){
            if (imm.isAcceptingText()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (countDownTimer!=null) {
            countDownTimer.cancel();
            countDownTimer =null;
        }
        super.onDestroy();
    }

    private static long totalSec = 0;
    private CountDownTimer countDownTimer = null;

    /**
     * call api for fetch message
     */
    public void fetchCurrentPlane() {

        if (NetworkUtil.isConnected(Chat_List_Activity.this)) {


            try {

                AppUtils.getObj().hideSoftKeyboard(Chat_List_Activity.this);

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                retrofit2.Call<ResponseBody> call = mApiInterface.currentPlan(preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID));

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(retrofit2.Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");
                            JSONArray data = mJsonObj.optJSONArray("data");
                            JSONObject dataJsonObj = data.getJSONObject(0);

                            if (dataJsonObj.has("subscription_time")) {

                                Log.e("TAG","CHAT, Subscribed Time : "+dataJsonObj.optString("subscription_time"));

                                if (dataJsonObj.optString("subscription_time").equalsIgnoreCase("0")) {
                                    new SingleActionDialog().showDialogForSubscription(getResources().getString(R.string.Please_subscribe_Online_Tutoring), Chat_List_Activity.this);

                                }else {
                                    Log.e("TAG","Time Fetched: "+fetchedTime);
                                    fetchedTime = dataJsonObj.optString("subscription_time");
                                    if (firstStatus!=null) {
                                        if (firstStatus) {
                                            if (CallScreenActivity.IS_RETURNING_FROM_CALL){
                                                fetchedTime = String.valueOf(Long.valueOf(fetchedTime) - CallScreenActivity.CALL_TIME);
                                                CallScreenActivity.IS_RETURNING_FROM_CALL=false;
                                            }
                                            initTimer(fetchedTime);
                                        } else {
                                            tvTimer.setText(getDisplayFormat(fetchedTime));
                                        }
                                    }

                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String getDisplayFormat(String numberStr){
        Long secs = Long.valueOf(numberStr);
        long seconds = secs % 60;
        long minutes = secs / 60;
        return getString(R.string.timer_time,twoDigitString(minutes),twoDigitString(seconds));
    }


    private void initTimer(String receivedTimeFromServer) {
        try {
            final long initValue =Long.parseLong(receivedTimeFromServer)*1000;

            if (countDownTimer!=null){
                countDownTimer.cancel();
                countDownTimer = null;
            }

            countDownTimer =  new CountDownTimer(initValue, 1000) {

                @Override
                public void onTick(long millisUntilFinished) {

                    Log.e("TAG","Chat, totalSec: "+totalSec);
                    totalSec = millisUntilFinished/1000;

                    long seconds = totalSec % 60;
                    long minutes = totalSec / 60;

                    if (totalSec<61){
                        if (totalSec==60){
                            show1MinuteLeftDialog();
                        }
                        if (totalSec<0){
                            countDownTimer.cancel();
                            Chat_List_Activity.this.finish();
                        }
                    }

                    String displeMin = twoDigitString(minutes);
                    String displaySec = twoDigitString(seconds);

                    tvTimer.setText(getString(R.string.timer_time,displeMin,displaySec));
                }



                @Override
                public void onFinish() {
                    totalSec = 0;
                    tvTimer.setText(getString(R.string.timer_time,"00","00"));
                    navigateToRatingScreen();
                }
            };
            countDownTimer.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String twoDigitString(long number) {
        if (number>-1){
            if (number<10){
                return "0" + number;
            }else{
                return String.valueOf(number);
            }
        }else{
            return "00";
        }
    }


    PreferenceUserUtil preferenceUserUtil;
    /**
     * call api for update plan time
     */
    public void updateTimeCurrentPlane() {

        if (fetchedTime.isEmpty()){
            return;
        }

        if (NetworkUtil.isConnected(Chat_List_Activity.this)) {

            try {
                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Log.e("TAG","Chat, updating on server: "+totalSec);
                long timeParam = Long.valueOf(fetchedTime) - totalSec;
                if (timeParam<0){
                    timeParam=0;
                }
                retrofit2.Call<ResponseBody> call = mApiInterface.updateSubscribTime(preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID),""+timeParam,tutor_id);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(retrofit2.Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");
                            JSONArray data = mJsonObj.optJSONArray("data");
                            JSONObject dataJsonObj = data.getJSONObject(0);

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {}
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            fetchedTime = "";
            totalSec=0;
        }
    }

    boolean exitTriggered = false;
    private void navigateToRatingScreen(){

        if (exitTriggered) return;
        Log.e("CHAT_LIST_TEST","navigateToRatingScreen :" + System.currentTimeMillis());
        exitTriggered=true;
        chatDatabase.removeEventListener(newMessageListener);
        tutorChatDatabase.setValue(null);
        referenceTutor.child("total_request_count").setValue(0);
        userChatFirebaseDatabase.child("is_active").setValue("false");
        llBottom.setVisibility(View.GONE);
        firstStatus = null;
        if (OneMinuteDialog!=null && OneMinuteDialog.isShowing()){
            OneMinuteDialog.hide();
        }
        Intent intent = new Intent(this,RatingActivity.class);
        intent.putExtra("_user_id",userId);
        intent.putExtra("_tutor_id",tutor_id);
        startActivity(intent);
        ViewAnimUtils.activityEnterTransitions(Chat_List_Activity.this);
        finish();
    }

    private void show1MinuteLeftDialog(){
        if (!isLive)return;
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this,R.style.NoTutorDialogStyle);

        OneMinuteDialog = builder.setTitle("")
                .setMessage(R.string.message_one_min_left)
                .setPositiveButton("Add More", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Chat_List_Activity.this, BuyAcitvity.class));
                    }
                })
                .setNegativeButton("Later", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setCancelable(true)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create();

        OneMinuteDialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                OneMinuteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.bg_violet));
            }
        });

        OneMinuteDialog.show();
    }


    private void showNoMinuteLeftDialog(){
        if (!isLive)return;
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this,R.style.NoTutorDialogStyle);

        AlertDialog dialog = builder.setTitle("")
                .setMessage(R.string.message_no_more_mins)
                .setPositiveButton("Add More", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Chat_List_Activity.this, BuyAcitvity.class));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setCancelable(true)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create();

        dialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.bg_violet));
            }
        });
        dialog.show();
    }

    private void showCallingOptions(){
        if (!isLive)return;
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.NoTutorDialogStyle);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        String[] animals = {"Audio Call", "Video Call"};
        builder.setItems(animals, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // audio
                        if(totalSec<1){
                            showNoMinuteLeftDialog();
                            return;
                        }
                        type = "audio";

                        if (shouldAskPermissions()) {
                            verifyStoragePermissions();
                        } else {
                            callAudio();
                        }
                        break;

                    case 1: // video
                        if(totalSec<1){
                            showNoMinuteLeftDialog();
                            return;
                        }
                        type = "video";
                        if (shouldAskPermissions()) {
                            verifyStoragePermissions();
                        } else {
                            callVideo();
                        }
                        break;

                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                //noTutorDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.status_bar_color));
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.bg_violet));
            }
        });
        dialog.show();
    }

    private void showEndChatDialog(){
        if (!isLive)return;
        AlertDialog.Builder builder = new AlertDialog.Builder(Chat_List_Activity.this,R.style.NoTutorDialogStyle);
        builder.setIcon(R.mipmap.ic_launcher);

        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setMessage("Are you sure, you want to close this session?");


        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                referenceTutor.child("chat_rooms").setValue(null);
                //tutorChatDatabase.setValue(null);
                referenceTutor.child("total_request_count").setValue(0);
                userChatFirebaseDatabase.child("is_active").setValue("false");
                llBottom.setVisibility(View.GONE);
            }
        });


        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        int textColorId = getResources().getIdentifier("alertMessage", "id", "android");
        TextView textColor = (TextView) alertDialog.findViewById(textColorId);
        if (textColor != null) {
            textColor.setTextColor(Color.BLACK);
        }
        alertDialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.bg_violet));
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.bg_violet));
            }
        });

        alertDialog.show();
    }

    @Override
    public void onStartFailed(SinchError error) {
        callBtn.setImageResource(R.drawable.ic_warning);
        callBtn.setVisibility(View.VISIBLE);
        callBtn.setTag(1);
        callBtnProgress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onStarted() {
        callBtnProgress.setVisibility(View.INVISIBLE);
        callBtn.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onServiceConnected() {
        if (getSinchServiceInterface().isStarted()){
            onStarted();
        }else{
            getSinchServiceInterface().setStartListener(this);
        }
    }

    @Override
    protected void onServiceDisconnected() {
        super.onServiceDisconnected();
    }
}
