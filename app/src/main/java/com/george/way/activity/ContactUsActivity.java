package com.george.way.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.constant.AppConstant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactUsActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * declare fields of xm
     */
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;

    @BindView(R.id.linearLayoutEmail)
    LinearLayout linearLayoutEmail;

    @BindView(R.id.linearLayoutPhone)
    LinearLayout linearLayoutPhone;

    @BindView(R.id.textViewEmail)
    TextView textViewEmail;

    @BindView(R.id.textViewPhone)
    TextView textViewPhone;

    private Activity mActivity;

    @BindView(R.id.app_bar)
    Toolbar app_bar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_contact_us);

        /**
         * init
         */
        ButterKnife.bind(this);

        /**
         * init
         */
        init();

        /**
         * set on click events
         */
        setOnClickEvents();

    }

    /**
     * setting up onclick on views
     */
    private void setOnClickEvents() {

        imageViewBack.setOnClickListener(this);
        linearLayoutEmail.setOnClickListener(this);
        linearLayoutPhone.setOnClickListener(this);

    }


    /* * initialize class
    */
    private void init() {

        mActivity = this;
        textViewEmail.setText(AppConstant.CONTACT_EMAIL);
        textViewPhone.setText(AppConstant.CONTACT_NUMBER);

    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(mActivity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                finish();
                ViewAnimUtils.activityExitTransitions(mActivity);
                break;

            case R.id.linearLayoutEmail:

                /**
                 * open email
                 */
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.putExtra(Intent.EXTRA_EMAIL  , new String[]{AppConstant.getObj().CONTACT_EMAIL});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contact GeorgeWay");
                emailIntent.putExtra(Intent.EXTRA_TEXT   , "Hey i want to connect to GeorgeWay.");
                try {
                    startActivity(Intent.createChooser(emailIntent, "Send Mail"));
                } catch (android.content.ActivityNotFoundException ex) {
                 }


                break;

            case R.id.linearLayoutPhone:


                if (android.os.Build.VERSION.SDK_INT >= 23) {

                    /***********CHECKING PERMISSION FOR MARSHMALLOW****************/
                    checkAppPermission();
                } else {

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + AppConstant.CONTACT_NUMBER));
                    startActivity(callIntent);
                }


                break;

            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;

        }
    }

    /**
     * Method for checking GPS Permission Os 6.0
     */
    private void checkAppPermission() {

        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(mActivity, new String[]{
                    Manifest.permission.CALL_PHONE
            }, 1);
        } else {

            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + AppConstant.CONTACT_NUMBER));
            startActivity(callIntent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (requestCode == 1) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                /**
                 * Call the below method for holding splash screen for few seconds and start application introduction screen
                 */
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + AppConstant.CONTACT_NUMBER));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);


            }
        }
    }

}
