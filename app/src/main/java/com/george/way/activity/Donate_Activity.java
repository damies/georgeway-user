package com.george.way.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.george.way.R;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.dialog.ValidationErrorDialog;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JAI on 11/5/2017.
 */

public class Donate_Activity extends AppCompatActivity
{
    private static final int REQUEST_CODE_PAYMENT = 101;
    private EditText editTextAmount,editTextEmail,editTextMonth;
    private Button subcategory_btn;
    private BottomSheetDialog paymentBottomSheetDialog;
    private ImageView imageViewBack;
    private String month = "";
    /**
     * pay pal configurations
     */
    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(AppConstant.KEY_CLIENT_ID)
            .merchantName("GeorgeWay");
    private RadioGroup radioGroup;
    private TextView title_template;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.donate_activity);
        imageViewBack = (ImageView)findViewById(R.id.imageViewBack);
        paymentBottomSheetDialog = new BottomSheetDialog(Donate_Activity.this);

        subcategory_btn = (Button)findViewById(R.id.subcategory_btn);
        title_template  = (TextView)findViewById(R.id.title_template);
        editTextAmount  = (EditText)findViewById(R.id.editTextAmount);
        editTextEmail   = (EditText)findViewById(R.id.editTextEmail);
        imageViewBack   = (ImageView)findViewById(R.id.imageViewBack);
        radioGroup      = (RadioGroup)findViewById(R.id.radioGroup);
        editTextMonth   = (EditText)findViewById(R.id.editTextMonth);
        updateDonateTitle();

        subcategory_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (editTextMonth.getVisibility()==View.GONE) {
                    if (!editTextEmail.getText().toString().isEmpty())
                    {
                        if (!editTextAmount.getText().toString().isEmpty())
                        {
                            month = "1";
                            openPaymetDialog(Double.parseDouble(editTextAmount.getText().toString()),"1");
                        }else
                        {
                            ValidationErrorDialog.getValidationUtilsObj(Donate_Activity.this).showErrorDialog("Please enter amount");
                        }
                    }else
                    {
                        ValidationErrorDialog.getValidationUtilsObj(Donate_Activity.this).showErrorDialog("Please enter email");
                    }
                } else
                {
                    if (!editTextEmail.getText().toString().isEmpty())
                    {
                        if (!editTextAmount.getText().toString().isEmpty())
                        {
                            if (!editTextMonth.getText().toString().isEmpty())
                            {
                                month = editTextMonth.getText().toString();
                                openPaymetDialog(Double.parseDouble(editTextAmount.getText().toString()),editTextMonth.getText().toString());
                            }else
                            {
                                ValidationErrorDialog.getValidationUtilsObj(Donate_Activity.this).showErrorDialog("Please enter months");
                            }
                        }else
                        {
                            ValidationErrorDialog.getValidationUtilsObj(Donate_Activity.this).showErrorDialog("Please enter amount");
                        }
                    }else
                    {
                        ValidationErrorDialog.getValidationUtilsObj(Donate_Activity.this).showErrorDialog("Please enter email");
                    }
                }
            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) findViewById(selectedId);
                if (radioButton.getText().toString().equalsIgnoreCase("Monthly"))
                {
                    editTextMonth.setVisibility(View.VISIBLE);
                }else
                {
                    editTextMonth.setVisibility(View.GONE);
                }
            }
        });

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                ViewAnimUtils.activityExitTransitions(Donate_Activity.this);
            }
        });
        PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(Donate_Activity.this);

        if (msPreferenceUserUtil.getString(msPreferenceUserUtil.KEY_EMAIL).isEmpty())
        {
            editTextEmail.setText("");
        }else
        {
            editTextEmail.setText(msPreferenceUserUtil.getString(msPreferenceUserUtil.KEY_EMAIL));
        }

    }

    private void openPaymetDialog(final Double amount, final String month) {

        paymentBottomSheetDialog.setContentView(R.layout.caomplete_action);
        TextView paypalButton = (TextView) paymentBottomSheetDialog.findViewById(R.id.paypalButton);
        TextView stripButton = (TextView) paymentBottomSheetDialog.findViewById(R.id.stripButton);
        TextView cancelButton = (TextView) paymentBottomSheetDialog.findViewById(R.id.cancelButton);
        TextView payAmount_txt = (TextView)paymentBottomSheetDialog.findViewById(R.id.payAmount_txt);

        payAmount_txt.setText("Pay $"+amount);


        paymentBottomSheetDialog.show();

        paypalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentBottomSheetDialog.cancel();
                onBuyPressed(month);
//                ((SubscriptionActivity)mActivity).onBuyPressed(amount,plan_type,plan_time,plansList.get(position1).getPakageid(),totalSecond);

            }
        });

        stripButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentBottomSheetDialog.cancel();

                startActivity(new Intent(Donate_Activity.this, Stripe_Payment_Activity.class).putExtra("donate",amount).putExtra("email",editTextEmail.getText().toString()));
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentBottomSheetDialog.cancel();
            }
        });

    }
    public void onBuyPressed(String month) {

        PayPalPayment thingToBuy = getStuffToBuy(PayPalPayment.PAYMENT_INTENT_ORDER);
        Intent intent = new Intent(Donate_Activity.this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);

    }

    /*
     * This method shows use of optional payment details and item list.
     */
    private PayPalPayment getStuffToBuy(String paymentIntent) {
        Double amountTotal = 0.0;

        try {
            amountTotal = Double.parseDouble(editTextAmount.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        PayPalItem[] items = {new PayPalItem("Donate", 1, new BigDecimal(amountTotal), "USD",
                "")};
        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal("0");
        BigDecimal tax = new BigDecimal("0");
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        PayPalPayment payment = new PayPalPayment(amount, "USD", "Donate", paymentIntent);
        payment.items(items).paymentDetails(paymentDetails);
        payment.custom("");

        return payment;
    }


    protected void displayResultText(String result) {
        new SingleActionDialog().showDialogNormal(result,Donate_Activity.this,false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i("tg", confirm.toJSONObject().toString(4));
                        Log.i("tg", confirm.getPayment().toJSONObject().toString(4));

//                        updatePlanOnServerWebApi();


                    } catch (JSONException e) {

                        Toast.makeText(Donate_Activity.this, "Sorry something went wrong.", Toast.LENGTH_SHORT).show();

                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {

                displayResultText("Payment process has been cancelled.");

            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {

                displayResultText("Invalid Payment details was submitted.");

            }
        }
    }
    public void updateDonateWebApi() {

        if (NetworkUtil.isConnected(Donate_Activity.this)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(Donate_Activity.this);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(Donate_Activity.this).showDialog1();
                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(Donate_Activity.this);
                String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);
                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.userDonate(editTextEmail.getText().toString(),""+Double.parseDouble(editTextAmount.getText().toString()),"",month);


                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(Donate_Activity.this).dismissDialog1();

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");


                            if (!TextUtils.isEmpty(status_code)) {

                                if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {

                                    // SingleActionDialog.getDialogObj(mActivity).showDialogNormal(success_message);
//                                    displayResultText("Your Plan has been subscribed successfully.Thanks for subscribing the plan.");

                                    try {



                                        /**
                                         * updating plan data
                                         */


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } else {

                                    String error_message = mJsonObj.optString(AppConstant.getObj().ERROR_MESSAGE);
                                    new SingleActionDialog().showDialogNormal(error_message,Donate_Activity.this,false);

                                }
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(Donate_Activity.this).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(Donate_Activity.this, networkCallBack3);
        }
    }
    DialogInterface.OnClickListener networkCallBack3 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            updateDonateWebApi();
        }
    };
    /**
     * call api for update plan time
     */
    public void updateDonateTitle() {

        if (NetworkUtil.isConnected(Donate_Activity.this)) {


            try {

                /**
                 * show if progress dialog is not showing
                 */
                    GWProgressDialog.getProgressDialog(Donate_Activity.this).showDialog1();
                /**
                 * hide keyboard if open
                 */
                AppUtils.getObj().hideSoftKeyboard(Donate_Activity.this);

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.donateTitle();

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(Donate_Activity.this).dismissDialog1();

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");

                            if (status_code.equalsIgnoreCase("200"))
                            {
                                title_template.setText(mJsonObj.optString("data"));
                            }


                        } catch (Exception ex) {
                            ex.printStackTrace();
                            GWProgressDialog.getProgressDialog(Donate_Activity.this).dismissDialog1();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(Donate_Activity.this).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(Donate_Activity.this, networkCallBack);
        }
    }
    DialogInterface.OnClickListener networkCallBack = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            updateDonateTitle();
        }
    };
}
