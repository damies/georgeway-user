package com.george.way.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.agsw.FabricView.FabricView;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.george.way.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by JAI on 3/11/2018.
 */

public class Drwaering_activity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageView cancelAction;
    private TextView sendChatTxt;
    private TextView colurChatTxt;
    private TextView clearChatTxt;
    private FabricView faricView;
    private int currentBackgroundColor = 0xffffffff;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawing_activity);
        initView();
        colurChatTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ColorPickerDialogBuilder
                        .with(Drwaering_activity.this)
                        .setTitle("Choose color")
                        .initialColor(currentBackgroundColor)
                        .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                        .density(12)
                        .setOnColorSelectedListener(new OnColorSelectedListener() {
                            @Override
                            public void onColorSelected(int selectedColor) {
                                faricView.setColor(selectedColor);
                            }

                        })
                        .setPositiveButton("ok", new ColorPickerClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                                faricView.setColor(selectedColor);
                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .build()
                        .show();
            }
        });
        sendChatTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("path",save(faricView.getCanvasBitmap()));
                setResult(5001, intent);
                finish();
            }
        });
        clearChatTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                faricView.cleanPage();
            }
        });

        cancelAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public String save(Bitmap bitmap) {
        String filename;
        Date date = new Date(0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        filename = sdf.format(date);

        try {
            String path= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath();
            OutputStream fOut = null;
            File file = new File(path,  filename + ".jpg");
            fOut = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();

            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
            filename = file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filename;
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        cancelAction = (ImageView) findViewById(R.id.cancel_action);
        sendChatTxt = (TextView) findViewById(R.id.send_chat_txt);
        colurChatTxt = (TextView) findViewById(R.id.colur_chat_txt);
        clearChatTxt = (TextView) findViewById(R.id.clear_chat_txt);
        faricView = (FabricView) findViewById(R.id.faricView);
    }
}
