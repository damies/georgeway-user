package com.george.way.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.adapter.FeedbackListAdapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.model.feedbacks.Datum;
import com.george.way.model.feedbacks.FeedbackDataModel;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.pullToRefLib.PullToRefreshBase;
import com.george.way.pullToRefLib.PullToRefreshListView;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener {


    /**
     * declare variables of class
     */
    @BindView(R.id.listViewFeedbacks)
    PullToRefreshListView listViewFeedBacks;

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;

    @BindView(R.id.textViewHeader)
    TextView textViewHeader;

    @BindView(R.id.linearSuccessStory)
    LinearLayout linearSuccessStory;

    @BindView(R.id.linearSubmit)
    LinearLayout linearSubmit;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.textViewNoFeedback)
    TextView textViewNoFeedback;

    @BindView(R.id.app_bar)
    Toolbar app_bar;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;


    private FeedbackListAdapter mFeedbackAdapter;
    private ArrayList<Datum> feedBacksList;
    private Activity mActivity;
    private int defaultPageNumber = 1;
    private LinearLayoutManager linearLayoutManager;
    private TextView tvAskTutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_feedback);
        tvAskTutor = findViewById(R.id.ask_tutor);
        tvAskTutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FeedbackActivity.this,SearchTutor_Activity.class));
                ViewAnimUtils.activityEnterTransitions(FeedbackActivity.this);
            }
        });

        /**
         * init
         */
        ButterKnife.bind(this);

        /**
         * init
         */
        init();

        /**
         * set on click events
         */
        setOnClickEvents();

        /**
         * setting up plan adapter
         */
        settingUpFeedbackAdapter();


    }


    /**
     * setting up onclick on views
     */
    private void setOnClickEvents() {

        imageViewBack.setOnClickListener(this);
        linearSubmit.setOnClickListener(this);
        textViewNoFeedback.setOnClickListener(this);
        linearSuccessStory.setOnClickListener(this);

    }


    /**
     * initialize class
     */
    private void init() {

        mActivity = this;
        feedBacksList = new ArrayList<>();
        textViewHeader.setText(getResources().getString(R.string.Feedback));

        PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
        String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);
       /* if (TextUtils.isEmpty(userId)) {
            linearLayoutSend.setVisibility(View.GONE);
        } else {
            linearLayoutSend.setVisibility(View.VISIBLE);
        }*/
        linearSubmit.setVisibility(View.VISIBLE);


    }

    @Override
    protected void onResume() {
        super.onResume();
        /**
         * update theme
         */
        /*try {
            ThemeUtils.getObj(mActivity).setUpTheme(app_bar, new ImageView[]{imageViewBack,imageViewCalculator},textViewHeader);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        /**
         * get feedBacks listing
         */
        getFeedBacksRequest(defaultPageNumber, false);
    }

    /**
     * setting up feedback data into the list
     */
    private void settingUpFeedbackAdapter() {

        mFeedbackAdapter = new FeedbackListAdapter(mActivity, feedBacksList);
        listViewFeedBacks.setAdapter(mFeedbackAdapter);

        // Set a listener to be invoked when the list should be refreshed.
        listViewFeedBacks.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        listViewFeedBacks.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {

                getFeedBacksRequest(defaultPageNumber, true);

            }
        });

    }

    /**
     * setting up feedback data into the list
     */
    private void updatingFeedbackAdapter() {

        mFeedbackAdapter.notifyDataSetChanged();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(mActivity);
    }

    /**
     * get feedBacks
     */
    public void getFeedBacksRequest(int page_num, boolean isRefreshRequest) {


        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                if (isRefreshRequest) {
                    progressBar.setVisibility(View.GONE);
                    GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                } else {
                    progressBar.setVisibility(View.GONE);
                    GWProgressDialog.getProgressDialog(mActivity).showDialog1();
                }


                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<FeedbackDataModel> call = mApiInterface.getFeedBacksRequest(page_num);

                call.enqueue(new Callback<FeedbackDataModel>() {
                    @Override
                    public void onResponse(Call<FeedbackDataModel> call, Response<FeedbackDataModel> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            progressBar.setVisibility(View.GONE);
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                            FeedbackDataModel mFeedbackDataModel = response.body();
                            if (mFeedbackDataModel != null && mFeedbackDataModel.getData() != null && mFeedbackDataModel.getData().size() > 0) {

                                defaultPageNumber++;

                                for (int index = 0; index < mFeedbackDataModel.getData().size(); index++) {
                                    feedBacksList.add(mFeedbackDataModel.getData().get(index));
                                }

                            }

                            if(feedBacksList.size()>0){
                                textViewNoFeedback.setVisibility(View.GONE);
                            }else {
                                textViewNoFeedback.setVisibility(View.VISIBLE);
                            }
                            /**
                             * setting up plan adapter
                             */
                            updatingFeedbackAdapter();

                            listViewFeedBacks.onRefreshComplete();

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<FeedbackDataModel> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        progressBar.setVisibility(View.GONE);
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                        listViewFeedBacks.onRefreshComplete();

                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }
    }

    DialogInterface.OnClickListener networkCallBack2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            getFeedBacksRequest(defaultPageNumber, false);
        }
    };


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                finish();
                ViewAnimUtils.activityExitTransitions(mActivity);
                break;


            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);
                break;

            case R.id.linearSubmit:


                PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(mActivity);
                String userId1 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_USER_ID);

                if (TextUtils.isEmpty(userId1)) {

                    new SingleActionDialog().showDialogForLogin(getResources().getString(R.string.please_login_feedback),mActivity);

                } else {

                    Intent feedbackIntent = new Intent(mActivity, UploadFeedbackActivity.class);
                    feedbackIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(feedbackIntent);
                    ViewAnimUtils.activityEnterTransitions(mActivity);

                }

                break;

            case R.id.linearSuccessStory:


                /**
                 * open success story screen
                 */
                Intent intentSuccessStory = new Intent(mActivity, SuccessStoryActivity.class);
                intentSuccessStory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentSuccessStory.putExtra("fromFeedback",true);
                startActivity(intentSuccessStory);
                ViewAnimUtils.activityEnterTransitions(mActivity);


                break;
        }
    }


}
