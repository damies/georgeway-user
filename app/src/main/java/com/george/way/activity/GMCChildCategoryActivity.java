package com.george.way.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.adapter.GMCChildCategoryAdapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.model.category.ChildCategory;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GMCChildCategoryActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * declare fields of xml
     */
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;

    @BindView(R.id.category_expandable_list)
    ListView category_expandable_list;

    @BindView(R.id.app_bar)
    Toolbar app_bar;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;

    @BindView(R.id.textViewHeader)
    TextView textViewHeader;

    /*
    declare objects of this class
     */
    public static final String KEY_CHILD_DATA = "KEY_CHILD_DATA";

    private Activity mActivity;
    private ChildCategory mChildCategory;
    private GMCChildCategoryAdapter mSubCategoryListAdapter;

    private TextView tvAskTutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gmcsub_category);
        tvAskTutor = findViewById(R.id.ask_tutor);
        tvAskTutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GMCChildCategoryActivity.this,SearchTutor_Activity.class));
                ViewAnimUtils.activityEnterTransitions(GMCChildCategoryActivity.this);
            }
        });

        /**
         * init
         */
        ButterKnife.bind(this);

        /**
         * class data init
         */
        init();

        /**
         * set click events
         */
        setOnClick();
    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * update theme
         */
        /*try {
            ThemeUtils.getObj(mActivity).setUpTheme(app_bar, new ImageView[]{imageViewBack,imageViewCalculator},textViewHeader);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    /**
     * class data init
     */
    private void init() {

        mActivity = this;

        try {
            Intent dataIntent = getIntent();
            if(dataIntent.hasExtra(KEY_CHILD_DATA)){

                mChildCategory = (ChildCategory) dataIntent.getSerializableExtra(KEY_CHILD_DATA);
                mSubCategoryListAdapter = new GMCChildCategoryAdapter(mActivity,mChildCategory.getChildSubCategory());
                category_expandable_list.setAdapter(mSubCategoryListAdapter);
                textViewHeader.setText(mChildCategory.getName());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * setting up sub category adapter
     */
    private void settingUpSubCatAdapter(){

    }

    /**
     * set click events
     */
    private void setOnClick() {

        imageViewBack.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack:

                finish();
                ViewAnimUtils.activityExitTransitions(mActivity);

                break;


            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(mActivity);
    }

}
