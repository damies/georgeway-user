package com.george.way.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.adapter.GMCMainCategoryAdapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.model.category.CategoryDataModel;
import com.george.way.model.category.SubCategory;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GMCMainCategoryActivity extends Activity implements View.OnClickListener {

    private GMCMainCategoryAdapter mGMCCategoryAdapter;
    private Activity mActivity;

    @BindView(R.id.app_bar)
    Toolbar app_bar;

    private ArrayList<SubCategory> categoryDataList;
    ListView categoryExpandableList;
    ImageView imageViewBack;

    @BindView(R.id.textViewHeader)
    TextView textViewHeader;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;

    @BindView(R.id.textViewNoFeedback)
    TextView textViewNoFeedback;
    TextView tvAskTuor;
   // private TextView tvAskTutor;

    @BindView(R.id.editText_query)
    EditText editText_query;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_george_math_class);
        //tvAskTutor = findViewById(R.id.ask_tutor);
        tvAskTuor = findViewById(R.id.ask_tutor);
        tvAskTuor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GMCMainCategoryActivity.this,SearchTutor_Activity.class));
                ViewAnimUtils.activityEnterTransitions(GMCMainCategoryActivity.this);
            }
        });

        /**
         * init
         */
        ButterKnife.bind(this);

        /**
         * class data init
         */
        init();

        /**
         * calling web api
         */
        callFetchCategoryApi();

        editText_query.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mGMCCategoryAdapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(mActivity);
    }


    @Override
    protected void onResume() {
        super.onResume();

        /**
         * update theme
         */
        /*try {
            ThemeUtils.getObj(mActivity).setUpTheme(app_bar, new ImageView[]{imageViewBack, imageViewCalculator}, textViewHeader);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    /**
     * class data init
     */
    private void init() {

        mActivity = this;
        categoryDataList = new ArrayList<>();
        categoryExpandableList = (ListView) findViewById(R.id.category_expandable_list);
        imageViewBack = (ImageView) findViewById(R.id.imageViewBack);
        imageViewBack.setOnClickListener(this);

        PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
        String text = preferenceUserUtil.getString(preferenceUserUtil.KEY_GMC_CONTENT);
        if (TextUtils.isEmpty(text)) {
            textViewHeader.setText(getResources().getString(R.string.George_Math_Class));
        } else {
            textViewHeader.setText(text);
        }
        /**
         * setting up adapter
         */
        settingUpCategoryAdapter();

    }


    /**
     * call api for fetching categories password
     */
    public void callFetchCategoryApi() {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<CategoryDataModel> call = mApiInterface.fetchCategoryRequest();

                call.enqueue(new Callback<CategoryDataModel>() {
                    @Override
                    public void onResponse(Call<CategoryDataModel> call, Response<CategoryDataModel> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            CategoryDataModel mCategoryDataModel = response.body();

                            if (mCategoryDataModel != null
                                    && mCategoryDataModel.getData() != null
                                    && mCategoryDataModel.getData().size() > 0
                                    && mCategoryDataModel.getData().get(0).getSubCategory() != null
                                    && mCategoryDataModel.getData().get(0).getSubCategory().size() > 0) {
                                {

                                    categoryDataList.clear();

                                    for (int index = 0; index < mCategoryDataModel.getData().get(0).getSubCategory().size(); index++) {

                                        categoryDataList.add(mCategoryDataModel.getData().get(0).getSubCategory().get(index));

                                    }

                                }
                            }

                            settingUpCategoryAdapter();

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<CategoryDataModel> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                        textViewNoFeedback.setVisibility(View.VISIBLE);
                        categoryExpandableList.setVisibility(View.GONE);
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }
    }

    private void settingUpCategoryAdapter() {

        if (categoryDataList.size() == 0) {
            textViewNoFeedback.setVisibility(View.VISIBLE);
            categoryExpandableList.setVisibility(View.GONE);
        } else {
            textViewNoFeedback.setVisibility(View.GONE);
            categoryExpandableList.setVisibility(View.VISIBLE);
        }
        mGMCCategoryAdapter = new GMCMainCategoryAdapter(this, categoryDataList);
        categoryExpandableList.setAdapter(mGMCCategoryAdapter);

    }

    DialogInterface.OnClickListener networkCallBack2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            callFetchCategoryApi();
        }
    };


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack:
                finish();
                ViewAnimUtils.activityExitTransitions(mActivity);
                break;
            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;
        }
    }
}
