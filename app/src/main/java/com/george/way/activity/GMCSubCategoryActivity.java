package com.george.way.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.adapter.GMCSubCategoryAdapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.model.category.SubCategory;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GMCSubCategoryActivity extends Activity implements View.OnClickListener {


    @BindView(R.id.app_bar)
    Toolbar app_bar;

    ListView categoryExpandableList;
    ImageView imageViewBack;

    @BindView(R.id.textViewHeader)
    TextView textViewHeader;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;

    @BindView(R.id.editText_query)
    EditText editText_query;
    /*
   declare objects of this class
    */
    public static final String KEY_PARENT_DATA = "KEY_CHILD_DATA";

    private Activity mActivity;
    private SubCategory mSubCategory;
    private GMCSubCategoryAdapter mGMCSubCategoryAdapter;
    private TextView tvAskTutor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_george_math_class);
        tvAskTutor = (TextView) findViewById(R.id.ask_tutor);

        tvAskTutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GMCSubCategoryActivity.this, SearchTutor_Activity.class));
                ViewAnimUtils.activityEnterTransitions(GMCSubCategoryActivity.this);
            }
        });
        /**
         * init
         */
        ButterKnife.bind(this);

        /**
         * class data init
         */
        init();

        editText_query.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mGMCSubCategoryAdapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(mActivity);
    }


    @Override
    protected void onResume() {
        super.onResume();

        /**
         * update theme
         */
        /*try {
            ThemeUtils.getObj(mActivity).setUpTheme(app_bar, new ImageView[]{imageViewBack, imageViewCalculator}, textViewHeader);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    /**
     * class data init
     */
    private void init() {

        mActivity = this;
        categoryExpandableList = (ListView) findViewById(R.id.category_expandable_list);
        imageViewBack = (ImageView) findViewById(R.id.imageViewBack);
        imageViewBack.setOnClickListener(this);

        try {
            Intent dataIntent = getIntent();
            if (dataIntent.hasExtra(KEY_PARENT_DATA)) {

                mSubCategory = (SubCategory) dataIntent.getSerializableExtra(KEY_PARENT_DATA);

                mGMCSubCategoryAdapter = new GMCSubCategoryAdapter(mActivity, mSubCategory.getChildCategory());
                categoryExpandableList.setAdapter(mGMCSubCategoryAdapter);
                textViewHeader.setText(mSubCategory.getName());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack:
                finish();
                ViewAnimUtils.activityExitTransitions(mActivity);
                break;
            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;
        }
    }
}
