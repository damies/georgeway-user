package com.george.way.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.george.way.R;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.dialog.QuestionOfTheDayDialog;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.jcVideoPlayer.JCVideoPlayer;
import com.george.way.jcVideoPlayer.JCVideoPlayerStandard;
import com.george.way.model.QuestionOfTheDayResponse;
import com.george.way.model.allQuestions.QuestionsDataModel;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.slider.NavDrawerItem;
import com.george.way.slider.NavDrawerListAdapter;
import com.george.way.utils.AppUtils;
import com.george.way.utils.GeorgeWayApplication;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;
import com.george.way.webApi.GetThumbNailServerAsync;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.memfis19.annca.Annca;
import io.github.memfis19.annca.internal.configuration.AnncaConfiguration;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeorgeHomeActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int CAPTURE_MEDIA = 368;
    /**
     * declare variables of class
     */
    @BindView(R.id.listSliderMenu)
    ListView listSliderMenu;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.videoplayer)
    JCVideoPlayerStandard mJCVideoPlayerStandard;

    @BindView(R.id.imageViewUser)
    ImageView imageViewUser;

    @BindView(R.id.textViewUser)
    TextView textViewUser;

    @BindView(R.id.textViewEmail)
    TextView textViewEmail;

    @BindView(R.id.imageViewMenu)
    ImageView imageViewMenu;

    @BindView(R.id.wrapper_tiles)
    View wrapperTiles;

    /*@BindView(R.id.gridViewCategory)
    GridView gridViewCategory;*/

    @BindView(R.id.img_fb_sign_in)
    View ivSignInFb;

    @BindView(R.id.img_google_sign_in)
    View ivSignInGoogle;

    @BindView(R.id.button_sign_up)
    View buttonSignUp;

    @BindView(R.id.textViewNoCategory)
    TextView textViewNoCategory;

    @BindView(R.id.georgemath_tv)
    TextView georgeMathTv;

    @BindView(R.id.question_tv)
    TextView questionTv;

    @BindView(R.id.subscribe_tv)
    TextView subscribeTv;

    @BindView(R.id.buttonLogin)
    Button buttonLogin;

    @BindView(R.id.test_preparation_tv)
    TextView test_preparation_tv;

    @BindView(R.id.feedback_tv)
    TextView feedbackTv;

    @BindView(R.id.online_tv)
    TextView online_tv;

    @BindView(R.id.textViewPlan)
    TextView textViewPlan;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;

    @BindView(R.id.progressBarImg)
    ProgressBar progressBarImg;


    @BindView(R.id.app_bar)
    Toolbar app_bar;

    @BindView(R.id.textViewTitle)
    TextView textViewTitle;

    @BindView(R.id.textViewStartDate)
    TextView textViewStartDate;

    @BindView(R.id.textViewEndDate)
    TextView textViewEndDate;

    @BindView(R.id.date_layout)
    LinearLayout date_layout;


    @BindView(R.id.optionLayout)
    View optionLayout;

    private JSONObject jsonObjectTheme;

    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter_list;
    private TypedArray navMenuIcons;
    private Activity mActivity;
    public static GeorgeHomeActivity homeActivity;

    public static ArrayList<com.george.way.model.allQuestions.Datum> mQuestionsDataList;
    public static final String KEY_PUSH_TYPE = "KEY_PUSH_TYPE";
    private String mPushType = AppConstant.getObj().EMPTY_STRING;
    private boolean shouldCallApi = true;

    private final String PUSH_TYPE_CHAT = "1";
    private final String PUSH_TYPE_NORMAL = "2";
    private final String PUSH_TYPE_QOD = "3";
    public static final String KEY_SHOULD_CALL_API = "KEY_SHOULD_CALL_API";
    private String userId = "";
    private boolean isPlan_Active = false;

    @BindView(R.id.relMain)
    LinearLayout relMain;

    View wraperMyAcc;

    String s = "";
    private int ACTION_TAKE_VIDEO = 1100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gorge_home);

        PreferenceUserUtil preferenceUserUtilq = PreferenceUserUtil.getInstance(getBaseContext());
        s = preferenceUserUtilq.getString(preferenceUserUtilq.KEY_TUTORING_SERVICE);
        Log.e("ssssss ", " " + s);
        /**
         * init
         */
        ButterKnife.bind(this);
        mActivity = this;
        homeActivity = this;
        mQuestionsDataList = new ArrayList<>();
        /**
         * call api for fetching current plan details
         */
        fetchCurrentPlane();

        /**
         * set slide navigation drawer
         */
        settingUpSlider();


        /**
         * setting on click events on views
         */
        setonClickEvents();

        /**
         * call api for fetching categories password
         */
        callFetchQuestionsApi();


        /**
         * update device token
         */
        try {
            String token = FirebaseInstanceId.getInstance().getToken();
            PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
            preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_DEVICE_TOKEN, token);
            Log.e("tg", "token = " + token);

        } catch (Exception e) {
            e.printStackTrace();
        }


        /**
         * update ui as per received push notifications
         */
        try {
            upDateUiAsPusType();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * update ui as per received push notifications
     */
    private void upDateUiAsPusType() throws Exception {

        Intent dataIntent = getIntent();

        if (dataIntent != null && dataIntent.hasExtra(KEY_PUSH_TYPE)) {

            mPushType = dataIntent.getStringExtra(KEY_PUSH_TYPE);

            shouldCallApi = dataIntent.getBooleanExtra(KEY_SHOULD_CALL_API, true);

            if (shouldCallApi) {

                /**
                 * get home screen data
                 */
                getHomeDataWebApi(true);

            } else {

                /**
                 * update UI in case of notification
                 */
                updateUI(mPushType);
            }

        } else {
            /**
             * get home screen data
             */
            getHomeDataWebApi(false);
        }
    }


    public static GeorgeHomeActivity getInstance() {
        return homeActivity;
    }

    /**
     * update UI in case of notification
     */
    public void updateUI(String pushType) throws Exception {


        if (!TextUtils.isEmpty(pushType)) {

            if (pushType.equalsIgnoreCase(PUSH_TYPE_CHAT)) {


                // open chat screen in case of push notification is 1
                final PreferenceUserUtil preferenceUserUtil3 = PreferenceUserUtil.getInstance(GeorgeHomeActivity.this);
                preferenceUserUtil3.setPreferenceIntData(preferenceUserUtil3.KEY_BTN_TYPE, 3);
                String userId3 = preferenceUserUtil3.getString(preferenceUserUtil3.KEY_USER_ID);

                if (TextUtils.isEmpty(userId3)) {

                    new SingleActionDialog().showDialogForLogin(getResources().getString(R.string.please_login_Online_Tutoring), mActivity);

                } else {


                    String isOnlineTutoring = preferenceUserUtil3.getString(preferenceUserUtil3.KEY_TUTORING_SERVICE);

                    Log.e("isOnlineTutoring ", " " + isOnlineTutoring);

                    if (!isOnlineTutoring.isEmpty() || !s.isEmpty()) {
                        if (isOnlineTutoring.equalsIgnoreCase("1") || s.equalsIgnoreCase("1")) {

//                            Intent intentTutoring = new Intent(mActivity, OnlineTutoringActivity.class);
//                            startActivity(intentTutoring);
//                            ViewAnimUtils.activityEnterTransitions(mActivity);

                            Intent intentTutoring = new Intent(mActivity, SearchTutor_Activity.class);
                            startActivity(intentTutoring);
                            ViewAnimUtils.activityEnterTransitions(mActivity);


                        } else {
                            new SingleActionDialog().showDialogForSubscription(getResources().getString(R.string.Please_subscribe_Online_Tutoring), mActivity);
                        }
                    } else {
                        new SingleActionDialog().showDialogForSubscription(getResources().getString(R.string.Please_subscribe_Online_Tutoring), mActivity);
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            String isOnlineTutoring = preferenceUserUtil3.getString(preferenceUserUtil3.KEY_TUTORING_SERVICE);

                        }
                    }, 4000);

                }

            } else if (pushType.equalsIgnoreCase(PUSH_TYPE_QOD)) {

                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                Date current = new Date();

                PreferenceUserUtil preferenceUserUtil3 = PreferenceUserUtil.getInstance(GeorgeHomeActivity.this);
                if (!preferenceUserUtil3.getString("Now").isEmpty())
                {

                    Date saveeDate = df.parse(preferenceUserUtil3.getString("Now"));
                    //create a date one day before current date
                    //create date object
                    //compare both dates
                    if(!isSameDay(current,saveeDate)){
                        if (Build.VERSION.SDK_INT < 23) {
                            launchAnnca();
                        } else {
                            pendingActionId = ACTION_ID_LAUNCH_CAMERA;
                            checkAppPermission(1000);
                        }
                    } else {
                        System.out.println("The date is future day");
                    }
                    openQodDialog();
                }else if (!preferenceUserUtil3.getString("isAlways").isEmpty())
                {
                    openQodDialog();
                }else if (!preferenceUserUtil3.getString("isVideo").isEmpty())
                {
                    Date saveeDate = df.parse(preferenceUserUtil3.getString("time"));
                    if(!isSameDay(current,saveeDate)){
                        if (Build.VERSION.SDK_INT < 23) {
                            launchAnnca();
                        } else {
                            pendingActionId = ACTION_ID_LAUNCH_CAMERA;
                            checkAppPermission(1000);
                        }
                    } else {
                        openQodDialog();
                    }

                } else
                {
                    if (Build.VERSION.SDK_INT < 23) {
                        launchAnnca();
                    } else {
                        pendingActionId = ACTION_ID_LAUNCH_CAMERA;
                        checkAppPermission(1000);
                    }
                }




            } else {

                //else do nothing
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

        /**
         * update slider data
         */
        settingUpSliderViewsData();

        GeorgeWayApplication.activityResumed();

    }


    @Override
    protected void onPause() {
        super.onPause();
        GeorgeWayApplication.activityPaused();

        try {
            JCVideoPlayer.releaseAllVideos();
        } catch (Exception e) {
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GeorgeWayApplication.activityDestroyed();
    }

    /**
     * set on click event on views
     */
    private void setonClickEvents() {

        imageViewMenu.setOnClickListener(this);
        georgeMathTv.setOnClickListener(this);
        //buttonsingin.setOnClickListener(this);
        questionTv.setOnClickListener(this);
        subscribeTv.setOnClickListener(this);
        feedbackTv.setOnClickListener(this);
        online_tv.setOnClickListener(this);
        test_preparation_tv.setOnClickListener(this);
        imageViewCalculator.setOnClickListener(this);
        imageViewUser.setOnClickListener(this);
        optionLayout.setOnClickListener(this);
        buttonSignUp.setOnClickListener(this);
        buttonLogin.setOnClickListener(this);
        ivSignInFb.setOnClickListener(this);
        ivSignInGoogle.setOnClickListener(this);

    }


    /**
     * setting up user profile
     */
    private void settingUpSliderViewsData() {

        PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
        userId = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_USER_ID);
        String firstName = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_FIRST_NAME);
        String lastName = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_LAST_NAME);
        String subscribedPlan = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_SUBSCRIPTION_PLAN);
        String image = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_USER_IMAGE);

        if (!TextUtils.isEmpty(userId)) {
            optionLayout.setVisibility(View.GONE);

        } else {
            optionLayout.setVisibility(View.VISIBLE);

        }
        if (TextUtils.isEmpty(firstName)) {

            textViewUser.setText("Guest");
            date_layout.setVisibility(View.GONE);
            textViewEmail.setVisibility(View.GONE);
        } else {
            textViewUser.setText(firstName + " " + lastName);

            if (msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).subscription_start) != null) {
                if (!msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).subscription_start).isEmpty()) {
                    date_layout.setVisibility(View.VISIBLE);
                    String text = "Plan Start Date: <font color=\"#67BCB9\">" + msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).subscription_start) + ".</font>";
                    textViewStartDate.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

                    String text_ = "Plan End Date: <font color=\"#67BCB9\">" + msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).subscription_end) + ".</font>";
                    textViewEndDate.setText(Html.fromHtml(text_), TextView.BufferType.SPANNABLE);
                }
            }


        }

        if (isPlan_Active) {
            textViewPlan.setText("Subscription plan:");
//            textViewEmail.setText(subscribedPlan);
        } else {
            textViewPlan.setText("Subscription plan:");
            textViewEmail.setText("No Plan Active");
        }

//        if (TextUtils.isEmpty(subscribedPlan)) {
//            textViewPlan.setText("Subscription plan:");
//            textViewEmail.setText("No Plan Active");
//        } else {
//            textViewPlan.setText("Subscription plan:");
//            textViewEmail.setText(subscribedPlan);
//        }

        if (!TextUtils.isEmpty(image)) {

            String path = "";
            if (image.startsWith("http://")) {
                path = image;
            } else {
                path = ApiClient.BASE_URL + image;
            }
            ImageLoader.getInstance().displayImage(path, imageViewUser, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    progressBarImg.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    progressBarImg.setVisibility(View.GONE);
                    imageViewUser.setImageResource(R.drawable.user);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    progressBarImg.setVisibility(View.GONE);

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    progressBarImg.setVisibility(View.GONE);
                    imageViewUser.setImageResource(R.drawable.user);

                }
            });
        } else {
            progressBarImg.setVisibility(View.GONE);
            imageViewUser.setImageResource(R.drawable.user);
        }

    }

    /**
     * set up video player
     */
    private void setUpVideoOnPlayer(String videoUrl) {

        try {
            videoUrl = videoUrl.replaceAll("\\s+", "%20");
            mJCVideoPlayerStandard.setUp(ApiClient.BASE_URL + videoUrl, AppConstant.getObj().EMPTY_STRING);
            new GetThumbNailServerAsync(mActivity, ApiClient.BASE_URL + videoUrl, mJCVideoPlayerStandard, mJCVideoPlayerStandard.ivThumb).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * setting up slide menu
     */
    private void settingUpSlider() {

        /**
         * Setting slidemenu view
         */
        wraperMyAcc = findViewById(R.id.wrapper_my_account);
        wraperMyAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceUserUtil preferenceUserUtil3 = PreferenceUserUtil.getInstance(mActivity);
                preferenceUserUtil3.setPreferenceIntData(preferenceUserUtil3.KEY_BTN_TYPE, 3);
                String userId3 = preferenceUserUtil3.getString(preferenceUserUtil3.KEY_USER_ID);
                if (TextUtils.isEmpty(userId3)) {
                    Intent mIntentEditProfile = new Intent(mActivity, RegistrationActivity.class);
                    mIntentEditProfile.putExtra("isEditProfile", false);
                    startActivity(mIntentEditProfile);
                    ViewAnimUtils.activityEnterTransitions(mActivity);
                } else {
                    Intent mIntentEditProfile = new Intent(mActivity, RegistrationActivity.class);
                    mIntentEditProfile.putExtra("isEditProfile", true);
                    startActivity(mIntentEditProfile);
                    ViewAnimUtils.activityEnterTransitions(mActivity);
                }
            }
        });
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        navDrawerItems = new ArrayList<>();

        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.Home), navMenuIcons.getResourceId(0, -1)));//0
        //navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.my_account), navMenuIcons.getResourceId(0, -1))); //1
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.George_Math_Class), navMenuIcons.getResourceId(0, -1)));         //2
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.Test_Preparation), navMenuIcons.getResourceId(0, -1)));            //3
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.Online_tutoring_Service), navMenuIcons.getResourceId(0, -1)));     //4
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.Question_Of_The_day), navMenuIcons.getResourceId(0, -1)));         //5
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.qod_winner_list), navMenuIcons.getResourceId(0, -1)));            //6
        //navDrawerItems.add(new NavDrawerItem("DONATE", R.drawable.donate));                                                   //7
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.Scientific_Calculator), navMenuIcons.getResourceId(0, -1)));                  //8
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.Feedback), navMenuIcons.getResourceId(0, -1)));                    //9
        //navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.subscribe), navMenuIcons.getResourceId(0, -1)));//10
        //navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.Success_Story), navMenuIcons.getResourceId(0, -1)));//11
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.About_Us), navMenuIcons.getResourceId(0, -1)));                      //12
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.Terms_and_Conditions), navMenuIcons.getResourceId(0, -1)));          //13
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.Contact_Us), navMenuIcons.getResourceId(0, -1)));                    //14
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.Logout), navMenuIcons.getResourceId(0, -1)));                        //15
        navMenuIcons.recycle();


        /**
         * Setting data in slide list
         */
        adapter_list = new NavDrawerListAdapter(mActivity, navDrawerItems);
        listSliderMenu.setAdapter(adapter_list);
        listSliderMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                    PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                    preferenceUserUtil.setPreferenceBooleanData(preferenceUserUtil.KEY_BTN_CLICKED, true);
                    preferenceUserUtil.setPreferenceIntData(preferenceUserUtil.KEY_BTN_TYPE, 0);

                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                    switch (pos) {
                        case 0:
                            mDrawerLayout.closeDrawer(Gravity.LEFT);
                            break;

                        /*case 1:
                            PreferenceUserUtil preferenceUserUtil3 = PreferenceUserUtil.getInstance(mActivity);
                            preferenceUserUtil3.setPreferenceIntData(preferenceUserUtil3.KEY_BTN_TYPE, 3);
                            String userId3 = preferenceUserUtil3.getString(preferenceUserUtil3.KEY_USER_ID);
                            if (TextUtils.isEmpty(userId3)) {
                                Intent mIntentEditProfile = new Intent(mActivity, RegistrationActivity.class);
                                mIntentEditProfile.putExtra("isEditProfile", false);
                                startActivity(mIntentEditProfile);
                                ViewAnimUtils.activityEnterTransitions(mActivity);
                            } else {
                                Intent mIntentEditProfile = new Intent(mActivity, RegistrationActivity.class);
                                mIntentEditProfile.putExtra("isEditProfile", true);
                                startActivity(mIntentEditProfile);
                                ViewAnimUtils.activityEnterTransitions(mActivity);
                            }

                            break;*/


                        case 1:

                            /*Call quiz activity*/
                            callQuizActivity();

                            break;
                        case 2://previous 3

                            PreferenceUserUtil prefUtil = PreferenceUserUtil.getInstance(mActivity);
                            prefUtil.setPreferenceIntData(prefUtil.KEY_BTN_TYPE, 2);

                            Intent intent = new Intent(mActivity, TestPreparationActivity.class);
                            startActivity(intent);
                            ViewAnimUtils.activityEnterTransitions(mActivity);

                            break;

                        case 3: //previous 4


                            PreferenceUserUtil preferenceUserUtil3_ = PreferenceUserUtil.getInstance(mActivity);
                            preferenceUserUtil3_.setPreferenceIntData(preferenceUserUtil3_.KEY_BTN_TYPE, 3);
                            String userId3_ = preferenceUserUtil3_.getString(preferenceUserUtil3_.KEY_USER_ID);

                            if (TextUtils.isEmpty(userId3_)) {

                                new SingleActionDialog().showDialogForLogin(getResources().getString(R.string.please_login_Online_Tutoring), mActivity);

                            } else {


                                Intent intentTutoring = new Intent(mActivity, SearchTutor_Activity.class);
                                startActivity(intentTutoring);
                                ViewAnimUtils.activityEnterTransitions(mActivity);
//                            String isOnlineTutoring = preferenceUserUtil3_.getString(preferenceUserUtil3_.KEY_TUTORING_SERVICE);
//                            if (!isOnlineTutoring.isEmpty()) {
//                                if (isOnlineTutoring.equalsIgnoreCase("1")) {
//
////                                    Intent intentTutoring = new Intent(mActivity, OnlineTutoringActivity.class);
////                                    startActivity(intentTutoring);
////                                    ViewAnimUtils.activityEnterTransitions(mActivity);
//
//
//                                    Intent intentTutoring = new Intent(mActivity, Chat_History_Activity.class);
//                                    startActivity(intentTutoring);
//                                    ViewAnimUtils.activityEnterTransitions(mActivity);
//
//                                } else {
//                                    new SingleActionDialog().showDialogForSubscription(getResources().getString(R.string.Please_subscribe_Online_Tutoring), mActivity);
//                                }
//                            } else {
//                                new SingleActionDialog().showDialogForSubscription(getResources().getString(R.string.Please_subscribe_Online_Tutoring), mActivity);
//                            }
                            }


                            break;

                        case 4: //previous 5

                            /**
                             * open qod dialog
                             */
//                        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
//                        builder.setTitle(getResources().getString(R.string.app_name));
//                        builder.setIcon(R.mipmap.ic_launcher);
//                        builder.setMessage("Please select video your video");
//                        builder.setNegativeButton("Record Video", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
                            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                            Date current = new Date();

                            PreferenceUserUtil preferenceUserUtil2 = PreferenceUserUtil.getInstance(GeorgeHomeActivity.this);
                            if (!preferenceUserUtil2.getString("Now").isEmpty()) {

                                Date saveeDate = null;
                                try {
                                    saveeDate = df.parse(preferenceUserUtil2.getString("Now"));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                //create a date one day before current date
                                //create date object
                                //compare both dates
                                if (!isSameDay(current, saveeDate)) {
                                    if (Build.VERSION.SDK_INT < 23) {
                                        launchAnnca();
                                    } else {
                                        pendingActionId = ACTION_ID_LAUNCH_CAMERA;
                                        checkAppPermission(1000);
                                    }
                                } else {
                                    System.out.println("The date is future day");
                                }
                                openQodDialog();
                            } else if (!preferenceUserUtil2.getString("isAlways").isEmpty()) {
                                openQodDialog();
                            } else if (!preferenceUserUtil2.getString("isVideo").isEmpty()) {
                                Date saveeDate = null;
                                try {
                                    saveeDate = df.parse(preferenceUserUtil2.getString("time"));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if (!isSameDay(current, saveeDate)) {
                                    if (Build.VERSION.SDK_INT < 23) {
                                        launchAnnca();
                                    } else {
                                        pendingActionId = ACTION_ID_LAUNCH_CAMERA;
                                        checkAppPermission(1000);
                                    }
                                } else {
                                    openQodDialog();
                                }

                            } else {
                                if (Build.VERSION.SDK_INT < 23) {
                                    launchAnnca();
                                } else {
                                    pendingActionId = ACTION_ID_LAUNCH_CAMERA;
                                    checkAppPermission(1000);
                                }
                            }

//                            }
//                        });
//
//                        builder.setPositiveButton("Georgeway Video", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                openQodDialog();
//                            }
//                        });
//
//                        AlertDialog alertDialog = builder.create();
//                        alertDialog.show();
                            break;
                        /*case 7:

                            Intent intentDonate = new Intent(mActivity, Donate_Activity.class);
                            intentDonate.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentDonate);
                            ViewAnimUtils.activityEnterTransitions(mActivity);

                            break;*/

                        case 5: //previous 6
                            Intent intentQODWinner = new Intent(mActivity, QODRewardActivity.class);
                            intentQODWinner.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentQODWinner);
                            ViewAnimUtils.activityEnterTransitions(mActivity);
                            break;

                        case 6: //previous 8
                            /**
                             * open calculator screen
                             */
                            Intent calculatorIntent = null;
                            if (Utils.hasLollipop()) {
                                calculatorIntent = new Intent(mActivity, CalculatorActivityL.class);
                            } else {
                                calculatorIntent = new Intent(mActivity, CalculatorActivityGB.class);
                            }
                            calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(calculatorIntent);
                            ViewAnimUtils.activityEnterTransitions(mActivity);

                            break;


                        case 7: //previous 9
                            PreferenceUserUtil preferenceUserUtilF = PreferenceUserUtil.getInstance(mActivity);
                            preferenceUserUtilF.setPreferenceIntData(preferenceUserUtilF.KEY_BTN_TYPE, 5);

                            Intent intentFeedBack = new Intent(mActivity, FeedbackActivity.class);
                            intentFeedBack.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentFeedBack);
                            ViewAnimUtils.activityEnterTransitions(mActivity);

                            break;

                        /*case 10:
                            *//*
                             * first of all finish current activity , then start new activity
                             *//*
                            PreferenceUserUtil preferenceUserUtilS = PreferenceUserUtil.getInstance(mActivity);
                            preferenceUserUtilS.setPreferenceIntData(preferenceUserUtilS.KEY_BTN_TYPE, 6);

                            Intent mIntentSubscribe = new Intent(mActivity, SubscriptionActivity.class);
                            mIntentSubscribe.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(mIntentSubscribe);
                            ViewAnimUtils.activityEnterTransitions(mActivity);

                            break;*/

                        /*case 11:

                            Intent intentSStory = new Intent(mActivity, SuccessStoryActivity.class);
                            intentSStory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentSStory);
                            ViewAnimUtils.activityEnterTransitions(mActivity);

                            break;*/

                        case 8: //previous 12

                            Intent intentAboutUS = new Intent(mActivity, AboutUsActivity.class);
                            intentAboutUS.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentAboutUS);
                            ViewAnimUtils.activityEnterTransitions(mActivity);


                            break;

                        case 10:    //previous13

                            Intent intentContactUs = new Intent(mActivity, ContactUsActivity.class);
                            intentContactUs.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentContactUs);
                            ViewAnimUtils.activityEnterTransitions(mActivity);


                            break;


                        case 9:    //previous 14

                            Intent intentTandC = new Intent(mActivity, TermsAndConditionsActivity.class);
                            intentTandC.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentTandC);
                            ViewAnimUtils.activityEnterTransitions(mActivity);

                            break;


                        case 11:       //prev 15

                            new SingleActionDialog().showDialogForLogout(getResources().getString(R.string.Do_you_want), mActivity);

                            break;
                    }

                }
        });
    }


    /**
     * update data in case of user logout
     */
    public void settingUpLogoutData() {

        PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);

//        if (("U_"+preferenceUserUtil.getPreferenceObjectData(preferenceUserUtil.KEY_USER_ID)).equals(getSinchServiceInterface().getUserName())) {
//            getSinchServiceInterface().stopClient();
//        }


        String questionId = preferenceUserUtil.getString(preferenceUserUtil.KEY_QUESTION_ID);
        String formattedDate = preferenceUserUtil.getString(preferenceUserUtil.KEY_DATE);
        QuestionOfTheDayResponse mCategordyDataModel = preferenceUserUtil.getPreferenceObjectData(preferenceUserUtil.KEY_QUESTION_DATA);
        String resultQuestionId = preferenceUserUtil.getString(preferenceUserUtil.KEY_RESULT_QUESTION);
        String resultFormattedDate = preferenceUserUtil.getString(preferenceUserUtil.KEY_RESULT_DATE);

        /**
         * clear preferences data
         */
        PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
        msPreferenceUserUtil.clearAllPreference();


        preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_RESULT_QUESTION, questionId);
        preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_RESULT_DATE, formattedDate);
        preferenceUserUtil.setPreferenceObjectData(preferenceUserUtil.KEY_QUESTION_DATA, mCategordyDataModel);
        preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_RESULT_QUESTION, resultQuestionId);
        preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_RESULT_DATE, resultFormattedDate);


        /**
         * update themes data
         */
        if (jsonObjectTheme != null) {
            updateDataTheme(jsonObjectTheme);
        }


        /**
         * update slider data
         */
        settingUpSliderViewsData();


    }

    /**
     * logout user
     * if everything is fine then open login screen
     */

    private void startLoginScreen() {

        /**
         * first of all finish current activity , then start new activity
         */
        Intent mIntent = new Intent(mActivity, LoginActivity.class);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mIntent);
        ViewAnimUtils.activityEnterTransitions(mActivity);

    }

    /*
     grid view click
    * */
    private boolean doubleBackToExitPressedOnce;

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, this.getResources().getString(R.string.exit_toast), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    @Override
    public void onClick(View view) {

        /**
         * update click button
         */
        PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
        preferenceUserUtil.setPreferenceBooleanData(preferenceUserUtil.KEY_BTN_CLICKED, true);


        switch (view.getId()) {
            case R.id.imageViewMenu:

                mDrawerLayout.openDrawer(Gravity.LEFT);

                break;

            case R.id.georgemath_tv:

                PreferenceUserUtil prefUtil = PreferenceUserUtil.getInstance(mActivity);
                prefUtil.setPreferenceIntData(prefUtil.KEY_BTN_TYPE, 1);


                /*Call quiz activity*/
                callQuizActivity();

                break;

            case R.id.online_tv:


                PreferenceUserUtil preferenceUserUtil3 = PreferenceUserUtil.getInstance(mActivity);
                preferenceUserUtil3.setPreferenceIntData(preferenceUserUtil3.KEY_BTN_TYPE, 3);
                String userId3 = preferenceUserUtil3.getString(preferenceUserUtil3.KEY_USER_ID);

                if (TextUtils.isEmpty(userId3)) {

                    new SingleActionDialog().showDialogForLogin(getResources().getString(R.string.please_login_Online_Tutoring), mActivity);

                } else {

                    if (isPlan_Active) {

                        Intent intentTutoring = new Intent(mActivity, SearchTutor_Activity.class);
                        startActivity(intentTutoring);
                        ViewAnimUtils.activityEnterTransitions(mActivity);

                    } else {
                        Intent intentTutoring = new Intent(mActivity, SearchTutor_Activity.class);
                        startActivity(intentTutoring);
                        ViewAnimUtils.activityEnterTransitions(mActivity);
                    }
                }

                break;
            case R.id.buttonLogin:

                /**
                 * first of all finish current activity , then start new activity
                 */
                Intent mIntent = new Intent(mActivity, LoginActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;

            case R.id.bottomLinearLayout:

                /**
                 * first of all finish current activity , then start new activity
                 */
                Intent mIntent1 = new Intent(mActivity, LoginActivity.class);
                mIntent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mIntent1);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;

            case R.id.question_tv:


                /**
                 * open qod dialog
                 //                 */
//                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
//                builder.setTitle(getResources().getString(R.string.app_name));
//                builder.setIcon(R.mipmap.ic_launcher);
//                builder.setMessage("Please select video your video");
//                builder.setNegativeButton("Record Video", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {

                /*SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                Date current = new Date();

                PreferenceUserUtil preferenceUserUtil2 = PreferenceUserUtil.getInstance(GeorgeHomeActivity.this);
                if (!preferenceUserUtil2.getString("Now").isEmpty())
                {

                    Date saveeDate = null;
                    try {
                        saveeDate = df.parse(preferenceUserUtil2.getString("Now"));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    //create a date one day before current date
                    //create date object
                    //compare both dates
                    if(!isSameDay(current,saveeDate)){
                        if (Build.VERSION.SDK_INT < 23) {
                            launchAnnca();
                        } else {
                            pendingActionId = ACTION_ID_LAUNCH_CAMERA;
                            checkAppPermission(1000);
                        }
                    } else {
                        System.out.println("The date is future day");
                    }
                    openQodDialog();
                }else if (!preferenceUserUtil2.getString("isAlways").isEmpty())
                {
                    openQodDialog();
                }else if (!preferenceUserUtil2.getString("isVideo").isEmpty())
                {
                    Date saveeDate = null;
                    try {
                        saveeDate = df.parse(preferenceUserUtil2.getString("time"));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(!isSameDay(current,saveeDate)){
                        if (Build.VERSION.SDK_INT < 23) {
                            launchAnnca();
                        } else {
                            pendingActionId = ACTION_ID_LAUNCH_CAMERA;
                            checkAppPermission(1000);
                        }
                    } else {
                        openQodDialog();
                    }

                } else
                {
                    if (Build.VERSION.SDK_INT < 23) {
                        launchAnnca();
                    } else {
                        pendingActionId = ACTION_ID_LAUNCH_CAMERA;
                        checkAppPermission(1000);
                    }
                }*/



//                    }
//                });

//                builder.setPositiveButton("Georgeway Video", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//
//                    }
//                });
//
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();

                openQodDialog();
                break;

            case R.id.subscribe_tv:


                /*
                     * first of all finish current activity , then start new activity
                     */
                PreferenceUserUtil preferenceUserUtilS = PreferenceUserUtil.getInstance(mActivity);
                preferenceUserUtilS.setPreferenceIntData(preferenceUserUtilS.KEY_BTN_TYPE, 6);

                Intent mIntentSubscribe = new Intent(mActivity, SubscriptionActivity.class);
                mIntentSubscribe.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mIntentSubscribe);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;

            case R.id.test_preparation_tv:

                PreferenceUserUtil preferenceUserUtilT = PreferenceUserUtil.getInstance(mActivity);
                preferenceUserUtilT.setPreferenceIntData(preferenceUserUtilT.KEY_BTN_TYPE, 2);
                Intent intent = new Intent(mActivity, TestPreparationActivity.class);
                startActivity(intent);
                ViewAnimUtils.activityEnterTransitions(mActivity);


                break;

            case R.id.feedback_tv:

                PreferenceUserUtil preferenceUserUtilF = PreferenceUserUtil.getInstance(mActivity);
                preferenceUserUtilF.setPreferenceIntData(preferenceUserUtilF.KEY_BTN_TYPE, 5);

                Intent intentFeedBack = new Intent(mActivity, FeedbackActivity.class);
                startActivity(intentFeedBack);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;


            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;
            case R.id.imageViewUser:
                if (!TextUtils.isEmpty(userId)) {
                    Intent mIntentEditProfile = new Intent(mActivity, RegistrationActivity.class);
                    mIntentEditProfile.putExtra("isEditProfile", true);
                    startActivity(mIntentEditProfile);
                    ViewAnimUtils.activityEnterTransitions(mActivity);
                }

                break;

            case R.id.button_sign_up:
                Intent intentSignUp = new Intent(mActivity, RegistrationActivity.class);
                intentSignUp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentSignUp);
                ViewAnimUtils.activityEnterTransitions(mActivity);
                break;

            case R.id.img_fb_sign_in:
                Intent mIntent6 = new Intent(mActivity, LoginActivity.class);
                mIntent6.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent6.putExtra("_direct_action",LoginActivity.DIRECT_FACEBOOK_LOGIN);
                startActivity(mIntent6);
                break;

            case R.id.img_google_sign_in:
                Intent mIntent5 = new Intent(mActivity, LoginActivity.class);
                mIntent5.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mIntent5.putExtra("_direct_action",LoginActivity.DIRECT_GOOGLE_LOGIN);
                startActivity(mIntent5);
                //ViewAnimUtils.activityEnterTransitions(mActivity);
                break;
        }

    }
    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }
    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }
    /*Call quiz activity*/
    private void callQuizActivity() {

        PreferenceUserUtil prefUtil = PreferenceUserUtil.getInstance(mActivity);
        prefUtil.setPreferenceIntData(prefUtil.KEY_BTN_TYPE, 1);

        Intent i = new Intent(GeorgeHomeActivity.this, GMCMainCategoryActivity.class);
        startActivity(i);
        ViewAnimUtils.activityEnterTransitions(mActivity);
    }

    /**
     * call api for fetching categories password
     */
    public void callFetchQuestionsApi() {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<QuestionsDataModel> call = mApiInterface.fetchQuestionsRequest();

                call.enqueue(new Callback<QuestionsDataModel>() {
                    @Override
                    public void onResponse(Call<QuestionsDataModel> call, Response<QuestionsDataModel> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            QuestionsDataModel mQuestionsDataModel = response.body();
                            mQuestionsDataList.clear();
                            if (mQuestionsDataModel != null && mQuestionsDataModel.getData() != null && mQuestionsDataModel.getData().size() > 0) {
                                {

                                    for (int index = 0; index < mQuestionsDataModel.getData().size(); index++) {

                                        mQuestionsDataList.add(mQuestionsDataModel.getData().get(index));

                                    }
                                }
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<QuestionsDataModel> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }
    }


    @SuppressLint("MissingPermission")
    private void launchAnnca(){
        pendingActionId = 0;
        AnncaConfiguration.Builder videoLimited = new AnncaConfiguration.Builder(GeorgeHomeActivity.this, CAPTURE_MEDIA);
        videoLimited.setMediaAction(AnncaConfiguration.MEDIA_ACTION_VIDEO);
        videoLimited.setMediaQuality(AnncaConfiguration.MEDIA_QUALITY_AUTO);
        videoLimited.setVideoFileSize(5 * 1024 * 1024);
        videoLimited.setMinimumVideoDuration(1 * 10 * 1000);
        new Annca(videoLimited.build()).launchCamera();
    }


    DialogInterface.OnClickListener networkCallBack2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            callFetchQuestionsApi();
        }
    };

    /**
     * call api
     */
    public void getHomeDataWebApi(final boolean updateIfPush) {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.getHomeDataRequest(userId);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            ResponseBody mResponseBody = response.body();


                            if (mResponseBody != null) {

                                JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                                Log.e("tg", "response from server = " + mJsonObj.toString());
                                String status_code = mJsonObj.optString("status_code");
                                if (!TextUtils.isEmpty(status_code)) {

                                    if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {
                                        try {
                                            relMain.setVisibility(View.VISIBLE);
                                            String success_message = mJsonObj.optString("success_message");
                                            JSONArray dataJsonArr = mJsonObj.getJSONArray("data");
                                            JSONObject dataJsonObj = dataJsonArr.getJSONObject(0);
                                            String home_video = dataJsonObj.optString("home_video");

                                            jsonObjectTheme = dataJsonObj;
                                            updateDataTheme(jsonObjectTheme);

                                            PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(mActivity);

                                            try {
                                                String is_plan_active = dataJsonObj.optString("is_plan_active");
                                                String plan_id = dataJsonObj.optString("plan_id");
                                                String plan_name = dataJsonObj.optString("plan_name");
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_PLAN_ID, plan_id);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_SUBSCRIPTION_PLAN, plan_name);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_IS_PLAN_ACTIVE, is_plan_active);

                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.contact_phone, dataJsonObj.optString("contact_phone"));
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.contact_email, dataJsonObj.optString("contact_email"));
                                                AppConstant.getObj().CONTACT_EMAIL = dataJsonObj.optString("contact_email");
                                                AppConstant.getObj().CONTACT_NUMBER = dataJsonObj.optString("contact_phone");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                            try {
                                                String isMathClass = dataJsonObj.optString("isMathClass");
                                                String isTestPreparation = dataJsonObj.optString("isTestPreparation");
                                                String isTutoringService = dataJsonObj.optString("isTutoringService");
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_MATH_CLASS, isMathClass);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_TEST_PREPARATION, isTestPreparation);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_TUTORING_SERVICE, isTutoringService);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }


                                            /**
                                             * set up video player
                                             */
                                            setUpVideoOnPlayer(home_video);

                                            if (updateIfPush) {

                                                /**
                                                 * update UI in case of notification
                                                 */
                                                updateUI(mPushType);

                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        relMain.setVisibility(View.GONE);
                                    }

                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack3);
        }
    }

    DialogInterface.OnClickListener networkCallBack3 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            getHomeDataWebApi(false);
        }
    };


    private void updateDataTheme(JSONObject dataJsonObj) {

        final String home_content = dataJsonObj.optString("home_content");
        final String GMC_content = dataJsonObj.optString("GMC_content");
        final String GMC_color = dataJsonObj.optString("GMC_color");
        final String OTS_content = dataJsonObj.optString("OTS_content");
        final String OTS_color = dataJsonObj.optString("OTS_color");
        final String TP_content = dataJsonObj.optString("TP_content");
        final String TP_color = dataJsonObj.optString("TP_color");
        final String QTD_content = dataJsonObj.optString("QTD_content");
        final String QTD_color = dataJsonObj.optString("QTD_color");

        final String admin_email1 = dataJsonObj.optString("admin_email1");
        final String admin_email2 = dataJsonObj.optString("admin_email2");
        final String admin_email3 = dataJsonObj.optString("admin_email3");
        final String admin_email4 = dataJsonObj.optString("admin_email4");
        final String admin_email5 = dataJsonObj.optString("admin_email5");


        PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(mActivity);

        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_GMC_CONTENT, GMC_content);
        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_GMC_COLOR, GMC_color);
        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_OTS_CONTENT, OTS_content);
        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_OTS_COLOR, OTS_color);
        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_TP_CONTENT, TP_content);
        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_TP_COLOR, TP_color);
        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_QTD_CONTENT, QTD_content);
        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_QTD_COLOR, QTD_color);


        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_ADMIN_EMAIL_1, admin_email1);
        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_ADMIN_EMAIL_2, admin_email2);
        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_ADMIN_EMAIL_3, admin_email3);
        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_ADMIN_EMAIL_4, admin_email4);
        preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_ADMIN_EMAIL_5, admin_email5);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        online_tv.setText(OTS_content);
                        georgeMathTv.setText(GMC_content);
                        questionTv.setText(QTD_content);
                        test_preparation_tv.setText(TP_content);

                        try {
                            if (!home_content.isEmpty()) {
                                if (Build.VERSION.SDK_INT >= 24) {

                                    textViewTitle.setText(AppUtils.getObj().noTrailingwhiteLines(Html.fromHtml(home_content, Html.FROM_HTML_MODE_LEGACY)));

                                } else {
                                    textViewTitle.setText(AppUtils.getObj().noTrailingwhiteLines(Html.fromHtml(home_content)));
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        try {
                            GradientDrawable OTSShape = (GradientDrawable) online_tv.getBackground();
                            OTSShape.setColor(Color.parseColor(OTS_color));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        try {
                            GradientDrawable shape = (GradientDrawable) georgeMathTv.getBackground();
                            shape.setColor(Color.parseColor(GMC_color));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }


                        try {
                            GradientDrawable shape = (GradientDrawable) questionTv.getBackground();
                            shape.setColor(Color.parseColor(QTD_color));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        try {
                            GradientDrawable shape = (GradientDrawable) test_preparation_tv.getBackground();
                            shape.setColor(Color.parseColor(TP_color));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        wrapperTiles.setVisibility(View.VISIBLE);
                        settingUpSlider();


                    }
                });
            }
        }, 100);
    }

    private void callQuestionOfTheDayApi() {
        /**
         * call api for fetching categories password
         */


        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                if (!GWProgressDialog.getProgressDialog(mActivity).isDialogShowing()) {
                    GWProgressDialog.getProgressDialog(mActivity).showDialog1();
                }


                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<QuestionOfTheDayResponse> call = mApiInterface.FETCH_QUESTION_OFTHEDAY();

                call.enqueue(new Callback<QuestionOfTheDayResponse>() {
                    @Override
                    public void onResponse(Call<QuestionOfTheDayResponse> call, Response<QuestionOfTheDayResponse> response) {

                        try {

                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            QuestionOfTheDayResponse mCategordyDataModel = response.body();

                            if (mCategordyDataModel != null && mCategordyDataModel.getData() != null && mCategordyDataModel.getData().size() > 0) {


                                /**
                                 * set data into preference and update on views
                                 */
                                Calendar currentDate = Calendar.getInstance();
                                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                                String formattedDate = df.format(currentDate.getTime());

                                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                                String questionId = mCategordyDataModel.getData().get(0).getQuestionid();
                                preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_QUESTION_ID, questionId);
                                preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_DATE, formattedDate);
                                preferenceUserUtil.setPreferenceObjectData(preferenceUserUtil.KEY_QUESTION_DATA, mCategordyDataModel);


                                /**
                                 * show QOD dialog
                                 */
                                PreferenceUserUtil preferenceUserUtilq = PreferenceUserUtil.getInstance(mActivity);
                                preferenceUserUtilq.setPreferenceIntData(preferenceUserUtilq.KEY_BTN_TYPE, 4);

                                QuestionOfTheDayDialog questionOfTheDayDialog = QuestionOfTheDayDialog.getObj(GeorgeHomeActivity.this);
                                questionOfTheDayDialog.showQODDialog(mCategordyDataModel);


                            } else {
                                new SingleActionDialog().showDialogNormal(mActivity.getString(R.string.no_qod_available), mActivity, false);
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<QuestionOfTheDayResponse> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                        new SingleActionDialog().showDialogNormal(mActivity.getString(R.string.no_qod_available), mActivity, false);

                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    /**
     * open qod dialog
     */
    private void openQodDialog() {


        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String currentFormattedDate = df.format(currentDate.getTime());
        PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
        String savedDate = preferenceUserUtil.getString(preferenceUserUtil.KEY_DATE);
        String savedQId = preferenceUserUtil.getString(preferenceUserUtil.KEY_QUESTION_ID);
        QuestionOfTheDayResponse questionDataModel = preferenceUserUtil.getPreferenceObjectData(preferenceUserUtil.KEY_QUESTION_DATA);

        String resultDate = preferenceUserUtil.getString(preferenceUserUtil.KEY_RESULT_DATE);
        String questionIdResult = preferenceUserUtil.getString(preferenceUserUtil.KEY_RESULT_QUESTION);


        if (!TextUtils.isEmpty(savedDate)
                && !TextUtils.isEmpty(savedQId)
                && savedDate.equalsIgnoreCase(currentFormattedDate)
                && questionDataModel != null) {

            if (!TextUtils.isEmpty(resultDate)
                    && !TextUtils.isEmpty(questionIdResult)
                    && resultDate.equalsIgnoreCase(currentFormattedDate)) {

                new SingleActionDialog().showDialogNormal(mActivity.getString(R.string.no_question_for_today), mActivity, false);


            } else {

                PreferenceUserUtil preferenceUserUtilq = PreferenceUserUtil.getInstance(mActivity);
                preferenceUserUtilq.setPreferenceIntData(preferenceUserUtilq.KEY_BTN_TYPE, 4);

                QuestionOfTheDayDialog questionOfTheDayDialog = QuestionOfTheDayDialog.getObj(GeorgeHomeActivity.this);
                questionOfTheDayDialog.showQODDialog(questionDataModel);

            }

        } else {

            callQuestionOfTheDayApi();
        }

    }

    /**
     * call api for fetch message
     */
    public void fetchCurrentPlane() {

        if (NetworkUtil.isConnected(mActivity)) {
            try {
                AppUtils.getObj().hideSoftKeyboard(mActivity);

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.currentPlan(PreferenceUserUtil.getInstance(mActivity).getString(PreferenceUserUtil.getInstance(mActivity).KEY_USER_ID));

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");
                            JSONArray data = mJsonObj.optJSONArray("data");
                            JSONObject dataJsonObj = data.getJSONObject(0);

                            if (dataJsonObj.has("subscription_time")) {
                                if (!dataJsonObj.optString("subscription_time").equalsIgnoreCase("0") && !dataJsonObj.optString("subscription_time").equalsIgnoreCase("-1")) {
                                    isPlan_Active = true;
            Log.e("TAG","HOME, Subscribed Time : "+dataJsonObj.optString("subscription_time"));
                                    showSubcribedTime(dataJsonObj.optString("subscription_time"));
                                    PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(GeorgeHomeActivity.this);
                                    preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_TUTORING_SERVICE, "1");
                                } else {
                                    isPlan_Active = false;
                                    PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(GeorgeHomeActivity.this);
                                    preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_TUTORING_SERVICE, "0");
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        fetchCurrentPlane();

    }

    private void showSubcribedTime(String endDate) {

        try {
            long fetchedTime = Long.parseLong(endDate);

            long seconds = fetchedTime % 60;
            long minutes = fetchedTime / 60;

            String displeMin = Chat_List_Activity.twoDigitString(minutes);
            String displaySec = Chat_List_Activity.twoDigitString(seconds);

            textViewEmail.setText(getString(R.string.timer_time,displeMin,displaySec));
            textViewEmail.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        PreferenceUserUtil preferenceUserUtil3 = PreferenceUserUtil.getInstance(GeorgeHomeActivity.this);

        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(currentDate.getTime());


        //create a date one day before current date

        if (AnncaConfiguration.mType.equalsIgnoreCase("Now"))
        {
            preferenceUserUtil3.setPreferenceStringData("Now",formattedDate);

        }else  if (AnncaConfiguration.mType.equalsIgnoreCase("Always")){
            preferenceUserUtil3.setPreferenceStringData("isAlways","true");
        }else if (requestCode == CAPTURE_MEDIA) {
            if (data!=null)
            {
                preferenceUserUtil3.setPreferenceStringData("isVideo",data.getStringExtra("io.memfis19.annca.camera_video_file_path"));
                preferenceUserUtil3.setPreferenceStringData("time",formattedDate);
            }

        }

    }
    private void checkAppPermission(int type) {

        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity, new String[]{
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
            }, type);

        } else if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, type);

        } else {
            launchAnnca();
        }
    }

    int pendingActionId = 0;
    private final int ACTION_ID_LAUNCH_CAMERA = 77;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==1000){
            boolean result=true;
            for (int o:grantResults){
                if (o!=PackageManager.PERMISSION_GRANTED){
                    result = false;
                    break;
                }
            }
            if (result){
                launchAnnca();
            }else{
                Toast.makeText(mActivity, "Permission Denied, Couldn't open requested feature.", Toast.LENGTH_SHORT).show();
            }

        }else super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}