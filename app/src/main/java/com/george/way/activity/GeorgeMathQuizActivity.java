package com.george.way.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.adapter.AnswerAdapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.jcVideoPlayer.JCPlayerListener;
import com.george.way.jcVideoPlayer.JCVideoPlayer;
import com.george.way.jcVideoPlayer.JCVideoPlayerStandard;
import com.george.way.model.category.ChildSubCategory;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.GetThumbNailServerAsync;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GeorgeMathQuizActivity extends AppCompatActivity implements View.OnClickListener {


    /**
     * declare variables of class
     */
    @BindView(R.id.jCVideoPlayerSimple1)
    JCVideoPlayerStandard mJCVideoPlayerStandard1;

    @BindView(R.id.jCVideoPlayerSimple2)
    JCVideoPlayerStandard mJCVideoPlayerStandard2;


    @BindView(R.id.textViewHeader)
    TextView textViewHeader;

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;

    @BindView(R.id.text_question)
    TextView mTextQuestion;

    @BindView(R.id.text_correct)
    TextView text_correct;

    @BindView(R.id.answer_select)
    RecyclerView mAnswerSelect;

    @BindView(R.id.buttonOk)
    Button buttonOk;

    @BindView(R.id.relImageLayout)
    RelativeLayout relImageLayout;

    @BindView(R.id.answer_img)
    ImageView answer_img;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.app_bar)
    Toolbar app_bar;

    @BindView(R.id.textViewLectureVideo)
    TextView textViewLectureVideo;

    @BindView(R.id.textViewPracticeVideo)
    TextView textViewPracticeVideo;

    @BindView(R.id.textViewQuestion)
    TextView textViewQuestion;

    @BindView(R.id.linearQuestionLayout)
    LinearLayout linearQuestionLayout;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;

    @BindView(R.id.textViewNoVideos)
    TextView textViewNoVideos;

    @BindView(R.id.scrollViewData)
    ScrollView scrollViewData;

    @BindView(R.id.textViewYourPractice)
    TextView textViewYourPractice;

    @BindView(R.id.textViewGeorgePractice)
    TextView textViewGeorgePractice;

    @BindView(R.id.textViewLecture)
    TextView textViewLecture;

    @BindView(R.id.imageViewResult)
    ImageView imageViewResult;

    @BindView(R.id.textExplaination)
    TextView text_answer;

    @BindView(R.id.textViewExpDesc)
    TextView textViewExpDesc;

    private ArrayList<String> arrayListAnswer;
    private AnswerAdapter mAnswerAdapter;

    private Activity mActivity;
    private ChildSubCategory mChildSubCategory;
    public static final String KEY_CHILD_DATA = "KEY_CHILD_DATA";

    com.george.way.model.allQuestions.Datum questionData = null;
    private String correctAnswer = "";
    boolean isAnswerCorrect = false;
    ArrayList<String> correctAnswerList;
    private TextView tvAskTutor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_george_math_quiz);

        tvAskTutor = findViewById(R.id.ask_tutor);
        tvAskTutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GeorgeMathQuizActivity.this,SearchTutor_Activity.class));
                ViewAnimUtils.activityEnterTransitions(GeorgeMathQuizActivity.this);
            }
        });
        /**
         * init
         */
        ButterKnife.bind(this);

        /**
         * class data init
         */
        init();

        /**
         * set click events
         */
        setOnClick();


    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * update theme
         */
       /* try {

            ThemeUtils.getObj(mActivity).setUpTheme(app_bar, new ImageView[]{imageViewBack, imageViewCalculator}, textViewHeader);
            ThemeUtils.getObj(mActivity).updateTextViews(textViewYourPractice);
            ThemeUtils.getObj(mActivity).updateTextViews(textViewGeorgePractice);
            ThemeUtils.getObj(mActivity).updateTextViews(textViewLecture);

        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    /**
     * class data init
     */
    private void init() {

        mActivity = this;
        arrayListAnswer = new ArrayList<>();
        correctAnswerList = new ArrayList<>();
        Intent dataIntent = getIntent();
        if (dataIntent.hasExtra(KEY_CHILD_DATA)) {
            mChildSubCategory = (ChildSubCategory) dataIntent.getSerializableExtra(KEY_CHILD_DATA);
        }


        settingUpDataIntoViews();

        /**
         * set scroll to top
         */
        try {
            scrollViewData.setFocusableInTouchMode(true);
            scrollViewData.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    /**
     * set click events
     */
    private void setOnClick() {

        imageViewBack.setOnClickListener(this);
        buttonOk.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack:

                finish();
                ViewAnimUtils.activityExitTransitions(mActivity);

                break;

            case R.id.buttonOk:
                onSubmit();
                break;

            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(mActivity);
    }

    /**
     * setting up data into views
     */
    private void settingUpDataIntoViews() {

        if (mChildSubCategory != null) {
            try {
                if (GeorgeHomeActivity.mQuestionsDataList.size() > 0) {

                    for (int index = 0; index < GeorgeHomeActivity.mQuestionsDataList.size(); index++) {
                        questionData = GeorgeHomeActivity.mQuestionsDataList.get(index);
                        if (questionData != null && !TextUtils.isEmpty(questionData.getCategoryName())
                                && questionData.getCategoryName().equalsIgnoreCase(mChildSubCategory.getName())) {
                            break;
                        } else {
                            questionData = null;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            textViewHeader.setText(mChildSubCategory.getName());

            settingUpQuestionVideo();


            if (questionData != null) {


                if (!TextUtils.isEmpty(questionData.getQuestion())) {
                    if (questionData.getQuestion().contains(".png") || questionData.getQuestion().contains(".jpg")) {
                        relImageLayout.setVisibility(View.VISIBLE);
                        mTextQuestion.setText("Ques. : ");
                        ImageLoader.getInstance().displayImage(ApiClient.BASE_URL + questionData.getQuestion(), answer_img, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                                progressBar.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                progressBar.setVisibility(View.GONE);

                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                progressBar.setVisibility(View.GONE);

                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {
                                progressBar.setVisibility(View.GONE);

                            }
                        });
                    } else {

                        relImageLayout.setVisibility(View.GONE);

                        if (Build.VERSION.SDK_INT >= 24) {
                            mTextQuestion.setText(AppUtils. getObj().noTrailingwhiteLines(Html.fromHtml(questionData.getQuestion(), Html.FROM_HTML_MODE_LEGACY)));

                        } else {
                            mTextQuestion.setText(AppUtils. getObj().noTrailingwhiteLines(Html.fromHtml(questionData.getQuestion())));
                        }
                    }

                    if (!TextUtils.isEmpty(questionData.getFieldA()))
                        arrayListAnswer.add(questionData.getFieldA());

                    if (!TextUtils.isEmpty(questionData.getFieldB()))
                        arrayListAnswer.add(questionData.getFieldB());

                    if (!TextUtils.isEmpty(questionData.getFieldC()))
                        arrayListAnswer.add(questionData.getFieldC());

                    if (!TextUtils.isEmpty(questionData.getFieldD()))
                        arrayListAnswer.add(questionData.getFieldD());

                    if (!TextUtils.isEmpty(questionData.getFieldE()))
                        arrayListAnswer.add(questionData.getFieldE());


                    correctAnswer = questionData.getAnswerNumber();
                    correctAnswerList.clear();
                    if (correctAnswer.contains(",")) {
                        String[] correctAnswerArr = correctAnswer.split(",");
                        correctAnswerList = new ArrayList<String>(Arrays.asList(correctAnswerArr));
                    } else {
                        correctAnswerList.add(correctAnswer);
                    }

                    /**
                     * add answer in  answer view
                     */

                    buildQuestion();

                    linearQuestionLayout.setVisibility(View.VISIBLE);
                    textViewQuestion.setVisibility(View.GONE);

                    textViewNoVideos.setVisibility(View.GONE);
                    scrollViewData.setVisibility(View.VISIBLE);

                    try {

                        if (Build.VERSION.SDK_INT >= 24) {
                            text_answer.setText(AppUtils. getObj().noTrailingwhiteLines(Html.fromHtml(questionData.getExplaination(), Html.FROM_HTML_MODE_LEGACY)));

                        } else {
                            text_answer.setText(AppUtils. getObj().noTrailingwhiteLines(Html.fromHtml(questionData.getExplaination())));
                        }

                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                } else {

                    linearQuestionLayout.setVisibility(View.GONE);
                    textViewQuestion.setVisibility(View.VISIBLE);

                    textViewNoVideos.setVisibility(View.VISIBLE);
                    scrollViewData.setVisibility(View.GONE);
                }

            } else {
                linearQuestionLayout.setVisibility(View.GONE);
                textViewQuestion.setVisibility(View.VISIBLE);

                textViewNoVideos.setVisibility(View.VISIBLE);
                scrollViewData.setVisibility(View.GONE);
            }


        } else {
            textViewNoVideos.setVisibility(View.VISIBLE);
            scrollViewData.setVisibility(View.GONE);
        }


    }

    /**
     * build up questions
     */
    private void buildQuestion() {

        mAnswerSelect.setLayoutManager(new LinearLayoutManager(mActivity));
        mAnswerAdapter = new AnswerAdapter(mActivity, arrayListAnswer);
        mAnswerSelect.setAdapter(mAnswerAdapter);

        //mAnswerSelect.loadAnswers(arrayListAnswer, questionData.getAnswerNumber());

    }


    /**
     * set question video
     */
    private void settingUpQuestionVideo() {

        if (questionData != null) {

            final String isSubscribed = questionData.getSubscribe_status();


            String videoUrl1 = questionData.getQuestionVideo();
            videoUrl1 = videoUrl1.replaceAll("\\s+", "%20");

            String videoUrl2 = questionData.getQuestionVideo1();
            videoUrl2 = videoUrl2.replaceAll("\\s+", "%20");

            final String videoPath1 = ApiClient.BASE_URL + videoUrl1;
            final String videoPath2 = ApiClient.BASE_URL + videoUrl2;


            if (TextUtils.isEmpty(videoPath1)) {
                textViewLectureVideo.setVisibility(View.VISIBLE);
                mJCVideoPlayerStandard1.setVisibility(View.GONE);
            } else {
                textViewLectureVideo.setVisibility(View.GONE);
                mJCVideoPlayerStandard1.setVisibility(View.VISIBLE);
            }

            if (TextUtils.isEmpty(videoPath1)) {
                textViewPracticeVideo.setVisibility(View.VISIBLE);
                mJCVideoPlayerStandard2.setVisibility(View.GONE);
            } else {
                textViewPracticeVideo.setVisibility(View.GONE);
                mJCVideoPlayerStandard2.setVisibility(View.VISIBLE);
            }

            mJCVideoPlayerStandard1.setUp(videoPath1, AppConstant.getObj().EMPTY_STRING);
            mJCVideoPlayerStandard2.setUp(videoPath2, AppConstant.getObj().EMPTY_STRING);


            /**
             * get video thumbs
             */
            new GetThumbNailServerAsync(mActivity, videoPath1, mJCVideoPlayerStandard1, mJCVideoPlayerStandard1.ivThumb).execute();
            new GetThumbNailServerAsync(mActivity, videoPath2, mJCVideoPlayerStandard2, mJCVideoPlayerStandard2.ivThumb).execute();

            mJCVideoPlayerStandard1.setOnJcClickListener(new JCPlayerListener() {
                @Override
                public void onVideoClickListener(JCVideoPlayer jcVideoPlayer) {

                    PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(mActivity);
                    String userId1 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_USER_ID);

                    if (TextUtils.isEmpty(userId1)) {

                        new SingleActionDialog().showDialogForLogin(getResources().getString(R.string.please_login_George),mActivity);

                    } else {

                        String planId = preferenceUserUtil1.getString(PreferenceUserUtil.getInstance(mActivity).KEY_MATH_CLASS);


//                        if (!planId.isEmpty()) {
//                            if(planId.equalsIgnoreCase("0"))
//                            {
//                                new SingleActionDialog().showDialogForSubscription(getResources().getString(R.string.please_login_subscribe_for_George),mActivity);
//                            }else {
                                jcVideoPlayer.startVideo();
//                            }
//                        }else
//                        {
//                            new SingleActionDialog().showDialogForSubscription(getResources().getString(R.string.please_login_subscribe_for_George),mActivity);
//                        }



//                        if(TextUtils.isEmpty(planId) && isSubscribed.equalsIgnoreCase(AppConstant.getInstance().TRUE))
//                        {
//                            new SingleActionDialog().showDialogForSubscription(getResources().getString(R.string.please_login_subscribe_for_George),mActivity);
//                        }else {
//                            jcVideoPlayer.startVideo();
//                        }

                    }

                }
            });

            mJCVideoPlayerStandard2.setOnJcClickListener(new JCPlayerListener() {
                @Override
                public void onVideoClickListener(JCVideoPlayer jcVideoPlayer) {

                    PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(mActivity);
                    String userId1 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_USER_ID);

                    if (TextUtils.isEmpty(userId1)) {

                        new SingleActionDialog().showDialogForLogin(getResources().getString(R.string.please_login_George),mActivity);

                    } else {

                        String planId = preferenceUserUtil1.getString(PreferenceUserUtil.getInstance(mActivity).KEY_PLAN_ID);

//                        if(TextUtils.isEmpty(planId) && isSubscribed.equalsIgnoreCase(AppConstant.getInstance().TRUE)) {
//                            new SingleActionDialog().showDialogForSubscription(getResources().getString(R.string.please_login_subscribe_for_George),mActivity);
//                        }else {
                            jcVideoPlayer.startVideo();
//                        }

                    }

                }
            });


        } else {

            textViewLectureVideo.setVisibility(View.VISIBLE);
            mJCVideoPlayerStandard1.setVisibility(View.GONE);
            textViewPracticeVideo.setVisibility(View.VISIBLE);
            mJCVideoPlayerStandard2.setVisibility(View.GONE);

        }
    }

    /**
     * detect if video already login or not
     */
    private void checkIfUserLoggedIn(JCVideoPlayerStandard mJCVideoPlayerStandard, String isSubscribed) {


        PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(mActivity);
        String userId1 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_USER_ID);

        if (TextUtils.isEmpty(userId1)) {

            new SingleActionDialog().showDialogForLogin(getResources().getString(R.string.please_login_George),mActivity);

        } else {

            String planId = preferenceUserUtil1.getString(PreferenceUserUtil.getInstance(mActivity).KEY_PLAN_ID);

            if(TextUtils.isEmpty(planId) && isSubscribed.equalsIgnoreCase(AppConstant.getObj().TRUE)) {

                new SingleActionDialog().showDialogForSubscription(getResources().getString(R.string.please_login_subscribe_for_George),mActivity);

            }else {

                mJCVideoPlayerStandard.getImageView().performClick();

            }

        }

    }



    /**
     * submit answer finally
     */
    public void onSubmit() {


        PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(mActivity);
        String userId1 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_USER_ID);

        if (TextUtils.isEmpty(userId1)) {

            new SingleActionDialog().showDialogForLogin(getResources().getString(R.string.please_login_George),mActivity);

        } else {

            if (mAnswerAdapter != null && mAnswerAdapter.selectedAnswer!=null && mAnswerAdapter.selectedAnswer.size()>0) {
                //if (correctAnswerList.size() == mAnswerAdapter.selectedAnswer.size()) {
                    for (int i = 0; i < mAnswerAdapter.selectedAnswer.size(); i++) {
                        if (correctAnswerList.contains(mAnswerAdapter.selectedAnswer.get(i))) {
                            isAnswerCorrect = true;
                        } else {
                            isAnswerCorrect = false;
                            break;
                        }
                    }

                text_correct.setVisibility(View.VISIBLE);
                imageViewResult.setVisibility(View.VISIBLE);

                if (isAnswerCorrect) {
                    imageViewResult.setImageResource(R.drawable.ic_correct_ans);
                    text_correct.setText("Congratulations ! \nYour answer is correct.");
                    text_answer.setVisibility(View.GONE);
                    textViewExpDesc.setVisibility(View.GONE);
                } else {
                    imageViewResult.setImageResource(R.drawable.ic_wrong_ans);
                    text_correct.setText("Sorry ! \nYour answer is wrong.\nCorrect answer is : "+correctAnswer);
                    text_answer.setVisibility(View.VISIBLE);
                    textViewExpDesc.setVisibility(View.VISIBLE);

                }
            }else {

                new SingleActionDialog().showDialogNormal(getResources().getString(R.string.please_select_option),mActivity,false);
            }




        }


    }


}
