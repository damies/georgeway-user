package com.george.way.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.george.way.R;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calling.BaseActivity;
import com.george.way.calling.SinchService;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.DeviceInfoUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.validationUtils.ValidationUtils;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.sinch.android.rtc.SinchError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener, SinchService.StartFailedListener {

    public static final int DIRECT_FACEBOOK_LOGIN = 568;
    public static final int DIRECT_GOOGLE_LOGIN = 578;

    private File mSocialDp = null;

    /**
     * declare variables of class
     */
    @BindView(R.id.buttonSignIn)
    Button buttonSignIn;

    @BindView(R.id.textViewRegister)
    TextView textViewRegister;


    @BindView(R.id.inputEdtPassword)
    EditText inputEdtPassword;

    @BindView(R.id.inputEdtEmail)
    EditText inputEdtEmail;

    @BindView(R.id.textViewForgetPwd)
    TextView textViewForgetPwd;

    @BindView(R.id.bottomLinearLayout)
    LinearLayout bottomLinearLayout;

    @BindView(R.id.textViewTitle)
    TextView textViewTitle;

    private Activity mActivity;
    private SimpleDateFormat mFormatter = new SimpleDateFormat("dd,MMM,yyyy, hh:mm aa");
    private SimpleDateFormat mFormatter_ = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    CallbackManager callbackManager;
    private String first_name = "", last_name = "", facebokEmail = "", mfacebookId = "", mGender = "", mDob;
    private LoginButton loginButton;
    private TextView facebook_img;
    private ImageView google_img;
    private static final String TAG = "SignInActivity";
    public static final int RC_SIGN_IN = 9001;

    private GoogleSignInClient mGoogleSignInClient;
    private String imageURL = "";
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_login);
        AppUtils.mGenerateHashKey(LoginActivity.this);
        GWProgressDialog.msGWProgressDialog = null;
        callbackManager = CallbackManager.Factory.create();
        databaseReference = FirebaseDatabase.getInstance().getReference("users");
        /**
         * init
         */
        ButterKnife.bind(this);
        mActivity = this;


        /**
         * set click events on views
         */
        setClickEvents();


        /**
         * set keyboard open listener
         */
        AppUtils.getObj().setKeyboardListener(mActivity, bottomLinearLayout);

        /**
         * open home screen if already login
         */
        openHomeScreenIfAlreadyLogin();


        /**
         * update device token
         */
        try {
            String token = FirebaseInstanceId.getInstance().getToken();
            PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
            preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_DEVICE_TOKEN, token);
            Log.e("tg", "token = " + token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        loginButton = (LoginButton) findViewById(R.id.login_button);
        google_img = (ImageView) findViewById(R.id.google_img);
        facebook_img = (TextView) findViewById(R.id.facebook_img);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        // [END build_client]

        /**
         * setting up font on textview
         */
        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/wolf_rain_font.ttf");
        //textViewTitle.setTypeface(face);

        facebook_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButton.performClick();
            }
        });

        google_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("login", " " + loginResult);

                if (AccessToken.getCurrentAccessToken() != null) {

                    Profile profile = Profile.getCurrentProfile();
                    if (profile != null) {
                        Log.e("facebook_id", profile.getId());
                        Log.e("f_name", profile.getFirstName());
                        Log.e("l_name", profile.getLastName());
                        Log.e("full_name", profile.getName());
                        first_name = profile.getFirstName();
                        last_name = profile.getLastName();
                        mfacebookId = profile.getId();

                    }
                    RequestData();
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.e("facebbok", " " + error.toString());
            }
        });

        int action = 0;
        Bundle b = getIntent().getExtras();
        if (b != null) {
            action = b.getInt("_direct_action");
        }
        if (action == DIRECT_GOOGLE_LOGIN) {
            google_img.performClick();
        } else if (action == DIRECT_FACEBOOK_LOGIN) {
            loginButton.performClick();
        }


    }

    @Override
    protected void onServiceConnected() {
        getSinchServiceInterface().setStartListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStartFailed(SinchError error) {
    }

    @Override
    public void onStarted() {
    }


    /**
     * open home screen if already login
     */
    private void openHomeScreenIfAlreadyLogin() {

        PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
        String userId = msPreferenceUserUtil.getString(msPreferenceUserUtil.KEY_USER_ID);
        if (!TextUtils.isEmpty(userId)) {
            startHomeScreen();
        }


    }

    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                JSONObject json = response.getJSONObject();
                System.out.println("Json data :" + json);
                try {
                    if (json != null) {
//                        details_txt.setText(Html.fromHtml(text));
//                        profile.setProfileId(json.getString("id"));
                        Log.e("data ", " " + json);
                        String rawName = json.optString("name");
                        if (rawName!=null && !rawName.isEmpty()){
                            String[] names = rawName.split("\\s+");
                            if (names.length>0){
                                first_name = names[0];
                            }
                            if (names.length>1){
                                last_name = names[1];
                            }
                        }
                        facebokEmail = json.optString("email");
                        mGender = json.optString("gender");
                        imageURL = "http://graph.facebook.com/" + json.getString("id") + "/picture?type=large";
                        downloadImage(imageURL);
//                        mDob = mConvertDate(json.optString("birthday"));
                        if (NetworkUtil.isConnected(mActivity)) {
                            //   mFacebooklogin(json.getString("id"),"fb",facebokEmail);
                            callSocialLoginWebApi(json.getString("id"), "fb");
                        } else {
                            Snackbar.make(loginButton, "No, Internet Connection, Please try again.", Snackbar.LENGTH_INDEFINITE).setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            }).show();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,birthday,gender,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    /**
     * set click events on views
     */
    private void setClickEvents() {

        buttonSignIn.setOnClickListener(this);
        textViewRegister.setOnClickListener(this);
        textViewForgetPwd.setOnClickListener(this);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    // [START handleSignInResult]
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            facebokEmail = account.getEmail();
            mGender = "";
            String fullname = account.getDisplayName();
            imageURL = "";

            String[] parts = fullname.split(" ");
            Log.d("Length-->", "" + parts.length);
            if (parts.length == 2) {
                first_name = parts[0];
                last_name = parts[1];
                Log.d("First-->", "" + first_name);
                Log.d("Last-->", "" + last_name);
            } else if (parts.length == 3) {
                first_name = parts[0];
                String middlename = parts[1];
                last_name = parts[2];
            }
            if (NetworkUtil.isConnected(mActivity)) {
                callSocialLoginWebApi(account.getId(), "google");
//                mFacebooklogin(account.getId(),"google",facebokEmail);
            } else {
                Snackbar.make(loginButton, "No, Internet Connection, Please try again.", Snackbar.LENGTH_INDEFINITE).setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
            }
            // Signed in successfully, show authenticated UI.
//            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.buttonSignIn:

                AppUtils.getObj().hideSoftKeyboard(mActivity);

                if (ValidationUtils.getLoginObj(mActivity).validateLoginForm(inputEdtEmail, inputEdtPassword)) {

                    String email = inputEdtEmail.getText().toString();
                    String password = inputEdtPassword.getText().toString();
                    callLoginWebApi(email, password);

                }

                break;

            case R.id.textViewRegister:

                Intent intentSignUp = new Intent(mActivity, RegistrationActivity.class);
                intentSignUp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentSignUp);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;

            case R.id.textViewForgetPwd:

                /**
                 * show forget password dialog
                 */
                forgetPasswordDialog();

                break;

        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(mActivity);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //closeRevealActivity();
            finish();
            ViewAnimUtils.activityExitTransitions(mActivity);
        } else {
            super.onBackPressed();
        }*/
    }

    /**
     * open forget password dialog for sending password reset link
     */
    private EditText inputEdtEmailForgetPwd;
    private Button buttonSubmit;

    private void forgetPasswordDialog() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(LoginActivity.this);
        final View promptsView = li.inflate(R.layout.layout_dialog_forget_password, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        alertDialogBuilder.setView(promptsView);

        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();

        /**
         * setting up window design
         */
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window mWindow = alertDialog.getWindow();
        mWindow.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mWindow.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mWindow.setDimAmount(0.6f);
        mWindow.setGravity(Gravity.CENTER);

        // show it
        alertDialog.show();


        inputEdtEmailForgetPwd = (EditText) promptsView.findViewById(R.id.inputEdtEmail);
        buttonSubmit = (Button) promptsView.findViewById(R.id.buttonSubmit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppUtils.getObj().hideSoftKeyboardDialog(promptsView);

                if (ValidationUtils.getLoginObj(mActivity).validateForgetPwdForm(inputEdtEmailForgetPwd)) {
                    alertDialog.dismiss();
                    String username = inputEdtEmailForgetPwd.getText().toString();
                    callForgetPwdWebApi(username);

                }
            }
        });
    }

    /**
     * call login api
     *
     * @param mUserName
     * @param mPassword
     */
    public void callLoginWebApi(String mUserName, String mPassword) {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                String device_token = preferenceUserUtil.getString(preferenceUserUtil.KEY_DEVICE_TOKEN);
                Log.e("tg", "token = " + device_token);

                String device_type = DeviceInfoUtils.getObj().getDeviceType();
                String device_id = DeviceInfoUtils.getObj().getDeviceId(mActivity);


                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.loginRequest(
                        mUserName,
                        mPassword,
                        device_token,
                        device_type,
                        device_id);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");


                            if (!TextUtils.isEmpty(status_code)) {

                                if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {

                                    String success_message = mJsonObj.optString("success_message");
                                    //  Toast.makeText(mActivity, getResources().getString(R.string.login_success), Toast.LENGTH_SHORT).show();

                                    /**
                                     * set user data in preference
                                     */
                                    setUserDataInPreference(mJsonObj);

                                    /**
                                     * start home screen
                                     */
                                    startHomeScreen();


                                } else {

                                    String error_message = mJsonObj.optString(AppConstant.getObj().ERROR_MESSAGE);
                                    new SingleActionDialog().showDialogNormal(error_message, mActivity, false);

                                }
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack1);
        }
    }

    // [START signOut]
    private void signOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                // [START_EXCLUDE]
                // [END_EXCLUDE]
            }
        });
    }

    private void downloadImage(String url) {
        ImageLoader.getInstance().loadImage(url, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                String imaegNaem = imageUri.substring(imageUri.lastIndexOf("/") + 1);
                try {
                    mSocialDp = new File(getCacheDir(), imaegNaem);

                    if (mSocialDp.createNewFile()) {

                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        //bitmap.compress(CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                        byte[] bitmapdata = bos.toByteArray();

                        FileOutputStream fos = new FileOutputStream(mSocialDp);
                        fos.write(bitmapdata);
                        fos.flush();
                        fos.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * call Social login api
     *
     * @param socialType
     * @param socialId
     */
    public void callSocialLoginWebApi(final String socialId, final String socialType) {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);
            LoginManager.getInstance().logOut();
            signOut();
            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                String device_token = preferenceUserUtil.getString(preferenceUserUtil.KEY_DEVICE_TOKEN);

                if (device_token.isEmpty()) {
                    Toast.makeText(mActivity, "Please restart app or try after a while", Toast.LENGTH_SHORT).show();
                    GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    return;
                }
                Log.e("tg", "token = " + device_token);

                String device_type = "android";

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.socialloginRequest(facebokEmail,
                        socialId,
                        socialType,
                        device_token,
                        device_type,
                        Build.DEVICE);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");


                            if (!TextUtils.isEmpty(status_code)) {

                                if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {

                                    setUserDataInPreference(mJsonObj);
                                    GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                                    startHomeScreen();

                                } else if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_Error)) {

                                    String address="",age="";

                                    callRegisterSocialWebApi(
                                            first_name,
                                            last_name,
                                            facebokEmail,
                                            address,
                                            age,
                                            device_type,
                                            device_token,
                                            socialType,
                                            socialId
                                    );
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack1);
        }
    }

    public void callRegisterSocialWebApi(String first_name,
                                         String last_name,
                                         String email,
                                         String address,
                                         String age,
                                         String device_type,
                                         String device_token,
                                         String socialType,
                                         String socialId) {

        if (NetworkUtil.isConnected(mActivity)) {

            AppUtils.getObj().hideSoftKeyboard(mActivity);
            try {

                /**
                 * show if progress dialog is not showing
                 */
                //GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                try {
                    MultipartBody.Part bodyFile=null;
                    if (mSocialDp != null) {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), mSocialDp);
                        bodyFile = MultipartBody.Part.createFormData("image", mSocialDp.getName(), requestFile);

                    } else {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                        bodyFile = MultipartBody.Part.createFormData("image", "", requestFile);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);

                Call<ResponseBody> call = mApiInterface.socialregisterUserRequest(
                        RequestBody.create(MediaType.parse("text/plain"), first_name),
                        RequestBody.create(MediaType.parse("text/plain"), last_name),
                        RequestBody.create(MediaType.parse("text/plain"), email),
                        RequestBody.create(MediaType.parse("text/plain"), address),
                        RequestBody.create(MediaType.parse("text/plain"), age),
                        /*RequestBody.create(MediaType.parse("text/plain"), username),*/
                        RequestBody.create(MediaType.parse("text/plain"), device_type),
                        RequestBody.create(MediaType.parse("text/plain"), device_token),
                        RequestBody.create(MediaType.parse("text/plain"), socialType),
                        RequestBody.create(MediaType.parse("text/plain"), socialId)
                );

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            //GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                            PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");

                            if (status_code.equalsIgnoreCase("201") || status_code.equalsIgnoreCase("200")) {
                                if (!TextUtils.isEmpty(status_code)) {

                                    if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS) || status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_CHANGED)) {

                                        callSocialLoginWebApi(socialType,socialId);


                                    } else {
                                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                                        String error_message = mJsonObj.optString(AppConstant.getObj().ERROR_MESSAGE);
                                        new SingleActionDialog().showDialogNormal(error_message, mActivity, false);

                                    }
                                }
                            }else{
                                GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                            }


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                        // SingleActionDialog.getDialogObj(mActivity).showDialogNormal("Link send on your email to verify email.", mActivity);

                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack1);
        }
    }

    DialogInterface.OnClickListener networkCallBack1 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            String email = inputEdtEmail.getText().toString();
            String password = inputEdtPassword.getText().toString();
            callLoginWebApi(email, password);

        }
    };

    /**
     * setting up data in preference
     *
     * @param mJSONObject
     */
    private void setUserDataInPreference(JSONObject mJSONObject) {

        PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
        /**
         * in case of login we will get a single json object in data array for user information
         * so get it by 0 index
         */
        try {

            JSONArray data = mJSONObject.getJSONArray("data");
            String userid = "";
            if (data != null) {
                JSONObject dataJsonObj = data.getJSONObject(0);
                AppUtils.jsonToMap(dataJsonObj.toString());
                if (dataJsonObj.has("user_id")) {
                    databaseReference.child(dataJsonObj.optString("user_id")).child("details").setValue(AppUtils.jsonToMap(dataJsonObj.toString()));

                    databaseReference.child(dataJsonObj.optString("user_id") + "/is_online/").setValue("true");

                    getSinchServiceInterface().startClient("U_" + dataJsonObj.optString("user_id"));


                    userid = dataJsonObj.optString("user_id");

                } else {
                    databaseReference.child(dataJsonObj.optString("userid")).child("details").setValue(AppUtils.jsonToMap(dataJsonObj.toString()));

                    databaseReference.child(dataJsonObj.optString("userid") + "/is_online/").setValue("true");

                    getSinchServiceInterface().startClient("U_" + dataJsonObj.optString("userid"));


                    userid = dataJsonObj.optString("userid");

                }


                String first_name = dataJsonObj.optString("first_name");
                String last_name = dataJsonObj.optString("last_name");
                String address = dataJsonObj.optString("address");
                String username = dataJsonObj.optString("username");
                String email = dataJsonObj.optString("email");
                String subscription_plan = dataJsonObj.optString("subscription_plan");
                String image = dataJsonObj.optString("image");
                String social_type = dataJsonObj.optString("social_type");

                String password = dataJsonObj.optString("password");
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_PASSWORD, password);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_ID, userid);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_FIRST_NAME, first_name);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_LAST_NAME, last_name);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_ADDRESS, address);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_NAME, username);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_EMAIL, email);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_SOCIAL, social_type);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_SUBSCRIPTION_PLAN, subscription_plan);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_IMAGE, image);


                if (dataJsonObj.has("subscription_start")) {
                    String start_date = dataJsonObj.optString("subscription_start");
                    Date strt_date = mFormatter_.parse(start_date);

                    String end_date = dataJsonObj.optString("subscription_end");
                    Date endd_date = mFormatter_.parse(end_date);

                    msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.subscription_start, mFormatter.format(strt_date));
                    msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.subscription_end, mFormatter.format(endd_date));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * if everything is fine then open home screen
     */

    private boolean isFromLoginBtn = false;

    private void startHomeScreen() {

        PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);

        try {
            isFromLoginBtn = preferenceUserUtil.getPreferenceBooleanData(preferenceUserUtil.KEY_RE_LOGIN);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isFromLoginBtn) {
            /**
             * finish current activity
             */
            finish();
            ViewAnimUtils.activityEnterTransitions(this);

        } else {

            /**
             * finish current activity
             */
            finish();

            /**
             * after the Login successfully start new DashBoard activity
             * first of all finish current activity , then start new activity
             */
            Intent mIntent = new Intent(mActivity, GeorgeHomeActivity.class);
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mIntent);
            ViewAnimUtils.activityEnterTransitions(this);

            preferenceUserUtil.setPreferenceBooleanData(preferenceUserUtil.KEY_RE_LOGIN, false);

        }


    }

    /**
     * call api for forget password
     *
     * @param mEmail
     */
    public void callForgetPwdWebApi(String mEmail) {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.forgetPwdRequest(
                        mEmail);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");


                            if (!TextUtils.isEmpty(status_code)) {

                                if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {

                                    String success_message = mJsonObj.optString("success_message");
                                    new SingleActionDialog().showDialogNormal(success_message, mActivity, false);

                                } else {

                                    String error_message = mJsonObj.optString(AppConstant.getObj().ERROR_MESSAGE);
                                    new SingleActionDialog().showDialogNormal(error_message, mActivity, false);

                                }
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }
    }

    DialogInterface.OnClickListener networkCallBack2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            String username = inputEdtEmailForgetPwd.getText().toString();
            callForgetPwdWebApi(username);


        }
    };


}