package com.george.way.activity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.adapter.ChatAdapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.chatDatabase.Database;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.dialog.GeorgeWayImagePicker;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.easyImagePick.DefaultCallback;
import com.george.way.easyImagePick.GeorgeWayImagePick;
import com.george.way.model.FetchChatResponse;
import com.george.way.model.chat.MsgBean;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.GeorgeWayApplication;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hemendrag on 5/4/2017.
 */

public class OnlineTutoringActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView userChatRv;
    private LinearLayout llBottom;
    private EditText msgEt;
    private ImageButton sendImageBtn;
    private ImageButton sendBtn;
    ImageView imageViewBack, imageViewEmail;
    TextView textViewHeader;
    Activity mActivity;
    ArrayList<MsgBean> msgBeanArrayList = new ArrayList<>();
    ChatAdapter chatAdapter;
    Database mDatabase;
    PreferenceUserUtil preferenceUserUtil;
    FetchChatResponse fetchChatResponse;
    List<FetchChatResponse.Datum> datumList = new ArrayList<>();
    int PICK_IMAGE_REQUEST_CAMERA = 111;
    int PICK_IMAGE_REQUEST_GALLERY = 222;
    private String imagePath;
    private String imageName;
    private File imageFileToSend;
    public static int IMAGE_MAX_SIZE = 1024;
    @BindView(R.id.app_bar)
    Toolbar app_bar;
    TextView textViewMore;

    private final long FIVE_SECONDS = 5000;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;
    private Handler handler;

    private int listSizeCount = 0;
    private BottomSheetDialog bottomSheetDialog;
    private TextView mailButton, calculatorButton, editButton, cancelButton;
    private static long totalSec = 0;
    private CountDownTimer countDownTimer = null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_online);

        initView();

        /**
         * init
         */
        ButterKnife.bind(this);

        fetchCurrentPlane();

    }

    private void initView() {



        mActivity = this;
        preferenceUserUtil = PreferenceUserUtil.getInstance(OnlineTutoringActivity.this);
        userChatRv = (RecyclerView) findViewById(R.id.user_chat_rv);
        msgEt = (EditText) findViewById(R.id.msg_et);
        sendImageBtn = (ImageButton) findViewById(R.id.send_image_btn);
        sendBtn = (ImageButton) findViewById(R.id.send_btn);
        imageViewBack = (ImageView) findViewById(R.id.imageViewBack);
        textViewHeader = (TextView) findViewById(R.id.textViewHeader);
        userChatRv.setLayoutManager(new LinearLayoutManager(mActivity));
        imageViewEmail = (ImageView) findViewById(R.id.imageViewEmail);
        textViewMore = (TextView) findViewById(R.id.textViewMore);
        bottomSheetDialog = new BottomSheetDialog(OnlineTutoringActivity.this);
        bottomSheetDialog.setContentView(R.layout.chat_more_option);
        cancelButton = (TextView) bottomSheetDialog.findViewById(R.id.cancelButton);
        mailButton = (TextView) bottomSheetDialog.findViewById(R.id.mailButton);
        calculatorButton = (TextView) bottomSheetDialog.findViewById(R.id.calculatorButton);
        editButton = (TextView) bottomSheetDialog.findViewById(R.id.editButton);

        textViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textViewMore.getText().toString().equalsIgnoreCase("More")) {
                    bottomSheetDialog.show();
                } else {
                    new SingleActionDialog().showDialogForChatDelete("Are you sure, you want to delete chat message?", mActivity);
                }
            }
        });


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        mailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();

                PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(mActivity);
                admin_email1 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_1);
                admin_email2 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_2);
                admin_email3 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_3);
                admin_email4 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_4);
                admin_email5 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_5);


                /**
                 * open email
                 */
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{
                        admin_email1,
                        admin_email2,
                        admin_email3,
                        admin_email4,
                        admin_email5});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contact GeorgeWay");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Hey i want to connect to GeorgeWay.");
                try {
                    startActivity(Intent.createChooser(emailIntent, "Send Mail"));
                } catch (android.content.ActivityNotFoundException ex) {
                }
            }
        });

        calculatorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();

                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(OnlineTutoringActivity.this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(OnlineTutoringActivity.this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                textViewMore.setText("Delete");
                chatAdapter.setCheckBox(true);
            }
        });

        PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(this);
        String text = preferenceUserUtil.getString(preferenceUserUtil.KEY_OTS_CONTENT);
        if (TextUtils.isEmpty(text)) {
            textViewHeader.setText(getResources().getString(R.string.Online_tutoring_Service));
        } else {
            textViewHeader.setText(text);
        }

        /*set click listeners*/
        setClickListener();

        //open Database;
        accessDataBase(true);

        handler = new Handler();

    }


    public void getDataFromApi() {
        handler.postDelayed(upDater, FIVE_SECONDS);
    }

    public Runnable upDater = new Runnable() {

        @Override
        public void run() {

            //open Database;
            accessDataBase(false);

            handler.postDelayed(this, FIVE_SECONDS);

        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * update theme
         */
        /*try {
            ThemeUtils.getObj(mActivity).setUpTheme(app_bar, new ImageView[]{imageViewBack, imageViewCalculator}, textViewHeader);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        /**
         * fetch data in every 5 seconds
         */
        getDataFromApi();


    }

    private void accessDataBase(boolean isShowDialog) {
        mDatabase = new Database(mActivity);
        mDatabase.open();
        //getChatHistory();
        fetchChatData(isShowDialog);
    }

    private void getChatHistory() {
        msgBeanArrayList = mDatabase.getMessages();
        setAdapter(true);
    }

    public void setClickListener() {

        sendImageBtn.setOnClickListener(this);
        sendBtn.setOnClickListener(this);
        imageViewBack.setOnClickListener(this);
        imageViewEmail.setOnClickListener(this);

    }

    String admin_email1 = AppConstant.getObj().EMPTY_STRING,
            admin_email2 = AppConstant.getObj().EMPTY_STRING,
            admin_email3 = AppConstant.getObj().EMPTY_STRING,
            admin_email4 = AppConstant.getObj().EMPTY_STRING,
            admin_email5 = AppConstant.getObj().EMPTY_STRING;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imageViewEmail:

                PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(mActivity);
                admin_email1 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_1);
                admin_email2 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_2);
                admin_email3 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_3);
                admin_email4 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_4);
                admin_email5 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_ADMIN_EMAIL_5);


                /**
                 * open email
                 */
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{
                        admin_email1,
                        admin_email2,
                        admin_email3,
                        admin_email4,
                        admin_email5});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contact GeorgeWay");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Hey i want to connect to GeorgeWay.");
                try {
                    startActivity(Intent.createChooser(emailIntent, "Send Mail"));
                } catch (android.content.ActivityNotFoundException ex) {
                }


                break;

            case R.id.imageViewBack:
               onBackPressed();
                break;

            case R.id.send_btn:
                String msg = msgEt.getText().toString().trim();
                if (msg.length() > 0) {
                    sendChatData(msg);
                }
                break;

            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;
            case R.id.send_image_btn:
                showImageDialog();
                break;

        }
    }

    public void showImageDialog() {

        /**
         * select contractFilePDF/image/camera
         */
        GeorgeWayImagePicker.getImagePickObj(this).openConfirmationDialog();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (requestCode == GeorgeWayImagePicker.getImagePickObj(this).PICK_IMAGE_REQUEST) {

            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                /**
                 * Call the below method for uploading image
                 */
                GeorgeWayImagePick.openChooserWithGallery(mActivity, GeorgeWayImagePicker.getImagePickObj(this).CHOOSE_IMAGE, GeorgeWayImagePicker.getImagePickObj(this).PICK_IMAGE_REQUEST);


            }
        } else if (requestCode == GeorgeWayImagePicker.getImagePickObj(this).REQUEST_IMAGE_CAPTURE) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                /**
                 * Call the below method for uploading image
                 */
                GeorgeWayImagePick.openCamera(mActivity, GeorgeWayImagePicker.getImagePickObj(this).REQUEST_IMAGE_CAPTURE);

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        GeorgeWayImagePick.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, GeorgeWayImagePick.ImageSource source, int type) {
                e.printStackTrace();
            }

            @Override
            public void onImagesCaptured(@NonNull List<Uri> imageUriList, GeorgeWayImagePick.ImageSource source, int type) {


                /**
                 * set image from gallery/camera into image views
                 */
                if (imageUriList != null && imageUriList.size() > 0) {
                    imagePath = GeorgeWayImagePick.getRealPathFromURI(mActivity, imageUriList.get(0));
                    imageName = new File(imageUriList.get(0).getPath()).getName();
                    sendChatData(imagePath);
                }

            }

            @Override
            public void onImagesPicked(List<File> imageFiles, GeorgeWayImagePick.ImageSource source, int type) {

                /**
                 * set image from gallery/camera into image views
                 */
                if (imageFiles != null && imageFiles.size() > 0) {

                    imagePath = imageFiles.get(0).getAbsolutePath();
                    imageName = imageFiles.get(0).getName();
                    sendChatData(imagePath);

                }

            }

            @Override
            public void onFilePicked(@NonNull Uri imageFileUri, GeorgeWayImagePick.ImageSource source, int type) {
                /**
                 * set image from gallery/camera into image views
                 */
                if (imageFileUri != null) {
                    imagePath = GeorgeWayImagePick.getRealPathFromURI(mActivity, imageFileUri);
                    imageName = new File(imageFileUri.getPath()).getName();
                    sendChatData(imagePath);


                }

            }

            @Override
            public void onCanceled(GeorgeWayImagePick.ImageSource source, int type) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == GeorgeWayImagePick.ImageSource.CAMERA) {
                    File photoFile = GeorgeWayImagePick.lastlyTakenButCanceledPhoto(mActivity);
                    if (photoFile != null) photoFile.delete();
                }
            }
        });
    }

    /**
     * call api for send message
     */
    private MultipartBody.Part bodyFile;

    public void sendChatData(String msg) {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);
            Call<ResponseBody> call = null;
            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                MultipartBody.Part feed_photo1 = null;
                String imageencodepath = null;
                try {
                    imageFileToSend = new File(imagePath);
                    //  imageencodepath = AppUtils.getInstance().encodeBase64(this,getImageUri(imagePath));
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (imageFileToSend != null) {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFileToSend);
                        bodyFile = MultipartBody.Part.createFormData("image", imageFileToSend.getName(), requestFile);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (!TextUtils.isEmpty(imagePath)) {
                    call = mApiInterface.saveChat_Image(RequestBody.create(MediaType.parse("text/plain"), preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID)),
                            bodyFile);
                } else {

                    //msg = msg.replaceAll("\\s+", "%20");
                    call = mApiInterface.saveChat(preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID) + "", msg);
                }

                final String finalMsg = msg;
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            if (response.code() == 200) {
                                imagePath = "";
                                GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                                Calendar calendar = Calendar.getInstance();

                                MsgBean msgBean = new MsgBean();
                                msgBean.setChatType("message");
                                msgBean.setMsgID(calendar.getTimeInMillis() + "");
                                msgBean.setDateTime(calendar.getTimeInMillis() + "");
                               /* if(!imagePath.equalsIgnoreCase(""))
                                msgBean.setMessage(msg);*/
                                msgBean.setImage(finalMsg);
                                msgBean.setSenderMemberId(preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID));
                                msgBean.setAdminId("admin");
                                msgBeanArrayList.add(msgBean);
                                msgEt.setText("");
                                mDatabase.insertUserChatDataTable(msgBean);
                                fetchChatData(true);
                            } else {
                                GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                            }

                        } catch (Exception ex) {
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }
    }

    private void setAdapter(boolean isShowDialog) {

        if (chatAdapter == null) {
            chatAdapter = new ChatAdapter(this, datumList);
            userChatRv.setAdapter(chatAdapter);

        } else {

            chatAdapter.setMessageList(datumList);

        }

        chatAdapter.notifyDataSetChanged();


        if (listSizeCount < datumList.size()) {
            try {

                listSizeCount = datumList.size();

                userChatRv.post(new Runnable() {
                    @Override
                    public void run() {
                        userChatRv.smoothScrollToPosition(chatAdapter.getItemCount() - 1);
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public static String getProfileImage(Bitmap bitmap) {


        byte[] byteArrayImage = null;

        try {

            // Bitmap bitmap = ((BitmapDrawable)imgViewProfileBg.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byteArrayImage = baos.toByteArray();
        } catch (Exception e) {

            Log.e("", e.toString());
            e.printStackTrace();

        }

        String imageEncoded = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
        return imageEncoded;

    }

    DialogInterface.OnClickListener networkCallBack2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
        }
    };

    public static Bitmap getBitmap(Uri uri, Context mContext) {
        InputStream in = null;
        Bitmap returnedBitmap = null;
        ContentResolver mContentResolver;
        try {
            mContentResolver = mContext.getContentResolver();
//            file:///storage/emulated/0/Pictures/aviary-image-1465806238409.jpeg
           /* if(uri.getPath().contains("file:///")) {*/
            in = mContentResolver.openInputStream(uri);
           /* }
            else{
                String file_append="file:///";
                in = mContentResolver.openInputStream(Uri.parse(file_append+uri));
            }*/
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();
            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            /*if(uri.getPath().contains("file:///")) {*/
            in = mContentResolver.openInputStream(uri);
           /* }
            else{
                String file_append="file:///";
                in = mContentResolver.openInputStream(Uri.parse(file_append+uri));
//                in = mContentResolver.openInputStream(Uri.parse(file_append+uri));
            }
*/
            Bitmap bitmap = BitmapFactory.decodeStream(in, null, o2);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            in.close();

            //First check
            ExifInterface ei = new ExifInterface(uri.getPath());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    returnedBitmap = rotateImage(bitmap, 90);
                    //Free up the memory
                    bitmap.recycle();
                    bitmap = null;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    returnedBitmap = rotateImage(bitmap, 180);
                    //Free up the memory
                    bitmap.recycle();
                    bitmap = null;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    returnedBitmap = rotateImage(bitmap, 270);
                    //Free up the memory
                    bitmap.recycle();
                    bitmap = null;
                    break;
                default:
                    returnedBitmap = bitmap;
            }
            return returnedBitmap;
        } catch (FileNotFoundException e) {
            //  L.e(e);
        } catch (IOException e) {
            //L.e(e);
        }
        return null;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static Uri getImageUri(String path) {
        try {
            return Uri.fromFile(new File(path));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

    public void updateMoreUi(Boolean aBoolean) {
        textViewMore.setText("More");
        fetchChatData(aBoolean);
    }

    /**
     * call api for fetch message
     */
    public void fetchChatData(final boolean isShowDialog) {

        if (NetworkUtil.isConnected(mActivity)) {


            try {

                /**
                 * show if progress dialog is not showing
                 */
                if (isShowDialog) {
                    GWProgressDialog.getProgressDialog(mActivity).showDialog1();
                    /**
                     * hide keyboard if open
                     */
                    AppUtils.getObj().hideSoftKeyboard(mActivity);

                }
                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<FetchChatResponse> call = mApiInterface.fetchUserChat(preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID));

                call.enqueue(new Callback<FetchChatResponse>() {
                    @Override
                    public void onResponse(Call<FetchChatResponse> call, Response<FetchChatResponse> response) {

                        if (isShowDialog) {
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                        }

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            if (response.code() == 200) {
                                fetchChatResponse = response.body();
                                datumList = fetchChatResponse.getData();
                                if (datumList != null)
                                    setAdapter(isShowDialog);
                            } else {

                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<FetchChatResponse> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }
    }

    /**
     * call api for fetch message
     */
    public void fetchCurrentPlane() {

        if (NetworkUtil.isConnected(mActivity)) {


            try {

                /**
                 * show if progress dialog is not showing
                 */
//                    GWProgressDialog.getProgressDialog(mActivity).showDialog1();
                /**
                 * hide keyboard if open
                 */
                AppUtils.getObj().hideSoftKeyboard(mActivity);

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.currentPlan(preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID));

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                        try {
                            /**
                             * dismiss dialog if open
                             */

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");
                            JSONArray data = mJsonObj.optJSONArray("data");
                            JSONObject dataJsonObj = data.getJSONObject(0);

                            if (dataJsonObj.has("subscription_time"))
                            {
                                if (dataJsonObj.optString("subscription_time").equalsIgnoreCase("0"))
                                {
                                    new SingleActionDialog().showDialogForSubscription(getResources().getString(R.string.Please_subscribe_Online_Tutoring), OnlineTutoringActivity.this);
                                }else
                                {
                                    getPlanDurrention(dataJsonObj.optString("subscription_time"));
                                }

                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
//                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (countDownTimer!=null)
        {
            countDownTimer.cancel();
            countDownTimer =null;
        }
        updateTimeCurrentPlane();
        finish();
        ViewAnimUtils.activityExitTransitions(this);
        GeorgeWayApplication.msg_id.clear();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(upDater);
    }

    private void getPlanDurrention(String endDate)
    {
        try {
            final long diff =Long.parseLong(endDate)*1000;


           countDownTimer =  new CountDownTimer(diff, 1000)
            {
                @Override
                public void onTick(long millisUntilFinished) {

                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = time / (60 * 60 * 1000);
                    totalSec = seconds;

                    long tt = diff-millisUntilFinished;
                    totalSec = tt/1000;

                    Log.e("total "," time "+tt/1000);

                    textViewHeader.setText(twoDigitString(diffHours)+"h:"+twoDigitString(minutes)  + "m:"+ twoDigitString(seconds)+"s");

                    if (twoDigitString(diffHours).equalsIgnoreCase("00")&&twoDigitString(minutes).equalsIgnoreCase("00")&&twoDigitString(seconds).equalsIgnoreCase("00"))
                    {
                        finish();
                        ViewAnimUtils.activityExitTransitions(OnlineTutoringActivity.this);
                    }
                }
                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }
                @Override
                public void onFinish() {
                    textViewHeader.setText("00h:00m:00s");
                    totalSec = diff/1000;
                    updateTimeCurrentPlane();
                    finish();
                    ViewAnimUtils.activityExitTransitions(OnlineTutoringActivity.this);

                }
            };
           countDownTimer.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * call api for update plan time
     */
    public void updateTimeCurrentPlane() {

        if (NetworkUtil.isConnected(mActivity)) {


            try {

                /**
                 * show if progress dialog is not showing
                 */
//                    GWProgressDialog.getProgressDialog(mActivity).showDialog1();
                /**
                 * hide keyboard if open
                 */
                AppUtils.getObj().hideSoftKeyboard(mActivity);

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.updateSubscribTime(preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID),""+totalSec,"");

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                        try {
                            /**
                             * dismiss dialog if open
                             */

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");
                            JSONArray data = mJsonObj.optJSONArray("data");
                            JSONObject dataJsonObj = data.getJSONObject(0);


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
//                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }
    }


}
