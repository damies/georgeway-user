package com.george.way.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.adapter.QODRewardAdapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.model.reward.Datum;
import com.george.way.model.reward.RewardDataModel;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QODRewardActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.rv_reward)
    RecyclerView mRecyclerReward;

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;

    @BindView(R.id.textViewHeader)
    TextView textViewHeader;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;


    @BindView(R.id.textViewNoFeedback)
    TextView textViewNoFeedback;

    private Activity mActivity;
    private QODRewardAdapter mQODRewardAdapter;
    private ArrayList<Datum> rewardList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_qod_reward);

        /**
         * bind views of xml
         */
        ButterKnife.bind(this);

        /**
         * init
         */
        initView();

        /**
         * setting up onclick events
         */
        setOnClickListener();

        /**
         * get list of reward list if winners from server
         */
         getRewardApiRequest();

    }

    private void initView() {

        mActivity = this;
        rewardList = new ArrayList<>();

    }


    /**
     * set on click event
     */
    public void setOnClickListener() {

        imageViewBack.setOnClickListener(this);
        imageViewCalculator.setOnClickListener(this);

    }

    /**
     * setting up success story adapter
     */
    private void settingUpAdapter() {

        if (rewardList.size() == 0) {
            textViewNoFeedback.setVisibility(View.VISIBLE);
            mRecyclerReward.setVisibility(View.GONE);
        } else {
            mRecyclerReward.setVisibility(View.VISIBLE);
            textViewNoFeedback.setVisibility(View.GONE);
        }

        mQODRewardAdapter = new QODRewardAdapter(mActivity, rewardList);
        mRecyclerReward.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerReward.setAdapter(mQODRewardAdapter);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack:
                finish();
                ViewAnimUtils.activityExitTransitions(this);
                break;
            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(this);
    }

    /**
     * get list of reward list if winners from server
     */
    public void getRewardApiRequest() {


        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                String month = AppConstant.getObj().EMPTY_STRING;
                String year = AppConstant.getObj().EMPTY_STRING;

                try {
                    Calendar calendar = Calendar.getInstance();

                    int currentMonth = calendar.get(Calendar.MONTH);
                    currentMonth = currentMonth+1;
                    if (currentMonth < 10) {
                        month = "0" + currentMonth;
                    } else {
                        month = AppConstant.getObj().EMPTY_STRING + currentMonth;
                    }
                    int currentYear = calendar.get(Calendar.YEAR);
                    year = AppConstant.getObj().EMPTY_STRING + currentYear;

                }catch (Exception ex){
                    ex.printStackTrace();
                }

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<RewardDataModel> call = mApiInterface.fetchRewardApiRequest(month,
                        year);

                call.enqueue(new Callback<RewardDataModel>() {
                    @Override
                    public void onResponse(Call<RewardDataModel> call, Response<RewardDataModel> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            RewardDataModel mRewardDataModel = response.body();

                            rewardList.clear();
                            if (mRewardDataModel != null && mRewardDataModel.getData() != null && mRewardDataModel.getData().size() > 0) {

                                for (int index = 0; index < mRewardDataModel.getData().size(); index++) {

                                    rewardList.add(mRewardDataModel.getData().get(index));

                                }
                                /**
                                 * setting up success story adapter
                                 */
                                settingUpAdapter();


                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<RewardDataModel> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(getApplicationContext()).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }

    }


    DialogInterface.OnClickListener networkCallBack2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            getRewardApiRequest();
        }
    };

}
