package com.george.way.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

import com.george.way.R;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RatingActivity extends AppCompatActivity {


    @BindView(R.id.rating_bar)
    AppCompatRatingBar ratingBar;
    @BindView(R.id.btn_submit_rating)
    Button btnSubmitRating;

    PreferenceUserUtil mPrefs;
    String userId;
    String tutorId;

    float rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        ButterKnife.bind(this);

        Bundle b = getIntent().getExtras();

        if (b!=null){
            userId = b.getString("_user_id");
            tutorId = b.getString("_tutor_id");
        }

        mPrefs = PreferenceUserUtil.getInstance(this);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                RatingActivity.this.rating = rating;
            }
        });

    }

    @OnClick(R.id.btn_submit_rating)
    public void onViewClicked() {

        if (rating==0){
            Toast.makeText(this, "Please select any star", Toast.LENGTH_SHORT).show();
            return;
        }

        if (userId.equals("0") || tutorId.equals("0")){
            finish();
        }

        ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
        retrofit2.Call<ResponseBody> call = mApiInterface.sendRating(userId,tutorId,String.format(Locale.getDefault(),"%.1f",rating));
        GWProgressDialog.getProgressDialog(this).showDialog1();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                GWProgressDialog.getProgressDialog(RatingActivity.this).dismissDialog1();
                finish();
                ViewAnimUtils.activityExitTransitions(RatingActivity.this);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                GWProgressDialog.getProgressDialog(RatingActivity.this).dismissDialog1();
                finish();
                ViewAnimUtils.activityExitTransitions(RatingActivity.this);
            }
        });


    }
}
