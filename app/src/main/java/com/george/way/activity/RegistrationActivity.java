package com.george.way.activity;

import android.animation.Animator;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.george.way.R;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calling.BaseActivity;
import com.george.way.calling.SinchService;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.dialog.GeorgeWayImagePicker;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.easyImagePick.DefaultCallback;
import com.george.way.easyImagePick.GeorgeWayImagePick;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.DeviceInfoUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.utils.ViewUtils;
import com.george.way.validationUtils.ValidationUtils;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.sinch.android.rtc.SinchError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends BaseActivity implements View.OnClickListener,SinchService.StartFailedListener {


    /**
     * declare variables of class
     */
    @BindView(R.id.buttonSignUp)
    Button buttonSignUp;

    @BindView(R.id.textViewSignIn)
    TextView textViewSignIn;

    @BindView(R.id.rootLayout)
    RelativeLayout rootLayout;

    @BindView(R.id.bottomLinearLayout)
    LinearLayout bottomLinearLayout;

    @BindView(R.id.editTextFirstName)
    EditText editTextFirstName;

    @BindView(R.id.editTextLastName)
    EditText editTextLastName;

    @BindView(R.id.editTextAddress)
    EditText editTextAddress;

    @BindView(R.id.editTextAge)
    EditText editTextAge;

    @BindView(R.id.editTextEmail)
    EditText editTextEmail;

    @BindView(R.id.editTextUserName)
    EditText editTextUserName;

    @BindView(R.id.editTextPassword)
    EditText editTextPassword;

    @BindView(R.id.editTextCPassword)
    EditText editTextCPassword;

    @BindView(R.id.mCircleImageViewProfile)
    CircleImageView mCircleImageViewProfile;

    private Activity mActivity;
    private Uri selectedImageUri;

    @BindView(R.id.textViewTitle)
    TextView textViewTitle;


    boolean isEditProfile = false;
    String userId="",socialId="",socialType = "";
    private DatabaseReference databaseReference;
    CallbackManager callbackManager;
    private String first_name = "", last_name = "", facebokEmail = "", mfacebookId = "", mGender = "", mDob;
    private LoginButton loginButton;
    private TextView facebook_img;
    private ImageView google_img;
    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    private GoogleSignInClient mGoogleSignInClient;
    private String imageURL = "";
    private LinearLayout socialView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.layout_activity_registration);
        callbackManager = CallbackManager.Factory.create();
        databaseReference = FirebaseDatabase.getInstance().getReference("users");
        ButterKnife.bind(this);
        mActivity = this;

        loginButton = (LoginButton) findViewById(R.id.login_button);
        google_img = (ImageView) findViewById(R.id.google_img);
        facebook_img = (TextView) findViewById(R.id.facebook_img);
        socialView = (LinearLayout)findViewById(R.id.socialView);
        /*
        *  get edit Tag from bundle*/
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isEditProfile = bundle.getBoolean("isEditProfile");
        }

        if (getIntent()!=null)
        {
            if (getIntent().getStringExtra("firstName")!=null)
            {
                editTextFirstName.setText(getIntent().getStringExtra("firstName"));
                editTextLastName.setText(getIntent().getStringExtra("lastName"));
                socialId = getIntent().getStringExtra("socialId");
                socialType = getIntent().getStringExtra("socialType");
                editTextEmail.setText(getIntent().getStringExtra("email"));
                ImageLoader.getInstance().displayImage(getIntent().getStringExtra("image"),mCircleImageViewProfile);
                editTextPassword.setVisibility(View.GONE);
                editTextCPassword.setVisibility(View.GONE);
            }
        }


        /**
         * set click events on views
         */
        setClickEvents();


        /**
         * set keyboard open listener
         */
        //  AppUtils.getInstance().setKeyboardListener(mActivity, bottomLinearLayout);


         /*
        * set view for edit profile
        * */

        editTextLastName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    new SingleActionDialog().showDialogNormal("Make sure you put correct email",RegistrationActivity.this,false);
                    return true;
                }
                return false;
            }
        });

        if (isEditProfile)
        {
            setEditProfileView();
            socialView.setVisibility(View.GONE);
        }else
        {
            bottomLinearLayout.setVisibility(View.VISIBLE);
            buttonSignUp.setText(getString(R.string.Register));
        }


        /**
         * update device token
         */
        try {
            String token = FirebaseInstanceId.getInstance().getToken();
            PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
            preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_DEVICE_TOKEN, token);
            Log.e("tg", "token = " + token);
        } catch (Exception e) {
            e.printStackTrace();
        }


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // [END configure_signin]

        // [START build_client]
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        // [END build_client]

        /**
         * setting up font on textview
         */
        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/wolf_rain_font.ttf");
        //textViewTitle.setTypeface(face);

        facebook_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginButton.performClick();
            }
        });

        google_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("login", " " + loginResult);

                if (AccessToken.getCurrentAccessToken() != null) {

                    Profile profile = Profile.getCurrentProfile();
                    if (profile != null) {
                        Log.e("facebook_id", profile.getId());
                        Log.e("f_name", profile.getFirstName());
                        Log.e("l_name", profile.getLastName());
                        Log.e("full_name", profile.getName());
                        first_name = profile.getFirstName();
                        last_name = profile.getLastName();
                        mfacebookId = profile.getId();

                    }
                    RequestData();
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.e("facebbok", " " + error.toString());
            }
        });

    }
    @Override
    protected void onServiceConnected() {
        getSinchServiceInterface().setStartListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStartFailed(SinchError error) {
    }
    @Override
    public void onStarted()
    {
    }

    private void setEditProfileView() {
        buttonSignUp.setText(getString(R.string.update));
        bottomLinearLayout.setVisibility(View.GONE);
        textViewTitle.setVisibility(View.GONE);
        editTextEmail.setEnabled(false);
        editTextUserName.setEnabled(false);
        editTextEmail.setAlpha(.5f);
        editTextUserName.setAlpha(.5f);
        PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
        userId = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_USER_ID);
        String firstName = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_FIRST_NAME);
        String lastName = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_LAST_NAME);
        String userName = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_USER_NAME);
        String image = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_USER_IMAGE);
        String email = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_EMAIL);
        String password = msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_PASSWORD);


        editTextFirstName.setText(firstName);
        editTextLastName.setText(lastName);
        editTextUserName.setText(userName);
        editTextEmail.setText(email);
        editTextPassword.setText(password);
        editTextCPassword.setText(password);
        if (!msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(mActivity).KEY_USER_SOCIAL).isEmpty())
        {
            editTextPassword.setVisibility(View.GONE);
            editTextCPassword.setVisibility(View.GONE);
            editTextPassword.setText("jaiia123456");
            editTextCPassword.setText("jaiia123456");
        }

        if (!TextUtils.isEmpty(image)) {

            String path = "";
            if (image.startsWith("http://")) {
                path = image;
            } else {
                path = ApiClient.BASE_URL + image;
            }
            ImageLoader.getInstance().displayImage(path, mCircleImageViewProfile, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    // progressBarImg.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    //  progressBarImg.setVisibility(View.GONE);
                    mCircleImageViewProfile.setImageResource(R.drawable.user);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    ////  progressBarImg.setVisibility(View.GONE);

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    //  progressBarImg.setVisibility(View.GONE);
                    mCircleImageViewProfile.setImageResource(R.drawable.user);

                }
            });

        }
    }


    /**
     * set click events on views
     */
    private void setClickEvents() {

        buttonSignUp.setOnClickListener(this);
        textViewSignIn.setOnClickListener(this);
        mCircleImageViewProfile.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.buttonSignUp:


                /**
                 * validate form and send edit profile details to server
                 */
                AppUtils.getObj().hideSoftKeyboard(mActivity);

                if (isEditProfile) {
                    if (ValidationUtils.getLoginObj(mActivity).validateEditProfileForm(
                            editTextFirstName,
                            editTextLastName,
                            editTextPassword,
                            editTextCPassword)){


                        String first_name = editTextFirstName.getText().toString();
                        String last_name = editTextLastName.getText().toString();
                        String password = editTextPassword.getText().toString();


                        callEditProfileWebApi(first_name,last_name,password,userId);


                    }
                }
                /**
                 * validate form and send registration details to server
                 */

                else {
                    if (editTextPassword.getVisibility()==View.VISIBLE) {
                        if (ValidationUtils.getLoginObj(mActivity).validateRegistrationForm(
                                editTextFirstName,
                                editTextLastName,
                               /* editTextUserName,*/
                                editTextAge,
                                editTextEmail,
                                editTextPassword,
                                editTextCPassword)) {

                            String first_name = editTextFirstName.getText().toString();
                            String last_name = editTextLastName.getText().toString();
                            String email = editTextEmail.getText().toString();
                            String address = editTextAddress.getText().toString();
                            String age = editTextAge.getText().toString();
                            String username = editTextUserName.getText().toString();
                            String password = editTextPassword.getText().toString();
                            String device_type = DeviceInfoUtils.getObj().getDeviceType();
                            String device_id = DeviceInfoUtils.getObj().getDeviceId(mActivity);
                            String os_version = DeviceInfoUtils.getObj().getDeviceOs();
                            String subscription_plan = "";

                            String device_token = null;
                            try {
                                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                                device_token = preferenceUserUtil.getString(preferenceUserUtil.KEY_DEVICE_TOKEN);
                                Log.e("tg", "token = " + device_token);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            String image = AppConstant.getObj().EMPTY_STRING;
                            try {
                                if (selectedImageUri != null) {
                                    //   image = AppUtils.getInstance().encodeBase64(mActivity, selectedImageUri);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }


                            //   if (selectedImageUri != null && !TextUtils.isEmpty(selectedImageUri.getPath())) {

                            /**
                             * register user to server
                             */
                            callRegisterWebApi(
                                    first_name,
                                    last_name,
                                    email,
                                    password,
                                    address, age,
                                    username,
                                    device_type,
                                    device_token,
                                    device_id,
                                    os_version,
                                    subscription_plan);

                         /*   } else {

                                new SingleActionDialog().showDialogNormal(getResources().getString(R.string.please_select_image), mActivity, false);

                            }*/


                        }
                    } else {
                        if (ValidationUtils.getLoginObj(mActivity).validateRegistrationForm(editTextFirstName,
                                editTextLastName,
                                editTextEmail
                               )) {

                            String first_name = editTextFirstName.getText().toString();
                            String last_name = editTextLastName.getText().toString();
                            String email = editTextEmail.getText().toString();
                            String address = editTextAddress.getText().toString();
                            String age = editTextAge.getText().toString();
                            String device_type = DeviceInfoUtils.getObj().getDeviceType();
                            String device_id = DeviceInfoUtils.getObj().getDeviceId(mActivity);
                            String os_version = DeviceInfoUtils.getObj().getDeviceOs();
                            String subscription_plan = "";

                            String device_token = null;
                            try {
                                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                                device_token = preferenceUserUtil.getString(preferenceUserUtil.KEY_DEVICE_TOKEN);
                                Log.e("tg", "token = " + device_token);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            String image = AppConstant.getObj().EMPTY_STRING;
                            try {
                                if (selectedImageUri != null) {
                                    //   image = AppUtils.getInstance().encodeBase64(mActivity, selectedImageUri);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }


                            //   if (selectedImageUri != null && !TextUtils.isEmpty(selectedImageUri.getPath())) {

                            /**
                             * register user to server
                             */
                            callRegisterSocialWebApi(
                                    first_name,
                                    last_name,
                                    email,
                                    address,
                                    age,
                                    device_type,
                                    device_token,
                                    device_id,
                                    os_version,
                                    subscription_plan);
                        }
                    }
                }
                break;

            case R.id.textViewSignIn:

                Intent intentSignUp = new Intent(mActivity, LoginActivity.class);
                intentSignUp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentSignUp);
                ViewAnimUtils.activityEnterTransitions(this);


                break;

            case R.id.mCircleImageViewProfile:

                /**
                 * select contractFilePDF/image/camera
                 */
                GeorgeWayImagePicker.getImagePickObj(this).openConfirmationDialog();


                break;


        }

    }


    private void closeRevealActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            int cx = rootLayout.getWidth() / 2;
            int cy = rootLayout.getHeight() / 2;

            float finalRadius = Math.max(rootLayout.getWidth(), rootLayout.getHeight());

            // create the animator for this view (the start radius is zero)
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(rootLayout, cx, cy, finalRadius, 0);
            circularReveal.setDuration(500);

            // make the view visible and start the animation
            rootLayout.setVisibility(View.VISIBLE);
            circularReveal.start();
            circularReveal.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    rootLayout.setVisibility(View.INVISIBLE);
                    finish();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(mActivity);

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //closeRevealActivity();
            finish();
            ViewAnimUtils.activityExitTransitions(mActivity);
        } else {
            super.onBackPressed();
        }*/
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (requestCode == GeorgeWayImagePicker.getImagePickObj(this).PICK_IMAGE_REQUEST) {

            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                /**
                 * Call the below method for uploading image
                 */
                GeorgeWayImagePick.openChooserWithGallery(mActivity, GeorgeWayImagePicker.getImagePickObj(this).CHOOSE_IMAGE, GeorgeWayImagePicker.getImagePickObj(this).PICK_IMAGE_REQUEST);


            }
        } else if (requestCode == GeorgeWayImagePicker.getImagePickObj(this).REQUEST_IMAGE_CAPTURE) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                /**
                 * Call the below method for uploading image
                 */
                GeorgeWayImagePick.openCamera(mActivity, GeorgeWayImagePicker.getImagePickObj(this).REQUEST_IMAGE_CAPTURE);

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);

            GeorgeWayImagePick.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, GeorgeWayImagePick.ImageSource source, int type) {
                    e.printStackTrace();
                }

                @Override
                public void onImagesCaptured(@NonNull List<Uri> imageUriList, GeorgeWayImagePick.ImageSource source, int type) {


                    /**
                     * set image from gallery/camera into image views
                     */
                    if (imageUriList != null && imageUriList.size() > 0) {
                        selectedImageUri = imageUriList.get(0);
                        ViewUtils.getObj().updateBitmapOnImageViews(mActivity, selectedImageUri, mCircleImageViewProfile);
                    }

                }

                @Override
                public void onImagesPicked(List<File> imageFiles, GeorgeWayImagePick.ImageSource source, int type) {

                    /**
                     * set image from gallery/camera into image views
                     */
                    if (imageFiles != null && imageFiles.size() > 0) {
                        selectedImageUri = Uri.parse(imageFiles.get(0).getAbsolutePath());
                        ViewUtils.getObj().updateBitmapOnImageViews(mActivity, selectedImageUri, mCircleImageViewProfile);
                    }

                }

                @Override
                public void onFilePicked(@NonNull Uri imageFileUri, GeorgeWayImagePick.ImageSource source, int type) {
                    /**
                     * set image from gallery/camera into image views
                     */
                    if (imageFileUri != null) {
                        selectedImageUri = imageFileUri;
                        ViewUtils.getObj().updateBitmapOnImageViews(mActivity, imageFileUri, mCircleImageViewProfile);
                    }

                }

                @Override
                public void onCanceled(GeorgeWayImagePick.ImageSource source, int type) {
                    //Cancel handling, you might wanna remove taken photo if it was canceled
                    if (source == GeorgeWayImagePick.ImageSource.CAMERA) {
                        File photoFile = GeorgeWayImagePick.lastlyTakenButCanceledPhoto(mActivity);
                        if (photoFile != null) photoFile.delete();
                    }
                }
            });
        }

    }

    /**
     * call login api
     */
    private File imgFile;
    private MultipartBody.Part bodyFile;


    public void callRegisterWebApi(String first_name,
                                   String last_name,
                                   String email,
                                   String password,
                                   String address,
                                   String age,
                                   String username,
                                   String device_type,
                                   String device_token,
                                   String device_id,
                                   String os_version,
                                   String subscription_plan) {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                try {
                    String imagePath = GeorgeWayImagePick.getRealPathFromURI(RegistrationActivity.this, selectedImageUri);

                    imgFile = new File(imagePath);
                    if (imgFile != null) {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile);
                        bodyFile = MultipartBody.Part.createFormData("image", imgFile.getName(), requestFile);

                    }else{
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                        bodyFile = MultipartBody.Part.createFormData("image", "", requestFile);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);

                Call<ResponseBody> call = mApiInterface.registerUserRequest(
                        RequestBody.create(MediaType.parse("text/plain"), first_name),
                        RequestBody.create(MediaType.parse("text/plain"), last_name),
                        RequestBody.create(MediaType.parse("text/plain"), email),
                        RequestBody.create(MediaType.parse("text/plain"), password),
                        RequestBody.create(MediaType.parse("text/plain"), address),
                        RequestBody.create(MediaType.parse("text/plain"), age),
                        RequestBody.create(MediaType.parse("text/plain"), device_type),
                        RequestBody.create(MediaType.parse("text/plain"), device_token),
                        RequestBody.create(MediaType.parse("text/plain"), device_id),
                        RequestBody.create(MediaType.parse("text/plain"), os_version),
                        RequestBody.create(MediaType.parse("text/plain"), subscription_plan), bodyFile);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                            PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");

                                if (status_code.equalsIgnoreCase("201") ||status_code.equalsIgnoreCase("200")) {
                                    JSONArray data = mJsonObj.getJSONArray("data");
                                    if (data != null) {
                                        JSONObject dataJsonObj = data.getJSONObject(0);
                                        String userid = dataJsonObj.optString("userid");
                                        String first_name = dataJsonObj.optString("first_name");
                                        String last_name = dataJsonObj.optString("last_name");
                                        String address = dataJsonObj.optString("address");
                                        String username = dataJsonObj.optString("username");
                                        String email = dataJsonObj.optString("email");
                                        String subscription_plan = dataJsonObj.optString("subscription_plan");
                                        String image = dataJsonObj.optString("image");

                                        String password = dataJsonObj.optString("password");
                                        msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_PASSWORD, password);
                                        msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_ID, userid);
                                        msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_FIRST_NAME, first_name);
                                        msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_LAST_NAME, last_name);
                                        msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_ADDRESS, address);
                                        msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_NAME, username);
                                        msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_EMAIL, email);
                                        msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_SUBSCRIPTION_PLAN, subscription_plan);
                                        msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_IMAGE, image);
                                    }
                                }


                                if (!TextUtils.isEmpty(status_code))
                                {

                                    if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS) || status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_CHANGED)) {

                                        showDialogNormal(getResources().getString(R.string.You_have_registered), mActivity, true);


                                    } else {

                                        String error_message = mJsonObj.optString(AppConstant.getObj().ERROR_MESSAGE);
                                        new SingleActionDialog().showDialogNormal(error_message, mActivity, false);

                                    }
                                }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                        // SingleActionDialog.getDialogObj(mActivity).showDialogNormal("Link send on your email to verify email.", mActivity);

                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack1);
        }
    }

    public void callRegisterSocialWebApi(String first_name,
                                   String last_name,
                                   String email,
                                   String address,
                                   String age,
                                   String device_type,
                                   String device_token,
                                   String device_id,
                                   String os_version,
                                   String subscription_plan) {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                try {
                    String imagePath = GeorgeWayImagePick.getRealPathFromURI(RegistrationActivity.this, selectedImageUri);

                    imgFile = new File(imagePath);
                    if (imgFile != null) {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile);
                        bodyFile = MultipartBody.Part.createFormData("image", imgFile.getName(), requestFile);

                    }else{
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                        bodyFile = MultipartBody.Part.createFormData("image", "", requestFile);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);

                Call<ResponseBody> call = mApiInterface.socialregisterUserRequest(
                        RequestBody.create(MediaType.parse("text/plain"), first_name),
                        RequestBody.create(MediaType.parse("text/plain"), last_name),
                        RequestBody.create(MediaType.parse("text/plain"), email),
                        RequestBody.create(MediaType.parse("text/plain"), address),
                        RequestBody.create(MediaType.parse("text/plain"), age),
                        /*RequestBody.create(MediaType.parse("text/plain"), username),*/
                        RequestBody.create(MediaType.parse("text/plain"), device_type),
                        RequestBody.create(MediaType.parse("text/plain"), device_token),
                        RequestBody.create(MediaType.parse("text/plain"), socialType),
                        RequestBody.create(MediaType.parse("text/plain"), socialId)
                        );

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                            PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");

                            if (status_code.equalsIgnoreCase("201") ||status_code.equalsIgnoreCase("200")) {
                                if (!TextUtils.isEmpty(status_code))
                                {

                                    if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS) || status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_CHANGED)) {

                                        showDialogNormal(getResources().getString(R.string.You_have_registered), mActivity, true);


                                    } else {

                                        String error_message = mJsonObj.optString(AppConstant.getObj().ERROR_MESSAGE);
                                        new SingleActionDialog().showDialogNormal(error_message, mActivity, false);

                                    }
                                }
                            }



                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                        // SingleActionDialog.getDialogObj(mActivity).showDialogNormal("Link send on your email to verify email.", mActivity);

                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack1);
        }
    }



    public void callEditProfileWebApi(String first_name,
                                      String last_name,
                                      String password, String userId) {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                try {
                    if(selectedImageUri!=null) {
                        String imagePath = GeorgeWayImagePick.getRealPathFromURI(RegistrationActivity.this, selectedImageUri);
                        imgFile = new File(imagePath);
                        if (imgFile != null) {
                            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile);
                            bodyFile = MultipartBody.Part.createFormData("image", imgFile.getName(), requestFile);

                        }
                    }else{
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                        bodyFile = MultipartBody.Part.createFormData("image", "", requestFile);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);

                Call<ResponseBody> call = mApiInterface.editPRofileRequest(
                        RequestBody.create(MediaType.parse("text/plain"), first_name),
                        RequestBody.create(MediaType.parse("text/plain"), last_name),
                        RequestBody.create(MediaType.parse("text/plain"), userId),
                        RequestBody.create(MediaType.parse("text/plain"), password),
                        bodyFile);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");


                            if (!TextUtils.isEmpty(status_code)) {

                                if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS) || status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_CHANGED)) {

                                    Toast.makeText(mActivity,mActivity.getResources().getString(R.string.profile_updated),Toast.LENGTH_SHORT).show();
                                    setUserDataInPreference(mJsonObj);
                                    mActivity.finish();
                                    ViewAnimUtils.activityExitTransitions(mActivity);
                                } else {

                                    String error_message = mJsonObj.optString(AppConstant.getObj().ERROR_MESSAGE);
                                    new SingleActionDialog().showDialogNormal(error_message, mActivity, false);

                                }
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                        // SingleActionDialog.getDialogObj(mActivity).showDialogNormal("Link send on your email to verify email.", mActivity);

                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack1);
        }
    }



    DialogInterface.OnClickListener networkCallBack1 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

            String first_name = editTextFirstName.getText().toString();
            String last_name = editTextLastName.getText().toString();
            String email = editTextEmail.getText().toString();
            String address = editTextAddress.getText().toString();
            String age = editTextAge.getText().toString();
            String username = editTextEmail.getText().toString();
            String password = editTextPassword.getText().toString();
            String device_type = DeviceInfoUtils.getObj().getDeviceType();
            String device_token = "12345";
            String device_id = "12345";
            String os_version = DeviceInfoUtils.getObj().getDeviceOs();
            String subscription_plan = "123";

            String image = AppConstant.getObj().EMPTY_STRING;
            try {
                if (selectedImageUri != null) {
                    // image = AppUtils.getInstance().encodeBase64(mActivity, selectedImageUri);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (selectedImageUri != null && !TextUtils.isEmpty(selectedImageUri.getPath())) {

                /**
                 * register user to server
                 */
                callRegisterWebApi(
                        first_name,
                        last_name,
                        email,
                        password,
                        address, age,
                        username,
                        device_type,
                        device_token,
                        device_id,
                        os_version,
                        subscription_plan);

            } else {

                new SingleActionDialog().showDialogNormal(getResources().getString(R.string.please_select_image), mActivity, false);

            }


        }
    };


    /**
     * setting up data in preference
     *
     * @param mJSONObject
     */
    private void setUserDataInPreference(JSONObject mJSONObject) {

        PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
        /**
         * in case of login we will get a single json object in data array for user information
         * so get it by 0 index
         */
        try {

            JSONArray data = mJSONObject.getJSONArray("data");
            if (data != null) {

                JSONObject dataJsonObj = data.getJSONObject(0);
                String userid = dataJsonObj.optString("userid");
                String first_name = dataJsonObj.optString("first_name");
                String last_name = dataJsonObj.optString("last_name");
                String image = dataJsonObj.optString("image");
                String password = dataJsonObj.optString("password");
                databaseReference.child(userid).child("details").setValue(AppUtils.jsonToMap(dataJsonObj.toString()));

                databaseReference.child(userid+"/is_online/").setValue("true");
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_PASSWORD, password);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_ID, userid);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_FIRST_NAME, first_name);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_LAST_NAME, last_name);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_IMAGE, image);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    /**
     * if everything is fine then open home screen
     */

    private boolean isFromLoginBtn = false;
    private void startHomeScreen() {

        PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);

        try {
            isFromLoginBtn = preferenceUserUtil.getPreferenceBooleanData(preferenceUserUtil.KEY_RE_LOGIN);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isFromLoginBtn) {
            /**
             * finish current activity
             */
            finish();
            ViewAnimUtils.activityEnterTransitions(this);

        } else {

            /**
             * finish current activity
             */
            finish();

            /**
             * after the Login successfully start new DashBoard activity
             * first of all finish current activity , then start new activity
             */
            Intent mIntent = new Intent(mActivity, GeorgeHomeActivity.class);
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mIntent);
            ViewAnimUtils.activityEnterTransitions(this);

            preferenceUserUtil.setPreferenceBooleanData(preferenceUserUtil.KEY_RE_LOGIN, false);

        }
    }
    private  AlertDialog alertDialogBack;
    public void showDialogNormal(String errorTitle, final Activity activity, final boolean isKill) {

        try {
            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_dialog_normal, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder.setView(promptsView);

            if(alertDialogBack == null)
                alertDialogBack = alertDialogBuilder.create();

            /**
             * setting up window design
             */
            Window mWindow = alertDialogBack.getWindow();
            mWindow.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mWindow.setDimAmount(0.5f);
            mWindow.setGravity(Gravity.CENTER);

            if(alertDialogBack.isShowing()){
                alertDialogBack.dismiss();
            }

            alertDialogBack.show();

            TextView textViewErrorTitle = (TextView) promptsView.findViewById(R.id.textViewErrorTitle);
            textViewErrorTitle.setText(errorTitle);

            Button buttonOk = (Button) promptsView.findViewById(R.id.buttonOk);
            buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(isKill){
                        startHomeScreen();
                    }else {
                        alertDialogBack.dismiss();
                    }

                }
            });

            alertDialogBack.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    if(isKill){
                        alertDialogBack.dismiss();
                        activity.finish();
                        ViewAnimUtils.activityExitTransitions(activity);
                    }else {
                        alertDialogBack.dismiss();

                    }
                }
            });

            alertDialogBack.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    if(isKill){
                        alertDialogBack.dismiss();
                        activity.finish();
                        ViewAnimUtils.activityExitTransitions(activity);
                    }else {
                        alertDialogBack.dismiss();

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                JSONObject json = response.getJSONObject();
                System.out.println("Json data :" + json);
                try {
                    if (json != null) {
//                        details_txt.setText(Html.fromHtml(text));
//                        profile.setProfileId(json.getString("id"));
                        Log.e("data ", " " + json);
                        facebokEmail = json.optString("email");
                        mGender = json.optString("gender");
                        imageURL = "http://graph.facebook.com/"+json.getString("id")+"/picture?type=large";
//                        mDob = mConvertDate(json.optString("birthday"));
                        if (NetworkUtil.isConnected(mActivity)) {
                            //   mFacebooklogin(json.getString("id"),"fb",facebokEmail);
                            callSocialLoginWebApi(json.getString("id"), "fb");
                        } else {
                            Snackbar.make(loginButton, "No, Internet Connection, Please try again.", Snackbar.LENGTH_INDEFINITE).setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            }).show();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,birthday,gender,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    /**
     * set click events on views
     */


    // [START handleSignInResult]
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            facebokEmail = account.getEmail();
            mGender = "";
            String fullname = account.getDisplayName();
            imageURL = "";

            String[] parts = fullname.split(" ");
            Log.d("Length-->", "" + parts.length);
            if (parts.length == 2) {
                first_name = parts[0];
                last_name = parts[1];
                Log.d("First-->", "" + first_name);
                Log.d("Last-->", "" + last_name);
            } else if (parts.length == 3) {
                first_name = parts[0];
                String middlename = parts[1];
                last_name = parts[2];
            }
            if (NetworkUtil.isConnected(mActivity)) {
                callSocialLoginWebApi(account.getId(), "google");
            } else {
                Snackbar.make(loginButton, "No, Internet Connection, Please try again.", Snackbar.LENGTH_INDEFINITE).setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
            }
            // Signed in successfully, show authenticated UI.
//            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }
    // [START signOut]
    private void signOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                // [START_EXCLUDE]
                // [END_EXCLUDE]
            }
        });
    }
    public void callSocialLoginWebApi(final String socialId, final String socialType) {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);
            LoginManager.getInstance().logOut();
            signOut();
            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                String device_token = preferenceUserUtil.getString(preferenceUserUtil.KEY_DEVICE_TOKEN);
                Log.e("tg", "token = " + device_token);

                String device_type = DeviceInfoUtils.getObj().getDeviceType();
                String device_id = DeviceInfoUtils.getObj().getDeviceId(mActivity);


                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.socialloginRequest(facebokEmail,
                        socialId,
                        socialType,
                        device_token,
                        device_type,
                        Build.DEVICE);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");


                            if (!TextUtils.isEmpty(status_code)) {

                                if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {

                                    String success_message = mJsonObj.optString("success_message");
                                    //  Toast.makeText(mActivity, getResources().getString(R.string.login_success), Toast.LENGTH_SHORT).show();

                                    /**
                                     * set user data in preference
                                     */
                                    setUserDataSocialInPreference(mJsonObj);

                                    /**
                                     * start home screen
                                     */
                                    startHomeScreen();


                                } else if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_Error)){

                                    editTextFirstName.setText(first_name);
                                    editTextLastName.setText(last_name);
                                    editTextEmail.setText(facebokEmail);
                                  //  ImageLoader.getInstance().displayImage(getIntent().getStringExtra("image"),mCircleImageViewProfile);
                                    editTextPassword.setVisibility(View.GONE);
                                    editTextCPassword.setVisibility(View.GONE);
                                    socialView.setVisibility(View.GONE);
                                }
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack1);
        }
    }

    /**
     * setting up data in preference
     *
     * @param mJSONObject
     */
    private SimpleDateFormat mFormatter = new SimpleDateFormat("dd,MMM,yyyy, hh:mm aa");
    private SimpleDateFormat mFormatter_ = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private void setUserDataSocialInPreference(JSONObject mJSONObject) {

        PreferenceUserUtil msPreferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
        /**
         * in case of login we will get a single json object in data array for user information
         * so get it by 0 index
         */
        try {

            JSONArray data = mJSONObject.getJSONArray("data");
            if (data != null) {
                JSONObject dataJsonObj = data.getJSONObject(0);
                AppUtils.jsonToMap(dataJsonObj.toString());
                databaseReference.child(dataJsonObj.optString("user_id")).child("details").setValue(AppUtils.jsonToMap(dataJsonObj.toString()));

                databaseReference.child(dataJsonObj.optString("user_id")+"/is_online/").setValue("true");

                getSinchServiceInterface().startClient("U_"+dataJsonObj.optString("user_id"));


                String userid = dataJsonObj.optString("user_id");
                String first_name = dataJsonObj.optString("first_name");
                String last_name = dataJsonObj.optString("last_name");
                String address = dataJsonObj.optString("address");
                String username = dataJsonObj.optString("username");
                String email = dataJsonObj.optString("email");
                String subscription_plan = dataJsonObj.optString("subscription_plan");
                String image = dataJsonObj.optString("image");
                String social_type = dataJsonObj.optString("social_type");

                String password = dataJsonObj.optString("password");
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_PASSWORD, password);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_ID, userid);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_FIRST_NAME, first_name);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_LAST_NAME, last_name);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_ADDRESS, address);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_NAME, username);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_EMAIL, email);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_SOCIAL, social_type);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_SUBSCRIPTION_PLAN, subscription_plan);
                msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.KEY_USER_IMAGE, image);


                if (dataJsonObj.has("subscription_start")) {
                    String start_date = dataJsonObj.optString("subscription_start");
                    Date strt_date = mFormatter_.parse(start_date);

                    String end_date = dataJsonObj.optString("subscription_end");
                    Date endd_date = mFormatter_.parse(end_date);

                    msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.subscription_start, mFormatter.format(strt_date));
                    msPreferenceUserUtil.setPreferenceStringData(msPreferenceUserUtil.subscription_end, mFormatter.format(endd_date));
                }


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}