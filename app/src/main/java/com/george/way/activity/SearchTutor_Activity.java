package com.george.way.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.model.Current_Request_User_Model;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JAI on 2/17/2018.
 */

public class SearchTutor_Activity extends NaviBaseActivity {

    private ListView schoolList;
    private ListView courseList;
    private String schoolId = "", courseId = "", schoolName = "", courseName = "";
    private ApiInterface mApiInterface;
    private List<String> schoolNameList, schoolIdList, courseNameList, courseIdList, selectCourse;
    boolean isActivityAlive=false;
    TextView toolbarTitle;
    private String current_request_key = "";
    private int onlineTutorCount = 0;
    private long timeCountInMilliSeconds = 60000;
    private boolean timefetched=false, schoolListLoaded=false, courseListLoaded=false, tutorsLoaded=false;
    private String remainingSubscribedTime="";

    private enum TimerStatus {
        STARTED,
        STOPPED
    }
    private TimerStatus timerStatus = TimerStatus.STOPPED;
    private CountDownTimer countDownTimer;
    private DatabaseReference referenceTutors, currentReference, referenceMyChatRooms;
    private ArrayList<JSONObject> onlineTutorsList = new ArrayList<>();
    private ArrayList<JSONObject> requestedTutorsList = new ArrayList<>();
    private TextView total_tutor_txt,seachtextiew,total_min_txt,addmoremin;
    View wrapperSearch;
    EditText edtSearch;
    boolean isForSchool=true;
    ArrayAdapter<String> adapterSchool;
    ArrayAdapter<String> adapterCourse;
    PreferenceUserUtil mPrefs;
    ProgressBar bar;


    Handler mHandler;

    @Override
    int getContentViewId() {
        return R.layout.search_tutor_activity;
    }

    public String findSelectedSchoolId(String item) {
        int pos=0;
        for (String obj : schoolNameList){
            if (obj.equals(item)){
                break;
            }
            pos++;
        }
        return schoolIdList.get(pos);
    }

    @Override
    protected void onDestroy() {
        isActivityAlive = false;
        super.onDestroy();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isActivityAlive = true;
        mPrefs = PreferenceUserUtil.getInstance(SearchTutor_Activity.this);
        String userId = mPrefs.getString(mPrefs.KEY_USER_ID);

        mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
        currentReference = FirebaseDatabase.getInstance().getReference("tutors/" + userId + "/current_requests");
        referenceTutors = FirebaseDatabase.getInstance().getReference("tutors");
        referenceMyChatRooms = FirebaseDatabase.getInstance().getReference("users/" + userId + "/chat_rooms");

        GWProgressDialog.msGWProgressDialog = null;
        mHandler = new Handler();
        initView();
        fetchCurrentPlane();
        getSchoolList();

        referenceTutors.orderByChild("is_online").equalTo("true").addChildEventListener(is_onlineListener);
        referenceMyChatRooms.addChildEventListener(requestAccept);
        seachtextiew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (remainingSubscribedTime.equalsIgnoreCase("0")){
                    showNoMinutesDialog();
                    return;
                }

                if (!tutorsLoaded){
                    showTutorsLoadingDialog();
                    return;
                }
                total_tutor_txt.setVisibility(View.GONE);
                wrapperSearch.setVisibility(View.VISIBLE);
                schoolList.setVisibility(View.VISIBLE);
                isForSchool =true;
                edtSearch.setText("");
                seachtextiew.setVisibility(View.GONE);
                toolbarTitle.setText("Select School");
            }
        });

        schoolList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                schoolList.setEnabled(false);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!isActivityAlive) return;
                        schoolName = (String) adapterView.getItemAtPosition(i);
                        schoolId = findSelectedSchoolId(schoolName);

                        Log.e("School", " Id" + schoolId);
                        toolbarTitle.setText("Select Course Name");
                        String schoolArray[] = new String[schoolIdList.size()];
                        schoolArray = schoolNameList.toArray(schoolArray);
                        adapterSchool = new ArrayAdapter<String>(SearchTutor_Activity.this, R.layout.rowview_school_n_course_list, schoolArray);
                        schoolList.setVisibility(View.GONE);
                        edtSearch.removeTextChangedListener(textWatcher);
                        schoolList.setAdapter(adapterSchool);
                        edtSearch.setText("");
                        isForSchool = false;
                        edtSearch.addTextChangedListener(textWatcher);
                        wrapperSearch.setVisibility(View.VISIBLE);
                        courseList.setVisibility(View.VISIBLE);
                        total_tutor_txt.setVisibility(View.GONE);
                        courseList.setEnabled(true);
                        schoolList.setEnabled(true);
                    }
                },1000);
            }
        });


        courseList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int j, long l) {
                courseList.setEnabled(false);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!isActivityAlive) return;
                        current_request_key = currentReference.push().getKey();
                        if (courseList.getCheckedItemPositions().size() > 0) {
                            int countChoice = courseList.getCount();
                            selectCourse = new ArrayList<>();
                            SparseBooleanArray sparseBooleanArray = courseList.getCheckedItemPositions();


                            for (int i = 0; i < countChoice; i++) {

                                if (sparseBooleanArray.get(i)) {

                                    courseId += courseIdList.get(i) /*+ ","*/;
                                    courseName += courseNameList.get(i) /*+ ","*/;
                                    selectCourse.add(courseNameList.get(i));
                                }

                            }
                            Log.e("course","Course ID: "+courseId+ "Course Name: "+courseName);

                            String userId = mPrefs.getString(mPrefs.KEY_USER_ID);
                            String firstName = mPrefs.getString(mPrefs.KEY_FIRST_NAME);
                            String lastName = mPrefs.getString(mPrefs.KEY_LAST_NAME);
                            String image = mPrefs.getString(mPrefs.KEY_USER_IMAGE);
                            String tutorId = "";

                            Current_Request_User_Model current_request_model = new Current_Request_User_Model(firstName, lastName, image, userId, schoolName);
                            if (onlineTutorsList.size() > 0) {
                                boolean noTutorFound =true;
                                for (int i = 0; i < onlineTutorsList.size(); i++) {
                                    JSONObject jsonObject = onlineTutorsList.get(i);
                                    Object chat_rooms = jsonObject.optJSONObject("chat_rooms");
                                    Object current_requests = jsonObject.optJSONObject("current_requests");
                                    if (chat_rooms==null && current_requests==null){
                                        JSONObject tutorDetails = jsonObject.optJSONObject("details");
                                        String tutorSchoolName  = tutorDetails.optString("school_name");

                                        if (tutorSchoolName.equalsIgnoreCase(schoolName)){
                                            JSONArray tutorCourses = tutorDetails.optJSONArray("tutor_courses");

                                            for (int p = 0;p<tutorCourses.length();p++ ){
                                                String tutorCourseName = tutorCourses.optJSONObject(p).optString("name");

                                                if (tutorCourseName.equalsIgnoreCase(courseName)){
                                                    tutorId = tutorDetails.optString("id");
                                                    referenceTutors.child(tutorId + "/current_requests").setValue(null);
                                                    referenceTutors.child(tutorId + "/current_requests/" + current_request_key).setValue(current_request_model);
                                                    referenceTutors.child(tutorId + "/total_request_count").setValue(1);
                                                    Log.e("TAG","Request Sent to Tutor : " + tutorId);
                                                    noTutorFound = false;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                if (noTutorFound){
                                    showNoTutorFoundDialog();
                                    resetSearchViewsAfterSearch();
                                    return;
                                }
                            }

                            HashMap<String,Object> apiData = new HashMap<>();
                            apiData.put("name",firstName + lastName);
                            apiData.put("school",schoolName);
                            apiData.put("course",courseName);
                            apiData.put("tutors",tutorId);
                            mApiInterface.sendNotification(ApiClient.NOTIFICATION_URL,apiData).enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {}
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {}
                            });
                            bar.setVisibility(View.VISIBLE);
                    /*View alertView = LayoutInflater.from(SearchTutor_Activity.this).inflate(R.layout.tuttor_filter_activity, null);
                    AlertDialog.Builder builder = new AlertDialog.Builder(SearchTutor_Activity.this);
                    builder.setView(alertView);
                    TextView schoolName_txt = (TextView) alertView.findViewById(R.id.schoolName_txt);
                    TextView courseName_txt = (TextView) alertView.findViewById(R.id.courseName_txt);
                    final TextView timerTxt = (TextView) alertView.findViewById(R.id.timerTxt);

                    RelativeLayout schoolView = (RelativeLayout) alertView.findViewById(R.id.schoolView);
                    final ProgressBar progressBarCircle = (ProgressBar) alertView.findViewById(R.id.progressBarCircle);
                    Button buttoncancel = (Button) alertView.findViewById(R.id.buttoncancel);

                    alertDialog = builder.create();
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();

                    if (schoolName.isEmpty()) {
                        schoolView.setVisibility(View.GONE);
                    }
                    schoolName_txt.setText(schoolName);
                    courseName_txt.setText(courseName);

                    buttoncancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            toggleTimer(progressBarCircle, timerTxt);
                            removeSentRequests();
                            alertDialog.dismiss();
                            courseName = "";
                            courseId = "";
                            toolbarTitle.setText("Home");
                            seachtextiew.setVisibility(View.VISIBLE);
                            requestedTutorsList.clear();
                        }
                    });*/
                            toggleTimer(toolbarTitle);
                            referenceTutors.orderByChild("current_requests").addChildEventListener(currentRequestListener);
                            resetSearchViewsAfterSearch();

                        } else {
                            new SingleActionDialog().showDialogNormal("Please select atleast one course name.", SearchTutor_Activity.this, false);
                        }
                        courseList.setEnabled(true);
                    }
                },1000);
            }
        });
    }

    private void resetSearchViewsAfterSearch() {
        courseList.setVisibility(View.GONE);
        wrapperSearch.setVisibility(View.GONE);
        edtSearch.removeTextChangedListener(textWatcher);
        edtSearch.setText("");
        String courseArray[] = new String[courseNameList.size()];
        courseArray = courseNameList.toArray(courseArray);
        adapterCourse = new ArrayAdapter<String>(SearchTutor_Activity.this, R.layout.rowview_school_n_course_list, courseArray);
        courseList.setAdapter(adapterCourse);
        edtSearch.addTextChangedListener(textWatcher);
    }



    @Override
    int getNavigationMenuItemId() {
        return R.id.navigation_serach;
    }

    TextWatcher textWatcher;
    private void initView() {
        Toolbar toolbar = findViewById(R.id.toolbar_top);
        setSupportActionBar(toolbar);
        toolbarTitle = findViewById(R.id.toolbar_title);

        toolbarTitle.setText("Home");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        schoolList = (ListView) findViewById(R.id.schoolList);
        courseList = (ListView) findViewById(R.id.courseList);
        total_tutor_txt = (TextView) findViewById(R.id.total_tutor_txt);
        seachtextiew = (TextView)findViewById(R.id.seachtextiew);
        addmoremin = (TextView)findViewById(R.id.addmoremin);
        total_min_txt = (TextView)findViewById(R.id.total_min_txt);
        wrapperSearch = findViewById(R.id.wrapper_search_bar);
        edtSearch = findViewById(R.id.search_bar);
        bar = findViewById(R.id.progressBar);

        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isForSchool){
                    if(adapterSchool!=null) {
                        if (adapterSchool.getFilter()!=null){
                            adapterSchool.getFilter().filter(s);
                        }
                    }
                }else{
                    if (adapterCourse!=null) {
                        if (adapterCourse!=null){
                            adapterCourse.getFilter().filter(s);
                        }
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable s) {}
        };

        edtSearch.addTextChangedListener(textWatcher);

        addmoremin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SearchTutor_Activity.this, BuyAcitvity.class));
            }
        });
    }

    private void getSchoolList() {
        //GWProgressDialog.getProgressDialog(SearchTutor_Activity.this).showDialog1();
        Call<ResponseBody> getSchoolResponse = mApiInterface.getSchoolList();
        getSchoolResponse.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                schoolListLoaded = true;
                try {
                    dismissDialog();
                    JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                    if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                        schoolIdList = new ArrayList<>();
                        schoolNameList = new ArrayList<>();

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            schoolNameList.add(jsonArray.optJSONObject(i).optString("name"));
                            schoolIdList.add(jsonArray.optJSONObject(i).optString("id"));
                        }

                        String schoolArray[] = new String[schoolIdList.size()];
                        schoolArray = schoolNameList.toArray(schoolArray);
                        adapterSchool = new ArrayAdapter<String>(SearchTutor_Activity.this, R.layout.rowview_school_n_course_list, schoolArray);
                        schoolList.setAdapter(adapterSchool);
                    }
                    getCourseList();
                } catch (Exception e) {
                    e.printStackTrace();
                    GWProgressDialog.getProgressDialog(SearchTutor_Activity.this).dismissDialog1();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                schoolListLoaded = true;
                GWProgressDialog.getProgressDialog(SearchTutor_Activity.this).dismissDialog1();
            }
        });
    }

    private void dismissDialog(){
        if (timefetched && schoolListLoaded && courseListLoaded){
            GWProgressDialog.getProgressDialog(SearchTutor_Activity.this).dismissDialog1();
            seachtextiew.setVisibility(View.VISIBLE);
            total_tutor_txt.setVisibility(View.VISIBLE);
            seachtextiew.setEnabled(true);
        }
    }

    private void getCourseList() {
        //GWProgressDialog.getProgressDialog(SearchTutor_Activity.this).showDialog1();
        Call<ResponseBody> getSchoolResponse = mApiInterface.getCourseList();
        getSchoolResponse.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    courseListLoaded = true;
                    dismissDialog();

                    JSONObject jsonObject = new JSONObject(new String(response.body().string()));
                    if (jsonObject.optString("status_code").equalsIgnoreCase("200")) {
                        courseIdList = new ArrayList<>();
                        courseNameList = new ArrayList<>();
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            courseNameList.add(jsonArray.optJSONObject(i).optString("name"));
                            courseIdList.add(jsonArray.optJSONObject(i).optString("id"));
                        }
                        String courseArray[] = new String[courseNameList.size()];
                        courseArray = courseNameList.toArray(courseArray);
                        adapterCourse = new ArrayAdapter<String>(SearchTutor_Activity.this, R.layout.rowview_school_n_course_list, courseArray);

                        courseList.setAdapter(adapterCourse);
                        seachtextiew.setEnabled(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    GWProgressDialog.getProgressDialog(SearchTutor_Activity.this).dismissDialog1();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                GWProgressDialog.getProgressDialog(SearchTutor_Activity.this).dismissDialog1();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
//        getMenuInflater().inflate(R.menu.tutor_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.action_done:
                if (schoolList.getVisibility() == View.VISIBLE) {

                } else {

                }

                break;

            case android.R.id.home:
                handleBack();
                break;
        }
        return true;
    }


    private void handleBack() {
        if (schoolList.getVisibility() == View.VISIBLE) {

            toolbarTitle.setText("Home");
            schoolList.setVisibility(View.GONE);
            courseList.setVisibility(View.GONE);
            wrapperSearch.setVisibility(View.INVISIBLE);
            seachtextiew.setVisibility(View.VISIBLE);
            total_tutor_txt.setVisibility(View.VISIBLE);

        } else if (courseList.getVisibility() == View.VISIBLE) {

            toolbarTitle.setText("Select School");
            wrapperSearch.setVisibility(View.VISIBLE);
            schoolList.setVisibility(View.VISIBLE);
            courseList.setVisibility(View.GONE);
            seachtextiew.setVisibility(View.GONE);
            /*String schoolArray[] = new String[schoolIdList.size()];
            schoolArray = schoolNameList.toArray(schoolArray);
            adapterSchool = new ArrayAdapter<String>(SearchTutor_Activity.this, android.R.layout.simple_list_item_single_choice, schoolArray);
            schoolList.setAdapter(adapterSchool);*/
            edtSearch.setText("");
            isForSchool =true;

        }else{
            ViewAnimUtils.activityExitTransitions(SearchTutor_Activity.this);
            finish();
        }
    }

    /**
     * method to reset count down timer
     */
    private void reset() {
        stopCountDownTimer();
    }



    /**
     * method to start and stop count down timer
     *
     * //@param progressBarCircle
     * @param timerTxt
     */
    private void toggleTimer(/*ProgressBar progressBarCircle, */TextView timerTxt) {
        if (timerStatus == TimerStatus.STOPPED) {

            // call to initialize the timer values
            setTimerValues();
            // call to initialize the progress bar values
            //setProgressBarValues(progressBarCircle);
            // showing the reset icon
            // changing the timer status to started
            timerStatus = TimerStatus.STARTED;
            // call to start the count down timer
            startCountDownTimer(/*progressBarCircle, */timerTxt);
        } else {

            // changing the timer status to stopped
            timerStatus = TimerStatus.STOPPED;
            stopCountDownTimer();
        }
    }

    /**
     * method to initialize the values for count down timer
     */
    private void setTimerValues() {

        // assigning values after converting to milliseconds
        timeCountInMilliSeconds = 30000;
    }

    /**
     * method to start count down timer
     *
     * //@param progressBarCircle
     * @param textViewTime
     */
    private void startCountDownTimer(/*final ProgressBar progressBarCircle, */final TextView textViewTime) {

        countDownTimer = new CountDownTimer(timeCountInMilliSeconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                if (!isActivityAlive) return;
                textViewTime.setText(hmsTimeFormatter(millisUntilFinished));
                //progressBarCircle.setProgress((int) (millisUntilFinished / 1000));

            }

            @Override
            public void onFinish() {
                if (!isActivityAlive) return;
                textViewTime.setText(hmsTimeFormatter(timeCountInMilliSeconds));
                // call to initialize the progress bar values
                //setProgressBarValues(progressBarCircle);
                // hiding the reset icon
                // changing the timer status to stopped
                timerStatus = TimerStatus.STOPPED;
                removeSentRequests();
                bar.setVisibility(View.GONE);
                toolbarTitle.setText(R.string.home);
                requestedTutorsList.clear();
                showNoTutorFoundDialog();
            }

        }.start();
        countDownTimer.start();
    }

    private void removeSentRequests() {
        if (requestedTutorsList.size() > 0) {
            for (int i = 0; i < requestedTutorsList.size(); i++) {
                JSONObject jsonObject = requestedTutorsList.get(i);
                String userId = jsonObject.optJSONObject("details").optString("id");
                int total_request_count = jsonObject.optInt("total_request_count");
                if (total_request_count!=0){
                    //referenceTutors.child(userId+"/current_requests/"+current_request_key).setValue(null);
                    //referenceTutors.child(userId + "/total_request_count").setValue(total_request_count -1);
                    referenceTutors.child(userId+"/current_requests/").setValue(null);
                    referenceTutors.child(userId + "/total_request_count").setValue(0);
                    Log.e("TAG","Requests removed : " + userId);
                }
            }
        }
    }

    /**
     * method to stop count down timer
     */
    private void stopCountDownTimer() {
        countDownTimer.cancel();
    }

    /**
     * method to set circular progress bar values
     *
     * @param progressBarCircle
     */
    private void setProgressBarValues(ProgressBar progressBarCircle) {

        progressBarCircle.setMax((int) timeCountInMilliSeconds / 1000);
        progressBarCircle.setProgress((int) timeCountInMilliSeconds / 1000);
    }

    /* @param milliSeconds
    * @return mm:ss time formatted string
    */
    private String hmsTimeFormatter(long milliSeconds) {

        String hms = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliSeconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));

        return hms;


    }

    public void onBackPressed() {
        handleBack();
    }

    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    private ChildEventListener currentRequestListener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String s) {
            if (dataSnapshot!=null && dataSnapshot.getValue() instanceof HashMap) {
                String key = dataSnapshot.getKey();
                HashMap hashMap = (HashMap) dataSnapshot.getValue();
                if (hashMap != null) {
                    Object currentRequestNode = hashMap.get("current_requests");
                    if (currentRequestNode != null) {
                        HashMap requestsMap = (HashMap) currentRequestNode;
                        Object userRequest = requestsMap.get(current_request_key);
                        if (userRequest != null) {
                            JSONObject jsonObject = new JSONObject(hashMap);
                            requestedTutorsList.add(jsonObject);
                        }
                    }
                }
            }
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {
            if (dataSnapshot!=null && dataSnapshot.getValue() instanceof HashMap) {
                String key = dataSnapshot.getKey();
                HashMap hashMap = (HashMap) dataSnapshot.getValue();
                if (hashMap != null) {
                    JSONObject jsonObject = new JSONObject(hashMap);
                    String id = jsonObject.optJSONObject("details").optString("id");
                    ArrayList<JSONObject> removableList = new ArrayList<>();
                    for (JSONObject o : requestedTutorsList) {
                        if (id.equalsIgnoreCase(o.optJSONObject("details").optString("id"))) {
                            removableList.add(o);
                            break;
                        }
                    }
                    requestedTutorsList.removeAll(removableList);
                    requestedTutorsList.add(jsonObject);
                }
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot!=null && dataSnapshot.getValue() instanceof HashMap) {
                HashMap hashMap = (HashMap) dataSnapshot.getValue();
                if (hashMap != null) {
                    Object currentRequestNode = hashMap.get("current_requests");
                    if (currentRequestNode != null) {
                        HashMap requestsMap = (HashMap) currentRequestNode;
                        Object userRequest = requestsMap.get(current_request_key);
                        if (userRequest != null) {
                            JSONObject details = (JSONObject) hashMap.get("details");
                            String snapShotUserId = details.optString("id");
                            for (JSONObject o : requestedTutorsList) {
                                if (snapShotUserId.equalsIgnoreCase(o.optJSONObject("details").optString("id"))) {
                                    requestedTutorsList.remove(o);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {}

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {}
    };

    private ChildEventListener requestAccept = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String s) {
            if (dataSnapshot != null) {
                String key = dataSnapshot.getKey();
                if (key == null) return;
                if (dataSnapshot.getKey().equalsIgnoreCase(current_request_key)) {

                    String tutor_id = "";
                    String tutor_name = "";

                    if (dataSnapshot.getValue() instanceof HashMap) {
                        HashMap hashMap = (HashMap) dataSnapshot.getValue();
                        if (hashMap == null) return;
                        tutor_id = "" + hashMap.get("tutor_id");
                        tutor_name = "" + hashMap.get("first_name") + " " + hashMap.get("last_name");

                        if (requestedTutorsList.size() > 0) {
                            for (int i = 0; i < requestedTutorsList.size(); i++) {
                                JSONObject jsonObject = requestedTutorsList.get(i);
                                String userId = jsonObject.optJSONObject("details").optString("id");
                                int total_request_count = jsonObject.optInt("total_request_count");
                                if (userId.equalsIgnoreCase(tutor_id)) {
                                    referenceTutors.child(userId + "/total_request_count").setValue(total_request_count);
                                } else {
                                    referenceTutors.child(userId + "/total_request_count").setValue(0);
                                }

                                //referenceTutors.child(userId+"/current_requests/"+current_request_key).setValue(null);
                                referenceTutors.child(userId + "/current_requests/" + current_request_key).setValue(null);
                            }
                            bar.setVisibility(View.GONE);
                            toolbarTitle.setText(R.string.home);
                            requestedTutorsList.clear();
                        }
                        startActivity(new Intent(SearchTutor_Activity.this, Chat_List_Activity.class).putExtra("key", current_request_key).putExtra("tutor_id", tutor_id).putExtra("name", tutor_name));
                        referenceTutors.removeEventListener(is_onlineListener);
                        referenceMyChatRooms.removeEventListener(requestAccept);
                        referenceTutors.removeEventListener(currentRequestListener);
                        finish();
                    }
                }
            }
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    private ChildEventListener is_onlineListener = new ChildEventListener() {
        @Override
        public void onChildAdded( DataSnapshot dataSnapshot, String s) {
            if (dataSnapshot != null && dataSnapshot.getValue() instanceof HashMap) {
                HashMap hashMap = (HashMap) dataSnapshot.getValue();
                if (hashMap != null) {

                    JSONObject jsonObject = new JSONObject(hashMap);
                    JSONObject details = jsonObject.optJSONObject("details");
                    if (details!=null){

                    }
                    if (!onlineTutorsList.contains(jsonObject)) {
                        onlineTutorsList.add(jsonObject);
                    }
                }
                    onlineTutorCount = onlineTutorsList.size();
                    total_tutor_txt.setText(String.format("%d Tutors online", onlineTutorCount));
            }
            tutorsLoaded = true;
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {
            if (dataSnapshot!=null){
                String key = dataSnapshot.getKey();
                if (dataSnapshot.getValue() instanceof HashMap){
                    HashMap hashMap = (HashMap) dataSnapshot.getValue();
                    if (hashMap!=null){
                        JSONObject json = new JSONObject(hashMap);
                        JSONObject temp=null;
                        for (JSONObject j: onlineTutorsList){
                            JSONObject details = j.optJSONObject("details");
                            if (details!=null){
                                String id = details.optString("id");
                                if (id.equals(key)){
                                    temp = j;
                                    break;
                                }
                            }
                        }
                        if (temp!=null){
                            onlineTutorsList.remove(temp);
                            onlineTutorsList.add(json);
                        }
                    }
                }
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot != null) {
                HashMap hashMap = (HashMap) dataSnapshot.getValue();
                if (hashMap!=null){
                    JSONObject jsonObject = new JSONObject(hashMap);
                    String tutorId = jsonObject.optJSONObject("details").optString("id");
                    for (JSONObject o : onlineTutorsList ){
                        String itemId = o.optJSONObject("details").optString("id");
                        if (itemId.equalsIgnoreCase(tutorId)){
                            onlineTutorsList.remove(o);
                            break;
                        }
                    }
                    onlineTutorCount = onlineTutorsList.size();
                    total_tutor_txt.setText(String.format("%d Tutors online", onlineTutorCount));
                }
            }
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String s) { }
        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {}
    };

    /**
     * call api for fetch message
     */
    public void fetchCurrentPlane() {
        if (NetworkUtil.isConnected(SearchTutor_Activity.this)) {

            try {
                GWProgressDialog.getProgressDialog(SearchTutor_Activity.this).showDialog1();
                AppUtils.getObj().hideSoftKeyboard(SearchTutor_Activity.this);

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.currentPlan(mPrefs.getString(mPrefs.KEY_USER_ID));

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");
                            JSONArray data = mJsonObj.optJSONArray("data");
                            JSONObject dataJsonObj = data.getJSONObject(0);

                            if (dataJsonObj.has("subscription_time")) {

                                String remTime = dataJsonObj.optString("subscription_time");
                                remainingSubscribedTime = remTime;
                                Long val=null;
                                try{
                                    val = Long.valueOf(remTime);
                                }catch (Exception e){e.printStackTrace();}

                                if (val!=null) {
                                    if (val <= 1) {
                                        remainingSubscribedTime = "0";
                                        total_min_txt.setText("00 h:" + " 00 m:" + " 00 s");
                                    } else {
                                        getPlanDurrention(remTime);
                                    }
                                }else{
                                    if (remTime.equalsIgnoreCase("0") || remTime.equalsIgnoreCase("1")) {
                                        remainingSubscribedTime = "0";
                                        total_min_txt.setText("00 h:"+" 00 m:"+" 00 s");
                                    }else {
                                        getPlanDurrention(remTime);
                                    }
                                }

                                timefetched = true;
                                dismissDialog();
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                     //GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(SearchTutor_Activity.this, networkCallBack2);
        }
    }

    private void getPlanDurrention(String endDate)
    {
        try {
            long fetchedTime = Long.parseLong(endDate);

            long seconds = fetchedTime % 60;
            long minutes = fetchedTime / 60;

            String displeMin = Chat_List_Activity.twoDigitString(minutes);
            String displaySec = Chat_List_Activity.twoDigitString(seconds);

            total_min_txt.setText(getString(R.string.timer_time,displeMin,displaySec));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    DialogInterface.OnClickListener networkCallBack2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            fetchCurrentPlane();
        }
    };


    AlertDialog noTutorDialog;
    private void showNoTutorFoundDialog() {
        if (!isActivityAlive)return;
        if (noTutorDialog!=null &&  noTutorDialog.isShowing()){return;}
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this,R.style.NoTutorDialogStyle);

        noTutorDialog = builder.setTitle("")
                .setMessage(R.string.message_no_tutor_found)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setCancelable(true)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create();

        noTutorDialog.show();

        noTutorDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (!isActivityAlive) return;
                courseName = "";
                courseId = "";
                toolbarTitle.setText("Home");
                seachtextiew.setVisibility(View.VISIBLE);
                total_tutor_txt.setVisibility(View.VISIBLE);
            }
        });

        noTutorDialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                //noTutorDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.status_bar_color));
                noTutorDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.bg_violet));
            }
        });
        noTutorDialog.show();

    }

    private void showNoMinutesDialog(){
        if (!isActivityAlive)return;
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this,R.style.NoTutorDialogStyle);

        AlertDialog dialog = builder.setTitle("")
                .setMessage(R.string.message_no_more_mins)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setCancelable(true)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create();

        dialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.bg_violet));
            }
        });
        dialog.show();
    }

    private void showTutorsLoadingDialog(){
        if (!isActivityAlive)return;
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this,R.style.NoTutorDialogStyle);

        AlertDialog dialog = builder.setTitle("")
                .setMessage(R.string.message_searching_tutors)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setCancelable(true)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create();

        dialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.bg_violet));
            }
        });
        dialog.show();
    }

}
