package com.george.way.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.george.way.R;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by JAI on 10/11/2017.
 */

public class Stripe_Payment_Activity extends AppCompatActivity {
    private CardInputWidget cardInputWidget;
    private Button savePayment;
    private Card card;
    private String plan_time="",plan_type = "";
    private String plan_id = "";
    private Double amount = 0.0;
    private int totalSecond = 0 ;
    private ImageView imageViewBack,imageViewCalculator;
    private TextView textViewHeader;
    private String email="";
    private TextView tvAskTutor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stripe_card_add_activity);
        initView();
        tvAskTutor = findViewById(R.id.ask_tutor);
        tvAskTutor.setVisibility(View.GONE);
        if (getIntent()!=null)
        {
            if (getIntent().getStringExtra("plan_time")!=null) {

                plan_time = getIntent().getStringExtra("plan_time");
                plan_type = getIntent().getStringExtra("plan_type");
                plan_id = getIntent().getStringExtra("id");
                amount  =  getIntent().getDoubleExtra("amount",0.0);
                totalSecond  =  getIntent().getIntExtra("time",0);
            }else {

                amount  =  getIntent().getDoubleExtra("donate",0.0);
                email   =  getIntent().getStringExtra("email");
            }
        }
    }

    private void initView() {
        cardInputWidget = (CardInputWidget) findViewById(R.id.card_input_widget);
        savePayment = (Button) findViewById(R.id.save_payment);
        textViewHeader = (TextView)findViewById(R.id.textViewHeader);
        imageViewBack = (ImageView) findViewById(R.id.imageViewBack);
        imageViewCalculator = (ImageView) findViewById(R.id.imageViewCalculator);
        textViewHeader.setText("Pay with Credit or Debit Card");

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewAnimUtils.activityExitTransitions(Stripe_Payment_Activity.this);
                finish();
            }
        });

        imageViewCalculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(Stripe_Payment_Activity.this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(Stripe_Payment_Activity.this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(Stripe_Payment_Activity.this);
            }
        });

        savePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    card = cardInputWidget.getCard();

                    if (card.validateNumber()) {
                        if (card.validateExpiryDate()) {
                            if (card.validateCVC()) {
                                GWProgressDialog.getProgressDialog(Stripe_Payment_Activity.this).showDialog1();

                                Stripe stripe = new Stripe(Stripe_Payment_Activity.this, "pk_live_rBEFkNjBE7Szz43pRwDslRVn");
                                stripe.createToken(card,new TokenCallback() {

                                            public void onSuccess(Token token) {
                                                // Send token to your server
                                    //            GWProgressDialog.getProgressDialog(Stripe_Payment_Activity.this).dismissDialog1();
                                                token1 = ""+token.getId();
                                                Log.e("Token "," print "+token.getId());
                                                if (!plan_type.isEmpty())
                                                {
                                                    updatePlanOnServerWebApi();
                                                }else
                                                {
                                                    updateDonateWebApi();
                                                }

                                            }

                                            public void onError(Exception error)

                                            {
                                                GWProgressDialog.getProgressDialog(Stripe_Payment_Activity.this).dismissDialog1();
                                                // Show localized error message
                                                Toast.makeText(Stripe_Payment_Activity.this,
                                                        error.getMessage(),
                                                        Toast.LENGTH_LONG
                                                ).show();
                                            }
                                        }
                                );
                            } else {
                                Snackbar.make(savePayment, "Please enter valid cvc no..", Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(savePayment, "Please enter valid expiry date.", Snackbar.LENGTH_LONG).show();
                        }
                    } else {
                        Snackbar.make(savePayment, "Please enter valid card no.", Snackbar.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
private String token1 = "";
    /**
     * call api
     * @param
     */
    public void updatePlanOnServerWebApi() {

        if (NetworkUtil.isConnected(Stripe_Payment_Activity.this)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(Stripe_Payment_Activity.this);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(Stripe_Payment_Activity.this).showDialog1();
                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(Stripe_Payment_Activity.this);
                String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);
                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.subscribePlanRequest(
                        userId,
                        plan_id,
                        "" + plan_time,""+amount,plan_type,token1,""+totalSecond);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(Stripe_Payment_Activity.this).dismissDialog1();

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");


                            if (!TextUtils.isEmpty(status_code)) {

                                if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {

                                    // SingleActionDialog.getDialogObj(mActivity).showDialogNormal(success_message);
                                    displayResultText("Your Plan has been subscribed successfully.Thanks for subscribing the plan.");

                                    try {
                                        String success_message = mJsonObj.optString("success_message");
                                        JSONArray dataJsonArr = mJsonObj.getJSONArray("data");
                                        JSONObject dataJsonObj = dataJsonArr.getJSONObject(0);
                                        String plan_start = dataJsonObj.optString("plan_start");
                                        String plan_end = dataJsonObj.optString("plan_end");



                                        /**
                                         * updating plan data
                                         */
                                        getHomeDataWebApi();


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } else {

                                    String error_message = mJsonObj.optString(AppConstant.getObj().ERROR_MESSAGE);
                                    new SingleActionDialog().showDialogNormal(error_message,Stripe_Payment_Activity.this,false);

                                }
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(Stripe_Payment_Activity.this).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(Stripe_Payment_Activity.this, networkCallBack2);
        }
    }
    DialogInterface.OnClickListener networkCallBack2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            updatePlanOnServerWebApi();
        }
    };
    protected void displayResultText(String result) {
        new SingleActionDialog().showDialogNormal(result,Stripe_Payment_Activity.this,true);
    }

    /**
     * call api
     */
    public void getHomeDataWebApi() {

        if (NetworkUtil.isConnected(Stripe_Payment_Activity.this)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(Stripe_Payment_Activity.this);

            try {


                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(Stripe_Payment_Activity.this);
                String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.getHomeDataRequest(userId);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {

                            ResponseBody mResponseBody = response.body();


                            if (mResponseBody != null) {

                                JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                                Log.e("tg", "response from server = " + mJsonObj.toString());
                                String status_code = mJsonObj.optString("status_code");
                                if (!TextUtils.isEmpty(status_code)) {

                                    if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {
                                        try {
                                            JSONArray dataJsonArr = mJsonObj.getJSONArray("data");
                                            JSONObject dataJsonObj = dataJsonArr.getJSONObject(0);

                                            PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(Stripe_Payment_Activity.this);

                                            try {
                                                String is_plan_active = dataJsonObj.optString("is_plan_active");
                                                String plan_id = dataJsonObj.optString("plan_id");
                                                String plan_name = dataJsonObj.optString("plan_name");
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_PLAN_ID, plan_id);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_SUBSCRIPTION_PLAN, plan_name);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_IS_PLAN_ACTIVE, is_plan_active);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                String isMathClass = dataJsonObj.optString("isMathClass");
                                                String isTestPreparation = dataJsonObj.optString("isTestPreparation");
                                                String isTutoringService = dataJsonObj.optString("isTutoringService");
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_MATH_CLASS, isMathClass);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_TEST_PREPARATION, isTestPreparation);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_TUTORING_SERVICE, isTutoringService);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(Stripe_Payment_Activity.this).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void updateDonateWebApi() {

        if (NetworkUtil.isConnected(Stripe_Payment_Activity.this)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(Stripe_Payment_Activity.this);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(Stripe_Payment_Activity.this).showDialog1();
                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(Stripe_Payment_Activity.this);
                String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);
                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.userDonate(email,""+amount*100,token1,"1");

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(Stripe_Payment_Activity.this).dismissDialog1();

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");


                            if (!TextUtils.isEmpty(status_code)) {

                                if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {

                                    // SingleActionDialog.getDialogObj(mActivity).showDialogNormal(success_message);
//                                    displayResultText("Your Plan has been subscribed successfully.Thanks for subscribing the plan.");

                                    try {



                                        /**
                                         * updating plan data
                                         */


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } else {

                                    String error_message = mJsonObj.optString(AppConstant.getObj().ERROR_MESSAGE);
                                    new SingleActionDialog().showDialogNormal(error_message,Stripe_Payment_Activity.this,false);

                                }
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(Stripe_Payment_Activity.this).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(Stripe_Payment_Activity.this, networkCallBack3);
        }
    }
    DialogInterface.OnClickListener networkCallBack3 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            updateDonateWebApi();
        }
    };
}
