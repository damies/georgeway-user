package com.george.way.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.george.way.R;
import com.george.way.adapter.PlansListingAdapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.model.packages.Datum;
import com.george.way.model.packages.PlanPackages;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SubscriptionActivity extends Activity implements View.OnClickListener {


    /**
     * pay pal configurations
     */
    // note that these credentials will differ between live & sandbox environments.
     private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    private PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(AppConstant.KEY_CLIENT_ID)
            .merchantName("GeorgeWay");

    /**
     * declare objects and views of xml of the class
     */
    private Activity mActivity;
     private static final int REQUEST_CODE_PAYMENT = 1;
    private TextView textViewHeader;
    private RecyclerView listViewPlans;
    private ImageView imageViewBack;
    private ArrayList<Datum> plansList;
    private Datum purchasedPlanDatum;
    private PlansListingAdapter mPlansListingAdapter;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;


    @BindView(R.id.app_bar)
    Toolbar app_bar;

    private String plan_time="",plan_type = "";
    private String plan_id = "";
    private Double amount = 0.0;
    private int totalSecond = 0;
    private Button subcategory_btn;

    private TextView tvAskTutor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_subscription_paypal);

        ButterKnife.bind(this);
        textViewHeader = (TextView) findViewById(R.id.textViewHeader);
        textViewHeader.setText(getResources().getString(R.string.Subscription_Plans));
        listViewPlans = (RecyclerView) findViewById(R.id.listViewPlans);
        imageViewBack = (ImageView) findViewById(R.id.imageViewBack);
        subcategory_btn = (Button)findViewById(R.id.subcategory_btn);
        tvAskTutor = findViewById(R.id.ask_tutor);

        tvAskTutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SubscriptionActivity.this,SearchTutor_Activity.class));
                ViewAnimUtils.activityEnterTransitions(SubscriptionActivity.this);
            }
        });

        mActivity = this;
        plansList = new ArrayList<>();


        /**
         * setting up plans adapter
         */
        /**
         * get plans packages
         */
        getPlanPackagesRequest();


        /**
         * configure plan service
         */
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        imageViewBack.setOnClickListener(this);

        subcategory_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPlansListingAdapter.mCallSubPlan();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();

        /**
         * update theme
         */
        /*try {
            ThemeUtils.getObj(mActivity).setUpTheme(app_bar, new ImageView[]{imageViewBack,imageViewCalculator},textViewHeader);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(this);
    }


    /**
     * get plans packages
     */
    public void getPlanPackagesRequest() {


        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<PlanPackages> call = mApiInterface.getPlansRequest();

                call.enqueue(new Callback<PlanPackages>() {
                    @Override
                    public void onResponse(Call<PlanPackages> call, Response<PlanPackages> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            PlanPackages mPlanPackages = response.body();
                            plansList.clear();
                            if (mPlanPackages != null && mPlanPackages.getData() != null && mPlanPackages.getData().size() > 0) {

                                for (int index = 0; index < mPlanPackages.getData().size(); index++) {
                                    plansList.add(mPlanPackages.getData().get(index));
                                }

                            }
                            /**
                             * setting up plan adapter
                             */
                            settingUpAdapter();

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<PlanPackages> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(getApplicationContext()).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }
    }

    DialogInterface.OnClickListener networkCallBack2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            getPlanPackagesRequest();
        }
    };


    /**
     * setting up adapter of plans
     */
    private void settingUpAdapter() {

        /**
         * setting up layout manager
         */

        try {
            PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(mActivity);
            String planId = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_PLAN_ID);
            if (!TextUtils.isEmpty(planId)) {
                for (int index = 0; index < plansList.size(); index++) {
                    if (planId.equalsIgnoreCase(plansList.get(index).getPakageid())){
                        plansList.get(index).setPlanSubscribed(true);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mPlansListingAdapter = new PlansListingAdapter(this, plansList);
        listViewPlans.setLayoutManager(linearLayoutManager);
        listViewPlans.setAdapter(mPlansListingAdapter);

    }


    public void onBuyPressed(Double planData, String plan_type, String plan_time, String plan_id, int totalSecond) {

        this.plan_type = plan_type;
        this.plan_time = plan_time;
        this.plan_id = plan_id;
        this.amount = planData;
        this.totalSecond = totalSecond;
        PayPalPayment thingToBuy = getStuffToBuy(PayPalPayment.PAYMENT_INTENT_ORDER,planData,plan_type);
        Intent intent = new Intent(SubscriptionActivity.this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);

    }

    /*
     * This method shows use of optional payment details and item list.
     */
    private PayPalPayment getStuffToBuy(String paymentIntent, Double aDouble, String plan_type) {
        Double amountTotal = 0.0;

        try {
            amountTotal = aDouble;
        } catch (Exception e) {
            e.printStackTrace();
        }

        PayPalItem[] items = {new PayPalItem(plan_type, 1, new BigDecimal(amountTotal), "USD",
                "")};
        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal("0");
        BigDecimal tax = new BigDecimal("0");
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        PayPalPayment payment = new PayPalPayment(amount, "USD", plan_type, paymentIntent);
        payment.items(items).paymentDetails(paymentDetails);
        payment.custom("");

        return payment;
    }


    protected void displayResultText(String result) {
        new SingleActionDialog().showDialogNormal(result,mActivity,false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i("tg", confirm.toJSONObject().toString(4));
                        Log.i("tg", confirm.getPayment().toJSONObject().toString(4));

                            updatePlanOnServerWebApi();


                    } catch (JSONException e) {

                        Toast.makeText(mActivity, "Sorry something went wrong.", Toast.LENGTH_SHORT).show();

                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {

                displayResultText("Payment process has been cancelled.");

            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {

                displayResultText("Invalid Payment details was submitted.");

            }
        }
    }


    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    /**
     * call api
     */
    public void updatePlanOnServerWebApi() {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();
                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);
                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.subscribePlanRequest(
                        userId,
                        plan_id,
                        "" + plan_time,""+amount,plan_type,"",""+totalSecond);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");


                            if (!TextUtils.isEmpty(status_code)) {

                                if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {

                                    // SingleActionDialog.getDialogObj(mActivity).showDialogNormal(success_message);
                                    displayResultText("Your Plan has been subscribed successfully.Thanks for subscribing the plan.");

                                    try {
                                        String success_message = mJsonObj.optString("success_message");
                                        JSONArray dataJsonArr = mJsonObj.getJSONArray("data");
                                        JSONObject dataJsonObj = dataJsonArr.getJSONObject(0);
//                                        String plan_start = dataJsonObj.optString("plan_start");
//                                        String plan_end = dataJsonObj.optString("plan_end");
//
//                                        purchasedPlanDatum.setPlanStartDate(plan_start);
//                                        purchasedPlanDatum.setPlanEndDate(plan_end);


                                        /**
                                         * updating plan data
                                         */
                                        getHomeDataWebApi();


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
//                                    purchasedPlanDatum.setPlanSubscribed(true);
//                                    mPlansListingAdapter.notifyDataSetChanged();

                                } else {

                                    String error_message = mJsonObj.optString(AppConstant.getObj().ERROR_MESSAGE);
                                    new SingleActionDialog().showDialogNormal(error_message,mActivity,false);

                                }
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())    {
            case R.id.imageViewBack:

                finish();
                ViewAnimUtils.activityExitTransitions(mActivity);

                break ;
            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);
                break;
        }
    }

    /**
     * call api
     */
    public void getHomeDataWebApi() {

        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {


                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.getHomeDataRequest(userId);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {

                            ResponseBody mResponseBody = response.body();


                            if (mResponseBody != null) {

                                JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                                Log.e("tg", "response from server = " + mJsonObj.toString());
                                String status_code = mJsonObj.optString("status_code");
                                if (!TextUtils.isEmpty(status_code)) {

                                    if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {
                                        try {
                                            JSONArray dataJsonArr = mJsonObj.getJSONArray("data");
                                            JSONObject dataJsonObj = dataJsonArr.getJSONObject(0);

                                           PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance(mActivity);

                                            try {
                                                String is_plan_active = dataJsonObj.optString("is_plan_active");
                                                String plan_id = dataJsonObj.optString("plan_id");
                                                String plan_name = dataJsonObj.optString("plan_name");
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_PLAN_ID, plan_id);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_SUBSCRIPTION_PLAN, plan_name);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_IS_PLAN_ACTIVE, is_plan_active);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                String isMathClass = dataJsonObj.optString("isMathClass");
                                                String isTestPreparation = dataJsonObj.optString("isTestPreparation");
                                                String isTutoringService = dataJsonObj.optString("isTutoringService");
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_MATH_CLASS, isMathClass);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_TEST_PREPARATION, isTestPreparation);
                                                preferenceUserUtil1.setPreferenceStringData(preferenceUserUtil1.KEY_TUTORING_SERVICE, isTutoringService);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



}
