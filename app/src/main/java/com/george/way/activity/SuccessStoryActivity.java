package com.george.way.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.adapter.SuccessStoryAdapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.model.successStory.Datum;
import com.george.way.model.successStory.SuccessStoryModel;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SuccessStoryActivity extends AppCompatActivity implements View.OnClickListener {


    @BindView(R.id.video_rv)
    RecyclerView mRecyclerViewStory;

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;

    @BindView(R.id.textViewHeader)
    TextView textViewHeader;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;

    @BindView(R.id.app_bar)
    Toolbar app_bar;

    @BindView(R.id.textViewNoFeedback)
    TextView textViewNoFeedback;


    private Activity mActivity;
    private SuccessStoryAdapter mSuccessStoryAdapter;
    private ArrayList<Datum> successStoryList;
    private TextView tvAskTutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_success_story);

        tvAskTutor = findViewById(R.id.ask_tutor);
        tvAskTutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SuccessStoryActivity.this, SearchTutor_Activity.class));
                ViewAnimUtils.activityEnterTransitions(SuccessStoryActivity.this);
            }
        });

        /**
         * bind views of xml
         */
        ButterKnife.bind(this);

        /**
         * init
         */
        initView();

        /**
         * setting up onclick events
         */
        setOnClickListener();

        /**
         * call api for getting success story from server
         */
        getSuccessStoryReqquest();

    }

    private void initView() {

        mActivity = this;
        successStoryList = new ArrayList<>();
        textViewHeader.setText(getResources().getString(R.string.Success_Story));

    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * update theme
         */
        /*try {
            Intent dataIntent = getIntent();
            if (dataIntent.hasExtra("fromFeedback")) {

                ThemeUtils.getObj(mActivity).setUpTheme(app_bar, new ImageView[]{imageViewBack, imageViewCalculator}, textViewHeader);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    /**
     * set on click event
     */
    public void setOnClickListener() {

        imageViewBack.setOnClickListener(this);

    }

    /**
     * setting up success story adapter
     */
    private void settingUpAdapter() {

        if (successStoryList.size() == 0) {
            textViewNoFeedback.setVisibility(View.VISIBLE);
            mRecyclerViewStory.setVisibility(View.GONE);
        } else {
            mRecyclerViewStory.setVisibility(View.VISIBLE);
            textViewNoFeedback.setVisibility(View.GONE);
        }

        mSuccessStoryAdapter = new SuccessStoryAdapter(mActivity, successStoryList);
        mRecyclerViewStory.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerViewStory.setAdapter(mSuccessStoryAdapter);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewBack:
                finish();
                ViewAnimUtils.activityExitTransitions(this);
                break;
            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(this);
    }


    /**
     * get list of success story from server
     */
    public void getSuccessStoryReqquest() {


        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<SuccessStoryModel> call = mApiInterface.fetchSucessStoryRequest();

                call.enqueue(new Callback<SuccessStoryModel>() {
                    @Override
                    public void onResponse(Call<SuccessStoryModel> call, Response<SuccessStoryModel> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            SuccessStoryModel mSuccessStoryModel = response.body();

                            successStoryList.clear();
                            if (mSuccessStoryModel != null && mSuccessStoryModel.getData() != null && mSuccessStoryModel.getData().size() > 0) {

                                for (int index = 0; index < mSuccessStoryModel.getData().size(); index++) {

                                    successStoryList.add(mSuccessStoryModel.getData().get(index));

                                }
                                /**
                                 * setting up success story adapter
                                 */
                                settingUpAdapter();


                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<SuccessStoryModel> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(getApplicationContext()).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack2);
        }

    }


    DialogInterface.OnClickListener networkCallBack2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            getSuccessStoryReqquest();
        }
    };

}

