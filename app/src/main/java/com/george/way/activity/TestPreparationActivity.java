package com.george.way.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.adapter.TestPreparationAdapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.model.TestPreparationResponse;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestPreparationActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView testpreparateRv;
    TestPreparationAdapter testPreparationAdapter;
    ImageView imageViewBack;
    TextView textViewHeader;

    @BindView(R.id.app_bar)
    Toolbar app_bar;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;

    @BindView(R.id.textViewNoFeedback)
    TextView textViewNoFeedback;
    private TextView tvAskTutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_preparation_avtivity);
        initView();
        tvAskTutor = (TextView) findViewById(R.id.ask_tutor);
        tvAskTutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TestPreparationActivity.this,SearchTutor_Activity.class));
                ViewAnimUtils.activityEnterTransitions(TestPreparationActivity.this);
            }
        });

        /**
         * bind views of xml
         */
        ButterKnife.bind(this);

    }


    @Override
    protected void onResume() {
        super.onResume();

        /**
         * update theme
         */
        /*try {
            ThemeUtils.getObj(this).setUpTheme(app_bar, new ImageView[]{imageViewBack,imageViewCalculator},textViewHeader);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    private void initView() {

        testpreparateRv = (RecyclerView) findViewById(R.id.testpreparate_rv);
        imageViewBack=(ImageView)findViewById(R.id.imageViewBack);
        textViewHeader=(TextView) findViewById(R.id.textViewHeader);
        testpreparateRv.setLayoutManager(new LinearLayoutManager(this));


        testPreparation();
        setOnClickListener();

         PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(this);
        String text = preferenceUserUtil.getString(preferenceUserUtil.KEY_TP_CONTENT);
        if (TextUtils.isEmpty(text)) {
            textViewHeader.setText(getResources().getString(R.string.Test_Preparation));
        } else {
            textViewHeader.setText(text);
        }
        
    }


    public void setOnClickListener(){
        imageViewBack.setOnClickListener(this);
    }


    public void testPreparation() {


        if (NetworkUtil.isConnected(TestPreparationActivity.this)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(TestPreparationActivity.this);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(TestPreparationActivity.this).showDialog1();

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<TestPreparationResponse> call = mApiInterface.testPreprationRequest();

                call.enqueue(new Callback<TestPreparationResponse>() {
                    @Override
                    public void onResponse(Call<TestPreparationResponse> call, Response<TestPreparationResponse> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(TestPreparationActivity.this).dismissDialog1();

                            TestPreparationResponse mPreparationResponse = response.body();

                            if (mPreparationResponse != null && mPreparationResponse.getData() != null && mPreparationResponse.getData().size() > 0)
                                {

                                    AdapterCall(mPreparationResponse);
                                }else{
                                    textViewNoFeedback.setVisibility(View.VISIBLE);
                                    testpreparateRv.setVisibility(View.GONE);

                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                            textViewNoFeedback.setVisibility(View.VISIBLE);
                            testpreparateRv.setVisibility(View.GONE);

                        }

                    }

                    @Override
                    public void onFailure(Call<TestPreparationResponse> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(getApplicationContext()).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(TestPreparationActivity.this, networkCallBack2);
        }

    }

    public void AdapterCall(TestPreparationResponse mPreparationResponse) {


        if(mPreparationResponse!=null && mPreparationResponse.getData()!=null
                && mPreparationResponse.getData().size()>0){

            testPreparationAdapter = new TestPreparationAdapter(this, mPreparationResponse);
            testpreparateRv.setAdapter(testPreparationAdapter);
            textViewNoFeedback.setVisibility(View.GONE);
            testpreparateRv.setVisibility(View.VISIBLE);

        }else {
            textViewNoFeedback.setVisibility(View.VISIBLE);
            testpreparateRv.setVisibility(View.GONE);

        }
      }

    DialogInterface.OnClickListener networkCallBack2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            testPreparation();
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageViewBack:
                finish();
                ViewAnimUtils.activityExitTransitions(this);
                break;
            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(this);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(this);
    }
}
