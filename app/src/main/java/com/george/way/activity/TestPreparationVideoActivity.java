package com.george.way.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.adapter.TestPreparationVideoAdapter;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.model.TestPreparationResponse;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TestPreparationVideoActivity extends AppCompatActivity implements View.OnClickListener{
    private RecyclerView videoRv;
    Bundle bundle;
    TestPreparationResponse.Datum mDatum;
    TestPreparationVideoAdapter mTestPreparationVideoAdapter;
    ImageView imageViewBack;
    TextView textViewHeader;

    @BindView(R.id.app_bar)
    Toolbar app_bar;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;
    private TextView tvAskTutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_preparation_video);
        bundle=getIntent().getExtras();
        if(bundle!=null){
            mDatum= (TestPreparationResponse.Datum) bundle.getSerializable("video_dataArr");
        }

        tvAskTutor = findViewById(R.id.ask_tutor);
        tvAskTutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TestPreparationVideoActivity.this,SearchTutor_Activity.class));
                ViewAnimUtils.activityEnterTransitions(TestPreparationVideoActivity.this);
            }
        });
        initView();


        /**
         * bind views of xml
         */
        ButterKnife.bind(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * update theme
         */
        /*try {
            ThemeUtils.getObj(this).setUpTheme(app_bar, new ImageView[]{imageViewBack,imageViewCalculator},textViewHeader);
        } catch (Exception e) {
            e.printStackTrace();
        }
*/
    }

    private void initView() {

        imageViewBack=(ImageView)findViewById(R.id.imageViewBack);
        textViewHeader=(TextView) findViewById(R.id.textViewHeader);
        videoRv = (RecyclerView) findViewById(R.id.video_rv);
        videoRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mTestPreparationVideoAdapter=new TestPreparationVideoAdapter(this,  mDatum.getVideos());
        videoRv.setAdapter(mTestPreparationVideoAdapter);
        setOnClickListener();

        if(mDatum!=null){
            textViewHeader.setText(mDatum.getCatName());
        }

    }
    public void setOnClickListener(){
        imageViewBack.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imageViewBack:
                finish();
                ViewAnimUtils.activityExitTransitions(this);
                break;
            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(this);

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(this);
    }
}
