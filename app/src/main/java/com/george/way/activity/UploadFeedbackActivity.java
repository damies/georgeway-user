package com.george.way.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.calcy.CalculatorActivityGB;
import com.george.way.calcy.CalculatorActivityL;
import com.george.way.calcy.Utils;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.GWProgressDialog;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadFeedbackActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;

    @BindView(R.id.textViewHeader)
    TextView textViewHeader;

    @BindView(R.id.linearLayoutSend)
    LinearLayout linearLayoutSend;

    @BindView(R.id.linearLayoutSendBtn)
    LinearLayout linearLayoutSendBtn;

    @BindView(R.id.editTextFeedback)
    EditText editTextFeedback;
    private Activity mActivity;

    @BindView(R.id.app_bar)
    Toolbar app_bar;

    @BindView(R.id.imageViewCalculator)
    ImageView imageViewCalculator;
    private TextView tvAskTutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_upload_feedback);
        tvAskTutor = findViewById(R.id.ask_tutor);
        tvAskTutor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UploadFeedbackActivity.this,SearchTutor_Activity.class));
                ViewAnimUtils.activityEnterTransitions(UploadFeedbackActivity.this);
            }
        });
        /**
         * init
         */
        ButterKnife.bind(this);

        /**
         * init
         */
        init();

        /**
         * set on click events
         */
        setOnClickEvents();


    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * update theme
         */
       /* try {
            ThemeUtils.getObj(mActivity).setUpTheme(app_bar, new ImageView[]{imageViewBack,imageViewCalculator},textViewHeader);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }
    /**
     * setting up onclick on views
     */
    private void setOnClickEvents() {

        imageViewBack.setOnClickListener(this);
        linearLayoutSendBtn.setOnClickListener(this);
    }


    /**
     * initialize class
     */
    private void init() {

         mActivity = this;
         textViewHeader.setText(getResources().getString(R.string.Feedback));


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ViewAnimUtils.activityExitTransitions(mActivity);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageViewBack:
                finish();
                ViewAnimUtils.activityExitTransitions(mActivity);
                break;

            case R.id.linearLayoutSendBtn:

                String feedBackMsg = editTextFeedback.getText().toString();
                if (TextUtils.isEmpty(feedBackMsg)) {
                    new SingleActionDialog().showDialogNormal(getResources().getString(R.string.Feedback_message),mActivity,false);
                } else {
                    updateFeedBacksRequest(feedBackMsg);
                }

                break;

            case R.id.imageViewCalculator:

                /**
                 * open calculator screen
                 */
                Intent calculatorIntent = null;
                if (Utils.hasLollipop()) {
                    calculatorIntent = new Intent(this, CalculatorActivityL.class);
                } else {
                    calculatorIntent = new Intent(this, CalculatorActivityGB.class);
                }
                calculatorIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(calculatorIntent);
                ViewAnimUtils.activityEnterTransitions(mActivity);

                break;
        }
    }

    /**
     * update feedBacks
     */
    public void updateFeedBacksRequest(String feedBackMsg) {


        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();


                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);
                String emailId = preferenceUserUtil.getString(preferenceUserUtil.KEY_EMAIL);


                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.submitFeedBacksRequest(
                        userId,
                        emailId,
                        feedBackMsg);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            ResponseBody mResponseBody = response.body();
                            JSONObject mJsonObj = new JSONObject(new String(mResponseBody.bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String status_code = mJsonObj.optString("status_code");


                            if (!TextUtils.isEmpty(status_code)) {

                                if (status_code.equalsIgnoreCase(AppConstant.getObj().STATUS_CODE_SUCCESS)) {

                                    new SingleActionDialog().showDialogNormal("Thanks for your valuable feedback.",mActivity,true);

                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack3);
        }
    }

    DialogInterface.OnClickListener networkCallBack3 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            String feedBackMsg = editTextFeedback.getText().toString();
            updateFeedBacksRequest(feedBackMsg);
        }
    };


}
