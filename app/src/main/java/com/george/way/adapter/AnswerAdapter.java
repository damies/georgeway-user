package com.george.way.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.george.way.R;
import com.george.way.webApi.ApiClient;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

/**
 * Created by shachindrap on 5/1/2017.
 */

public class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.CourseHolder> {

    private ArrayList<String> answerOptionList;
    private Context mContext;
    public ArrayList<String> selectedAnswer=new ArrayList<>();
    public ArrayList<String> selectedAnswer_=new ArrayList<>();
    private   DisplayImageOptions mDisplayImageOptions;
    char x='a';
    public AnswerAdapter(Context context, ArrayList<String> answerOptionList) {
        this.answerOptionList = answerOptionList;
        this.mContext = context;
       mDisplayImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.no_media)
                .showImageForEmptyUri(R.drawable.no_media)
                .showImageOnFail(R.drawable.no_media)
                .resetViewBeforeLoading(false)
                .delayBeforeLoading(1000)
                .cacheInMemory(false)
                .cacheOnDisk(false)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new SimpleBitmapDisplayer())
                .handler(new Handler())
                .build();


    }

    @Override
    public CourseHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_answer_row_selector, viewGroup, false);
        CourseHolder mh = new CourseHolder(v);

        return mh;
    }

    @Override
    public void onBindViewHolder(final CourseHolder holder,  int position) {
        holder.answerCh.setTag(position);
        if(answerOptionList.get(position).contains(".png")||answerOptionList.get(position).contains(".jpg")){
            ImageLoader.getInstance().displayImage(ApiClient.BASE_URL + answerOptionList.get(position), holder.answer_img, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    holder.progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    holder.progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    holder.progressBar.setVisibility(View.GONE);

                }
            });
            holder.answer_img.setVisibility(View.VISIBLE);
        }else{
            holder.answer_img.setVisibility(View.GONE);
            holder.answerCh.setText(answerOptionList.get(position));
        }
        selectedAnswer_.add(x+"");
        x++;
        holder.answerCh.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int pos=(int)buttonView.getTag();
                if(selectedAnswer.contains(selectedAnswer_.get(pos))){
                    selectedAnswer.remove(selectedAnswer_.get(pos));
                }else{
                    selectedAnswer.add(selectedAnswer_.get(pos));
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return answerOptionList.size();
    }


    public class CourseHolder extends RecyclerView.ViewHolder {

        private CheckBox answerCh;
        ImageView answer_img;
        private ProgressBar progressBar;
        public CourseHolder(View view) {
            super(view);
            answerCh = (CheckBox) view.findViewById(R.id.answer_ch);
            answer_img=(ImageView)view.findViewById(R.id.answer_img);
            progressBar=(ProgressBar)view.findViewById(R.id.progressBar);
        }


    }
}
