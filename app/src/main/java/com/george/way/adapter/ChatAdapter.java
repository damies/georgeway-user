package com.george.way.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.activity.OnlineTutoringActivity;
import com.george.way.activity.TouchImageActivity;
import com.george.way.model.FetchChatResponse;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.GeorgeWayApplication;
import com.george.way.webApi.ApiClient;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by sachin on 3/16/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    public List<FetchChatResponse.Datum> messageList = new ArrayList<>();
    Context mContext;
    DisplayImageOptions options, options1;
    PreferenceUserUtil preferenceUserUtil;
    AppUtils appUtils;
    private boolean flag = false;

    public ChatAdapter(Context context, List<FetchChatResponse.Datum> messageList) {
        this.messageList = messageList;
        mContext = context;
        preferenceUserUtil = PreferenceUserUtil.getInstance((OnlineTutoringActivity) mContext);
        appUtils= AppUtils.getObj();

    }

    // Create new views
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_chat_row, parent, false);


        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int pos) {

        if (messageList.get(pos).getUserId1().equalsIgnoreCase(preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID))) {

               holder.clientRowDateOut.setText(appUtils.ChangeDateFormat(messageList.get(pos).getDatetime(), "yyyy-MM-dd hh:mm:ss", "hh:mm a"));
              if(!messageList.get(pos).getImage().equalsIgnoreCase("")) {
                  ImageLoader.getInstance().displayImage(ApiClient.BASE_URL + messageList.get(pos).getImage(), holder.clientRowImgOut, new ImageLoadingListener() {
                      @Override
                      public void onLoadingStarted(String imageUri, View view) {

                      }

                      @Override
                      public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                          holder.clientRowImgOut.setImageResource(R.drawable.no_images);
                      }

                      @Override
                      public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                      }

                      @Override
                      public void onLoadingCancelled(String imageUri, View view) {

                      }
                  });
                  holder.clientRowMsgOut.setVisibility(View.GONE);
                  holder.clientRowImgOut.setVisibility(View.VISIBLE);
              }else{
                  holder.clientRowMsgOut.setText(messageList.get(pos).getMessage());
                  holder.clientRowMsgOut.setVisibility(View.VISIBLE);
                  holder.clientRowImgOut.setVisibility(View.GONE);
              }
            holder.chatRowsLinerOut.setVisibility(View.VISIBLE);
            holder.chatRowsLinerIn.setVisibility(View.GONE);

            try {
                String image = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_IMAGE);
                if(!TextUtils.isEmpty(image)) {
                    ImageLoader.getInstance().displayImage(ApiClient.BASE_URL+image, holder.userImg, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            holder.progressBarUserImg.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            holder.progressBarUserImg.setVisibility(View.GONE);
                            holder.userImg.setImageResource(R.drawable.user);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.progressBarUserImg.setVisibility(View.GONE);

                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            holder.progressBarUserImg.setVisibility(View.GONE);
                            holder.userImg.setImageResource(R.drawable.user);

                        }
                    });

                }else {
                    holder.progressBarUserImg.setVisibility(View.GONE);
                    holder.userImg.setImageResource(R.drawable.user);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            if(!messageList.get(pos).getImage().equalsIgnoreCase("")) {
                ImageLoader.getInstance().displayImage(ApiClient.BASE_URL + messageList.get(pos).getImage(), holder.clientRowImgIn, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        holder.clientRowImgIn.setImageResource(R.drawable.no_images);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {}

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {

                    }
                });
                holder.clientRowMsgIn.setVisibility(View.GONE);
                holder.clientRowImgIn.setVisibility(View.VISIBLE);

            }else{

                holder.clientRowMsgIn.setText(messageList.get(pos).getMessage());
                holder.clientRowMsgIn.setVisibility(View.VISIBLE);
                holder.clientRowImgIn.setVisibility(View.GONE);

            }


            holder.clientRowMsgIn.setText(messageList.get(pos).getMessage());
            holder.clientRowDateIn.setText(appUtils.ChangeDateFormat(messageList.get(pos).getDatetime(), "yyyy-MM-dd hh:mm:ss", "hh:mm a"));
            holder.chatRowsLinerOut.setVisibility(View.GONE);
            holder.chatRowsLinerIn.setVisibility(View.VISIBLE);

        }
        holder.itemView.setTag(pos);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos =(int)view.getTag();
                if(!messageList.get(pos).getImage().equalsIgnoreCase("")){

                    Intent i=new Intent(mContext, TouchImageActivity.class);
                    i.putExtra("message_img",ApiClient.BASE_URL + messageList.get(pos).getImage());
                    mContext.startActivity(i);

                }

            }
        });

        if (flag)
        {
            holder.chat_rows_liner_in_checkbox.setVisibility(View.VISIBLE);
            holder.chat_rows_liner_out_checkbox.setVisibility(View.VISIBLE);
        }else
        {
            holder.chat_rows_liner_in_checkbox.setVisibility(View.GONE);
            holder.chat_rows_liner_out_checkbox.setVisibility(View.GONE);
        }

        holder.chat_rows_liner_out_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                {
                    GeorgeWayApplication.msg_id.add(messageList.get(pos).getId());
                }else
                {
                    GeorgeWayApplication.msg_id.remove(messageList.get(pos).getId());
                }
            }
        });

        holder.chat_rows_liner_in_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                {
                    GeorgeWayApplication.msg_id.add(messageList.get(pos).getId());
                }else
                {
                    GeorgeWayApplication.msg_id.remove(messageList.get(pos).getId());
                }
            }
        });

    }


    // Return the size arraylist
    @Override
    public int getItemCount() {
        return messageList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView chatTvDate;
        private LinearLayout chatRowsLinerIn;
        private CircleImageView partnerImg;
        private LinearLayout ChatRowIn;
        private TextView clientRowMsgIn;
        private TextView clientRowDateIn;
        private LinearLayout chatRowsLinerOut;
        private LinearLayout ChatRowOut;
        private TextView clientRowMsgOut;
        private ImageView clientRowImgOut, clientRowImgIn;
        private TextView clientRowDateOut;
        private CircleImageView userImg;
        private ProgressBar progressBarUserImg;
        private CheckBox chat_rows_liner_out_checkbox, chat_rows_liner_in_checkbox;


        ViewHolder(View convertView) {
            super(convertView);
            // create ViewHolder
            chatTvDate = (TextView) convertView.findViewById(R.id.chat_tv_date);
            chatRowsLinerIn = (LinearLayout) convertView.findViewById(R.id.chat_rows_liner_in);
            partnerImg = (CircleImageView) convertView.findViewById(R.id.partner_img);
            ChatRowIn = (LinearLayout) convertView.findViewById(R.id.ChatRow_in);
            clientRowMsgIn = (TextView) convertView.findViewById(R.id.client_row_msg_in);
            clientRowDateIn = (TextView) convertView.findViewById(R.id.client_row_date_in);
            chatRowsLinerOut = (LinearLayout) convertView.findViewById(R.id.chat_rows_liner_out);
            ChatRowOut = (LinearLayout) convertView.findViewById(R.id.ChatRow_out);
            clientRowMsgOut = (TextView) convertView.findViewById(R.id.client_row_msg_out);
            clientRowImgOut = (ImageView) convertView.findViewById(R.id.client_row_img_out);
            clientRowImgIn = (ImageView) convertView.findViewById(R.id.client_row_img_in);

            clientRowDateOut = (TextView) convertView.findViewById(R.id.client_row_date_out);
            userImg = (CircleImageView) convertView.findViewById(R.id.user_img);
            progressBarUserImg = (ProgressBar)convertView.findViewById(R.id.progressBarUserImg);
            chat_rows_liner_out_checkbox = (CheckBox)convertView.findViewById(R.id.chat_rows_liner_out_checkbox);
            chat_rows_liner_in_checkbox  = (CheckBox)convertView.findViewById(R.id.chat_rows_liner_in_checkbox);

        }

    }

    public void setMessageList(List<FetchChatResponse.Datum> messageList  ) {
        this.messageList = messageList;
    }

    public void setCheckBox(boolean box)
    {
        flag = box;
        notifyDataSetChanged();
    }

}
