package com.george.way.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.activity.FullImage_Activity;
import com.george.way.calling.SinchService;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.GeorgeWayApplication;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by JAI on 1/7/2018.
 */

public class Chat_Adapter extends BaseAdapter {
    private final PreferenceUserUtil preferenceUserUtil;
    private AppCompatActivity appCompatActivity;
    private LayoutInflater layoutInflater;
    private RelativeLayout messageMeLayout;
    private LinearLayout content;
    private TextView txtContent;
    private ImageView chatSdkImage;
    private LinearLayout extraLayout;
    private TextView txtTime;
    private RelativeLayout messageFrindLayout;
    private LinearLayout firndcontent;
    private TextView friendTxtContent;
    private ImageView friendchatSdkImage;
    private LinearLayout friendextraLayout;
    FrameLayout root;
    private TextView friendTxtTime;
    private ArrayList<JSONObject> jsonObjects;
    public static String replyPrefix;
    ListView listView;


    public Chat_Adapter(AppCompatActivity appCompatActivity, ArrayList<JSONObject> jsonObjects, ListView listView) {
        this.appCompatActivity = appCompatActivity;
        this.listView = listView;
        this.jsonObjects = jsonObjects;
        layoutInflater = (LayoutInflater) appCompatActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        preferenceUserUtil = PreferenceUserUtil.getInstance(appCompatActivity);
    }

    public View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition ) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    @Override
    public int getCount() {
        return jsonObjects.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void markUnSelected(){
        if (selectedMessagePositionForReply>-1) {
            FrameLayout r = (FrameLayout) getViewByPosition(selectedMessagePositionForReply, listView);
            if (r != null) {
                r.setForeground(null);
            }
            selectedMessagePositionForReply = -1;
        }
    }

    public static int selectedMessagePositionForReply=-1;
    private void getLongClickReplMessage(JSONObject parentMsg) {
        String result = "\"On ";
        result += AppUtils.getTimeStr(parentMsg.optLong("timestamp"));
        result += " ";
        result += SinchService.callerDisplayName;
        result += " ";
        result += "sent";
        result += " ";
        String msgType = parentMsg.optString("type");
        if (msgType.equalsIgnoreCase("m")){
            result += "Media Item";
        }else{
            result += parentMsg.optString("msg");
        }
        result += "\"\n\n";
        replyPrefix = result;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        view = layoutInflater.inflate(R.layout.chat_row_view, null);
        final View ref = view;
        initView(view);

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (selectedMessagePositionForReply>-1){
                    FrameLayout selectedItem;
                    try{
                        selectedItem = (FrameLayout) getViewByPosition(selectedMessagePositionForReply,listView);
                        if (selectedItem!=null){
                            selectedItem.setForeground(null);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                if (selectedMessagePositionForReply!=i){
                    selectedMessagePositionForReply=i;
                    FrameLayout currentView;
                    try{
                        currentView = (FrameLayout) ref;
                        currentView.setForeground(new ColorDrawable(ContextCompat.getColor(appCompatActivity, R.color.homeLayoutColor)));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else{
                    selectedMessagePositionForReply = -1;
                }
                getLongClickReplMessage(jsonObjects.get(i));
                return true;
            }
        });

        if (selectedMessagePositionForReply==i){
            FrameLayout currentView;
            try{
                currentView = (FrameLayout) ref;
                currentView.setForeground(new ColorDrawable(ContextCompat.getColor(appCompatActivity, R.color.homeLayoutColor)));
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        try {
            if (preferenceUserUtil.getString(PreferenceUserUtil.getInstance(appCompatActivity).KEY_USER_ID).equalsIgnoreCase(jsonObjects.get(i).getString("sender_id")))
            {
                messageMeLayout.setVisibility(View.VISIBLE);
                messageFrindLayout.setVisibility(View.GONE);
                if (jsonObjects.get(i).optString("type").equalsIgnoreCase("M"))
                {
                    txtContent.setVisibility(View.GONE);
                    chatSdkImage.setVisibility(View.VISIBLE);
                    ImageLoader.getInstance().displayImage(jsonObjects.get(i).optString("media"),chatSdkImage, GeorgeWayApplication.intitOptions1());
                }else
                {
                    txtContent.setVisibility(View.VISIBLE);
                    chatSdkImage.setVisibility(View.GONE);
                }
                txtContent.setText(jsonObjects.get(i).optString("msg"));
                txtTime.setText(AppUtils.dateTime(jsonObjects.get(i).getLong("timestamp")));
            }else
            {
                messageMeLayout.setVisibility(View.GONE);
                messageFrindLayout.setVisibility(View.VISIBLE);
                if (jsonObjects.get(i).optString("type").equalsIgnoreCase("M"))
                {
                    friendTxtContent.setVisibility(View.GONE);
                    friendchatSdkImage.setVisibility(View.VISIBLE);
                    ImageLoader.getInstance().displayImage(jsonObjects.get(i).optString("media"),friendchatSdkImage, GeorgeWayApplication.intitOptions1());
                }else
                {
                    friendTxtContent.setVisibility(View.VISIBLE);
                    friendchatSdkImage.setVisibility(View.GONE);
                }
                friendTxtContent.setText(jsonObjects.get(i).optString("msg"));
                friendTxtTime.setText(AppUtils.dateTime(jsonObjects.get(i).getLong("timestamp")));
            }
            chatSdkImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appCompatActivity.startActivity(new Intent(appCompatActivity, FullImage_Activity.class).putExtra("path",jsonObjects.get(i).optString("media")));
                }
            });

            friendchatSdkImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    appCompatActivity.startActivity(new Intent(appCompatActivity, FullImage_Activity.class).putExtra("path",jsonObjects.get(i).optString("media")));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void initView(View view) {
        root = (FrameLayout) view;
        messageMeLayout = (RelativeLayout) view.findViewById(R.id.message_me_layout);
        content = (LinearLayout)  view.findViewById(R.id.content);
        txtContent = (TextView)  view.findViewById(R.id.txt_content);
        chatSdkImage = (ImageView)  view.findViewById(R.id.chat_sdk_image);
        extraLayout = (LinearLayout)  view.findViewById(R.id.extra_layout);
        txtTime = (TextView)  view.findViewById(R.id.txt_time);
        messageFrindLayout = (RelativeLayout)  view.findViewById(R.id.message_frind_layout);
        firndcontent = (LinearLayout)  view.findViewById(R.id.firndcontent);
        friendTxtContent = (TextView)  view.findViewById(R.id.friend_txt_content);
        friendchatSdkImage = (ImageView)  view.findViewById(R.id.friendchat_sdk_image);
        friendextraLayout = (LinearLayout)  view.findViewById(R.id.friendextra_layout);
        friendTxtTime = (TextView)  view.findViewById(R.id.friend_txt_time);
    }

    public void updateChatList(JSONObject chatdata)
    {
        jsonObjects.add(chatdata);
        notifyDataSetChanged();
    }
}
