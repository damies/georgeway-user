package com.george.way.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.model.feedbacks.Datum;
import com.george.way.utils.AppUtils;
import com.george.way.webApi.ApiClient;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FeedbackListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Datum> mFeedbackDataList;


    public FeedbackListAdapter(Context mContext, ArrayList<Datum> mFeedbackDataList) {
        this.mContext = mContext;
        this.mFeedbackDataList = mFeedbackDataList;


    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


       /* if (convertView == null) {

            holder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row_feedbacks, parent, false);
            convertView.setTag(holder);
            holder.textViewName = (TextView) convertView.findViewById(R.id.textViewName);
            holder.textViewFeedback = (TextView) convertView.findViewById(R.id.textViewFeedback);
            holder.mCircleImageViewProfile = (CircleImageView) convertView.findViewById(R.id.mCircleImageViewProfile);
            holder.progressBarImg = (ProgressBar) convertView.findViewById(R.id.progressBarImg);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }
*/
        final ViewHolder holder = new ViewHolder();
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row_feedbacks, parent, false);
        convertView.setTag(holder);
        holder.textViewName = (TextView) convertView.findViewById(R.id.textViewName);
        holder.textViewFeedback = (TextView) convertView.findViewById(R.id.textViewFeedback);
        holder.mCircleImageViewProfile = (CircleImageView) convertView.findViewById(R.id.mCircleImageViewProfile);
        holder.progressBarImg = (ProgressBar) convertView.findViewById(R.id.progressBarImg);

        holder.feedBackTime = (TextView) convertView.findViewById(R.id.feedBackTime);
        try {

            Datum datumFeedback = mFeedbackDataList.get(position);
            holder.textViewName.setText(datumFeedback.getFirst_name() + " " + datumFeedback.getLast_name());
            holder.textViewFeedback.setText(datumFeedback.getMessage());
            holder.feedBackTime.setText(AppUtils.getObj().ChangeDateFormat(datumFeedback.getDatetime(), "yyyy-MM-dd hh:mm:ss", "dd-MM-yyyy hh:mm a"));

            try {
                if (!TextUtils.isEmpty(datumFeedback.getImage())) {
                    String imageUrl = ApiClient.BASE_URL + datumFeedback.getImage();
                    ImageLoader.getInstance().displayImage(imageUrl, holder.mCircleImageViewProfile, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            holder.progressBarImg.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            holder.progressBarImg.setVisibility(View.GONE);
                            holder.mCircleImageViewProfile.setImageResource(R.drawable.user);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.progressBarImg.setVisibility(View.GONE);

                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            holder.progressBarImg.setVisibility(View.GONE);
                            holder.mCircleImageViewProfile.setImageResource(R.drawable.user);

                        }
                    });
                } else {

                    holder.progressBarImg.setVisibility(View.GONE);
                    holder.mCircleImageViewProfile.setImageResource(R.drawable.user);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }


    @Override
    public int getCount() {

        return mFeedbackDataList.size();

    }

    @Override
    public Object getItem(int listRowPosition) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    static class ViewHolder {

        TextView textViewName, textViewFeedback,feedBackTime;
        private CircleImageView mCircleImageViewProfile;
        private ProgressBar progressBarImg;


    }

}
