package com.george.way.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.activity.GeorgeMathQuizActivity;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.model.category.ChildSubCategory;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.ThemeUtils;

import java.util.List;

public class GMCChildCategoryAdapter extends BaseAdapter {

    private Context mContext;
    private List<ChildSubCategory> childCatList;


    public GMCChildCategoryAdapter(Context context, List<ChildSubCategory> childCatList) {
        mContext = context;
        this.childCatList = childCatList;

    }

    @Override
    public int getCount() {

        return childCatList.size();

    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
         LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.subcategorylist, null);

        }

        try {

            TextView subcategory_tv = (TextView) convertView.findViewById(R.id.subcategory_tv);
            RelativeLayout relLayoutCat = (RelativeLayout)  convertView.findViewById(R.id.relLayoutCat);


            try {
                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance((Activity) mContext);
                String COLOR_TYPE = preferenceUserUtil.getString(preferenceUserUtil.KEY_SELECTED_COLOR_TYPE);
                try {
                    GradientDrawable shape = (GradientDrawable) relLayoutCat.getBackground();
                    if(COLOR_TYPE.equalsIgnoreCase(ThemeUtils.COLOR_TYPE_GREEN)){

                        shape.setColor(AppUtils.getObj().getColor(mContext,R.color.home_red));
                        subcategory_tv.setTextColor(AppUtils.getObj().getColor(mContext,R.color.colorWhite));

                    }else   if(COLOR_TYPE.equalsIgnoreCase(ThemeUtils.COLOR_TYPE_YELLOW)){
                        subcategory_tv.setTextColor(AppUtils.getObj().getColor(mContext,R.color.colorWhite));

                        shape.setColor(AppUtils.getObj().getColor(mContext,R.color.classGreenColor));
                    }else {
                        shape.setColor(AppUtils.getObj().getColor(mContext,R.color.colorWhite));
                        subcategory_tv.setTextColor(Color.BLACK);

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            ChildSubCategory mChildSubCategory= childCatList.get(position);
            subcategory_tv.setText(mChildSubCategory.getName());


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent mIntent = new Intent(mContext, GeorgeMathQuizActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mIntent.putExtra(GeorgeMathQuizActivity.KEY_CHILD_DATA,childCatList.get(position));
                    mContext.startActivity(mIntent);
                    ViewAnimUtils.activityEnterTransitions((Activity) mContext);


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }
}