package com.george.way.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.activity.GMCSubCategoryActivity;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.model.category.SubCategory;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.ThemeUtils;

import java.util.ArrayList;
import java.util.List;

public class GMCMainCategoryAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    private ArrayList<SubCategory> categoryDataList;
    private ArrayList<SubCategory> TempcategoryDataList;
    private  ItemFilter itemFilter = new ItemFilter();
    public GMCMainCategoryAdapter(Context context, ArrayList<SubCategory> categoryDataList) {
        mContext = context;
        this.categoryDataList = categoryDataList;
        this.TempcategoryDataList = categoryDataList;
    }

    @Override
    public int getCount() {
        return categoryDataList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.subcategorylist, null);

        }

        TextView subcategory_tv = (TextView) convertView.findViewById(R.id.subcategory_tv);
        RelativeLayout relLayoutCat =  (RelativeLayout) convertView.findViewById(R.id.relLayoutCat);
        ImageView imagecategory = (ImageView)convertView.findViewById(R.id.imagecategory);

        imagecategory.setVisibility(View.VISIBLE);

        try {
            final SubCategory mSubCategory = categoryDataList.get(position);
            subcategory_tv.setText(mSubCategory.getName());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (mSubCategory != null
                                && mSubCategory.getChildCategory() != null
                                && mSubCategory.getChildCategory().size() > 0) {


                            try {
                                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance((Activity) mContext);
                                if(!((position %2 )== 0))
                                {
                                    preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_SELECTED_COLOR_TYPE, ThemeUtils.getObj((Activity) mContext).COLOR_TYPE_GREEN);
                                }else {
                                    preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_SELECTED_COLOR_TYPE, ThemeUtils.getObj((Activity) mContext).COLOR_TYPE_YELLOW);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            Intent mIntent = new Intent(mContext, GMCSubCategoryActivity.class);
                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            mIntent.putExtra(GMCSubCategoryActivity.KEY_PARENT_DATA, mSubCategory);
                            mContext.startActivity(mIntent);
                            ViewAnimUtils.activityEnterTransitions((Activity) mContext);

                        }
                    } catch (
                            Exception e)

                    {
                        e.printStackTrace();
                    }


                }
            });
        } catch (
                Exception e)

        {
            e.printStackTrace();
        }

        if(!((position %2 )== 0)){

            try {
                GradientDrawable shape = (GradientDrawable) relLayoutCat.getBackground();
                shape.setColor(AppUtils.getObj().getColor(mContext,R.color.colorWhite));
                subcategory_tv.setTextColor(AppUtils.getObj().getColor(mContext,R.color.home_red));

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }else {

            try {
                GradientDrawable shape = (GradientDrawable) relLayoutCat.getBackground();
                shape.setColor(AppUtils.getObj().getColor(mContext,R.color.colorWhite));
                subcategory_tv.setTextColor(AppUtils.getObj().getColor(mContext,R.color.classGreenColor));

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return itemFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<SubCategory> list = TempcategoryDataList;

            int count = list.size();
            final ArrayList<SubCategory> nlist = new ArrayList<SubCategory>(count);
            String filterableString ;
            for (SubCategory subCategory:TempcategoryDataList) {
                if (subCategory.getName().toLowerCase().contains(filterString.toLowerCase())) {
                    nlist.add(subCategory);
                }
            }



            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            categoryDataList = (ArrayList<SubCategory>) results.values;
            notifyDataSetChanged();
        }

    }

}