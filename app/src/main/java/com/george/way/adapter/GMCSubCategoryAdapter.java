package com.george.way.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.activity.GeorgeMathQuizActivity;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.model.category.ChildCategory;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.ThemeUtils;

import java.util.ArrayList;
import java.util.List;

public class GMCSubCategoryAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    private List<ChildCategory> childDataList;
    private Boolean aBoolean = true;
    private List<ChildCategory> TempcategoryDataList;

    public GMCSubCategoryAdapter(Context context, List<ChildCategory> childDataList) {
        mContext = context;
        this.childDataList = childDataList;
        this.TempcategoryDataList = childDataList;
    }

    @Override
    public int getCount() {
        return childDataList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.subcategorylist, null);

        }

        TextView subcategory_tv = (TextView) convertView.findViewById(R.id.subcategory_tv);
        RelativeLayout relLayoutCat = (RelativeLayout) convertView.findViewById(R.id.relLayoutCat);
        final LinearLayout childLinerLayout = (LinearLayout) convertView.findViewById(R.id.childLinerLayout);
        final ImageView imagecategory = (ImageView) convertView.findViewById(R.id.imagecategory);
        imagecategory.setVisibility(View.VISIBLE);
        try {
            PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance((Activity) mContext);
            String COLOR_TYPE = preferenceUserUtil.getString(preferenceUserUtil.KEY_SELECTED_COLOR_TYPE);
            try {
                GradientDrawable shape = (GradientDrawable) relLayoutCat.getBackground();
                if (COLOR_TYPE.equalsIgnoreCase(ThemeUtils.COLOR_TYPE_GREEN)) {

                    shape.setColor(AppUtils.getObj().getColor(mContext, R.color.colorWhite));
                    subcategory_tv.setTextColor(AppUtils.getObj().getColor(mContext, R.color.home_red));


                } else if (COLOR_TYPE.equalsIgnoreCase(ThemeUtils.COLOR_TYPE_YELLOW)) {

                    shape.setColor(AppUtils.getObj().getColor(mContext, R.color.colorWhite));
                    subcategory_tv.setTextColor(AppUtils.getObj().getColor(mContext, R.color.classGreenColor));

                } else {
                    shape.setColor(AppUtils.getObj().getColor(mContext, R.color.colorWhite));
                    subcategory_tv.setTextColor(Color.BLACK);

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            final ChildCategory mChildCategory = childDataList.get(position);
            subcategory_tv.setText(mChildCategory.getName());


            subcategory_tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        childLinerLayout.removeAllViews();
                        if (childDataList.size() > 0) {
                            for (int index = 0; index < mChildCategory.getChildSubCategory().size(); index++) {
                                View view = LayoutInflater.from(mContext).inflate(R.layout.child_item_view, null);
                                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance((Activity) mContext);
                                String COLOR_TYPE = preferenceUserUtil.getString(preferenceUserUtil.KEY_SELECTED_COLOR_TYPE);
                                TextView subcategory_txt = (TextView) view.findViewById(R.id.subcategory_txt);

                                if (COLOR_TYPE.equalsIgnoreCase(ThemeUtils.COLOR_TYPE_YELLOW)) {
                                    subcategory_txt.setTextColor(AppUtils.getObj().getColor(mContext, R.color.classGreenColor));
                                } else {
                                    subcategory_txt.setTextColor(AppUtils.getObj().getColor(mContext, R.color.home_red));
                                }

                                subcategory_txt.setText(mChildCategory.getChildSubCategory().get(index).getName());

                                final int finalIndex = index;
                                subcategory_txt.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent mIntent = new Intent(mContext, GeorgeMathQuizActivity.class);
                                        mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        mIntent.putExtra(GeorgeMathQuizActivity.KEY_CHILD_DATA, mChildCategory.getChildSubCategory().get(finalIndex));
                                        mContext.startActivity(mIntent);
                                        ViewAnimUtils.activityEnterTransitions((Activity) mContext);

                                    }
                                });


                                childLinerLayout.addView(view);
                            }
                        }
                        if (aBoolean) {
                            imagecategory.setVisibility(View.VISIBLE);
                            childLinerLayout.setVisibility(View.VISIBLE);
                            aBoolean = false;
                        } else {
                            imagecategory.setVisibility(View.VISIBLE);
                            childLinerLayout.setVisibility(View.GONE);
                            aBoolean = true;
                        }
//                            if(mChildCategory!=null && mChildCategory.getChildSubCategory()!=null&& mChildCategory.getChildSubCategory().size()>0){
//                                Intent mIntent = new Intent(mContext, GMCChildCategoryActivity.class);
//                                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                mIntent.putExtra(GMCChildCategoryActivity.KEY_CHILD_DATA, mChildCategory);
//                                mContext.startActivity(mIntent);
//                                ViewAnimUtils.activityEnterTransitions((Activity) mContext);
//                            }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return itemFilter;
    }

    private ItemFilter itemFilter = new ItemFilter();

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<ChildCategory> list = TempcategoryDataList;

            int count = list.size();
            final ArrayList<ChildCategory> nlist = new ArrayList<ChildCategory>(count);
            String filterableString;
            for (ChildCategory subCategory : TempcategoryDataList) {
                if (subCategory.getName().toLowerCase().contains(filterString.toLowerCase())) {
                    nlist.add(subCategory);
                }
            }


            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            childDataList = (ArrayList<ChildCategory>) results.values;
            notifyDataSetChanged();
        }

    }

}