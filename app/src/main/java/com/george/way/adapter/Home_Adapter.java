package com.george.way.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.activity.Chat_List_Activity;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by JAI on 1/7/2018.
 */

public class Home_Adapter extends BaseAdapter {
    private AppCompatActivity appCompatActivity;
    private LayoutInflater layoutInflater;
    private ArrayList<JSONObject> jsonObjects;
    private CircleImageView userImageView;
    private TextView userMsgTxt;
    private TextView userMsgTimeTxt,userName_txt;
    private ImageView newmsg;
    private DatabaseReference databaseReference;
    PreferenceUserUtil msPreferenceUserUtil;


    public Home_Adapter(AppCompatActivity appCompatActivity, ArrayList<JSONObject> jsonObjects) {
        this.jsonObjects = jsonObjects;
        this.appCompatActivity = appCompatActivity;
        msPreferenceUserUtil = PreferenceUserUtil.getInstance(appCompatActivity);
        layoutInflater = (LayoutInflater) appCompatActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        databaseReference = FirebaseDatabase.getInstance().getReference("users/"+msPreferenceUserUtil.getString(PreferenceUserUtil.getInstance(appCompatActivity).KEY_USER_ID)).child("chat_rooms");
    }

    @Override
    public int getCount() {
        return jsonObjects.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        view = layoutInflater.inflate(R.layout.home_item_view, null);

        initView(view);

        try {
            if (jsonObjects.get(i).optString("is_read").equalsIgnoreCase("true"))
            {
                newmsg.setVisibility(View.GONE);
            }else
            {
                newmsg.setVisibility(View.VISIBLE);
            }
            userName_txt.setText(jsonObjects.get(i).optString("first_name")+" "+jsonObjects.get(i).optString("last_name"));
            userMsgTxt.setText(jsonObjects.get(i).optJSONObject("last_msg").optString("msg"));
            userMsgTimeTxt.setText(AppUtils.dateTime(jsonObjects.get(i).optJSONObject("last_msg").optLong("timestamp")));
            String imgUrl = jsonObjects.get(i).getString("image");
            if (TextUtils.isEmpty(imgUrl)) {
                ImageLoader.getInstance().displayImage(imgUrl, userImageView, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        userImageView.setImageResource(R.drawable.ic_person);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        userImageView.setImageBitmap(loadedImage);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        userImageView.setImageResource(R.drawable.ic_person);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    JSONObject tutorInfo = jsonObjects.get(i);
                    databaseReference.child(tutorInfo.getString("key")).child("is_read").setValue(""+true);
                    Intent intent = new Intent(appCompatActivity, Chat_List_Activity.class);
                    intent.putExtra("name",tutorInfo.optString("first_name")+" "+tutorInfo.optString("last_name"));
                    intent.putExtra("tutor_id",tutorInfo.optString("tutor_id"));
                    intent.putExtra("session",tutorInfo.optString("is_active"));
                    intent.putExtra("image",tutorInfo.optString("image"));
                    intent.putExtra("key",tutorInfo.optString("key"));

                    appCompatActivity.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return view;
    }

    public void updateList(JSONObject object) {
        jsonObjects.add(0,object);
        notifyDataSetChanged();

    }

    private void initView(View view) {
        userImageView = (CircleImageView) view.findViewById(R.id.userImageView);
        userMsgTxt = (TextView) view.findViewById(R.id.userMsg_Txt);
        userMsgTimeTxt = (TextView) view.findViewById(R.id.userMsg_time_txt);
        userName_txt = (TextView)view.findViewById(R.id.userName_txt);
        newmsg = (ImageView)view.findViewById(R.id.newmsg);
    }
}
