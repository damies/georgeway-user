package com.george.way.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.activity.Stripe_Payment_Activity;
import com.george.way.activity.SubscriptionActivity;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.model.packages.Datum;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.ViewUtils;

import java.util.ArrayList;

public class PlansListingAdapter extends RecyclerView.Adapter<PlansListingAdapter.PlansViewHolder> {

    private Activity mActivity;
    private ArrayList<Datum> plansList;
    private ArrayList<String> monthList;
    private SimpleSpinnerAdapter1 mSimpleSpinnerAdapter1;
    private BottomSheetDialog bottomSheetDialog,paymentBottomSheetDialog;
    private int position1 = 0;

    public PlansListingAdapter(Activity mActivity, ArrayList<Datum> plansList) {

        this.mActivity = mActivity;
        this.plansList = plansList;
        bottomSheetDialog = new BottomSheetDialog(mActivity);
        paymentBottomSheetDialog = new BottomSheetDialog(mActivity);

    }

    @Override
    public PlansViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_row_subscribe_plans, viewGroup, false);

        return new PlansViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlansViewHolder holder, final int position) {


        /**
         * setting up data into views
         */
        try {
            final Datum datum = plansList.get(position);
            holder.textViewPrice.setText("$" + datum.getPakageAmount());
            holder.textViewhrouPrice.setText("$" + datum.getHourly_pakage_amount());
            holder.textViewminPrice.setText("$" + datum.getMinutely_pakage_amount());
            holder.textViewDescription.setText(datum.getPackageDescription());
            holder.textViewTitle.setText(datum.getPakageName());
            if (TextUtils.isEmpty(datum.getPackageDescription())) {
                holder.linearLayoutDesc.setVisibility(View.GONE);
            } else {
                holder.linearLayoutDesc.setVisibility(View.VISIBLE);
            }

            openMonthSelectionDialog(datum);
            position1 = position;

            holder.des_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                    String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);

                    if (TextUtils.isEmpty(userId)) {

                        new SingleActionDialog().showDialogForLogin("Please login to subscribe plan.", mActivity);

                    } else {

                        /**
                         * first of all finish current activity , then start new activity
                         */
                        openMonthSelectionDialog(datum);
                        position1 = position;

                    }


                }
            });

            if (datum.isPlanSubscribed()) {

                holder.linearLayoutAlreadySubs.setVisibility(View.VISIBLE);
                holder.textViewBtnSubscribe.setVisibility(View.GONE);
                holder.linearLayoutEndDate.setVisibility(View.GONE);
                holder.linearLayoutStartDate.setVisibility(View.GONE);

            } else {

                holder.linearLayoutAlreadySubs.setVisibility(View.GONE);
                holder.textViewBtnSubscribe.setVisibility(View.GONE);
                holder.linearLayoutEndDate.setVisibility(View.GONE);
                holder.linearLayoutStartDate.setVisibility(View.GONE);

            }

            holder.textViewPlanStartDate.setText(datum.getPlanStartDate());
            holder.textViewPlanEndDate.setText(datum.getPlanEndDate());


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return plansList.size();
    }


    public class PlansViewHolder extends RecyclerView.ViewHolder {

        private TextView textViewBtnSubscribe, textViewPrice, textViewminPrice, textViewhrouPrice,
                textViewDescription, textViewTitle,
                textViewPlanStartDate,
                textViewPlanEndDate;
        private LinearLayout linearLayoutDesc,
                linearLayoutAlreadySubs,
                linearLayoutEndDate,des_layout,
                linearLayoutStartDate;

        public PlansViewHolder(View view) {
            super(view);

            textViewBtnSubscribe = (TextView) view.findViewById(R.id.textViewBtnSubscribe);
            textViewPrice = (TextView) view.findViewById(R.id.textViewPrice);
            textViewminPrice = (TextView) view.findViewById(R.id.textViewminPrice);
            textViewhrouPrice = (TextView) view.findViewById(R.id.textViewhrouPrice);
            textViewDescription = (TextView) view.findViewById(R.id.textViewDescription);
            textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
            textViewPlanStartDate = (TextView) view.findViewById(R.id.textViewPlanStartDate);
            textViewPlanEndDate = (TextView) view.findViewById(R.id.textViewPlanEndDate);

            linearLayoutDesc = (LinearLayout) view.findViewById(R.id.linearLayoutDesc);
            linearLayoutAlreadySubs = (LinearLayout) view.findViewById(R.id.linearLayoutAlreadySubs);
            linearLayoutEndDate = (LinearLayout) view.findViewById(R.id.linearLayoutEndDate);
            linearLayoutStartDate = (LinearLayout) view.findViewById(R.id.linearLayoutStartDate);
            des_layout = (LinearLayout)view.findViewById(R.id.des_layout);
        }

    }

    /**
     * open dialog for month selection
     */
    private EditText editTextView;
    private TextView title_template,cancelTextView,proceddTextView;
    private void openMonthSelectionDialog(final Datum plansDatum) {

        bottomSheetDialog.setContentView(R.layout.choose_plan_type);
        TextView mintueTextView = (TextView) bottomSheetDialog.findViewById(R.id.mintueTextView);
        TextView hourTextView   = (TextView) bottomSheetDialog.findViewById(R.id.hourTextView);
        TextView monthTextView  = (TextView) bottomSheetDialog.findViewById(R.id.monthTextView);
        TextView cancelTextView = (TextView) bottomSheetDialog.findViewById(R.id.cancelTextView);

        mintueTextView.setText("Mintue Plan  $"+plansDatum.getMinutely_pakage_amount()+"/min");
        hourTextView.setText("Hour Plan  $"+plansDatum.getHourly_pakage_amount()+"/hr");
        monthTextView.setText("Month Plan  $"+plansDatum.getPakageAmount()+"/month");
        bottomSheetDialog.show();

        mintueTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.cancel();
                openDialog("Enter number of mintues",Double.parseDouble(plansDatum.getMinutely_pakage_amount()),"Minutely");
            }
        });

        hourTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.cancel();
                openDialog("Enter number of hours",Double.parseDouble(plansDatum.getHourly_pakage_amount()),"Hourly");
            }
        });

        monthTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.cancel();
                openDialog("Enter number of months",Double.parseDouble(plansDatum.getHourly_pakage_amount()),"Monthly");
            }
        });

        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.cancel();
            }
        });
    }

    private void openDialog(String title, final double price, final String plan_type) {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(mActivity);
        final View promptsView = li.inflate(R.layout.alter_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        alertDialogBuilder.setView(promptsView);

        // create alert dialog
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);

        /**
         * setting up window design
         */
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window mWindow = alertDialog.getWindow();
        mWindow.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mWindow.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            mWindow.setDimAmount(0.6f);
        }
        mWindow.setGravity(Gravity.CENTER);

        // show it
        alertDialog.show();

        /**
         * setting up data in adapter
         */
        monthList = ViewUtils.getObj().getMonthList();
        proceddTextView = (TextView)promptsView.findViewById(R.id.proceddTextView);
        cancelTextView  = (TextView)promptsView.findViewById(R.id.cancelTextView);
        editTextView    =  (EditText) promptsView.findViewById(R.id.editTextView);
        title_template  =  (TextView) promptsView.findViewById(R.id.title_template);

        title_template.setText(title);
        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        proceddTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppUtils.getObj().hideSoftKeyboard(mActivity);
                if (!editTextView.getText().toString().isEmpty())
                {
                    alertDialog.dismiss();
                    int totalSecond = 0;
                    if(plan_type.equalsIgnoreCase("Minutely"))
                    {
                        totalSecond = 60*Integer.parseInt(editTextView.getText().toString());
                    }else if (plan_type.equalsIgnoreCase("Hourly"))
                    {
                        totalSecond = 60*60*Integer.parseInt(editTextView.getText().toString());
                    }else if (plan_type.equalsIgnoreCase("Monthly"))
                    {
                        totalSecond = 60*60*24*30*Integer.parseInt(editTextView.getText().toString());
                    }

                    openPaymetDialog(price*Double.parseDouble(editTextView.getText().toString()),plan_type,editTextView.getText().toString(),totalSecond);
                }else
                {
                    Snackbar.make(editTextView,"Please enter value",Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }
    private void openPaymetDialog(final Double amount, final String plan_type, final String plan_time, final int totalSecond) {

        paymentBottomSheetDialog.setContentView(R.layout.caomplete_action);
        TextView paypalButton = (TextView) paymentBottomSheetDialog.findViewById(R.id.paypalButton);
        TextView stripButton = (TextView) paymentBottomSheetDialog.findViewById(R.id.stripButton);
        TextView cancelButton = (TextView) paymentBottomSheetDialog.findViewById(R.id.cancelButton);
        TextView payAmount_txt = (TextView)paymentBottomSheetDialog.findViewById(R.id.payAmount_txt);

        payAmount_txt.setText("Pay $"+amount);


        paymentBottomSheetDialog.show();

        paypalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentBottomSheetDialog.cancel();
                ((SubscriptionActivity)mActivity).onBuyPressed(amount,plan_type,plan_time,plansList.get(position1).getPakageid(),totalSecond);

            }
        });

        stripButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentBottomSheetDialog.cancel();

                mActivity.startActivity(new Intent(mActivity, Stripe_Payment_Activity.class).putExtra("plan_type",plan_type).putExtra("plan_time",plan_time)
                        .putExtra("id",plansList.get(position1).getPakageid()).putExtra("amount",amount*100).putExtra("time",totalSecond));
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentBottomSheetDialog.cancel();
            }
        });

        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.cancel();
            }
        });
    }
    public void mCallSubPlan()
    {
        openMonthSelectionDialog(plansList.get(0));
    }
}
