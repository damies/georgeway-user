package com.george.way.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.model.reward.Datum;
import com.george.way.webApi.ApiClient;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by hemendrag on 7/8/2017.
 */

public class QODRewardAdapter extends RecyclerView.Adapter<QODRewardAdapter.CourseHolder> {

    private Context context;
    private List<Datum> rewardList;

    public QODRewardAdapter(Context applicationContext, List<Datum> rewardList) {
        this.context = applicationContext;
        this.rewardList = rewardList;

    }

    @Override
    public CourseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row_qod_reward, parent, false);
        CourseHolder mCourseHolder = new CourseHolder(v);
        return mCourseHolder;

    }

    @Override
    public void onBindViewHolder(final CourseHolder holder, int position) {


        try {

            Datum datumReward = rewardList.get(position);
            holder.textViewName.setText(datumReward.getFirstName() + " " + datumReward.getLastName());
            holder.textViewUser.setText(datumReward.getDeviceType() + " User");
            holder.textViewGiftGiven.setText(datumReward.getReward());
            try {

                if (!TextUtils.isEmpty(datumReward.getImage())) {
                    holder.imageView_prize.setVisibility(View.VISIBLE);
                    String imageUrl = ApiClient.BASE_URL + datumReward.getImage();
                    ImageLoader.getInstance().displayImage(imageUrl, holder.imageView_prize, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            holder.progressBarImgPrize.setVisibility(View.VISIBLE);
                            holder.imageView_prize.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            holder.progressBarImgPrize.setVisibility(View.GONE);
                            holder.imageView_prize.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.progressBarImgPrize.setVisibility(View.GONE);
                            holder.imageView_prize.setVisibility(View.VISIBLE);


                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            holder.progressBarImgPrize.setVisibility(View.GONE);
                            holder.imageView_prize.setVisibility(View.GONE);

                        }
                    });
                }else {
                    holder.imageView_prize.setVisibility(View.GONE);
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
            try {
                if (!TextUtils.isEmpty(datumReward.getUser_image())) {
                    String imageUrl = ApiClient.BASE_URL + datumReward.getUser_image();
                    ImageLoader.getInstance().displayImage(imageUrl, holder.mCircleImageViewProfile, new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            holder.progressBarImg.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            holder.progressBarImg.setVisibility(View.GONE);
                            holder.mCircleImageViewProfile.setImageResource(R.drawable.user);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.progressBarImg.setVisibility(View.GONE);

                        }

                        @Override
                        public void onLoadingCancelled(String imageUri, View view) {
                            holder.progressBarImg.setVisibility(View.GONE);
                            holder.mCircleImageViewProfile.setImageResource(R.drawable.user);

                        }
                    });
                } else {

                    holder.progressBarImg.setVisibility(View.GONE);
                    holder.mCircleImageViewProfile.setImageResource(R.drawable.user);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return rewardList.size();
    }

    public class CourseHolder extends RecyclerView.ViewHolder {

        ImageView imageView_prize;
        TextView textViewName, textViewUser, textViewGiftGiven;
        private CircleImageView mCircleImageViewProfile;
        private ProgressBar progressBarImg;
        private ProgressBar progressBarImgPrize;


        public CourseHolder(View view) {
            super(view);
            imageView_prize = (ImageView) view.findViewById(R.id.imageView_prize);
            textViewName = (TextView) view.findViewById(R.id.textViewName);
            textViewUser = (TextView) view.findViewById(R.id.textViewUser);
            textViewGiftGiven = (TextView) view.findViewById(R.id.textViewGiftGiven);
            mCircleImageViewProfile = (CircleImageView) view.findViewById(R.id.mCircleImageViewProfile);
            progressBarImg = (ProgressBar) view.findViewById(R.id.progressBarImg);
            progressBarImgPrize = (ProgressBar) view.findViewById(R.id.progressBarImgPrize);

        }


    }
}
