package com.george.way.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.utils.AppUtils;

import java.util.ArrayList;


/**
 * Created by hemendrag on 5/27/2016.
 */
public class SimpleSpinnerAdapter1 extends BaseAdapter {

    /******
     * view declaration
     *******/

    ArrayList<String> titleList;
    private Context mContext;
    private View convertView;

    /******
     * Constructor declaration
     *******/

    public SimpleSpinnerAdapter1(Activity mActivity, ArrayList<String> titleList) {

        mContext = mActivity;
        this.titleList = titleList;

    }


    @Override
    public View getView(final int position, View view, ViewGroup arg2) {


        /********************************************** INITIALIZATION ********************************************/
        convertView = view;

        ViewHolder mViewHolder = null;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        if (convertView == null) {

            convertView = inflater.inflate(R.layout.layout_row_months, null);
            mViewHolder = new ViewHolder();

            mViewHolder.mTextViewProviderTitle = (TextView) convertView.findViewById(R.id.title);

            convertView.setTag(mViewHolder);
            mViewHolder.mTextViewProviderTitle.setTag(mViewHolder);


        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }


        mViewHolder.mTextViewProviderTitle.setText(titleList.get(position));
        if(position==0){
            mViewHolder.mTextViewProviderTitle.setTextColor(AppUtils.getObj().getColor(mContext,R.color.viewBg));
        }


        return convertView;
    }


    @Override
    public int getCount() {
        return titleList.size();
    }

    @Override
    public Object getItem(int position) {
        return titleList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    /**
     * defining view holder
     */
    public class ViewHolder {

        private TextView mTextViewProviderTitle;

    }
}