package com.george.way.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.constant.AppConstant;
import com.george.way.jcVideoPlayer.JCVideoPlayerStandard;
import com.george.way.model.successStory.Datum;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.GetThumbNailServerAsync;

import java.util.List;

/**
 * Created by shachindrap on 5/2/2017.
 */

public class SuccessStoryAdapter extends RecyclerView.Adapter<SuccessStoryAdapter.CourseHolder> {

    private Context context;
    private List<Datum> videosList;

    public SuccessStoryAdapter(Context applicationContext, List<Datum> videosList) {
        this.context = applicationContext;
        this.videosList = videosList;

    }

    @Override
    public CourseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_test_prep_row, parent, false);
        CourseHolder mCourseHolder = new CourseHolder(v);
        return mCourseHolder;

    }

    @Override
    public void onBindViewHolder(CourseHolder holder, int position) {
        holder.mJcVideoPlayerSimple.setVisibility(View.VISIBLE);
        holder.linearLayoutVideo.setVisibility(View.VISIBLE);
        holder.textViewTitle.setVisibility(View.GONE);
        holder.subcategoryRl.setVisibility(View.GONE);


        try {

            String videoUrl = videosList.get(position).getVideo();
            videoUrl =  videoUrl.replaceAll("\\s+","%20");

            holder.mJcVideoPlayerSimple.setUp(ApiClient.BASE_URL + videoUrl, AppConstant.getObj().EMPTY_STRING);
            new GetThumbNailServerAsync((Activity) context, ApiClient.BASE_URL + videoUrl, holder.mJcVideoPlayerSimple, holder.mJcVideoPlayerSimple.ivThumb).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }    }

    @Override
    public int getItemCount() {
        return videosList.size();
    }

    public class CourseHolder extends RecyclerView.ViewHolder {

        TextView subcategory_tv,textViewTitle;
        JCVideoPlayerStandard mJcVideoPlayerSimple;
        RelativeLayout subcategoryRl;
        private LinearLayout linearLayoutVideo;

        public CourseHolder(View view) {
            super(view);
            subcategory_tv = (TextView) view.findViewById(R.id.subcategory_tv);
            mJcVideoPlayerSimple = (JCVideoPlayerStandard) view.findViewById(R.id.jCVideoPlayer_video);
            subcategoryRl=(RelativeLayout)view.findViewById(R.id.subcategory_rl);
            linearLayoutVideo = (LinearLayout)view.findViewById(R.id.linearLayoutVideo);
            textViewTitle = (TextView)view.findViewById(R.id.textViewTitle);

        }


    }
}
