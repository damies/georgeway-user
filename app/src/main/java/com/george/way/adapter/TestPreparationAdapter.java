package com.george.way.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.activity.TestPreparationVideoActivity;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.model.TestPreparationResponse;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;

/**
 * Created by shachindrap on 5/2/2017.
 */

public class TestPreparationAdapter extends RecyclerView.Adapter<TestPreparationAdapter.CourseHolder> {

    Context context;
    TestPreparationResponse mPreparationResponse;

    public TestPreparationAdapter(Context applicationContext, TestPreparationResponse mPreparationResponse) {
        this.context = applicationContext;
        this.mPreparationResponse = mPreparationResponse;
    }

    @Override
    public CourseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_test_prep_row, parent, false);
        CourseHolder mCourseHolder = new CourseHolder(v);
        return mCourseHolder;

    }

    @Override
    public void onBindViewHolder(CourseHolder holder, final int position) {
        holder.subcategory_tv.setTag(position);
        holder.subcategory_tv.setTextSize(18);
        holder.subcategory_tv.setText(mPreparationResponse.getData().get(position).getCatName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try {
                    PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance((Activity) context);
//                    if(!((position %2 )== 0)){
//                        preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_SELECTED_COLOR_TYPE, ThemeUtils.COLOR_TYPE_GREEN);
//                    }else {
//                        preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_SELECTED_COLOR_TYPE, ThemeUtils.COLOR_TYPE_YELLOW);
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(context, TestPreparationVideoActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("video_dataArr", mPreparationResponse.getData().get(position));
                intent.putExtras(bundle);
                context.startActivity(intent);
                ViewAnimUtils.activityEnterTransitions((Activity) context);
            }
        });

        holder.linearLayoutVideo.setVisibility(View.GONE);
        holder.subcategoryRl.setVisibility(View.VISIBLE);

        if(!((position %2 )== 0)){

            try {
                GradientDrawable shape = (GradientDrawable) holder.subcategoryRl.getBackground();
                shape.setColor(AppUtils.getObj().getColor(context, R.color.colorWhite));
                holder.subcategory_tv.setTextColor(AppUtils.getObj().getColor(context, R.color.classGreenColor));

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {

            try {
                GradientDrawable shape = (GradientDrawable) holder.subcategoryRl.getBackground();
                shape.setColor(AppUtils.getObj().getColor(context, R.color.colorWhite));
                holder.subcategory_tv.setTextColor(AppUtils.getObj().getColor(context, R.color.classYellowColor));

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

    }

    @Override
    public int getItemCount() {
        return mPreparationResponse.getData().size();
    }

    public class CourseHolder extends RecyclerView.ViewHolder {

        TextView subcategory_tv;
        RelativeLayout subcategoryRl;
        private LinearLayout linearLayoutVideo;

        public CourseHolder(View view) {
            super(view);
            subcategory_tv = (TextView) view.findViewById(R.id.subcategory_tv);
            subcategoryRl = (RelativeLayout) view.findViewById(R.id.subcategory_rl);
            linearLayoutVideo = (LinearLayout) view.findViewById(R.id.linearLayoutVideo);

        }


    }
}
