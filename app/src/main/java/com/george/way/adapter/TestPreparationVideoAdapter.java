package com.george.way.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.constant.AppConstant;
import com.george.way.dialog.SingleActionDialog;
import com.george.way.jcVideoPlayer.JCPlayerListener;
import com.george.way.jcVideoPlayer.JCVideoPlayer;
import com.george.way.jcVideoPlayer.JCVideoPlayerStandard;
import com.george.way.model.TestPreparationResponse;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.ThemeUtils;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.GetThumbNailServerAsync;

import java.util.List;

/**
 * Created by shachindrap on 5/2/2017.
 */

public class TestPreparationVideoAdapter extends RecyclerView.Adapter<TestPreparationVideoAdapter.CourseHolder> {

    Context context;
    List<TestPreparationResponse.Video> mPreparationResponse;


    public TestPreparationVideoAdapter(Context applicationContext, List<TestPreparationResponse.Video> videos) {
        this.context = applicationContext;
        this.mPreparationResponse = videos;

    }

    @Override
    public CourseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_test_prep_row, parent, false);
        CourseHolder mCourseHolder = new CourseHolder(v);
        return mCourseHolder;

    }

    @Override
    public void onBindViewHolder(final CourseHolder holder, int position) {


        try {
            String fileName = mPreparationResponse.get(position).getVideo().substring(mPreparationResponse.get(position).getVideo().lastIndexOf('/') + 1, mPreparationResponse.get(position).getVideo().length());
            String fileNameWithoutExtn = fileName.substring(0, fileName.lastIndexOf('.'));
            holder.textViewTitle.setText(fileNameWithoutExtn);

            ThemeUtils.getObj((Activity) context).updateTextViews(holder.textViewTitle);

            if (!TextUtils.isEmpty(fileNameWithoutExtn)) {

                holder.mJcVideoPlayerStandard.setVisibility(View.VISIBLE);
                holder.linearLayoutVideo.setVisibility(View.VISIBLE);
                holder.subcategoryRl.setVisibility(View.GONE);
                try {
                    String videoUrl = mPreparationResponse.get(position).getVideo();
                    videoUrl = videoUrl.replaceAll("\\s+", "%20");
                    holder.mJcVideoPlayerStandard.setUp(ApiClient.BASE_URL + videoUrl, AppConstant.getObj().EMPTY_STRING);
                    new GetThumbNailServerAsync((Activity) context, ApiClient.BASE_URL + videoUrl, holder.mJcVideoPlayerStandard, holder.mJcVideoPlayerStandard.ivThumb).execute();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


                holder.mJcVideoPlayerStandard.setOnJcClickListener(new JCPlayerListener() {
                    @Override
                    public void onVideoClickListener(JCVideoPlayer jcVideoPlayer) {

                        PreferenceUserUtil preferenceUserUtil1 = PreferenceUserUtil.getInstance((Activity) context);
                        String userId1 = preferenceUserUtil1.getString(preferenceUserUtil1.KEY_USER_ID);

                        if (TextUtils.isEmpty(userId1)) {

                            new SingleActionDialog().showDialogForLogin(context.getResources().getString(R.string.please_login_Test_Preparation), (Activity) context);

                        } else {

                            String planId = preferenceUserUtil1.getString(PreferenceUserUtil.getInstance((Activity) context).KEY_TEST_PREPARATION);

//                            if (!planId.isEmpty()) {
//                                if (planId.equalsIgnoreCase("0")) {
//                                    new SingleActionDialog().showDialogForSubscription(((Activity) context).getResources().getString(R.string.Subscription_requires), (Activity) context);
//                                } else {
                                    jcVideoPlayer.startVideo();
//                                }
//                            }else
//                            {
//                                new SingleActionDialog().showDialogForSubscription(((Activity) context).getResources().getString(R.string.Subscription_requires), (Activity) context);
//                            }


//                            if(TextUtils.isEmpty(planId))
//                            {
//                                new SingleActionDialog().showDialogForSubscription(((Activity) context).getResources().getString(R.string.Subscription_requires),(Activity) context);
//                            }else
//                                {
//                                jcVideoPlayer.startVideo();
//                            }

                        }

                    }
                });

            } else {

                holder.mJcVideoPlayerStandard.setVisibility(View.GONE);
                holder.linearLayoutVideo.setVisibility(View.GONE);
                holder.subcategoryRl.setVisibility(View.GONE);
            }

        } catch (Exception ex) {
            holder.mJcVideoPlayerStandard.setVisibility(View.GONE);
            holder.linearLayoutVideo.setVisibility(View.GONE);
            holder.subcategoryRl.setVisibility(View.GONE);
        }

        holder.setIsRecyclable(false);
    }

    @Override
    public int getItemCount() {
        return mPreparationResponse.size();
    }

    public class CourseHolder extends RecyclerView.ViewHolder {

        TextView subcategory_tv, textViewTitle;
        JCVideoPlayerStandard mJcVideoPlayerStandard;
        RelativeLayout subcategoryRl;
        private LinearLayout linearLayoutVideo;

        public CourseHolder(View view) {
            super(view);
            textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
            subcategory_tv = (TextView) view.findViewById(R.id.subcategory_tv);
            mJcVideoPlayerStandard = (JCVideoPlayerStandard) view.findViewById(R.id.jCVideoPlayer_video);
            subcategoryRl = (RelativeLayout) view.findViewById(R.id.subcategory_rl);
            linearLayoutVideo = (LinearLayout) view.findViewById(R.id.linearLayoutVideo);
        }


    }
}
