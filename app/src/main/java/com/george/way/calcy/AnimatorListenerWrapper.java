package com.george.way.calcy;

public interface AnimatorListenerWrapper {
    void onAnimationStart();
}
