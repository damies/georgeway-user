package com.george.way.calcy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class StartActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Utils.hasLollipop()) {
            startActivity(new Intent(this, CalculatorActivityL.class));
        } else {
            startActivity(new Intent(this, CalculatorActivityGB.class));
        }
        finish();
    }
}
