package com.george.way.calling;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.ClientRegistration;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;
import com.sinch.android.rtc.calling.CallClientListener;
import com.sinch.android.rtc.video.VideoController;

public class SinchService extends Service {

    private static final String APP_KEY = "2229a93e-57af-4ded-8083-963494de8970";
    private static final String APP_SECRET = "yFAG/bN7YEClSq0i+4XoMw==";
    private static final String ENVIRONMENT = "sandbox.sinch.com";

    public static final String CALL_ID = "CALL_ID";
    static final String TAG = SinchService.class.getSimpleName();
    public static String callerDisplayName;
    private SinchServiceInterface mSinchServiceInterface = new SinchServiceInterface();
    private SinchClient mSinchClient;
    private String mUserId;

    private StartFailedListener mListener;
    private PersistedSettings mSettings;

    @Override
    public void onCreate() {
        super.onCreate();
        mSettings = new PersistedSettings(getApplicationContext());
        String userName = mSettings.getUsername();
        if (!userName.isEmpty()) {
            start(userName);
        }
    }

    @Override
    public void onDestroy() {
        if (mSinchClient != null && mSinchClient.isStarted()) {
            mSinchClient.terminate();
        }
        super.onDestroy();
    }

    private void start(String userName) {
        if (mSinchClient == null) {
            mSettings.setUsername(userName);
            mUserId = userName;
            mSinchClient = Sinch.getSinchClientBuilder().context(getApplicationContext()).userId(userName)
                    .applicationKey(APP_KEY)
                    .applicationSecret(APP_SECRET)
                    .environmentHost(ENVIRONMENT).build();

            mSinchClient.setSupportCalling(true);
            mSinchClient.startListeningOnActiveConnection();

            mSinchClient.addSinchClientListener(new MySinchClientListener());
            mSinchClient.getCallClient().addCallClientListener(new SinchCallClientListener());
            mSinchClient.start();
        }
    }

    private void stop() {
        if (mSinchClient != null) {
            mSinchClient.terminate();
            mSinchClient = null;
        }
    }

    private boolean isStarted() {
        return (mSinchClient != null && mSinchClient.isStarted());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mSinchServiceInterface;
    }




    public class SinchServiceInterface extends Binder {

        public Call callPhoneNumber(String phoneNumber) {
            if (mSinchClient!=null){
                CallClient callClient = mSinchClient.getCallClient();
                if (callClient!=null){
                    return callClient.callPhoneNumber(phoneNumber);
                }else Log.d("SINCH_TAG", "Call Client found null");
            }else Log.d("SINCH_TAG", "Sinch Client found null, In callPhoneNumber");
            return null;
        }

        public Call callUser(String userId) {
            if (mSinchClient != null) {
                CallClient callClient = mSinchClient.getCallClient();
                if (callClient!=null){
                    return callClient.callUser(userId);
                }else Log.d("SINCH_TAG", "Call Client found null, In callUser");
            }else Log.d("SINCH_TAG", "Sinch Client found null, In callUser");
            return null;
        }

        public Call callUserVideo(String userId) {
            if (mSinchClient!=null){
                CallClient callClient = mSinchClient.getCallClient();
                if (callClient!=null){
                    return callClient.callUserVideo(userId);
                }else Log.d("SINCH_TAG", "Call Client found null, In callUserVideo");
            }else Log.d("SINCH_TAG", "Sinch Client found null, In callUserVideo");
            return null;
        }

        public String getUserName() {
            return mUserId;
        }

        public boolean isStarted() {
            return SinchService.this.isStarted();
        }

        public void startClient(String userName) {
            start(userName);
        }

        public void stopClient() {
            stop();
        }

        public void setStartListener(StartFailedListener listener) {
            mListener = listener;
        }

        public Call getCall(String callId) {
            if (mSinchClient!=null){
                CallClient callClient = mSinchClient.getCallClient();
                if (callClient!=null){
                    return callClient.getCall(callId);
                }else Log.d("SINCH_TAG", "Call Client found null, In getCall");
            }else Log.d("SINCH_TAG", "Sinch Client found null, In getCall");
            return null;
        }

        public VideoController getVideoController() {
            if (mSinchClient!=null && isStarted()) {
                return mSinchClient.getVideoController();
            }
            return null;
        }

        public AudioController getAudioController() {
            if (mSinchClient!=null && isStarted()) {
                return mSinchClient.getAudioController();
            }
            return null;
        }
    }




    public interface StartFailedListener {
        void onStartFailed(SinchError error);
        void onStarted();
    }

    private class MySinchClientListener implements SinchClientListener {

        @Override
        public void onClientFailed(SinchClient client, SinchError error) {
            if (mListener != null) {
                mListener.onStartFailed(error);
            }
            mSinchClient.terminate();
            mSinchClient = null;
        }

        @Override
        public void onClientStarted(SinchClient client) {
            Log.d(TAG, "SinchClient started");
            if (mListener != null) {
                mListener.onStarted();
            }
        }

        @Override
        public void onClientStopped(SinchClient client) {
            Log.d(TAG, "SinchClient stopped");
        }

        @Override
        public void onLogMessage(int level, String area, String message) {
            switch (level) {
                case Log.DEBUG:
                    Log.d(area, message);
                    break;
                case Log.ERROR:
                    Log.e(area, message);
                    break;
                case Log.INFO:
                    Log.i(area, message);
                    break;
                case Log.VERBOSE:
                    Log.v(area, message);
                    break;
                case Log.WARN:
                    Log.w(area, message);
                    break;
            }
        }

        @Override
        public void onRegistrationCredentialsRequired(SinchClient client,
                ClientRegistration clientRegistration) {
        }
    }

    private class SinchCallClientListener implements CallClientListener {

        @Override
        public void onIncomingCall(CallClient callClient, Call call) {
            Log.d(TAG, "Incoming call");
            Intent intent = new Intent(SinchService.this, IncomingCallScreenActivity.class);
            intent.putExtra(CALL_ID, call.getCallId());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            SinchService.this.startActivity(intent);
        }
    }

    private class PersistedSettings {

        private SharedPreferences mStore;

        private static final String PREF_KEY = "Sinch";

        public PersistedSettings(Context context) {
            mStore = context.getSharedPreferences(PREF_KEY, MODE_PRIVATE);
        }

        public String getUsername() {
            return mStore.getString("Username", "");
        }

        public void setUsername(String username) {
            SharedPreferences.Editor editor = mStore.edit();
            editor.putString("Username", username);
            editor.commit();
        }
    }
}
