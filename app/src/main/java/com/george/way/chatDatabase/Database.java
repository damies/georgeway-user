package com.george.way.chatDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.george.way.model.chat.MsgBean;

import java.util.ArrayList;

/**
 * Created by shachindrap on 8/9/2016.
 */
public class Database {
    private static final int DATABASE_VERSION = 1; // database version
    private static final String DATABASE_NAME = "Sociomx"; // database name
    private DatabaseHelper mDbHelper;
    public SQLiteDatabase mDb;
    private Context mContext;

    public Database(Context mCtx) {
        this.mContext = mCtx;
        mDbHelper = new DatabaseHelper(mCtx);
    }

    /*
    *  Tag use log
    * */
    private static final String TAG = "db";

    /*
    * Table for chat
    * */
    private static final String CHAT_TABLE = "chat_table";
    /*
    * Table for unread msg
    * */
    private static final String UNREAD_MESSAGE_COUNT_TABLE = "unread_message_count";

    /*
    *  last message chat  seen
    * */
    private static final String LAST_CHAT_TABLE = "last_chat_table";


    // auto increment id
    public static final String KEY_ID = "id";
    /**
     * *****media_table******
     */
    public static final String MEDIA_TABLE_URL = "url";
    public static final String MEDIA_TABLE_TYPE = "type";
    public static final String MEDIA_TABLE_DOWNLOAD_STATE = "download_state";
    public static final String MEDIA_TABLE_PATH = "path";

    /*
    *
    * chat table field
    * */

    /**
     * *****chat_table******
     */

    public static final String CHAT_TABLE_MESSAGE_ID = "message_id";
    public static final String CHAT_TABLE_SENDER_ID = "sender_id";
    public static final String CHAT_TABLE_SENDER_SINCH_ID = "sender_sinch_id";
    public static final String CHAT_TABLE_SENDER_NAME = "sender_name";
    public static final String CHAT_TABLE_SENDER_PROFILE_IMAGE = "sender_profile_image";

    public static final String CHAT_TABLE_RECEIVER_ID = "receiver_id";
    public static final String CHAT_TABLE_RECEIVER_SINCH_ID = "receiver_sinch_id";
    public static final String CHAT_TABLE_RECEIVER_NAME = "receiver_name";
    public static final String CHAT_TABLE_RECEIVER_PROFILE_IMAGE = "receiver_profile_image";


    public static final String CHAT_TABLE_MESSAGE = "message";
    public static final String CHAT_TABLE_TIME = "time";
    public static final String CHAT_TABLE_DOWNLOAD_STATE_CHAT = "download_state";
    public static final String CHAT_TABLE_CHAT_TYPE = "chat_type";


    /*
    * unread msg count
    * */
    public static final String CHAT_TABLE_UNREAD_MESSAGE_COUNT = "unread_message_count";


    /* table key that will store in DB*/
    public static final String[] MEDIA_TABLE_COLUMNS = {KEY_ID, MEDIA_TABLE_URL,
            MEDIA_TABLE_TYPE, MEDIA_TABLE_DOWNLOAD_STATE, MEDIA_TABLE_PATH};

    public static final String[] CHAT_TABLE_COLUMNS = {KEY_ID, CHAT_TABLE_MESSAGE_ID,
            CHAT_TABLE_SENDER_ID, CHAT_TABLE_SENDER_NAME, CHAT_TABLE_SENDER_PROFILE_IMAGE,
            CHAT_TABLE_RECEIVER_ID, CHAT_TABLE_RECEIVER_NAME};


    private static final String CHAT_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " + CHAT_TABLE + " (" + KEY_ID + " integer primary key autoincrement, "
            + CHAT_TABLE_MESSAGE_ID + " text, "
            + CHAT_TABLE_SENDER_ID + " text, "
            + CHAT_TABLE_SENDER_NAME + " text, "
            + CHAT_TABLE_SENDER_PROFILE_IMAGE + " text, "
            + CHAT_TABLE_RECEIVER_ID + " text, "
            + CHAT_TABLE_RECEIVER_NAME + " text, "
            + CHAT_TABLE_RECEIVER_PROFILE_IMAGE + " text, "
            + CHAT_TABLE_MESSAGE + " text, "
            + CHAT_TABLE_SENDER_SINCH_ID + " text, "
            + CHAT_TABLE_RECEIVER_SINCH_ID + " text, "
            + CHAT_TABLE_CHAT_TYPE + " text, "
            + CHAT_TABLE_TIME + " text " + ");";




    private static final String UNREAD_MESSAGE_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " +
            UNREAD_MESSAGE_COUNT_TABLE + " (" + KEY_ID + " integer primary key autoincrement, "
            + CHAT_TABLE_SENDER_ID + " text, "
            + CHAT_TABLE_RECEIVER_ID + " text, "
            + CHAT_TABLE_SENDER_SINCH_ID + " text, "
            + CHAT_TABLE_RECEIVER_SINCH_ID + " text, "
            + CHAT_TABLE_UNREAD_MESSAGE_COUNT + " text "
            + ");";

    // ***************************
    private class DatabaseHelper extends SQLiteOpenHelper {
        /*internal storage of database*/
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        /*external storage of database*/
       /* DatabaseHelper(Context context) {
            super(context, Environment.getExternalStorageDirectory()
                    + File.separator + "/SociomxDatabase/" + File.separator
                    + DATABASE_NAME, null, DATABASE_VERSION);
        }*/

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CHAT_TABLE_CREATE);
            db.execSQL(UNREAD_MESSAGE_TABLE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + CHAT_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + LAST_CHAT_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + UNREAD_MESSAGE_COUNT_TABLE);

            onCreate(db);
        }
    }

    /****
     * open virlen database
     */
    public Database open() throws SQLException {
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    /*
    * This method close the database
    */
    public void close() {
        mDbHelper.close();
    }


    /*
     * This method insert items in IMAGE_TABLE table
	 */
   /* public long insertImageDataTable(LessonDetail_Response.Link studyMaterial_bean) {

        ContentValues initialValues = new ContentValues();
        initialValues.put(MEDIA_TABLE_URL, studyMaterial_bean.getUrl());
        initialValues.put(MEDIA_TABLE_TYPE, studyMaterial_bean.getType());
        initialValues.put(MEDIA_TABLE_PATH, studyMaterial_bean.getPath());
        initialValues.put(MEDIA_TABLE_DOWNLOAD_STATE, studyMaterial_bean.getDownload_state());

        return mDb.insert(Media_TABLE, null, initialValues);

    }
*/

    /*
   * This method insert items in MESSAGE_TABLE table
   */
    public long insertUserChatDataTable(MsgBean mBean_GetMessageData) {

        String queryCheckId = "( " + CHAT_TABLE_MESSAGE_ID + "='" + mBean_GetMessageData.getMsgID() + "')";
        Cursor checkChatStatus = mDb.query(CHAT_TABLE, CHAT_TABLE_COLUMNS, queryCheckId, null, null, null, null);

        if (checkChatStatus != null && checkChatStatus.getCount() == 0) {
            ContentValues initialValues = new ContentValues();
            initialValues.put(CHAT_TABLE_MESSAGE_ID, mBean_GetMessageData.getMsgID());
            initialValues.put(CHAT_TABLE_SENDER_ID, mBean_GetMessageData.getSenderMemberId());
            initialValues.put(CHAT_TABLE_SENDER_NAME, mBean_GetMessageData.getSenderMemberName());
            initialValues.put(CHAT_TABLE_SENDER_PROFILE_IMAGE, mBean_GetMessageData.getSenderMemberImage());
            initialValues.put(CHAT_TABLE_RECEIVER_ID, mBean_GetMessageData.getAdminId());
            initialValues.put(CHAT_TABLE_RECEIVER_NAME,"");
            initialValues.put(CHAT_TABLE_RECEIVER_PROFILE_IMAGE, "");
            initialValues.put(CHAT_TABLE_MESSAGE, mBean_GetMessageData.getMessage());
            initialValues.put(CHAT_TABLE_TIME, mBean_GetMessageData.getDateTime());

            initialValues.put(CHAT_TABLE_CHAT_TYPE, mBean_GetMessageData.getChatType());
            return mDb.insert(CHAT_TABLE, null, initialValues);
        } else {
            return 0;
        }
    }

    public int checkUserChatDataTable(MsgBean mBean_GetMessageData) {

        String queryCheckId = "( " + CHAT_TABLE_MESSAGE_ID + "='" + mBean_GetMessageData.getMsgID() + "')";
        Cursor checkChatStatus = mDb.query(CHAT_TABLE, CHAT_TABLE_COLUMNS, queryCheckId, null, null, null, null);

        if (checkChatStatus != null && checkChatStatus.getCount() == 0) {

            return 0;
        } else {
            return 1;
        }
    }


    public ArrayList<MsgBean> getMessages () {

        // Select All Query
        String query = "SELECT * FROM " + CHAT_TABLE;// + " WHERE " + KEY_GROUP_ID + " = '" + group_id + "' ORDER BY " + KEY_MSG_DATE_TIME + " ASC";//"SELECT * FROM (SELECT * FROM " + TABLE_GROUP_MSG + " WHERE " + KEY_GROUP_ID + " = '" + group_id + "' ORDER BY " + KEY_MSG_DATE_TIME + " DESC LIMIT 0, 10 ) ORDER BY "+ KEY_MSG_DATE_TIME +" ASC";
        ArrayList<MsgBean> groupBeanList = new ArrayList<>();
        Cursor c = mDb.rawQuery(query, null);
        int message_id = c.getColumnIndex(CHAT_TABLE_MESSAGE_ID);
        int message_name = c.getColumnIndex(CHAT_TABLE_MESSAGE);
        int time = c.getColumnIndex(CHAT_TABLE_TIME);
        int sender_id = c.getColumnIndex(CHAT_TABLE_SENDER_ID);
        int sender_name = c.getColumnIndex(CHAT_TABLE_SENDER_NAME);
        int sender_profile_image = c.getColumnIndex(CHAT_TABLE_SENDER_PROFILE_IMAGE);
        int receiver_id = c.getColumnIndex(CHAT_TABLE_RECEIVER_ID);
        int receiver_name = c.getColumnIndex(CHAT_TABLE_RECEIVER_NAME);
        int receiver_profile_image = c.getColumnIndex(CHAT_TABLE_RECEIVER_PROFILE_IMAGE);
        int chatType = c.getColumnIndex(CHAT_TABLE_CHAT_TYPE);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                MsgBean MsgBean = new MsgBean();
                MsgBean.setSenderMemberId(c.getString(sender_id));
                MsgBean.setSenderMemberName(c.getString(sender_name));
                MsgBean.setSenderMemberImage(c.getString(sender_profile_image));
                MsgBean.setMessage(c.getString(message_name));
                MsgBean.setMsgID(c.getString(message_id));
                MsgBean.setDateTime(c.getString(time));
                MsgBean.setChatType(c.getString(chatType));
                groupBeanList.add(MsgBean);
            } while (c.moveToNext());
        }
        c.close();

        return groupBeanList;
    }





    public Cursor getChatData(String user_id, String friend_id) {
        StringBuilder buffer = new StringBuilder();
        String[] args = null;

        return mDb.query(CHAT_TABLE, null, buffer == null ? null : buffer.toString(), args, null, null, null);
    }


    public String getUnreadMessageCountWithUser(String sender_id, String receiver_id) {
        int unreadMsg = 0;
        String[] args = null;

        String query = "( " + "( " + CHAT_TABLE_RECEIVER_ID + "='" + receiver_id + "'"
                + " AND " + CHAT_TABLE_SENDER_ID + "='" + sender_id + "'" + " )"
                + " OR " + "( " + CHAT_TABLE_RECEIVER_ID + "='" + sender_id
                + "'" + " AND " + CHAT_TABLE_SENDER_ID + "='" + receiver_id+ "'" + " )" + " )" ;


        Cursor unreadMsgCursor = mDb.query(UNREAD_MESSAGE_COUNT_TABLE, null, query, args, null, null, null);

        while (unreadMsgCursor.moveToNext()) {
            int indexMessageCount = unreadMsgCursor.getColumnIndex(CHAT_TABLE_UNREAD_MESSAGE_COUNT);
            String msgCount = unreadMsgCursor.getString(indexMessageCount);
            unreadMsg += Integer.parseInt(msgCount);

        }
        return String.valueOf(unreadMsg);
    }

    public long insertUnreadMessageCountTable(MsgBean mBean_GetMessageData, String msgCount) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(CHAT_TABLE_RECEIVER_ID, mBean_GetMessageData.getAdminId());
        initialValues.put(CHAT_TABLE_SENDER_ID, mBean_GetMessageData.getSenderMemberId());
        initialValues.put(CHAT_TABLE_UNREAD_MESSAGE_COUNT, msgCount);
        return mDb.insert(UNREAD_MESSAGE_COUNT_TABLE, null, initialValues);
    }

    public Cursor getunReadMessageCount() {
        String[] args = null;
        return mDb.query(UNREAD_MESSAGE_COUNT_TABLE, null, null, args, null, null, null);
    }

    public Cursor getSingleUnreadMessageCount(String sender_id, String receiver_id) {

        String[] args = null;
        String query = "( " + CHAT_TABLE_RECEIVER_ID + "='" + receiver_id + "'"
                + " AND " + CHAT_TABLE_SENDER_ID + "='" + sender_id + "'" + ")" ;

        return mDb.query(UNREAD_MESSAGE_COUNT_TABLE, null, query, args, null, null, null);
    }

    public void updateUnreadMessageCount(String msgCount, String sender, String receiver) {


        String updateQuery = "UPDATE " + UNREAD_MESSAGE_COUNT_TABLE + " SET " +
                CHAT_TABLE_UNREAD_MESSAGE_COUNT + "='" + msgCount + "' WHERE "
                + CHAT_TABLE_RECEIVER_ID + "='" + receiver + "'"
                + " AND " + CHAT_TABLE_SENDER_ID + "='" + sender + "'" ;
        mDb.execSQL(updateQuery);
        //   mDb.query(UNREAD_MESSAGE_COUNT_TABLE, null, query, args, null, null, null);
    }


    public String getAllUnreadMessageCount(String receiver_name) {
        int unreadMsg = 0;
        String[] args = null;

/*****
 *      receiver_name ====== to sender name

 Query  for  check badges count for reciever name and sender name is same then badge count show because batch count only show receiver msg
 */
        String query = "( " + CHAT_TABLE_RECEIVER_ID + "='" + receiver_name + "'" + ")";
///////here

        Cursor unreadMsgCursor = mDb.query(UNREAD_MESSAGE_COUNT_TABLE, null, query, args, null, null, null);

        while (unreadMsgCursor.moveToNext()) {
            int indexMessageCount = unreadMsgCursor.getColumnIndex(CHAT_TABLE_UNREAD_MESSAGE_COUNT);
            String msgCount = unreadMsgCursor.getString(indexMessageCount);
            unreadMsg += Integer.parseInt(msgCount);

        }
        return String.valueOf(unreadMsg);
    }

    public void deleteAllUnreadCount() {
        mDb.execSQL("delete from " + UNREAD_MESSAGE_COUNT_TABLE);
    }

    public void resetDatabase() {

        mDb.execSQL("DROP TABLE IF EXISTS " + CHAT_TABLE);
        mDb.execSQL("DROP TABLE IF EXISTS " + LAST_CHAT_TABLE);
        mDb.execSQL("DROP TABLE IF EXISTS " + UNREAD_MESSAGE_COUNT_TABLE);

        mDb.execSQL(CHAT_TABLE_CREATE);
        
        mDb.execSQL(UNREAD_MESSAGE_TABLE_CREATE);

    }


        public void deleteChatMessage(String friendJid, String user_id) {


            /*StringBuilder buffer = new StringBuilder();
            String[] args = null;

            String query = "( " + "( " + CHAT_TABLE_RECEIVER_ID + "='" + friendJid + "'" + " OR "
                    + CHAT_TABLE_SENDER_ID + "='" + friendJid + "'" + " )"
                    + "AND ( " + CHAT_TABLE_RECEIVER_ID + "='" + user_id + "'" + " OR "
                    + CHAT_TABLE_SENDER_ID + "='" + user_id + "'" + " )" + ")"
                    ;
            buffer.append(query);
            Cursor c = mDb.query(LAST_CHAT_TABLE, null, buffer == null ? null : buffer.toString(), args, null, null, null);
            if (c.getCount() > 0) {*/


                String deleteLastChatQuery = "delete from " + LAST_CHAT_TABLE + " WHERE " + "(  ( "
                        + CHAT_TABLE_RECEIVER_ID + "='" + friendJid + "'" + " OR "
                        + CHAT_TABLE_SENDER_ID + "='" + friendJid + "'" + " )"
                        + "AND ( " + CHAT_TABLE_RECEIVER_ID + "='" + user_id + "'"
                        + " OR " + CHAT_TABLE_SENDER_ID + "='" + user_id + "'" + " ) )";
                mDb.execSQL(deleteLastChatQuery);


            String deleteChatQuery = "delete from " + CHAT_TABLE + " WHERE " + "(  ( "
                    + CHAT_TABLE_RECEIVER_ID + "='" + friendJid + "'" + " OR "
                    + CHAT_TABLE_SENDER_ID + "='" + friendJid + "'" + " )"
                    + "AND ( " + CHAT_TABLE_RECEIVER_ID + "='" + user_id + "'"
                    + " OR " + CHAT_TABLE_SENDER_ID + "='" + user_id + "'" + " ) )";
            mDb.execSQL(deleteChatQuery);

    }
}




