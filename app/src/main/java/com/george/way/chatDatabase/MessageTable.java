package com.george.way.chatDatabase;

/*
 * Created by umeshk on 3/15/2017.
 */

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.george.way.model.chat.MsgBean;

import java.util.ArrayList;
import java.util.List;

public class MessageTable {

    // Invite Member table name
    private static final String TABLE_GROUP_MSG = "user_chat";

    // TABLE_INVITE_MEMBER Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_MSG = "message";
    private static final String KEY_MSG_ID = "message_id";
    private static final String KEY_SENDER_MEMBER_ID = "member_id";
    private static final String KEY_SENDER_MEMBER_NAME = "member_name";
    private static final String KEY_SENDER_MEMBER_IMAGE = "member_image";
    private static final String KEY_MSG_DATE_TIME = "date_time";


    public void createGroupMessageTable(SQLiteDatabase db) {

        String CREATE_MY_GROUP_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_GROUP_MSG + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + KEY_SENDER_MEMBER_ID + " TEXT NOT NULL,"
                + KEY_SENDER_MEMBER_NAME + " TEXT NOT NULL,"
                + KEY_SENDER_MEMBER_IMAGE + " TEXT NOT NULL,"
                + KEY_MSG + " TEXT NOT NULL,"
                + KEY_MSG_ID + " TEXT,"
                + KEY_MSG_DATE_TIME + " DATETIME" + ")";

        db.execSQL(CREATE_MY_GROUP_TABLE);
    }

    public void addChatMessage(SQLiteDatabase db, MsgBean MsgBean) {

        if (!checkMsgIDExist(db, MsgBean.getMsgID())) {
            ContentValues values = new ContentValues();

            values.put(KEY_SENDER_MEMBER_ID, MsgBean.getSenderMemberId());
            values.put(KEY_SENDER_MEMBER_NAME, MsgBean.getSenderMemberName());
            values.put(KEY_SENDER_MEMBER_IMAGE, MsgBean.getSenderMemberImage());
            values.put(KEY_MSG, MsgBean.getMessage());
            values.put(KEY_MSG_ID, MsgBean.getMsgID());
            values.put(KEY_MSG_DATE_TIME, MsgBean.getDateTime());

            // Inserting Row
            db.insert(TABLE_GROUP_MSG, null, values);
        }
    }

    public List<MsgBean> getGroupMessages(Cursor cursor) {

        List<MsgBean> groupBeanList = new ArrayList<>();
        // Select All Query
        int senderMemberIdIndex = cursor.getColumnIndex(KEY_SENDER_MEMBER_ID);
        int senderMemberNameIndex = cursor.getColumnIndex(KEY_SENDER_MEMBER_NAME);
        int senderMemberImageIndex = cursor.getColumnIndex(KEY_SENDER_MEMBER_IMAGE);
        int messageIndex = cursor.getColumnIndex(KEY_MSG);
        int messageIdIndex = cursor.getColumnIndex(KEY_MSG_ID);
        int dateTimeIndex = cursor.getColumnIndex(KEY_MSG_DATE_TIME);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                MsgBean MsgBean = new MsgBean();
                MsgBean.setSenderMemberId(cursor.getString(senderMemberIdIndex));
                MsgBean.setSenderMemberName(cursor.getString(senderMemberNameIndex));
                MsgBean.setSenderMemberImage(cursor.getString(senderMemberImageIndex));
                MsgBean.setMessage(cursor.getString(messageIndex));
                MsgBean.setMsgID(cursor.getString(messageIdIndex));
                MsgBean.setDateTime(cursor.getString(dateTimeIndex));
                groupBeanList.add(MsgBean);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return groupBeanList;
    }

    public List<MsgBean> getGroupMessagesByLimit(SQLiteDatabase db, String group_id) {

        // Select All Query
        String query = "SELECT * FROM " + TABLE_GROUP_MSG;// + " WHERE " + KEY_GROUP_ID + " = '" + group_id + "' ORDER BY " + KEY_MSG_DATE_TIME + " ASC";//"SELECT * FROM (SELECT * FROM " + TABLE_GROUP_MSG + " WHERE " + KEY_GROUP_ID + " = '" + group_id + "' ORDER BY " + KEY_MSG_DATE_TIME + " DESC LIMIT 0, 10 ) ORDER BY "+ KEY_MSG_DATE_TIME +" ASC";
        List<MsgBean> groupBeanList = new ArrayList<>();
        Cursor cursor = db.rawQuery(query, null);

        int senderMemberIdIndex = cursor.getColumnIndex(KEY_SENDER_MEMBER_ID);
        int senderMemberNameIndex = cursor.getColumnIndex(KEY_SENDER_MEMBER_NAME);
        int senderMemberImageIndex = cursor.getColumnIndex(KEY_SENDER_MEMBER_IMAGE);
        int messageIndex = cursor.getColumnIndex(KEY_MSG);
        int messageIdIndex = cursor.getColumnIndex(KEY_MSG_ID);
        int dateTimeIndex = cursor.getColumnIndex(KEY_MSG_DATE_TIME);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                MsgBean MsgBean = new MsgBean();
                MsgBean.setSenderMemberId(cursor.getString(senderMemberIdIndex));
                MsgBean.setSenderMemberName(cursor.getString(senderMemberNameIndex));
                MsgBean.setSenderMemberImage(cursor.getString(senderMemberImageIndex));
                MsgBean.setMessage(cursor.getString(messageIndex));
                MsgBean.setMsgID(cursor.getString(messageIdIndex));
                MsgBean.setDateTime(cursor.getString(dateTimeIndex));
                groupBeanList.add(MsgBean);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return groupBeanList;
    }

    public synchronized void updateProfilePic(SQLiteDatabase db, String user_id, String senderImage) {

        String mWhere = KEY_SENDER_MEMBER_ID + " = ?";
        String[] mValues = {user_id};

        ContentValues values = new ContentValues();

        values.put(KEY_SENDER_MEMBER_IMAGE, senderImage);

        // Updating Row
        db.update(TABLE_GROUP_MSG, values, mWhere, mValues);
    }

    public synchronized void updateUserName(SQLiteDatabase db, String user_id, String userName) {

        String mWhere = KEY_SENDER_MEMBER_ID + " = ?";
        String[] mValues = {user_id};

        ContentValues values = new ContentValues();

        values.put(KEY_SENDER_MEMBER_NAME, userName);

        // Updating Row
        db.update(TABLE_GROUP_MSG, values, mWhere, mValues);
    }

    public String getMemberImage(SQLiteDatabase db, String user_id) {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_GROUP_MSG + " WHERE " + KEY_SENDER_MEMBER_ID + " = '" + user_id + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);

        int senderMemberImageIndex = cursor.getColumnIndex(KEY_SENDER_MEMBER_IMAGE);

        String imageName = "";
        // looping through all rows and adding to list
        if (cursor.getCount() > 0) {
            cursor.moveToLast();
            imageName = cursor.getString(senderMemberImageIndex);
        }
        cursor.close();
        return imageName;
    }

    public int getChatCount(SQLiteDatabase db, String user_id) {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_GROUP_MSG + " WHERE " + KEY_SENDER_MEMBER_ID + " = '" + user_id + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);

        int chatCount = cursor.getCount();
        // looping through all rows and adding to list
        cursor.close();
        return chatCount;
    }

    public int getTotalChatCount(SQLiteDatabase db, String group_id) {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_GROUP_MSG ;//+ " WHERE " + KEY_GROUP_ID + " = '" + group_id + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);

        int chatCount = cursor.getCount();
        // looping through all rows and adding to list
        cursor.close();
        return chatCount;
    }

  /*  public void deleteGroupMsgByGroupId(SQLiteDatabase db, String group_id) {
        String mWhere = KEY_GROUP_ID + " = ?";
        String[] mValues = {group_id};
        db.delete(TABLE_GROUP_MSG, mWhere, mValues);
    }*/

    private boolean checkMsgIDExist(SQLiteDatabase db, String msg_id) {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_GROUP_MSG + " WHERE " + KEY_MSG_ID + " = '" + msg_id + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);

        int chatCount = cursor.getCount();
        // looping through all rows and adding to list
        cursor.close();
        return chatCount > 0;
    }
}
