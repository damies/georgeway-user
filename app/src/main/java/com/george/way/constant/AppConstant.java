package com.george.way.constant;

/**
 * Created by hemendrag on 1/4/2017.
 * this class will hold the constant variables of splash screen
 */

public class AppConstant {


    /**
     * create a singleton method to access variables of this class
     */
    public static AppConstant msAppConstant;

    public static AppConstant getObj() {

        if (msAppConstant == null)
            msAppConstant = new AppConstant();

        return msAppConstant;
    }


    /**
     * declare the variables to the class
     */
    public int SPLASH_ANIM_DURATION = 3000;
    public String EMPTY_STRING = "";
    private final static int REQUEST_TIMEOUT = 10000;

    public String TRUE = "1";
    public String FALSE = "0";

    public final String YES = "Yes";
    public final String NO = "No";
    public  final  String  DEVICE_TYPE                  = "Android";

    public final String STATUS_CODE_SUCCESS = "200";
    public final String STATUS_CODE_FAILED  = "202";
    public final String STATUS_CODE_CHANGED = "201";
    public final String STATUS_CODE_Error = "204";

    public final String ERROR_MESSAGE = "error_message";
    public final String SUCCESS_MESSAGE = "success_message";

    //Sendbox Paypal
    public static String KEY_CLIENT_ID = "AWvBcGwl1g958ZYv09RTQ1jurNzpwbINQdU6JaqThzDsNUdrATFQ3bEyhiCmK2OXZI3zcF87QVOfc_el";
    public static String KEY_SECRET = "EMKqvFTZ8bPkoebpRSkWTkq7DdEkQEBDR2V5qUmR4jfUKg2p8k2kDpc0imo6CyMlABqRNddsUGRMLgRr";

    public static String CONTACT_NUMBER = "";
    public static String CONTACT_EMAIL = "";

    //Live Paypal
//    public static String KEY_CLIENT_ID = "ASC6qK85XOEG3QY4npdLjt1-L6NZsr5v-iPjdHCzJNvV8jnZsgDaMsqm8xYnOz9DA6U1flAtaJNnQtpU";



}