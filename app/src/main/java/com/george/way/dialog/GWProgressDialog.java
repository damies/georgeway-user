package com.george.way.dialog;

import android.app.ProgressDialog;
import android.content.Context;

import com.george.way.R;


/**
 * Created by hemendrag on 1/4/2017.
 * custom progress dialog
 */

public class GWProgressDialog extends ProgressDialog {

    /**
     * declare java objects
     */
    public static GWProgressDialog msGWProgressDialog;

    public GWProgressDialog(Context context) {
        super(context);
    }

    public GWProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    public static void show(Context context){
        getProgressDialog(context).showDialog1();
    }


    /**
     * generte singletone for custom progress dialog
     *
     * @param mContext
     * @return
     */
    public static GWProgressDialog getProgressDialog(Context mContext) {
        if (msGWProgressDialog == null) {
            msGWProgressDialog = new GWProgressDialog(mContext, R.style.TransparentProgressDialog);
            msGWProgressDialog.setContentView(R.layout.layout_custom_progreebar);
            msGWProgressDialog.setCancelable(false);
        }
        return msGWProgressDialog;
    }

    /**
     * show dialog if not showing
     */
    public void showDialog1() {

        if (msGWProgressDialog != null && !msGWProgressDialog.isShowing()) {
            try {
                msGWProgressDialog.show();
            } catch (Exception ex) {
                msGWProgressDialog = null;
                ex.printStackTrace();
            }
        }
    }

    public boolean isDialogShowing(){
        return msGWProgressDialog != null && msGWProgressDialog.isShowing();
    }

    /**
     * hide dialog if not showing
     */
    public void dismissDialog1() {
        try {
            if (msGWProgressDialog != null && msGWProgressDialog.isShowing()) {
                msGWProgressDialog.dismiss();
                msGWProgressDialog = null;
            }
        }catch (Exception ex){
            msGWProgressDialog = null;
            ex.printStackTrace();
        }
    }


}
