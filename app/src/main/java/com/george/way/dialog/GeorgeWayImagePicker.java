package com.george.way.dialog;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.easyImagePick.GeorgeWayImagePick;

/**
 * Created by hemendrag on 1/10/2017.
 */

public class GeorgeWayImagePicker {


    /**
     * create a singleton method to access variables of this class
     */
    public static GeorgeWayImagePicker mGeorgeWayImagePicker;
    private static Activity mActivity;

    public final int PICK_IMAGE_REQUEST = 1;
    public final int REQUEST_IMAGE_CAPTURE = 2;
    public final int PICK_DOCUMENT_REQUEST = 3;

    public String CHOOSE_IMAGE = "Please choose Image";
    public String CHOOSE_FILE = "Please choose file";


    public static GeorgeWayImagePicker getImagePickObj(Activity activity) {

        mActivity = activity;
        if (mGeorgeWayImagePicker == null)
            mGeorgeWayImagePicker = new GeorgeWayImagePicker();

        return mGeorgeWayImagePicker;
    }


    /**
     * open  dialog for image pick
     */
    public void openImageConfirmationDialog() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(mActivity);
        final View promptsView = li.inflate(R.layout.layout_dialog_confirmation, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        alertDialogBuilder.setView(promptsView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        float dpi = mActivity.getResources().getDisplayMetrics().density;
        alertDialog.setView(promptsView, (int) (19 * dpi), (int) (5 * dpi), (int) (14 * dpi), (int) (5 * dpi));


        alertDialog.show();


        TextView textViewGallery = (TextView) promptsView.findViewById(R.id.textViewGallery);
        TextView textViewCamera = (TextView) promptsView.findViewById(R.id.textViewCamera);

        textViewGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();
                if (Build.VERSION.SDK_INT < 23) {
                    GeorgeWayImagePick.openChooserWithGallery(mActivity, CHOOSE_IMAGE, PICK_IMAGE_REQUEST);
                } else {
                    checkAppPermission(PICK_IMAGE_REQUEST);
                }
            }
        });

        textViewCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if (Build.VERSION.SDK_INT < 23) {
                    GeorgeWayImagePick.openCamera(mActivity, REQUEST_IMAGE_CAPTURE);
                } else {
                    checkAppPermission(REQUEST_IMAGE_CAPTURE);
                }

            }
        });
    }

    /**
     * open  dialog for image pick
     */
    public void openConfirmationDialog() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(mActivity);
        final View promptsView = li.inflate(R.layout.layout_dialog_confirmation, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        alertDialogBuilder.setView(promptsView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        float dpi = mActivity.getResources().getDisplayMetrics().density;
        alertDialog.setView(promptsView, (int) (19 * dpi), (int) (5 * dpi), (int) (14 * dpi), (int) (5 * dpi));


        alertDialog.show();


        TextView textViewGallery = (TextView) promptsView.findViewById(R.id.textViewGallery);
        TextView textViewCamera = (TextView) promptsView.findViewById(R.id.textViewCamera);

        textViewGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

                if (Build.VERSION.SDK_INT < 23) {
                    GeorgeWayImagePick.openChooserWithGallery(mActivity, CHOOSE_IMAGE, PICK_IMAGE_REQUEST);
                } else {
                    checkAppPermission(PICK_IMAGE_REQUEST);
                }
            }
        });

        textViewCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if (Build.VERSION.SDK_INT < 23) {
                    GeorgeWayImagePick.openCamera(mActivity, REQUEST_IMAGE_CAPTURE);
                } else {
                    checkAppPermission(REQUEST_IMAGE_CAPTURE);
                }

            }
        });
    }


    /**
     * Method for checking GPS Permission Os 6.0
     */
    private void checkAppPermission(int type) {

        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(mActivity, new String[]{
                    Manifest.permission.CAMERA,
                    Manifest.permission.MANAGE_DOCUMENTS,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
            }, type);
        } else  if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(mActivity, new String[]{
                    Manifest.permission.CAMERA
            }, type);
        }
        else {
            if (type == PICK_DOCUMENT_REQUEST) {
                GeorgeWayImagePick.openChooserWithDocuments(mActivity, CHOOSE_FILE, PICK_DOCUMENT_REQUEST);

            } else  if (type == PICK_IMAGE_REQUEST) {

                GeorgeWayImagePick.openChooserWithGallery(mActivity,CHOOSE_IMAGE , PICK_IMAGE_REQUEST);
            } else {
                GeorgeWayImagePick.openCamera(mActivity, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    /**
     * open file chooser dialog
     */
    public void openFileChooser() {

        if (Build.VERSION.SDK_INT < 23) {

            GeorgeWayImagePick.openChooserWithDocuments(mActivity, CHOOSE_FILE, PICK_DOCUMENT_REQUEST);

         } else {
            checkAppPermission(PICK_DOCUMENT_REQUEST);
        }

    }

}
