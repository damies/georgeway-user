package com.george.way.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.george.way.R;
import com.george.way.constant.AppConstant;
import com.george.way.customWidget.AnswerView;
import com.george.way.interfaces.QuizInterface;
import com.george.way.model.QuestionOfTheDayResponse;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by hemendrag on 1/13/2017.
 */

public class QuestionOfTheDayDialog implements AnswerView.OnAnswerSelectedListener {

    /**
     * create a singleton method to access variables of this class
     */
    public static QuestionOfTheDayDialog mQODDailog;
    private static Activity mActivity;
    private LinearLayout linearVideoLayoutStart,
            linearQuestionLayout,
            linearVideoLayoutEnd,
            layoutAnswerResult;
    private VideoView videoPlayerStart, videoPlayerEnd;
    private TextView mQuestionText;
    private RecyclerView mAnswerSelect;
    private Button buttonOk, buttonSubmit;
    private String startingVideoUrl = AppConstant.getObj().EMPTY_STRING;
    private String endingVideoUrl = AppConstant.getObj().EMPTY_STRING;
    private QuizQOD mQuizQOD;
    private RelativeLayout relImageLayout;
    private AlertDialog alertDialog;
    private ImageView answer_img;
    private ProgressBar progressBar;
    private TextView timer_tv;
    private TextView congo_msg_tv;
    private TextView text_answer;
    private TextView textViewExpDesc;
    private ImageView imageViewResult;


    ImageView imageViewCross;
    RelativeLayout relMain;
    CounterClass timer;
    TextView textViewQOD;

    String correctAnswer = "", question_result = "", questionId = "";
    ArrayList<String> correctAnswerList = new ArrayList<>();
    AnswerAdapter answerAdapter;
    boolean isAnswerCorrect = false;
    private QuestionOfTheDayResponse questionDataModel;

    public static QuestionOfTheDayDialog getObj(Activity activity) {

        mActivity = activity;

        if (mQODDailog == null)
            mQODDailog = new QuestionOfTheDayDialog();

        return mQODDailog;
    }


    /**
     * show error dialog in case of error
     */
    public void showQODDialog(QuestionOfTheDayResponse mQuestionOfTheDayResponse) {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(mActivity);
        View promptsView = li.inflate(R.layout.layout_dialog_question_of_the_day, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        alertDialogBuilder.setView(promptsView);
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();


        questionDataModel = mQuestionOfTheDayResponse;

        /**
         * declare fields of class
         */

        linearVideoLayoutStart = (LinearLayout) alertDialog.findViewById(R.id.linearVideoLayoutStart);
        linearQuestionLayout = (LinearLayout) alertDialog.findViewById(R.id.linearQuestionLayout);
        linearVideoLayoutEnd = (LinearLayout) alertDialog.findViewById(R.id.linearVideoLayoutEnd);
        layoutAnswerResult = (LinearLayout) alertDialog.findViewById(R.id.layoutAnswerResult);
        videoPlayerStart = (VideoView) alertDialog.findViewById(R.id.videoPlayerQOD);
        videoPlayerEnd = (VideoView) alertDialog.findViewById(R.id.videoPlayerEnd);
        buttonOk = (Button) alertDialog.findViewById(R.id.buttonOk);
        answer_img = (ImageView) alertDialog.findViewById(R.id.answer_img);
        progressBar = (ProgressBar) alertDialog.findViewById(R.id.progressBar);
        relImageLayout = (RelativeLayout) alertDialog.findViewById(R.id.relImageLayout);
        mAnswerSelect = (RecyclerView) alertDialog.findViewById(R.id.answer_select);
        mQuestionText = (TextView) alertDialog.findViewById(R.id.text_question);
        buttonSubmit = (Button) alertDialog.findViewById(R.id.buttonSubmit);
        timer_tv = (TextView) alertDialog.findViewById(R.id.timer_tv);
        congo_msg_tv = (TextView) alertDialog.findViewById(R.id.congo_msg_tv);
        mAnswerSelect.setLayoutManager(new LinearLayoutManager(mActivity));
        imageViewResult = (ImageView) alertDialog.findViewById(R.id.imageViewResult);
        imageViewCross = (ImageView) alertDialog.findViewById(R.id.imageViewCross);
        text_answer = (TextView) alertDialog.findViewById(R.id.textExplaination);
        textViewExpDesc = (TextView) alertDialog.findViewById(R.id.textViewExpDesc);
        relMain = (RelativeLayout) alertDialog.findViewById(R.id.relMain);
        textViewQOD = (TextView) alertDialog.findViewById(R.id.textViewQOD);

        PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
        try {

            String text = preferenceUserUtil.getString(preferenceUserUtil.KEY_QTD_CONTENT);
            String colorCode = preferenceUserUtil.getString(preferenceUserUtil.KEY_QTD_COLOR);
            textViewQOD.setText(text);
            relMain.setBackgroundColor(Color.parseColor(colorCode));
            buttonOk.setBackgroundColor(Color.parseColor(colorCode));
            buttonSubmit.setBackgroundColor(Color.parseColor(colorCode));


            try {
                GradientDrawable shape = (GradientDrawable) timer_tv.getBackground();
                shape.setColor(Color.parseColor(colorCode));
                timer_tv.setTextColor(Color.WHITE);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        /**
         * get videos url
         */

        startingVideoUrl = "android.resource://" + mActivity.getPackageName() + "/" + R.raw.video_qod_starting_gp;
        endingVideoUrl = "android.resource://" + mActivity.getPackageName() + "/" + R.raw.video_qod_ending_gp;

        /*if (preferenceUserUtil.getString("isVideo").isEmpty()) {
            startingVideoUrl = "android.resource://" + mActivity.getPackageName() + "/" + R.raw.video_qod_starting_gp;
            endingVideoUrl = "android.resource://" + mActivity.getPackageName() + "/" + R.raw.video_qod_ending_gp;
        }else
        {
            startingVideoUrl = "file://"+preferenceUserUtil.getString("isVideo");
            endingVideoUrl = "file://"+preferenceUserUtil.getString("isVideo");
        }*/


        /**
         * start functionality
         */
        mQuizQOD = new QuizQOD();
        mQuizQOD.startQuiz();


        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSubmit();
            }
        });

        imageViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

                Calendar currentDate = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                String currentFormattedDate = df.format(currentDate.getTime());
                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                String savedDate = preferenceUserUtil.getString(preferenceUserUtil.KEY_RESULT_DATE);
                String questionId = preferenceUserUtil.getString(preferenceUserUtil.KEY_RESULT_QUESTION);

                if (!TextUtils.isEmpty(savedDate)
                        && !TextUtils.isEmpty(questionId)
                        && currentFormattedDate.equalsIgnoreCase(savedDate)){

                }else{
                    updateQODRequest(question_result);
                }

            }
        });

        /**
         * set answer listener
         */
        //mAnswerSelect.setOnAnswerSelectedListener(this);


    }

    public void onSubmit() {

        if (answerAdapter != null && answerAdapter.selectedAnswer!=null && answerAdapter.selectedAnswer.size()>0) {

            //if (correctAnswerList.size() == mAnswerAdapter.selectedAnswer.size()) {
            for (int i = 0; i < answerAdapter.selectedAnswer.size(); i++) {
                if (correctAnswerList.contains(answerAdapter.selectedAnswer.get(i))) {
                    isAnswerCorrect = true;
                } else {
                    isAnswerCorrect = false;
                    break;
                }
            }

            timer.cancel();
            timer_tv.setVisibility(View.GONE);

            videoPlayerStart.setVisibility(View.GONE);
            linearQuestionLayout.setVisibility(View.GONE);
            layoutAnswerResult.setVisibility(View.GONE);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mQuizQOD.submitQuestion();
                }
            }, 50);

        }
        else {

            new SingleActionDialog().showDialogNormal(mActivity.getResources().getString(R.string.please_select_option),mActivity,false);
        }

    }

    public void onSubmitTimeOut() {
        timer.cancel();
        timer_tv.setVisibility(View.GONE);

        if (answerAdapter != null && answerAdapter.selectedAnswer!=null && answerAdapter.selectedAnswer.size()>0) {

            //if (correctAnswerList.size() == mAnswerAdapter.selectedAnswer.size()) {
            for (int i = 0; i < answerAdapter.selectedAnswer.size(); i++) {
                if (correctAnswerList.contains(answerAdapter.selectedAnswer.get(i))) {
                    isAnswerCorrect = true;
                } else {
                    isAnswerCorrect = false;
                    break;
                }
            }


        }

        videoPlayerStart.setVisibility(View.GONE);
        linearQuestionLayout.setVisibility(View.GONE);
        layoutAnswerResult.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mQuizQOD.submitQuestion();
            }
        }, 50);

    }



    @Override
    public void onCorrectAnswerSelected() {

    }

    @Override
    public void onWrongAnswerSelected() {

    }


    private void updateData(QuestionOfTheDayResponse mCategordyDataModel) {
        /**
         * update data into preference
         */
        timer_tv.setVisibility(View.VISIBLE);
        timer = new CounterClass(180000, 1000);
        timer.start();

        if (mCategordyDataModel.getData().get(0).getQuestion().contains(".png") || mCategordyDataModel.getData().get(0).getQuestion().contains(".jpg")) {
            relImageLayout.setVisibility(View.VISIBLE);
            mQuestionText.setText("Ques. : ");
            ImageLoader.getInstance().displayImage(ApiClient.BASE_URL + mCategordyDataModel.getData().get(0).getQuestion(), answer_img, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    progressBar.setVisibility(View.GONE);

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    progressBar.setVisibility(View.GONE);

                }
            });
        } else {

            relImageLayout.setVisibility(View.GONE);
            mQuestionText.setText(AppUtils. getObj().noTrailingwhiteLines("Ques. : " + Html.fromHtml(mCategordyDataModel.getData().get(0).getQuestion())));

        }

        questionId = mCategordyDataModel.getData().get(0).getQuestionid();

        //Load answer strings
        ArrayList<String> options = new ArrayList<>();

        options.add(mCategordyDataModel.getData().get(0).getFieldA());
        options.add(mCategordyDataModel.getData().get(0).getFieldB());
        options.add(mCategordyDataModel.getData().get(0).getFieldC());

        if (!TextUtils.isEmpty(mCategordyDataModel.getData().get(0).getFieldD()))
            options.add(mCategordyDataModel.getData().get(0).getFieldD());

        if (!TextUtils.isEmpty(mCategordyDataModel.getData().get(0).getFieldE()))
            options.add(mCategordyDataModel.getData().get(0).getFieldE());


        correctAnswer = mCategordyDataModel.getData().get(0).getAnswerNumber();
        questionId = mCategordyDataModel.getData().get(0).getQuestionid();


        try {
            if (Build.VERSION.SDK_INT >= 24) {
                text_answer.setText(AppUtils. getObj().noTrailingwhiteLines(Html.fromHtml(mCategordyDataModel.getData().get(0).getExplaination(), Html.FROM_HTML_MODE_LEGACY)));

            } else {
                text_answer.setText(AppUtils. getObj().noTrailingwhiteLines(Html.fromHtml(mCategordyDataModel.getData().get(0).getExplaination())));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        correctAnswerList.clear();
        if (correctAnswer.contains(",")) {
            String[] correctAnswerArr = correctAnswer.split(",");
            correctAnswerList = new ArrayList<String>(Arrays.asList(correctAnswerArr));
        } else {
            correctAnswerList.add(correctAnswer);
        }

        answerAdapter = new AnswerAdapter(mActivity, options);
        mAnswerSelect.setAdapter(answerAdapter);
        //mAnswerSelect.loadAnswers(options, mCategordyDataModel.getData().get(0).getAnswerNumber());


    }

    public class QuizQOD implements QuizInterface {

        @Override
        public void startQuiz() {

            /**
             * update UI and play video
             */
            linearVideoLayoutStart.setVisibility(View.VISIBLE);
            videoPlayerStart.setVisibility(View.VISIBLE);
            linearQuestionLayout.setVisibility(View.GONE);
            linearVideoLayoutEnd.setVisibility(View.GONE);
            layoutAnswerResult.setVisibility(View.GONE);

            /**
             * play starting video
             */
            playStartingVideo();

        }

        @Override
        public void showQuestion() {

            linearVideoLayoutStart.setVisibility(View.GONE);
            linearQuestionLayout.setVisibility(View.VISIBLE);
            linearVideoLayoutEnd.setVisibility(View.GONE);
            layoutAnswerResult.setVisibility(View.GONE);


        }

        @Override
        public void submitQuestion() {

            /**
             * update UI and play video
             */
            linearQuestionLayout.setVisibility(View.GONE);
            linearVideoLayoutEnd.setVisibility(View.VISIBLE);
            linearVideoLayoutStart.setVisibility(View.GONE);
            layoutAnswerResult.setVisibility(View.GONE);


            /**
             * play ending video
             */
            playEndingVideo();

        }

        @Override
        public void stopQuiz() {

            /**
             * update UI and play video
             */
            linearQuestionLayout.setVisibility(View.GONE);
            linearVideoLayoutEnd.setVisibility(View.GONE);
            linearVideoLayoutStart.setVisibility(View.GONE);
            layoutAnswerResult.setVisibility(View.VISIBLE);
            imageViewResult.setVisibility(View.VISIBLE);


                if (isAnswerCorrect) {
                imageViewResult.setImageResource(R.drawable.ic_correct_ans);
                congo_msg_tv.setText("Congratulations ! \nYour answer is correct.");
                text_answer.setVisibility(View.GONE);
                textViewExpDesc.setVisibility(View.GONE);
            } else {
                imageViewResult.setImageResource(R.drawable.ic_wrong_ans);
                congo_msg_tv.setText("Sorry ! \nYour answer is wrong.\nCorrect answer is : " + correctAnswer);
                text_answer.setVisibility(View.VISIBLE);
                textViewExpDesc.setVisibility(View.VISIBLE);

            }

            try {
                question_result = AppConstant.getObj().EMPTY_STRING;
                for (int index = 0; index < answerAdapter.selectedAnswer.size(); index++) {
                    if (!TextUtils.isEmpty(question_result)) {
                        question_result = question_result + "," + answerAdapter.selectedAnswer.get(index);
                    } else {
                        question_result = answerAdapter.selectedAnswer.get(index);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
    }

    /**
     * play starting video player
     */
    private void playStartingVideo() {

        Uri uri = Uri.parse(startingVideoUrl);

        MediaController mc = new MediaController(mActivity);
        videoPlayerStart.setMediaController(mc);
        videoPlayerStart.setVideoURI(uri);
        videoPlayerStart.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                videoPlayerStart.start();
            }
        });
        videoPlayerStart.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoPlayerStart.pause();
                mQuizQOD.showQuestion();

                /**
                 * update data in qod
                 */
                updateData(questionDataModel);

            }
        });
    }

    /**
     * play ending video player
     */
    private void playEndingVideo() {

        timer_tv.setVisibility(View.GONE);
        timer.cancel();

        Uri uri = Uri.parse(endingVideoUrl);
        videoPlayerEnd.setVideoURI(uri);
        videoPlayerEnd.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                videoPlayerEnd.start();
            }
        });
        videoPlayerEnd.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoPlayerEnd.pause();
                mQuizQOD.stopQuiz();

            }
        });
    }

    /* class for coundown*/
    @SuppressLint("NewApi")
    public class CounterClass extends CountDownTimer {
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            timer_tv.setVisibility(View.GONE);
            timer.cancel();
            onSubmitTimeOut();

        }


        @SuppressLint("NewApi")

        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;

            String hms = String.format("%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            System.out.println(hms);
            timer_tv.setText(hms);
        }
    }


    /**
     * update QOD
     */
    public void updateQODRequest(String qustionResult) {


        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                GWProgressDialog.getProgressDialog(mActivity).showDialog1();


                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.updateQODRequest(
                        userId,
                        questionId,
                        qustionResult);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {
                            /**
                             * dismiss dialog if open
                             */
                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            /**
                             * set data into preference and update on views
                             */
                            Calendar currentDate = Calendar.getInstance();
                            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                            String formattedDate = df.format(currentDate.getTime());
                            PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                            preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_RESULT_QUESTION, questionId);
                            preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_RESULT_DATE, formattedDate);


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();
                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            NetworkUtil.getObj().showAlertDialog(mActivity, networkCallBack3);
        }
    }

    DialogInterface.OnClickListener networkCallBack3 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

            Calendar currentDate = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String currentFormattedDate = df.format(currentDate.getTime());
            PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
            String savedDate = preferenceUserUtil.getString(preferenceUserUtil.KEY_RESULT_DATE);
            String questionId = preferenceUserUtil.getString(preferenceUserUtil.KEY_RESULT_QUESTION);

            if (!TextUtils.isEmpty(savedDate)
                    && !TextUtils.isEmpty(questionId)
                    && currentFormattedDate.equalsIgnoreCase(savedDate)){

            }else{
                updateQODRequest(question_result);
            }

        }
    };

    public class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.CourseHolder> {

        private ArrayList<String> answerOptionList;
        private Context mContext;
        public ArrayList<String> selectedAnswer = new ArrayList<>();
        public ArrayList<String> selectedAnswer_ = new ArrayList<>();
        private DisplayImageOptions mDisplayImageOptions;
        char x = 'a';

        public AnswerAdapter(Context context, ArrayList<String> answerOptionList) {
            this.answerOptionList = answerOptionList;
            this.mContext = context;
            mDisplayImageOptions = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.no_media)
                    .showImageForEmptyUri(R.drawable.no_media)
                    .showImageOnFail(R.drawable.no_media)
                    .resetViewBeforeLoading(false)
                    .delayBeforeLoading(1000)
                    .cacheInMemory(false)
                    .cacheOnDisk(false)
                    .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                    .bitmapConfig(Bitmap.Config.ARGB_8888)
                    .displayer(new SimpleBitmapDisplayer())
                    .handler(new Handler())
                    .build();


        }

        @Override
        public CourseHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_answer_row_selector, viewGroup, false);
            CourseHolder mh = new CourseHolder(v);

            return mh;
        }

        @Override
        public void onBindViewHolder(final CourseHolder holder, int position) {
            holder.answerCh.setTag(position);
            if (answerOptionList.get(position).contains(".png") || answerOptionList.get(position).contains(".jpg")) {
                ImageLoader.getInstance().displayImage(ApiClient.BASE_URL + answerOptionList.get(position), holder.answer_img, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        holder.progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        holder.progressBar.setVisibility(View.GONE);

                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        holder.progressBar.setVisibility(View.GONE);

                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        holder.progressBar.setVisibility(View.GONE);

                    }
                });
                holder.answer_img.setVisibility(View.VISIBLE);
            } else {
                holder.answer_img.setVisibility(View.GONE);
                holder.answerCh.setText(answerOptionList.get(position));
            }
            selectedAnswer_.add(x + "");
            x++;
            holder.answerCh.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int pos = (int) buttonView.getTag();
                    if (selectedAnswer.contains(selectedAnswer_.get(pos))) {
                        selectedAnswer.remove(selectedAnswer_.get(pos));
                    } else {
                        selectedAnswer.add(selectedAnswer_.get(pos));
                    }
                }
            });
        }


        @Override
        public int getItemCount() {
            return answerOptionList.size();
        }


        public class CourseHolder extends RecyclerView.ViewHolder {

            private CheckBox answerCh;
            ImageView answer_img;
            private ProgressBar progressBar;

            public CourseHolder(View view) {
                super(view);
                answerCh = (CheckBox) view.findViewById(R.id.answer_ch);
                answer_img = (ImageView) view.findViewById(R.id.answer_img);
                progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            }


        }
    }


}
