package com.george.way.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.george.way.R;
import com.george.way.activity.BuyAcitvity;
import com.george.way.activity.GeorgeHomeActivity;
import com.george.way.activity.LoginActivity;
import com.george.way.activity.OnlineTutoringActivity;
import com.george.way.animUtils.ViewAnimUtils;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.GeorgeWayApplication;
import com.george.way.utils.NetworkUtil;
import com.george.way.webApi.ApiClient;
import com.george.way.webApi.ApiInterface;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by hemendrag on 1/31/2017.
 */

public class SingleActionDialog {





    /**
     * show normal dialog
     */
    private  AlertDialog alertDialogBack;
    public void showDialogNormal(String errorTitle, final Activity activity, final boolean isKill) {

         try {
            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_dialog_normal, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder.setView(promptsView);

            if(alertDialogBack == null)
            alertDialogBack = alertDialogBuilder.create();

            /**
             * setting up window design
             */
            Window mWindow = alertDialogBack.getWindow();
            mWindow.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mWindow.setDimAmount(0.5f);
            mWindow.setGravity(Gravity.CENTER);

            if(alertDialogBack.isShowing()){
                alertDialogBack.dismiss();
            }

            alertDialogBack.show();

            TextView textViewErrorTitle = (TextView) promptsView.findViewById(R.id.textViewErrorTitle);
            textViewErrorTitle.setText(errorTitle);

            Button buttonOk = (Button) promptsView.findViewById(R.id.buttonOk);
            buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(isKill){
                        alertDialogBack.dismiss();
                        activity.finish();
                        ViewAnimUtils.activityExitTransitions(activity);
                    }else {
                        alertDialogBack.dismiss();
                    }

                }
            });

            alertDialogBack.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    if(isKill){
                        alertDialogBack.dismiss();
                        activity.finish();
                        ViewAnimUtils.activityExitTransitions(activity);
                    }else {
                        alertDialogBack.dismiss();

                    }
                }
            });

            alertDialogBack.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    if(isKill){
                        alertDialogBack.dismiss();
                        activity.finish();
                        ViewAnimUtils.activityExitTransitions(activity);
                    }else {
                        alertDialogBack.dismiss();

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * show error dialog in case of error
     */
    private AlertDialog dialogLogin;

    public void showDialogForLogin(String title, final Activity activity) {

        try {
            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_dialog_ask_login, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder.setView(promptsView);

            dialogLogin = alertDialogBuilder.create();

            /**
             * setting up window design
             */
            Window mWindow = dialogLogin.getWindow();
            mWindow.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mWindow.setDimAmount(0.5f);
            mWindow.setGravity(Gravity.CENTER);

            if (dialogLogin.isShowing()) {
                dialogLogin.dismiss();
            }
            dialogLogin.show();

            TextView textViewErrorTitle = (TextView) promptsView.findViewById(R.id.textViewErrorTitle);
            textViewErrorTitle.setText(title);

            Button buttonSignUp = (Button) promptsView.findViewById(R.id.buttonSignUp);
            Button buttonCancel = (Button) promptsView.findViewById(R.id.buttonCancel);

            buttonSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(activity);
                    preferenceUserUtil.setPreferenceBooleanData(preferenceUserUtil.KEY_RE_LOGIN,true);

                    dialogLogin.dismiss();
                    Intent intentFeedBack = new Intent(activity, LoginActivity.class);
                    intentFeedBack.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intentFeedBack);
                    ViewAnimUtils.activityEnterTransitions(activity);

                }
            });

            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogLogin.dismiss();
                }
            });


        } catch (Exception e) {

            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_dialog_ask_login, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder.setView(promptsView);

            dialogLogin = alertDialogBuilder.create();

            /**
             * setting up window design
             */
            Window mWindow = dialogLogin.getWindow();
            mWindow.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mWindow.setDimAmount(0.5f);
            mWindow.setGravity(Gravity.CENTER);

            if (dialogLogin.isShowing()) {
                dialogLogin.dismiss();
            }
            dialogLogin.show();

            TextView textViewErrorTitle = (TextView) promptsView.findViewById(R.id.textViewErrorTitle);
            textViewErrorTitle.setText(title);

            Button buttonSignUp = (Button) promptsView.findViewById(R.id.buttonSignUp);
            Button buttonCancel = (Button) promptsView.findViewById(R.id.buttonCancel);

            buttonSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogLogin.dismiss();

                    PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(activity);
                    preferenceUserUtil.setPreferenceBooleanData(preferenceUserUtil.KEY_RE_LOGIN,true);

                    Intent intentFeedBack = new Intent(activity, LoginActivity.class);
                    intentFeedBack.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intentFeedBack);
                    ViewAnimUtils.activityEnterTransitions(activity);

                }
            });

            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogLogin.dismiss();
                }
            });

        }
    }



    /**
     * show error dialog in case of error
     */
    private AlertDialog dialogSubscribe;

    public void showDialogForSubscription(String title, final Activity activity) {

        try {
            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_dialog_ask_subscription, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder.setView(promptsView);

            dialogSubscribe = alertDialogBuilder.create();

            /**
             * setting up window design
             */
            Window mWindow = dialogSubscribe.getWindow();
            mWindow.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mWindow.setDimAmount(0.5f);
            mWindow.setGravity(Gravity.CENTER);

            if (dialogSubscribe.isShowing()) {
                dialogSubscribe.dismiss();
            }
            dialogSubscribe.show();

            TextView textViewErrorTitle = (TextView) promptsView.findViewById(R.id.textViewErrorTitle);
            textViewErrorTitle.setText(title);

            Button buttonSignUp = (Button) promptsView.findViewById(R.id.buttonSignUp);
            Button buttonCancel = (Button) promptsView.findViewById(R.id.buttonCancel);

            buttonSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /**
                     * update click button
                     */
                    PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(activity);
                    preferenceUserUtil.setPreferenceBooleanData(preferenceUserUtil.KEY_BTN_CLICKED,true);
                    preferenceUserUtil.setPreferenceIntData(preferenceUserUtil.KEY_BTN_TYPE,3);

                    dialogSubscribe.dismiss();
                    Intent intentFeedBack = new Intent(activity, BuyAcitvity.class);
                    intentFeedBack.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intentFeedBack);
                    ViewAnimUtils.activityEnterTransitions(activity);

                }
            });

            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogSubscribe.dismiss();
                }
            });


        } catch (Exception e) {

            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_dialog_ask_login, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder.setView(promptsView);

            dialogSubscribe = alertDialogBuilder.create();

            /**
             * setting up window design
             */
            Window mWindow = dialogSubscribe.getWindow();
            mWindow.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mWindow.setDimAmount(0.5f);
            mWindow.setGravity(Gravity.CENTER);

            if (dialogSubscribe.isShowing()) {
                dialogSubscribe.dismiss();
            }
            dialogSubscribe.show();

            TextView textViewErrorTitle = (TextView) promptsView.findViewById(R.id.textViewErrorTitle);
            textViewErrorTitle.setText(title);

            Button buttonSignUp = (Button) promptsView.findViewById(R.id.buttonSignUp);
            Button buttonCancel = (Button) promptsView.findViewById(R.id.buttonCancel);

            buttonSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    /**
                     * update click button
                     */
                    PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(activity);
                    preferenceUserUtil.setPreferenceBooleanData(preferenceUserUtil.KEY_BTN_CLICKED,true);
                    preferenceUserUtil.setPreferenceIntData(preferenceUserUtil.KEY_BTN_TYPE,6);
                     dialogSubscribe.dismiss();
                    Intent intentFeedBack = new Intent(activity, LoginActivity.class);
                    intentFeedBack.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intentFeedBack);
                    ViewAnimUtils.activityEnterTransitions(activity);




                }
            });

            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogSubscribe.dismiss();
                }
            });

        }
    }


    /**
     * show error dialog in case of error
     */
    private AlertDialog dialogLogout;

    public void showDialogForLogout(String title, final Activity activity) {

        try {
            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_dialog_ask_logout, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder.setView(promptsView);

            dialogLogout = alertDialogBuilder.create();

            /**
             * setting up window design
             */
            Window mWindow = dialogLogout.getWindow();
            mWindow.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mWindow.setDimAmount(0.5f);
            mWindow.setGravity(Gravity.CENTER);

            if (dialogLogout.isShowing()) {
                dialogLogout.dismiss();
            }
            dialogLogout.show();

            TextView textViewErrorTitle = (TextView) promptsView.findViewById(R.id.textViewErrorTitle);
            textViewErrorTitle.setText(title);

            Button buttonSignUp = (Button) promptsView.findViewById(R.id.buttonSignUp);
            Button buttonCancel = (Button) promptsView.findViewById(R.id.buttonCancel);

            buttonSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    dialogLogout.dismiss();

                    /**
                     * call api to log out user from device
                     */
                    callLogoutApi(activity);


                }
            });

            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogLogout.dismiss();
                }
            });


        } catch (Exception e) {

            e.printStackTrace();

        }
    }


    public void showDialogForChatDelete(String title, final Activity activity) {

        try {
            LayoutInflater li = LayoutInflater.from(activity);
            View promptsView = li.inflate(R.layout.layout_dialog_ask_logout, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder.setView(promptsView);

            dialogLogout = alertDialogBuilder.create();

            /**
             * setting up window design
             */
            Window mWindow = dialogLogout.getWindow();
            mWindow.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mWindow.setDimAmount(0.5f);
            mWindow.setGravity(Gravity.CENTER);

            if (dialogLogout.isShowing()) {
                dialogLogout.dismiss();
            }
            dialogLogout.show();

            TextView textViewErrorTitle = (TextView) promptsView.findViewById(R.id.textViewErrorTitle);
            textViewErrorTitle.setText(title);

            Button buttonSignUp = (Button) promptsView.findViewById(R.id.buttonSignUp);
            Button buttonCancel = (Button) promptsView.findViewById(R.id.buttonCancel);

            buttonSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    dialogLogout.dismiss();

                    /**
                     * call api to log out user from device
                     */
                    callChatDeleteApi(activity);


                }
            });

            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogLogout.dismiss();
                }
            });


        } catch (Exception e) {

            e.printStackTrace();

        }
    }


    /**
     * call api to log out user from device
     */
    private void callLogoutApi(final Activity mActivity) {
        /**
         * call api for fetching categories password
         */


        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                if (!GWProgressDialog.getProgressDialog(mActivity).isDialogShowing()) {
                    GWProgressDialog.getProgressDialog(mActivity).showDialog1();
                }

                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(mActivity);
                String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);

                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.logoutApiRequest(userId);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {

                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            JSONObject mJsonObj = new JSONObject(new String(response.body().bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String result = mJsonObj.optString("result");
                            if (!TextUtils.isEmpty(result) && result.equalsIgnoreCase("success")) {

                                Toast.makeText(mActivity,mActivity.getResources().getString(R.string.you_have_logged_out),Toast.LENGTH_SHORT).show();
                                /**
                                 * clear data in case of logout
                                 */
                                ((GeorgeHomeActivity)mActivity).settingUpLogoutData();

                            }else {

                                new SingleActionDialog().showDialogNormal(mActivity.getResources().getString(R.string.sorry_something_went_wrong),mActivity,false);
                            }



                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * call api to log out user from device
     */
    private void callChatDeleteApi(final Activity mActivity) {
        /**
         * call api for fetching categories password
         */


        if (NetworkUtil.isConnected(mActivity)) {

            /**
             * hide keyboard if open
             */
            AppUtils.getObj().hideSoftKeyboard(mActivity);

            try {

                /**
                 * show if progress dialog is not showing
                 */
                if (!GWProgressDialog.getProgressDialog(mActivity).isDialogShowing()) {
                    GWProgressDialog.getProgressDialog(mActivity).showDialog1();
                }


                ApiInterface mApiInterface = ApiClient.getClient().getRetrofitInstance().create(ApiInterface.class);
                Call<ResponseBody> call = mApiInterface.deleteChat(android.text.TextUtils.join(",", GeorgeWayApplication.msg_id));

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {

                            GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                            JSONObject mJsonObj = new JSONObject(new String(response.body().bytes()));
                            Log.e("tg", "response from server = " + mJsonObj.toString());
                            String result = mJsonObj.optString("status_code");
                            if (!TextUtils.isEmpty(result) && result.equalsIgnoreCase("200")) {

                                /**
                                 * clear data in case of logout
                                 */
                                ((OnlineTutoringActivity)mActivity).updateMoreUi(true);
                                GeorgeWayApplication.msg_id.clear();

                            }else {

                                new SingleActionDialog().showDialogNormal(mActivity.getResources().getString(R.string.sorry_something_went_wrong),mActivity,false);
                            }



                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        /**
                         * dismiss dialog if open
                         */
                        GWProgressDialog.getProgressDialog(mActivity).dismissDialog1();

                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


}
