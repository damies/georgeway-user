package com.george.way.dialog;

import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.george.way.R;


/**
 * Created by hemendrag on 1/13/2017.
 */

public class ValidationErrorDialog {

    /**
     * create a singleton method to access variables of this class
     */
    public  static ValidationErrorDialog mValidationErrorDialog;
    private static  Activity mActivity;

    public static  ValidationErrorDialog getValidationUtilsObj(Activity activity){

        mActivity = activity;

        if(mValidationErrorDialog == null)
            mValidationErrorDialog = new ValidationErrorDialog();

        return mValidationErrorDialog;
    }


    /**
     * show error dialog in case of error
     */
    public void showErrorDialog(String errorTitle) {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(mActivity);
        View promptsView = li.inflate(R.layout.layout_dialog_normal, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        alertDialogBuilder.setView(promptsView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        TextView textViewErrorTitle = (TextView) promptsView.findViewById(R.id.textViewErrorTitle);
        textViewErrorTitle.setText(errorTitle);

        Button buttonOk = (Button) promptsView.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

}
