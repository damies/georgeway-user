package com.george.way.drawingBoard.manager;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileManager
{
	public static Uri saveBitmap(Bitmap bitmap) {

		try{

		String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
				File.separator +
				"GeorgeWayDrawing"+
				File.separator;

		File dir = new File(file_path);

		if(!dir.exists()) {
			dir.mkdirs();
		}

		String name = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()).concat(".png");

		File outFile = new File(dir.getAbsolutePath() + File.separator + name);
		if (!outFile.exists()){
			outFile.createNewFile();
		}

		FileOutputStream fOut;

			fOut = new FileOutputStream(outFile);
			bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
			fOut.flush();
			fOut.close();
			return Uri.fromFile(outFile);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
