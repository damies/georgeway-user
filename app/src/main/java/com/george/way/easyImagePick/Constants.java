package com.george.way.easyImagePick;

public interface Constants {

    String DEFAULT_FOLDER_NAME = "VacAdmin";
    String TEMP_FOLDER_NAME = "VacAdmin";

    interface RequestCodes {
        int EASYIMAGE_IDENTIFICATOR = 0b1101101100; //876
        int SOURCE_CHOOSER = 1 << 14;

        int PICK_PICTURE_FROM_DOCUMENTS = EASYIMAGE_IDENTIFICATOR + (1 << 11);
        int PICK_PICTURE_FROM_GALLERY = EASYIMAGE_IDENTIFICATOR + (1 << 12);
        int TAKE_PICTURE = EASYIMAGE_IDENTIFICATOR + (1 << 13);
    }

    interface BundleKeys {
        String FOLDER_NAME = "com.vacadmin.folder_name";
        String ALLOW_MULTIPLE = "com.vacadmin.allow_multiple";
        String COPY_TAKEN_PHOTOS = "com.vacadmin.copy_taken_photos";
        String COPY_PICKED_IMAGES = "com.vacadmin.copy_picked_images";
    }
}