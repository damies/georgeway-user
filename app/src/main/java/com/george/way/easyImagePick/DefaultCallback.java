package com.george.way.easyImagePick;

public abstract class DefaultCallback implements GeorgeWayImagePick.Callbacks {

    @Override
    public void onImagePickerError(Exception e, GeorgeWayImagePick.ImageSource source, int type) {
    }

    @Override
    public void onCanceled(GeorgeWayImagePick.ImageSource source, int type) {
    }
}