package com.george.way.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.george.way.R;
import com.george.way.activity.GeorgeHomeActivity;
import com.george.way.prefrences.PreferenceUserUtil;
import com.george.way.utils.AppUtils;
import com.george.way.utils.GeorgeWayApplication;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(getApplicationContext());
        preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_DEVICE_TOKEN,s);

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());

         if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());


            try {
                String msgData = remoteMessage.getData().get("message");
                if (msgData.startsWith("http") || msgData.startsWith("https")) {
                    msgData = msgData.replaceAll("chat-image", "chat-images");
                }
                String type = remoteMessage.getData().get("type");
                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance(getBaseContext());
                if (type.equalsIgnoreCase("1"))
                {

                    preferenceUserUtil.setPreferenceStringData(preferenceUserUtil.KEY_TUTORING_SERVICE, "1");
                }
                Log.e(" sample "," "+preferenceUserUtil.getString(preferenceUserUtil.KEY_TUTORING_SERVICE));

                sendNotification(msgData, type);

            } catch (Exception e) {
                e.printStackTrace();
            }


            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.

            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See NotificationAsync method below.
    }
    // [END receive_message]


    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody, String type) throws Exception {

         Intent homeIntent = new Intent();

        if (!AppUtils.getObj().isAppIsInBackground(this)) {

            homeIntent = new Intent("android.intent.category.LAUNCHER");
            homeIntent.setClassName(getApplicationContext(), "com.george.way.activity.GeorgeHomeActivity");
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            homeIntent.putExtra(GeorgeHomeActivity.KEY_SHOULD_CALL_API, true);
            homeIntent.putExtra(GeorgeHomeActivity.KEY_PUSH_TYPE, type);


        } else if (GeorgeWayApplication.activityResumed()) {


            /**
             * update push type Ui
             */
            GeorgeHomeActivity.getInstance().updateUI(type);



        } else {

            homeIntent = new Intent("android.intent.category.LAUNCHER");
            homeIntent.setClassName(getApplicationContext(), "com.george.way.activity.GeorgeHomeActivity");
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            homeIntent.putExtra(GeorgeHomeActivity.KEY_SHOULD_CALL_API, true);
            homeIntent.putExtra(GeorgeHomeActivity.KEY_PUSH_TYPE, type);


        }


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, homeIntent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


    private void updateNotificationImage(String urlImg) {
        InputStream in;
        try {

            urlImg = urlImg.replaceAll("chat-image", "chat-images");
            URL url = new URL(urlImg);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            in = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(in);

            Intent intent = new Intent(getBaseContext(), GeorgeHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getBaseContext())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setAutoCancel(true)
                    .setLargeIcon(myBitmap)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0, notificationBuilder.build());

           /* String title="Notification !!!";
            int icon = R.mipmap.ic_logo;
            long when = System.currentTimeMillis();

            Notification notification = new Notification(icon, title, when);

            NotificationManager mNotificationManager =
                    (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

            RemoteViews contentView = new RemoteViews
                    (getPackageName(), R.layout.layout_row_custom_notification);
            contentView.setImageViewBitmap(R.id.image, myBitmap);
            contentView.setTextViewText(R.id.title, title);
            contentView.setTextViewText(R.id.text,
                    getResources().getString(R.string.app_name));
            notification.contentView = contentView;

            Intent notificationIntent = new Intent(this, GeorgeHomeActivity.class);
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent contentIntent = PendingIntent.getActivity(this,0,
                    notificationIntent, 0);
            notification.contentIntent = contentIntent;

            notification.flags |=
                    Notification.FLAG_NO_CLEAR; //Do not clear  the notification
            notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
            notification.defaults |= Notification.DEFAULT_VIBRATE;//Vibration
            notification.defaults |= Notification.DEFAULT_SOUND; // Sound

            mNotificationManager.notify(1, notification);*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private class NotificationAsync extends AsyncTask<String, Void, Bitmap> {

        Context ctx;

        public NotificationAsync(Context context) {
            super();
            this.ctx = context;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            InputStream in;
            try {

                String urlImg = params[0];
                urlImg.replaceAll("chat-image", "chat-images");
                URL url = new URL(urlImg);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                in = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(in);
                return myBitmap;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {

            super.onPostExecute(result);
            try {
                Intent intent = new Intent(ctx, GeorgeHomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(ctx, 0 /* Request code */, intent,
                        PendingIntent.FLAG_ONE_SHOT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(ctx)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}