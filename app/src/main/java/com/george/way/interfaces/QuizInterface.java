package com.george.way.interfaces;

public interface QuizInterface {
    void startQuiz();
    void showQuestion();
    void submitQuestion();
    void stopQuiz();
}