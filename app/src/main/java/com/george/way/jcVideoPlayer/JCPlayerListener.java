package com.george.way.jcVideoPlayer;

/**
 * Created by hemendrag on 6/30/2017.
 */

public interface JCPlayerListener {

    public abstract void onVideoClickListener(JCVideoPlayer jcVideoPlayer);

}
