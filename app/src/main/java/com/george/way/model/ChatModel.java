package com.george.way.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by JAI on 2/19/2018.
 */

public class ChatModel
{
    private String msg;
    private String sender_id;
    private Map<String, String> timestamp;
    private String type;
    private String media;
    private String send_by;

    public String getSend_by() {
        return send_by;
    }

    public void setSend_by(String send_by) {
        this.send_by = send_by;
    }

    public ChatModel(String msg, String sender_id, Map<String, String> timestamp, String type, String media, String send_by) {
        this.msg = msg;
        this.sender_id = sender_id;
        this.timestamp = timestamp;
        this.type = type;
        this.media = media;
        this.send_by = send_by;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public Map<String, String> getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(HashMap timestamp) {
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }
}
