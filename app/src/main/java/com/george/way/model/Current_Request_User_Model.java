package com.george.way.model;

import com.google.firebase.database.ServerValue;

/**
 * Created by JAI on 2/17/2018.
 */

public class Current_Request_User_Model
{
    private String first_name;
    private String last_name;
    private String image;
    private String id;
    private Object time_stamp;

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getImage() {
        return image;
    }

    public String getId() {
        return id;
    }

    public String getSchool() {
        return school;
    }

    private String school;

    public Current_Request_User_Model(String first_name, String last_name, String image, String id, String school) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.image = image;
        this.id = id;
        this.school = school;
        this.time_stamp = ServerValue.TIMESTAMP;
    }

    public Object getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(Object time_stamp) {
        this.time_stamp = time_stamp;
    }

}
