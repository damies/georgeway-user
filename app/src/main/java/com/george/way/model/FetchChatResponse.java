package com.george.way.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 13-May-17.
 */

public class FetchChatResponse {


        @SerializedName("status_code")
        @Expose
        private String statusCode;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("success_message")
        @Expose
        private String successMessage;
        @SerializedName("data")
        @Expose
        private List<Datum> data = null;

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSuccessMessage() {
            return successMessage;
        }

        public void setSuccessMessage(String successMessage) {
            this.successMessage = successMessage;
        }

        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }



    public class Datum {

        @SerializedName("user_id_1")
        @Expose
        private String userId1;
        @SerializedName("user_id_2")
        @Expose
        private String userId2;
        @SerializedName("datetime")
        @Expose
        private String datetime;
        @SerializedName("message")
        @Expose
        private String message;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @SerializedName("id")
        @Expose
        private String id;



        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        @SerializedName("media")
        @Expose
        private String image;

        public String getUserId1() {
            return userId1;
        }

        public void setUserId1(String userId1) {
            this.userId1 = userId1;
        }

        public String getUserId2() {
            return userId2;
        }

        public void setUserId2(String userId2) {
            this.userId2 = userId2;
        }

        public String getDatetime() {
            return datetime;
        }

        public void setDatetime(String datetime) {
            this.datetime = datetime;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

    }


}
