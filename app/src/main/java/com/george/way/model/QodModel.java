package com.george.way.model;

import java.util.ArrayList;

/**
 * Created by hem on 4/26/2017.
 */
public class QodModel {

    /**
     * declaring variables of class
     */
    private String  startingVideoUrl;
    private String  endingVideoUrl;
    private String  question;
    private ArrayList<String> answerList;


    /**
     * declare constructor
     * @param startingVideoUrl
     * @param endingVideoUrl
     * @param question
     * @param answerList
     */
    public QodModel(String startingVideoUrl,
                    String endingVideoUrl
                    , String question,
                    ArrayList<String> answerList) {

        this.startingVideoUrl = startingVideoUrl;
        this.endingVideoUrl = endingVideoUrl;
        this.question = question;
        this.answerList = answerList;

    }

    /**
     * generate the getter and setter
     */
    public String getStartingVideoUrl() {
        return startingVideoUrl;
    }

    public void setStartingVideoUrl(String startingVideoUrl) {
        this.startingVideoUrl = startingVideoUrl;
    }

    public String getEndingVideoUrl() {
        return endingVideoUrl;
    }

    public void setEndingVideoUrl(String endingVideoUrl) {
        this.endingVideoUrl = endingVideoUrl;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public ArrayList<String> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(ArrayList<String> answerList) {
        this.answerList = answerList;
    }
}
