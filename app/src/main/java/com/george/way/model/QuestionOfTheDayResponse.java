package com.george.way.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by shachindrap on 5/1/2017.
 */

public class QuestionOfTheDayResponse {
    @SerializedName("status_code")
    @Expose
    private String statusCode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("success_message")
    @Expose
    private String successMessage;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("questionid")
        @Expose
        private String questionid;
        @SerializedName("category_name")
        @Expose
        private String categoryName;
        @SerializedName("question_type")
        @Expose
        private String questionType;
        @SerializedName("question")
        @Expose
        private String question;
        @SerializedName("field_a")
        @Expose
        private String fieldA;
        @SerializedName("field_b")
        @Expose
        private String fieldB;
        @SerializedName("field_c")
        @Expose
        private String fieldC;
        @SerializedName("field_d")
        @Expose
        private String fieldD;
        @SerializedName("field_e")
        @Expose
        private String fieldE;
        @SerializedName("answer_number")
        @Expose
        private String answerNumber;
        @SerializedName("explaination")
        @Expose
        private String explaination;

        public String getQuestionid() {
            return questionid;
        }

        public void setQuestionid(String questionid) {
            this.questionid = questionid;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getQuestionType() {
            return questionType;
        }

        public void setQuestionType(String questionType) {
            this.questionType = questionType;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public String getFieldA() {
            return fieldA;
        }

        public void setFieldA(String fieldA) {
            this.fieldA = fieldA;
        }

        public String getFieldB() {
            return fieldB;
        }

        public void setFieldB(String fieldB) {
            this.fieldB = fieldB;
        }

        public String getFieldC() {
            return fieldC;
        }

        public void setFieldC(String fieldC) {
            this.fieldC = fieldC;
        }

        public String getFieldD() {
            return fieldD;
        }

        public void setFieldD(String fieldD) {
            this.fieldD = fieldD;
        }

        public String getFieldE() {
            return fieldE;
        }

        public void setFieldE(String fieldE) {
            this.fieldE = fieldE;
        }

        public String getAnswerNumber() {
            return answerNumber;
        }

        public void setAnswerNumber(String answerNumber) {
            this.answerNumber = answerNumber;
        }

        public String getExplaination() {
            return explaination;
        }

        public void setExplaination(String explaination) {
            this.explaination = explaination;
        }

    }
}

