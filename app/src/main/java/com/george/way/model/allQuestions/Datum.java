
package com.george.way.model.allQuestions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum implements Serializable
{

    @SerializedName("questionid")
    @Expose
    private String questionid;
    @SerializedName("question_video")
    @Expose
    private String questionVideo;
    @SerializedName("question_video1")
    @Expose
    private String questionVideo1;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("question_type")
    @Expose
    private String questionType;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("field_a")
    @Expose
    private String fieldA;
    @SerializedName("field_b")
    @Expose
    private String fieldB;
    @SerializedName("field_c")
    @Expose
    private String fieldC;
    @SerializedName("field_d")
    @Expose
    private String fieldD;
    @SerializedName("field_e")
    @Expose
    private String fieldE;
    @SerializedName("answer_number")
    @Expose
    private String answerNumber;
    @SerializedName("explaination")
    @Expose
    private String explaination;

    @SerializedName("subscribe_status")
    @Expose
    private String subscribe_status;



    private final static long serialVersionUID = 5896300366930048693L;

    public String getQuestionid() {
        return questionid;
    }

    public void setQuestionid(String questionid) {
        this.questionid = questionid;
    }

    public String getQuestionVideo() {
        return questionVideo;
    }

    public void setQuestionVideo(String questionVideo) {
        this.questionVideo = questionVideo;
    }

    public String getQuestionVideo1() {
        return questionVideo1;
    }

    public void setQuestionVideo1(String questionVideo1) {
        this.questionVideo1 = questionVideo1;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getFieldA() {
        return fieldA;
    }

    public void setFieldA(String fieldA) {
        this.fieldA = fieldA;
    }

    public String getFieldB() {
        return fieldB;
    }

    public void setFieldB(String fieldB) {
        this.fieldB = fieldB;
    }

    public String getFieldC() {
        return fieldC;
    }

    public void setFieldC(String fieldC) {
        this.fieldC = fieldC;
    }

    public String getFieldD() {
        return fieldD;
    }

    public void setFieldD(String fieldD) {
        this.fieldD = fieldD;
    }

    public String getFieldE() {
        return fieldE;
    }

    public void setFieldE(String fieldE) {
        this.fieldE = fieldE;
    }

    public String getAnswerNumber() {
        return answerNumber;
    }

    public void setAnswerNumber(String answerNumber) {
        this.answerNumber = answerNumber;
    }

    public String getExplaination() {
        return explaination;
    }

    public void setExplaination(String explaination) {
        this.explaination = explaination;
    }

    public String getSubscribe_status() {
        return subscribe_status;
    }

    public void setSubscribe_status(String subscribe_status) {
        this.subscribe_status = subscribe_status;
    }
}
