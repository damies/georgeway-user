
package com.george.way.model.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ChildCategory implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("parent_cat")
    @Expose
    private String parentCat;
    @SerializedName("child_sub_category")
    @Expose
    private List<ChildSubCategory> childSubCategory = null;
    private final static long serialVersionUID = -9196615002953827538L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentCat() {
        return parentCat;
    }

    public void setParentCat(String parentCat) {
        this.parentCat = parentCat;
    }

    public List<ChildSubCategory> getChildSubCategory() {
        return childSubCategory;
    }

    public void setChildSubCategory(List<ChildSubCategory> childSubCategory) {
        this.childSubCategory = childSubCategory;
    }

}
