package com.george.way.model.chat;

/*
 * Created by  on 3/21/2017.
 */
public class MsgBean {


    private String senderMemberId = "";
    private String senderMemberName = "";
    private String senderMemberImage = "";
    private String message = "";
    private String dateTime = "";
    private String msgID = "";
    private String chatType="";
    private String adminId = "";



    private String image = "";
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public String getSenderMemberId() {
        return senderMemberId;
    }

    public void setSenderMemberId(String senderMemberId) {
        this.senderMemberId = senderMemberId;
    }

    public String getSenderMemberName() {
        return senderMemberName;
    }

    public void setSenderMemberName(String senderMemberName) {
        this.senderMemberName = senderMemberName;
    }

    public String getSenderMemberImage() {
        return senderMemberImage;
    }

    public void setSenderMemberImage(String senderMemberImage) {
        this.senderMemberImage = senderMemberImage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getMsgID() {
        return msgID;
    }

    public void setMsgID(String msgID) {
        this.msgID = msgID;
    }
}
