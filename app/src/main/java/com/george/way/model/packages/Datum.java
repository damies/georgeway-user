
package com.george.way.model.packages;

import com.george.way.constant.AppConstant;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum implements Serializable
{

    @SerializedName("pakageid")
    @Expose
    private String pakageid;
    @SerializedName("pakage_name")
    @Expose
    private String pakageName;
    @SerializedName("pakage_amount")
    @Expose
    private String pakageAmount;
    @SerializedName("package_description")
    @Expose
    private String packageDescription;

    @SerializedName("hourly_pakage_amount")
    @Expose
    private String hourly_pakage_amount;

    public String getHourly_pakage_amount() {
        return hourly_pakage_amount;
    }

    public void setHourly_pakage_amount(String hourly_pakage_amount) {
        this.hourly_pakage_amount = hourly_pakage_amount;
    }

    public String getMinutely_pakage_amount() {
        return minutely_pakage_amount;
    }

    public void setMinutely_pakage_amount(String minutely_pakage_amount) {
        this.minutely_pakage_amount = minutely_pakage_amount;
    }

    @SerializedName("minutely_pakage_amount")
    @Expose
    private String minutely_pakage_amount;

    private boolean isPlanSubscribed = false;
    private int durationInMonth = 1;
    private String planStartDate = AppConstant.getObj().EMPTY_STRING;
    private String planEndDate = AppConstant.getObj().EMPTY_STRING;

    private final static long serialVersionUID = 612756126034510151L;

    public String getPakageid() {
        return pakageid;
    }

    public void setPakageid(String pakageid) {
        this.pakageid = pakageid;
    }

    public String getPakageName() {
        return pakageName;
    }

    public void setPakageName(String pakageName) {
        this.pakageName = pakageName;
    }

    public String getPakageAmount() {
        return pakageAmount;
    }

    public void setPakageAmount(String pakageAmount) {
        this.pakageAmount = pakageAmount;
    }

    public String getPackageDescription() {
        return packageDescription;
    }

    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }

    public boolean isPlanSubscribed() {
        return isPlanSubscribed;
    }

    public void setPlanSubscribed(boolean planSubscribed) {
        isPlanSubscribed = planSubscribed;
    }

    public int getDurationInMonth() {
        return durationInMonth;
    }

    public void setDurationInMonth(int durationInMonth) {
        this.durationInMonth = durationInMonth;
    }

    public String getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public String getPlanEndDate() {
        return planEndDate;
    }

    public void setPlanEndDate(String planEndDate) {
        this.planEndDate = planEndDate;
    }
}
