
package com.george.way.model.successStory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Datum implements Serializable
{

    @SerializedName("video")
    @Expose
    private String video;
    private final static long serialVersionUID = 4596497043549882326L;

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

}
