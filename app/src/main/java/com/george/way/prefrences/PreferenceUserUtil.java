package com.george.way.prefrences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.george.way.constant.AppConstant;
import com.george.way.model.QuestionOfTheDayResponse;
import com.google.gson.Gson;

/**
 * Created by tarunk on 5/19/2016.
 */
public class PreferenceUserUtil {


    protected static String MyPREFERENCES = "com.georgeway.PreferenceUserUtil";

    private static Activity msContext;
    private static Context context1;
    private static PreferenceUserUtil msPreferenceUserUtil;
    private static SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mSharedPreferencesEditor;

    /**
     * Application Keys
     */
    public final String KEY_USER_ID = "KEY_USER_ID";
    public final String KEY_EMAIL = "KEY_EMAIL";
    public final String KEY_FIRST_NAME = "KEY_FIRST_NAME";
    public final String KEY_LAST_NAME = "KEY_LAST_NAME";
    public final String KEY_ADDRESS = "KEY_ADDRESS";
    public final String KEY_USER_NAME = "KEY_USER_NAME";
    public final String KEY_USER_IMAGE = "KEY_USER_IMAGE";
    public final String KEY_USER_SOCIAL = "KEY_USER_SOCIAL";
    public final String subscription_start = "subscription_start";
    public final String subscription_end   = "subscription_end";
    public final String contact_email   = "contact_email";
    public final String contact_phone   = "contact_phone";

    public final String KEY_SUBSCRIPTION_PLAN = "KEY_SUBSCRIPTION_PLAN";
    public final String KEY_IS_PLAN_ACTIVE = "IS_PLAN_ACTIVE";
    public final String KEY_PLAN_ID = "PLAN_ID";
    public final String KEY_DATE = "KEY_DATE";
    public final String KEY_QUESTION_ID = "KEY_QUESTION_ID";
    public final String KEY_MATH_CLASS = "KEY_MATH_CLASS";
    public final String KEY_TEST_PREPARATION = "KEY_TEST_PREPARATION";
    public final String KEY_TUTORING_SERVICE = "KEY_TUTORING_SERVICE";
    public final String KEY_DEVICE_TOKEN = "KEY_DEVICE_TOKEN";
    public final String KEY_BTN_TYPE = "KEY_BTN_TYPE";
    public final String KEY_BTN_CLICKED = "KEY_BTN_CLICKED";

    public final String KEY_GMC_COLOR = "KEY_GMC_COLOR";
    public final String KEY_GMC_CONTENT = "KEY_GMC_CONTENT";

    public final String KEY_OTS_COLOR = "KEY_OTS_COLOR";
    public final String KEY_OTS_CONTENT = "KEY_OTS_CONTENT";

    public final String KEY_TP_COLOR = "KEY_TP_COLOR";
    public final String KEY_TP_CONTENT = "KEY_TP_CONTENT";

    public final String KEY_QTD_COLOR = "KEY_QTD_COLOR";
    public final String KEY_QTD_CONTENT = "KEY_QTD_CONTENT";

    public final String KEY_SELECTED_COLOR_TYPE = "KEY_SELECTED_COLOR_TYPE";
    public final String KEY_QUESTION_DATA = "KEY_QUESTION_DATA";
    public final String KEY_RESULT_QUESTION = "KEY_RESULT_QUESTION";
    public final String KEY_RESULT_DATE = "KEY_RESULT_DATE";
    public final String KEY_RE_LOGIN = "KEY_RE_LOGIN";

    public final String KEY_ADMIN_EMAIL_1 = "KEY_ADMIN_EMAIL_1";
    public final String KEY_ADMIN_EMAIL_2 = "KEY_ADMIN_EMAIL_2";
    public final String KEY_ADMIN_EMAIL_3 = "KEY_ADMIN_EMAIL_3";
    public final String KEY_ADMIN_EMAIL_4 = "KEY_ADMIN_EMAIL_4";
    public final String KEY_ADMIN_EMAIL_5 = "KEY_ADMIN_EMAIL_5";
    public final String KEY_PASSWORD = "KEY_PASSWORD";


    public static PreferenceUserUtil getInstance(Activity context) {

        msContext = context;

        if (msPreferenceUserUtil == null) {

            msPreferenceUserUtil = new PreferenceUserUtil();
        }

        return msPreferenceUserUtil;
    }

    public static PreferenceUserUtil getInstance(Context context) {

        context1 = context;
        if (msPreferenceUserUtil == null) {
            msPreferenceUserUtil = new PreferenceUserUtil(context1);
        }
        return msPreferenceUserUtil;
    }


    private PreferenceUserUtil() {
        mSharedPreferences = msContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    }

    private PreferenceUserUtil(Context context) {

        mSharedPreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    }

    public void setPreferenceStringData(String preferenceKey, String preferenceData) {

        mSharedPreferencesEditor = mSharedPreferences.edit();
        mSharedPreferencesEditor.putString(preferenceKey, preferenceData);
        mSharedPreferencesEditor.commit();

    }

    public String getString(String preferenceKey) {

        return mSharedPreferences.getString(preferenceKey, AppConstant.getObj().EMPTY_STRING);
    }


    public void setPreferenceIntData(String preferenceKey, int preferenceData) {

        mSharedPreferencesEditor = mSharedPreferences.edit();
        mSharedPreferencesEditor.putInt(preferenceKey, preferenceData);
        mSharedPreferencesEditor.commit();

    }

    public int getPreferenceIntData(String preferenceKey) {

        return mSharedPreferences.getInt(preferenceKey, 0);
    }


    public void setPreferenceBooleanData(String preferenceKey, boolean preferenceData) {

        mSharedPreferencesEditor = mSharedPreferences.edit();
        mSharedPreferencesEditor.putBoolean(preferenceKey, preferenceData);
        mSharedPreferencesEditor.commit();

    }

    public boolean getPreferenceBooleanData(String preferenceKey) {

        return mSharedPreferences.getBoolean(preferenceKey, false);
    }

    public void setPreferenceObjectData(String preferenceKey, QuestionOfTheDayResponse object) {

        mSharedPreferencesEditor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(object);
        mSharedPreferencesEditor.putString(preferenceKey, json);
        mSharedPreferencesEditor.commit();

    }

    public QuestionOfTheDayResponse getPreferenceObjectData(String preferenceKey) {
        Gson gson = new Gson();
        String json = mSharedPreferences.getString(preferenceKey, "");
        QuestionOfTheDayResponse obj = gson.fromJson(json, QuestionOfTheDayResponse.class);
        return obj;
    }


    public void clearAllPreference() {

        mSharedPreferencesEditor = mSharedPreferences.edit();
        mSharedPreferencesEditor.clear();
        mSharedPreferencesEditor.commit();

    }




}
