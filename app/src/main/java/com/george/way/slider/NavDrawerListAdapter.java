package com.george.way.slider;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.george.way.R;
import com.george.way.prefrences.PreferenceUserUtil;

import java.util.ArrayList;


public class NavDrawerListAdapter extends BaseAdapter {
	ToggleButton 							txtCount;
	private Activity 						context;
	private ArrayList<NavDrawerItem>	    navDrawerItems;
	
	public NavDrawerListAdapter(Activity context, ArrayList<NavDrawerItem> navDrawerItems){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {		
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }
		RelativeLayout mainREl = (RelativeLayout)convertView.findViewById(R.id.mainREl);
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
         View view  = (View) convertView.findViewById(R.id.view);
        imgIcon.setBackgroundResource(navDrawerItems.get(position).getIcon());

		PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance((Activity) context);
		try {
			if (navDrawerItems.get(position).getTitle().equalsIgnoreCase(context.getResources().getString(R.string.George_Math_Class))) {

				String text = preferenceUserUtil.getString(preferenceUserUtil.KEY_GMC_CONTENT);
				String colorCode = preferenceUserUtil.getString(preferenceUserUtil.KEY_GMC_COLOR);
				txtTitle.setText(text);
				txtTitle.setTextColor(Color.WHITE);
				mainREl.setBackgroundColor(Color.parseColor(colorCode));
			} else if (navDrawerItems.get(position).getTitle().equalsIgnoreCase(context.getResources().getString(R.string.Test_Preparation))) {

				String text = preferenceUserUtil.getString(preferenceUserUtil.KEY_TP_CONTENT);
				String colorCode = preferenceUserUtil.getString(preferenceUserUtil.KEY_TP_COLOR);
				txtTitle.setText(text);
				txtTitle.setTextColor(Color.WHITE);
				mainREl.setBackgroundColor(Color.parseColor(colorCode));
			} else if (navDrawerItems.get(position).getTitle().equalsIgnoreCase(context.getResources().getString(R.string.Online_tutoring_Service))) {

				String text = preferenceUserUtil.getString(preferenceUserUtil.KEY_OTS_CONTENT);
				String colorCode = preferenceUserUtil.getString(preferenceUserUtil.KEY_OTS_COLOR);
				txtTitle.setText(text);
				txtTitle.setTextColor(Color.WHITE);
				mainREl.setBackgroundColor(Color.parseColor(colorCode));
			} else if (navDrawerItems.get(position).getTitle().equalsIgnoreCase(context.getResources().getString(R.string.Question_Of_The_day))) {

				String text = preferenceUserUtil.getString(preferenceUserUtil.KEY_QTD_CONTENT);
				String colorCode = preferenceUserUtil.getString(preferenceUserUtil.KEY_QTD_COLOR);
				txtTitle.setText(text);
				txtTitle.setTextColor(Color.WHITE);
				mainREl.setBackgroundColor(Color.parseColor(colorCode));
			} else {

				if (navDrawerItems.get(position).getTitle().equalsIgnoreCase("Donate")) {
					imgIcon.setVisibility(View.VISIBLE);
					imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
					txtTitle.setTypeface(Typeface.DEFAULT_BOLD);
				}else
				{
					imgIcon.setVisibility(View.GONE);
				}
				mainREl.setBackgroundColor(Color.parseColor("#FFFFFF"));
				txtTitle.setText(navDrawerItems.get(position).getTitle());
				txtTitle.setTextColor(Color.BLACK);

			}
		}catch (Exception ex){
			mainREl.setBackgroundColor(Color.parseColor("#FFFFFF"));
			txtTitle.setText(navDrawerItems.get(position).getTitle());
			txtTitle.setTextColor(Color.BLACK);

		}


		String userId = preferenceUserUtil.getString(preferenceUserUtil.KEY_USER_ID);

		if (TextUtils.isEmpty(userId) && position== navDrawerItems.size()-1 && txtTitle.getText().toString().equalsIgnoreCase("Logout")){
			convertView.setVisibility(View.GONE);
 		}else {
			convertView.setVisibility(View.VISIBLE);
		}
      
          return convertView;
	}

}
