package com.george.way.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.george.way.constant.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jaikishang on 12/21/2016.
 */

public class AppUtils {

    public static View lastTouchLayout;

    /**
     * create a singleton method to access variables of this class
     */
    public static AppUtils mAppUtils;

    public static AppUtils getObj() {

        if (mAppUtils == null)
            mAppUtils = new AppUtils();

        return mAppUtils;
    }


    /**
     * the below method is used for adding a mandatory sign on top of text view
     *
     * @param mTextView
     */
    public void addStarOnTop(TextView mTextView) {

        String strTextInView = mTextView.getText().toString();
        if(strTextInView.contains("*")){
            strTextInView = strTextInView.replaceAll("\\*","");
        }

        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(strTextInView);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();

        builder.setSpan(new ForegroundColorSpan(Color.parseColor("#FE0103")), start, end,
                Spannable.SPAN_MARK_MARK);

        mTextView.setText(builder);
    }
    /**
     * the below method is used for adding a mandatory sign on top of text view
     *
     * @param mTextView
     */
    public void removeStarFromTop(TextView mTextView) {

        String strTextInView = mTextView.getText().toString();

        if(strTextInView.contains("*")){
            strTextInView = strTextInView.replaceAll("\\*","");
        }
        mTextView.setText(strTextInView);
    }



    /**
     * the below method is used for adding a mandatory sign on top of text view
     *
     * @param mEditText
     */
    public void addStarOnTopEditText(EditText mEditText) {

        String strTextInView = mEditText.getHint().toString();
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(strTextInView);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();

        builder.setSpan(new ForegroundColorSpan(Color.parseColor("#FE0103")), start, end,
                Spannable.SPAN_MARK_MARK);
        mEditText.setHint(builder);
    }


    public int getColor(Context context, int id) {

        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {

            return ContextCompat.getColor(context, id);

        } else {

            return context.getResources().getColor(id);

        }

    }


    /**
     * hide soft input keyboard
     */
    public void hideSoftKeyboard(Activity activity) {

        try {
            if(activity.getCurrentFocus()!=null) {
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * hide soft input keyboard
     */
    public void hideSoftKeyboardDialog(View view) {

        try {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public static void mGenerateHashKey(AppCompatActivity appCompatActivity) {
        try {
            PackageInfo info = appCompatActivity.getPackageManager().getPackageInfo("com.george.way", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * hide soft input keyboard
     */
    public void showSoftKeyboard(Activity activity) {

        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     *
     * @param mActivity
     * @param messageToShow
     */
    public void showMessageToast(Activity mActivity,String messageToShow){

        Toast.makeText(mActivity,messageToShow,Toast.LENGTH_SHORT).show();

    }


    /**
     * change format of date
     * @param dateToFormat
     * @return
     */
    public String changeDateFormat(String dateToFormat){

        String dateReversed = AppConstant.getObj().EMPTY_STRING;

        try {
            if(dateToFormat!=null && !TextUtils.isEmpty(dateToFormat)){

                String[] dateParts =dateToFormat.split("/");
                dateReversed = dateParts[2]+"-"+dateParts[1]+"-"+dateParts[0];

            }else {
                return dateReversed;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateReversed;

    }


    /**
     * change format of date
     * @param dateToFormat
     * @return
     */
    public String changeDateFormatToSlash(String dateToFormat){

        String dateReversed = AppConstant.getObj().EMPTY_STRING;

        try {
            if(dateToFormat!=null && !TextUtils.isEmpty(dateToFormat) && dateToFormat.contains("-")){

                String[] dateParts =dateToFormat.split("-");
                dateReversed = dateParts[2]+"/"+dateParts[1]+"/"+dateParts[0];

            }else {
                return dateReversed;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateReversed;

    }

    public static String getRealPathFromURI(Activity mActivity, Uri contentURI) {
        Cursor cursor = mActivity.getContentResolver().query(contentURI, null, null,
                null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }
    /**
     * get base 64 from image
     */
    public String encodeBase64(Activity activity,Uri imageUri) {

        try {
            if(imageUri!=null){
                File mImageFile = null;
                String uri = imageUri.toString();
                String tempPath = "";
                if (uri.startsWith("content")) {
                    //  tempPath = tempPath+"contractFilePDF://"+imageUri.getPath();
                    mImageFile = new File(getRealPathFromURI(activity, imageUri));
                } else {
                    tempPath = imageUri.getPath();
                    mImageFile = new File(tempPath);
                }

                final InputStream imageStream =activity. getContentResolver().openInputStream(Uri.fromFile(mImageFile));
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                Bitmap bitmap = getResizedBitmap(selectedImage, 80, 80);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
                byte[] b = baos.toByteArray();
                String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);
                return imageEncoded;

            }else {
                return AppConstant.getObj().EMPTY_STRING;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return AppConstant.getObj().EMPTY_STRING;
    }


    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    private Bitmap downscaleBitmapUsingDensities(final int sampleSize,Bitmap bmp)
    {
        final BitmapFactory.Options bitmapOptions=new BitmapFactory.Options();
        bitmapOptions.inDensity=sampleSize;
        bitmapOptions.inTargetDensity=1;
        bmp.setDensity(Bitmap.DENSITY_NONE);
        return bmp;
    }


    /**
     * reduces the size of the image
     * @param image
     * @param maxSize
     * @return
     */
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    /**
     * get image bmp
     * @param textView
     * @return
     */

    public Bitmap getImageBmpFromUri(Activity mActivity, TextView textView){
        try {
            Uri imageUri;
            String imagePath =textView.getText().toString();

            if(imagePath!=null && !TextUtils.isEmpty(imagePath)){
                imageUri = Uri.parse(imagePath);

                if (imageUri != null && !TextUtils.isEmpty(imageUri.toString())) {
                    final InputStream imageStream = mActivity.getContentResolver().openInputStream(imageUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                    //return scaleBitmap(selectedImage);
                    return selectedImage;
                }
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return  null;
    }
    /**
     * check if keyboard is open or not
     */
    public final void setKeyboardListener(Activity mActivity, final View views) {
        final View activityRootView = ((ViewGroup) mActivity.findViewById(android.R.id.content)).getChildAt(0);

        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {


            private final int DefaultKeyboardDP = 100;

            // From @nathanielwolf answer...  Lollipop includes button bar in the root. Add height of button bar (48dp) to maxDiff
            private final int EstimatedKeyboardDP = DefaultKeyboardDP + (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? 48 : 0);

            private final Rect r = new Rect();

            @Override
            public void onGlobalLayout() {
                // Convert the dp to pixels.
                int estimatedKeyboardHeight = (int) TypedValue
                        .applyDimension(TypedValue.COMPLEX_UNIT_DIP, EstimatedKeyboardDP, activityRootView.getResources().getDisplayMetrics());

                // Conclude whether the keyboard is shown or not.
                activityRootView.getWindowVisibleDisplayFrame(r);
                int heightDiff = activityRootView.getRootView().getHeight() - (r.bottom - r.top);
                boolean isShown = heightDiff >= estimatedKeyboardHeight;

                if (isShown) {
                    views.setVisibility(View.GONE);
                    return;
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            views.setVisibility(View.VISIBLE);
                        }
                    }, 50);
                    return;
                }

            }
        });
    }


    /**
     *
     * @param time
     * @param fromFormat
     * @param toFormat
     * @return
     */
    public String ChangeDateFormat(String time,String fromFormat,String toFormat) {

        SimpleDateFormat inputFormat = new SimpleDateFormat(fromFormat);
        SimpleDateFormat outputFormat = new SimpleDateFormat(toFormat);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;

}


    /**
     * detect if app is in foreground or background
     *
     * @param context
     * @return
     */
    public  boolean isAppIsInBackground(Context context) {

        boolean isInBackground = false;

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {

            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = true;
                            break;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = true;
                return isInBackground;
            }
        }

        return isInBackground;
    }




    public  boolean isAppForground(Context mContext) {

        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(mContext.getPackageName())) {
                return false;
            }
        }

        return true;
    }

    public CharSequence noTrailingwhiteLines(CharSequence text) {

        while (text.charAt(text.length() - 1) == '\n') {
            text = text.subSequence(0, text.length() - 1);
        }
        return text;
    }
    public static HashMap jsonToMap(String t) throws JSONException {

        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(t);
        Iterator<?> keys = jObject.keys();

        while( keys.hasNext() ){
            String key = (String)keys.next();
            String value = jObject.getString(key);
            map.put(key, value);

        }

        System.out.println("json : "+jObject);
        System.out.println("map : "+map);

        return  map;
    }

    public static String getTimeStr(long time){
        Date date = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        return simpleDateFormat.format(date);
    }

    public static String dateTime(Long dateTimestamp)
    {
        String timee= "";
        Timestamp timestamp = new Timestamp(dateTimestamp);
        Date date = new Date(timestamp.getTime());

        // S is the millisecond
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");

        timee = simpleDateFormat.format(timestamp);

        System.out.println(simpleDateFormat.format(timestamp));
        System.out.println(simpleDateFormat.format(date));
        return getTimeAgo(dateTimestamp);
    }

    public static String getFormattedDate(long smsTimeInMilis) {
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(smsTimeInMilis);

        Calendar now = Calendar.getInstance();

        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "EEEE, MMMM d, h:mm aa";
        final long HOURS = 60 * 60 * 60;
        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE) ) {
            return "" + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1  ){
            return "Yesterday " + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
            return DateFormat.format(dateTimeFormatString, smsTime).toString();
        } else {
            return DateFormat.format("MMMM dd yyyy, h:mm aa", smsTime).toString();
        }
    }
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

}
