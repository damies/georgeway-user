package com.george.way.utils;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import com.george.way.constant.AppConstant;

/**
 * Created by hemendrag on 2/20/2017.
 * this class holds the information of device
 */

public class DeviceInfoUtils {

    public static DeviceInfoUtils mDeviceInfoUtils;

    /**
     * create singleton method for getting single object of class
     *
     * @return
     */
    public static DeviceInfoUtils getObj() {

        if (mDeviceInfoUtils == null) {

            mDeviceInfoUtils = new DeviceInfoUtils();
        }

        return mDeviceInfoUtils;
    }


    /**
     * Returns the consumer friendly device type
     */
    public String getDeviceType() {
        return AppConstant.getObj().DEVICE_TYPE;
    }

    /**
     * Returns the consumer friendly device name
     */
    public String getDeviceModel() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }


    /**
     * get device id
     *
     * @return
     */
    public String getDeviceId(Context context) {

        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return android_id;
    }


    /**
     * Returns the consumer friendly device os
     */
    public String getDeviceOs() {
        return Build.VERSION.RELEASE;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }


}
