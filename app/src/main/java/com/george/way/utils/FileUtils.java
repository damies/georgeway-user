package com.george.way.utils;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by hemendrag on 5/29/2017.
 */

public class FileUtils {


    /**
     * create a singleton method to access variables of this class
     */
    public static FileUtils mFileUtils;
    private static Context mContext;
    private final String STORAGE_FILENAME = "GMC_VIDEO_FILE";

    public static  FileUtils getObj(Context context) {

        mContext = context;

        if (mFileUtils == null)
            mFileUtils = new FileUtils();

        return mFileUtils;
    }


    /**
     * save the video for later checking
     */
    public void saveVideoInFile(ArrayList<String> videoList) {
        try {
            FileOutputStream fos = mContext.openFileOutput(STORAGE_FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream of = new ObjectOutputStream(fos);
            of.writeObject(videoList);
            of.flush();
            of.close();
            fos.close();
        }
        catch (Exception e) {
            Log.e("InternalStorage", e.getMessage());
        }
    }


    /**
     * get the list of videos stored in file
     * @return
     */
    public ArrayList<String> getVideoListFromFile() {
        ArrayList<String> toReturn = new ArrayList<>();
        FileInputStream fis;
        try {
            fis = mContext.openFileInput(STORAGE_FILENAME);
            ObjectInputStream oi = new ObjectInputStream(fis);
            toReturn = (ArrayList<String>) oi.readObject();
            oi.close();
        } catch (Exception e) {
            Log.e("InternalStorage", e.getMessage());
        }
        return toReturn;
    }

}
