package com.george.way.utils;

import android.support.multidex.MultiDexApplication;

import com.george.way.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.ArrayList;

/**
 * Created by hem on 4/23/2017.
 */
public class GeorgeWayApplication extends MultiDexApplication {
    public static ArrayList<String> msg_id = new ArrayList<>();
    @Override
    public void onCreate() {
        super.onCreate();


        DisplayImageOptions opts = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisc(true).build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(opts)
                .build();
        ImageLoader.getInstance().init(config);

      //  MultiDex.install(this);

    }
    public static DisplayImageOptions intitOptions1() {
        // TODO Auto-generated method stub
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .showImageOnFail(R.drawable.img_placeholder)
                .showImageForEmptyUri(R.drawable.img_placeholder)
                .showImageOnLoading(R.drawable.img_placeholder)
                .considerExifParams(true)
                .displayer(new RoundedBitmapDisplayer((int) 27.5f)).build();
        return defaultOptions;
    }
    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static boolean activityResumed() {
        activityVisible = true;
        return  activityVisible;
    }

    public static boolean activityPaused() {
        activityVisible = false;
        return activityVisible;
    }
    public static boolean activityDestroyed() {
        activityVisible = false;
        return activityVisible;
    }
    private static boolean activityVisible;
}
