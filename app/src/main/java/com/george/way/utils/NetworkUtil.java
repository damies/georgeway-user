package com.george.way.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.george.way.R;
import com.george.way.webApi.ApiClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;


public class NetworkUtil {
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    public static NetworkUtil mNetworkUtil;
    /**
     * Set the number of retries when reestablishing Internet connection.
     */
    private static int retryConnectionNumber = 0;

    /**
     * The maximum number of retries allowed for Internet connection.
     */
    private final static int CONNECTION_RETRY_MAX = 1;

    /**
     * The timeout of the HTTP request when checking for Internet connection.
     */
    private final static int REQUEST_TIMEOUT = 700;

    public static NetworkUtil getObj() {
        if (mNetworkUtil == null) {

            mNetworkUtil = new NetworkUtil();
        }
        return mNetworkUtil;
    }

    public static int getConnectivityStatus(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (null != activeNetwork) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                    return TYPE_WIFI;

                if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                    return TYPE_MOBILE;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return TYPE_NOT_CONNECTED;
    }

    public static boolean isConnected(Context context) {
        int conn = NetworkUtil.getConnectivityStatus(context);
        if (conn == NetworkUtil.TYPE_MOBILE || conn == NetworkUtil.TYPE_WIFI) {
            return true;
        }


        return false;
    }

    /**
     * show the dailog in case if internet is not available
     *
     * @param context
     * @param okCallBack
     */
    private AlertDialog alertDialog;

    public void showAlertDialog(final Context context, DialogInterface.OnClickListener okCallBack) {

        if (alertDialog == null) {
            alertDialog = showDialog(context, okCallBack);
        }

        if (alertDialog.isShowing()) {
            alertDialog.dismiss();
        }

        AppCompatActivity appCompatActivity = (AppCompatActivity) context;
        if (appCompatActivity != null && !appCompatActivity.isFinishing()) {
            alertDialog.show();
        }

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
    }

    private AlertDialog.Builder alertDialogBuilder;

    private AlertDialog showDialog(final Context context, DialogInterface.OnClickListener okCallBack) {

        if (alertDialog == null) {
            alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder
                    .setTitle(R.string.network_error_title)
                    .setMessage(R.string.network_error_msg)
                    .setCancelable(false)
                    .setPositiveButton(R.string.setting, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            context.startActivity(new Intent(Settings.ACTION_SETTINGS));
                        }
                    })
                    .setNegativeButton(R.string.try_again, okCallBack);
            alertDialog = alertDialogBuilder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setCancelable(true);
        }

        if (alertDialog.isShowing()) {
            alertDialog.dismiss();
        }

        return alertDialog;
    }

    private AlertDialog showDialog1(final Context context) {

        if (alertDialog == null) {
            alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder
                    .setTitle(R.string.network_error_title)
                    .setMessage(R.string.network_error_msg)
                    .setCancelable(false)
                    .setPositiveButton(R.string.setting, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            context.startActivity(new Intent(Settings.ACTION_SETTINGS));
                        }
                    })
                    .setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog = alertDialogBuilder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setCancelable(true);
        }

        if (alertDialog.isShowing()) {
            alertDialog.dismiss();
        }

        return alertDialog;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = NetworkUtil.getConnectivityStatus(context);
        String status = null;

        if (conn == NetworkUtil.TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == NetworkUtil.TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }

    /**
     * Handler used that receives the connection status for the Internet.
     * If no active Internet connection will retry #CONNECTION_RETRY_MAX times
     */
    private static Handler listenForNetworkAvailability = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what != 1) { // code if not connected
                Log.i("NetworkCheck", "not connected");

                if (retryConnectionNumber <= CONNECTION_RETRY_MAX) {
                    Log.i("NetworkCheck", "checking for connectivity");

                    // Start the ping process again with delay
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isNetworkAvailable(listenForNetworkAvailability, REQUEST_TIMEOUT);
                        }
                    }, 5000);
                    retryConnectionNumber++;
                } else {
                    Log.i("NetworkCheck", "failed to establish an connection");
                    // code if not connected
                }
            } else {
                Log.i("NetworkCheck", "connected");
                retryConnectionNumber = 0;
                // code if connected
            }
        }
    };

    private static void isNetworkAvailable(final Handler handler,
                                           final int timeout) {
        new Thread() {
            private boolean responded = false;

            @Override
            public void run() {
                URL url = null;
                try {
                    url = new URL(ApiClient.BASE_URL);
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                }
                String host = "";
                if (null != url) {
                    host = url.getHost();
                }

                Log.i("NetworkCheck", "[PING] host: " + host);
                Process process = null;
                try {
                    process = new ProcessBuilder()
                            .command("/system/bin/ping", "-c 1",
                                    "-w " + (timeout / 1000), "-n", host)
                            .redirectErrorStream(true).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                InputStream in = process.getInputStream();
                StringBuilder s = new StringBuilder();
                int i;

                try {
                    while ((i = in.read()) != -1) {
                        s.append((char) i);

                        if ((char) i == '\n') {
                            Log.i("NetworkCheck",
                                    "[PING] log: " + s.toString());
                            if (s.toString().contains("64 bytes from")) {
                                // If there were a response from the server at
                                // all, we have Internet access
                                responded = true;
                            }
                            s.delete(0, s.length());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    // Destroy the PING process
                    process.destroy();

                    try {
                        // Close the input stream - avoid memory leak
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // Send the response to the handler, 1 for success, 0
                    // otherwise
                    handler.sendEmptyMessage(!responded ? 0 : 1);
                }
            }
        };
    }
}