package com.george.way.utils;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.george.way.R;
import com.george.way.prefrences.PreferenceUserUtil;

/**
 * Created by hemendrag on 5/29/2017.
 */

public class ThemeUtils {


    /**
     * create a singleton method to access variables of this class
     */
    public static ThemeUtils mFileUtils;
    private static Activity mContext;
    private int btnType = 0;
    public static final String COLOR_TYPE_GREEN = "COLOR_TYPE_GREEN";
    public static final String COLOR_TYPE_YELLOW = "COLOR_TYPE_YELLOW";
    public String COLOR_TYPE;

    public static ThemeUtils getObj(Activity context) {

        mContext = context;

        if (mFileUtils == null)
            mFileUtils = new ThemeUtils();

        return mFileUtils;
    }

    /**
     * set app theme
     *
     * @param toolbar
     * @param imagesArr
     */
    public void setUpTheme(Toolbar toolbar, ImageView[] imagesArr, TextView headerTextView) {

        PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance((Activity) mContext);
        btnType = preferenceUserUtil.getPreferenceIntData(preferenceUserUtil.KEY_BTN_TYPE);
        if (btnType == 1) {
            // GMC
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    String colorCode = preferenceUserUtil.getString(preferenceUserUtil.KEY_GMC_COLOR);
                    mContext.getWindow().setStatusBarColor(Color.parseColor(colorCode));
                    toolbar.setBackgroundColor(Color.parseColor(colorCode));

                    headerTextView.setTextColor(Color.WHITE);
                    for (int index = 0; index < imagesArr.length-1; index++) {
                        ImageView img = imagesArr[index];
                        img.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (btnType == 2) {
            // TEST

            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    String colorCode = preferenceUserUtil.getString(preferenceUserUtil.KEY_TP_COLOR);
                    mContext.getWindow().setStatusBarColor(Color.parseColor(colorCode));
                    toolbar.setBackgroundColor(Color.parseColor(colorCode));
                    headerTextView.setTextColor(Color.WHITE);

                    for (int index = 0; index < imagesArr.length-1; index++) {
                        ImageView img = imagesArr[index];
                        img.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (btnType == 3) {
            // Online

            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    String colorCode = preferenceUserUtil.getString(preferenceUserUtil.KEY_OTS_COLOR);
                    mContext.getWindow().setStatusBarColor(Color.parseColor(colorCode));
                    toolbar.setBackgroundColor(Color.parseColor(colorCode));
                    headerTextView.setTextColor(Color.WHITE);

                    for (int index = 0; index < imagesArr.length-1; index++) {
                        ImageView img = imagesArr[index];
                        img.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (btnType == 4) {
            // qod

            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    String colorCode = preferenceUserUtil.getString(preferenceUserUtil.KEY_QTD_COLOR);
                    mContext.getWindow().setStatusBarColor(Color.parseColor(colorCode));
                    toolbar.setBackgroundColor(Color.parseColor(colorCode));
                    headerTextView.setTextColor(Color.WHITE);


                    for (int index = 0; index < imagesArr.length-1; index++) {
                        ImageView img = imagesArr[index];
                        img.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (btnType == 5) {
            // feedback
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mContext.getWindow().setStatusBarColor(Color.parseColor("#E01E99"));
                    toolbar.setBackgroundColor(Color.parseColor("#E01E99"));
                    headerTextView.setTextColor(Color.WHITE);

                    for (int index = 0; index < imagesArr.length-1; index++) {
                        ImageView img = imagesArr[index];
                        img.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (btnType == 6) {
            // subscription
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mContext.getWindow().setStatusBarColor(Color.parseColor("#E19504"));
                    toolbar.setBackgroundColor(Color.parseColor("#E19504"));
                    headerTextView.setTextColor(Color.WHITE);

                    for (int index = 0; index < imagesArr.length-1; index++) {
                        ImageView img = imagesArr[index];
                        img.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    /**
     * update textView Background
     */
    public void updateTextViews(TextView textView) {


        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                PreferenceUserUtil preferenceUserUtil = PreferenceUserUtil.getInstance((Activity) mContext);
                boolean isBtnClicked = preferenceUserUtil.getPreferenceBooleanData(preferenceUserUtil.KEY_BTN_CLICKED);
                btnType = preferenceUserUtil.getPreferenceIntData(preferenceUserUtil.KEY_BTN_TYPE);

                if (isBtnClicked && btnType == 1) {


                    textView.setTextColor(Color.WHITE);

                    try {

                        COLOR_TYPE = preferenceUserUtil.getString(preferenceUserUtil.KEY_SELECTED_COLOR_TYPE);
                        if (COLOR_TYPE.equalsIgnoreCase(COLOR_TYPE_GREEN)) {
                            textView.setBackgroundColor(AppUtils.getObj().getColor(mContext, R.color.home_red));
                            textView.setTextColor(AppUtils.getObj().getColor(mContext, R.color.colorWhite));

                        } else if (COLOR_TYPE.equalsIgnoreCase(COLOR_TYPE_YELLOW)) {
                            textView.setBackgroundColor(AppUtils.getObj().getColor(mContext, R.color.classGreenColor));
                            textView.setTextColor(AppUtils.getObj().getColor(mContext, R.color.colorWhite));

                        } else {
                            textView.setBackgroundColor(AppUtils.getObj().getColor(mContext, R.color.planColorRed));
                            textView.setTextColor(Color.WHITE);

                        }
                    } catch (Exception ex) {
                        textView.setBackgroundColor(AppUtils.getObj().getColor(mContext, R.color.planColorRed));
                    }


                } else if (isBtnClicked && btnType == 2) {


                    try {

                        COLOR_TYPE = preferenceUserUtil.getString(preferenceUserUtil.KEY_SELECTED_COLOR_TYPE);
                        if (COLOR_TYPE.equalsIgnoreCase(COLOR_TYPE_GREEN)) {
                            textView.setBackgroundColor(AppUtils.getObj().getColor(mContext, R.color.colorWhite));
                            textView.setTextColor(AppUtils.getObj().getColor(mContext, R.color.classGreenColor));
                        } else if (COLOR_TYPE.equalsIgnoreCase(COLOR_TYPE_YELLOW)) {
                            textView.setBackgroundColor(AppUtils.getObj().getColor(mContext, R.color.colorWhite));
                            textView.setTextColor(AppUtils.getObj().getColor(mContext, R.color.classYellowColor));
                        } else {
                            textView.setBackgroundColor(AppUtils.getObj().getColor(mContext, R.color.planColorRed));
                            textView.setTextColor(Color.WHITE);
                        }
                    } catch (Exception ex) {
                        textView.setBackgroundColor(AppUtils.getObj().getColor(mContext, R.color.planColorRed));
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
