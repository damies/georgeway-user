package com.george.way.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;

import com.george.way.R;
import com.george.way.easyImagePick.GeorgeWayImagePick;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by hemendrag on 1/11/2017.
 */

public class ViewUtils {

    private final float BITMAP_SCALE = 0.4f;
    private final float BLUR_RADIUS = 7.5f;

    /**
     * create a singleton method to access variables of this class
     */
    public static ViewUtils mViewUtils;

    public static ViewUtils getObj() {

        if (mViewUtils == null)
            mViewUtils = new ViewUtils();

        return mViewUtils;
    }



    /**
     * setting up bitmap into image views
     **/
    public void updateBitmapOnImageViews(Context mContext, Uri uriToSet,
                                         ImageView imageView) {


           /* try {
                GeorgeWayImagePick.getRotatedImage(mContext,uriToSet, imageView);

          } catch (Exception e) {
              e.printStackTrace();
            }
*/

        try {
            String imagePath = GeorgeWayImagePick.getRealPathFromURI((Activity) mContext, uriToSet);
            File image = new File(imagePath);
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap convertedBmp = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
            imageView.setImageBitmap(convertedBmp);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * setting up image uri into image views
     */
    public void setUriInImage(String urlToSet, ImageView imageView) {

        if (urlToSet.isEmpty()) {
            imageView.setImageResource(R.drawable.img_not_available);

        } else {
            ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance
            imageLoader.displayImage(urlToSet, imageView);
        }


    }



    public String getExtensionFromUrl(String imgUrl) {

        String filenameArray[] = imgUrl.split("\\.");
        String extension = filenameArray[filenameArray.length - 1];
        return extension;
    }

    public static String getMimeType(Context context, Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }



    // the method validate_text that forces the user to a specific pattern
    public void validateTotalCost(EditText text) {

        InputFilter[] filter = new InputFilter[1];
        filter[0] = new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {

                if (end > start) {
                    String destText = dest.toString();
                    String resultingText = destText.substring(0, dstart)
                            + source.subSequence(start, end)
                            + destText.substring(dend);
                    if (!resultingText
                            .matches("^\\d{1,15}(\\.(\\d{1,2})?)?")) {
                        return "";
                    }
                }

                return null;
            }
        };
        text.setFilters(filter);
    }

    /**
     * set  data in list
     * @return
     */
    public ArrayList<String> getMonthList() {

        ArrayList<String> monthList = new ArrayList<>();
        monthList.add("Select Month");
        for (int index= 0; index<12;index++){
            monthList.add(""+(index+1));
        }
        return monthList;
    }

    public static String dataDecode(String textEncoded) {
        String text = "";
        try {
            byte[] data = Base64.decode(textEncoded, Base64.DEFAULT);
            text = new String(data, "UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return text;
    }

    public static String dataEncode(String textDecode) {
        String base64 = "";
        try {
            byte[] data = textDecode.getBytes("UTF-8");
            base64 = Base64.encodeToString(data, Base64.DEFAULT);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return base64;
    }


}
