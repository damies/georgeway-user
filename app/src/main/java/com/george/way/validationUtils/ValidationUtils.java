package com.george.way.validationUtils;

import android.app.Activity;
import android.text.TextUtils;
import android.widget.EditText;

import com.george.way.R;
import com.george.way.dialog.ValidationErrorDialog;

import java.util.regex.Pattern;

/**
 * Created by hemendrag on 1/13/2017.
 * this class is basically used for validating fields
 * of the project
 */

public class ValidationUtils {

    /**
     * create a singleton method to access variables of this class
     */
    public static ValidationUtils mValidationUtils;
    private static Activity mActivity;
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static ValidationUtils getLoginObj(Activity activity) {

        mActivity = activity;

        if (mValidationUtils == null)
            mValidationUtils = new ValidationUtils();

        return mValidationUtils;
    }


    /************************************LOGIN FORM VALIDATION ************************************/

    /**
     * method to validate the LOGIN form
     *
     * @return true if LOGIN fields have no issue
     */
    public static boolean validateLoginForm(EditText mEdtEmailAddress, EditText mEdtPassword) {


        if (isValidEmailAddress(mEdtEmailAddress)
                && isValidLoginPassword(mEdtPassword)) {

            return true;

        } else {

            return false;
        }
    }


    /**
     * method to validate the Forget password form
     *
     * @return true if  Forget password form fields have no issue
     */
    public static boolean validateForgetPwdForm(EditText mEdtEmailAddress) {

        if (isValidFwdEmailAddress(mEdtEmailAddress)) {

            return true;

        } else {

            return false;
        }
    }


    /************************************LOGIN FORM VALIDATION ************************************/

    /**
     * method to validate the LOGIN form
     *
     * @return true if Regsitration fields have no issue
     */
    public   boolean validateRegistrationForm(EditText editTextFirstName,
                                                   EditText editTextLastName,
                                                   /*EditText editTextUserName,*/
                                                   EditText editTextAge,
                                                   EditText editTextEmail,
                                                   EditText editTextPassword,
                                                   EditText editTextCPassword) {


        if (    isValidEditField(editTextFirstName)
                && isValidEditField(editTextFirstName)
                && isValidEditField(editTextLastName)
//                 && isValidEditField(editTextUserName)
               // && isValidEditField(editTextAge)
                && isValidEmailAddress(editTextEmail)
                && isValidLoginPassword(editTextPassword)
                && isValidLoginPassword(editTextCPassword)
                && isValidConfirmPwd(editTextPassword, editTextCPassword)){

            return true;

        } else{

            return false;
        }
    }


    public   boolean validateRegistrationForm(EditText editTextFirstName,
                                              EditText editTextLastName,
                                              EditText editTextEmail
                                             ) {


        if (    isValidEditField(editTextFirstName)
                && isValidEditField(editTextFirstName)
                && isValidEditField(editTextLastName)
//                 && isValidEditField(editTextUserName)
                // && isValidEditField(editTextAge)
                && isValidEmailAddress(editTextEmail)
               ){

            return true;

        } else{

            return false;
        }
    }

    /**
     * valid EMAIL
     *
     * @param edtTxtEmailAddress
     * @return
     */
    public static boolean isValidEmailAddress(EditText edtTxtEmailAddress) {

        boolean validation = true;
        Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(EMAIL_PATTERN);
        String login_email_address = edtTxtEmailAddress.getText().toString();

        String hintInField = edtTxtEmailAddress.getHint().toString().replace("*", "");


        String messageBlank = "Please enter your " + hintInField + ".";
        String Please_enter_valid = "Please enter a valid Email.";


        if (login_email_address.length() == 0) {

            validation = false;

            /**
             * show error dialog in case of error
             */
            ValidationErrorDialog.getValidationUtilsObj(mActivity).showErrorDialog(messageBlank);

        } else if (login_email_address.contains("@") && !EMAIL_ADDRESS_PATTERN.matcher(login_email_address).matches()) {

            validation = false;
            /**
             * show error dialog in case of error
             */
            ValidationErrorDialog.getValidationUtilsObj(mActivity).showErrorDialog(Please_enter_valid);

        }

        return validation;

    }

    /**
     * method to validate the room name string
     *
     * @return true if cost string is valid
     */
    public boolean isValidEditField(EditText editText) {
        boolean validation = true;

        String dateSelected = editText.getText().toString();
        String hintDate = editText.getHint().toString();
        if (hintDate.contains(".")) {
            hintDate = hintDate.replaceAll("\\.", "");
        }

        String messageToShow = "Please enter " + hintDate + ".";
        if (dateSelected.length() <= 0) {
            validation = false;

            /**
             * show error dialog in case of error
             */
            ValidationErrorDialog.getValidationUtilsObj(mActivity).showErrorDialog(messageToShow);
        }

        return validation;
    }

    /**
     * method to validate the  confirm password string
     *
     * @return true if  confirm password string is exactly same with password
     */
    public static boolean isValidConfirmPwd(EditText editTextPwd, EditText editTextConfirmPwd) {

        String pwd = editTextPwd.getText().toString();
        String confirmPwd = editTextConfirmPwd.getText().toString();

        if (!TextUtils.isEmpty(pwd) && TextUtils.isEmpty(confirmPwd)) {

            ValidationErrorDialog.getValidationUtilsObj(mActivity).showErrorDialog("Please confirm your password");
            return false;
        } else if (TextUtils.isEmpty(pwd) && !TextUtils.isEmpty(confirmPwd)) {

            ValidationErrorDialog.getValidationUtilsObj(mActivity).showErrorDialog("Confirm password will not be applied without password.");
            return false;
        } else if (!TextUtils.isEmpty(pwd)&& pwd.length() >0 && pwd.length() < 6) {

            ValidationErrorDialog.getValidationUtilsObj(mActivity).showErrorDialog(mActivity.getResources().getString(R.string.error_password_length_min));
            return false;

        } else if (!TextUtils.isEmpty(pwd) && !TextUtils.isEmpty(confirmPwd)) {

            if (!editTextPwd.getText().toString().equalsIgnoreCase(confirmPwd)) {

                ValidationErrorDialog.getValidationUtilsObj(mActivity).showErrorDialog("Password and confirm password does not match");
                return false;

            } else {

                return true;
            }
        }

        return true;

    }


    /**
     * valid login
     *
     * @param edtTxtEmailAddress
     * @return
     */
    public static boolean isValidFwdEmailAddress(EditText edtTxtEmailAddress) {

        boolean validation = true;
        Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(EMAIL_PATTERN);
        String login_email_address = edtTxtEmailAddress.getText().toString();

        String messageBlank = mActivity.getResources().getString(R.string.forgot_pwd);
        String Please_enter_valid = "Please enter a valid email id.";


        if (login_email_address.length() == 0) {

            validation = false;

            /**
             * show error dialog in case of error
             */
            ValidationErrorDialog.getValidationUtilsObj(mActivity).showErrorDialog(messageBlank);

        } else if (!EMAIL_ADDRESS_PATTERN.matcher(login_email_address).matches()) {

            validation = false;
            /**
             * show error dialog in case of error
             */
            ValidationErrorDialog.getValidationUtilsObj(mActivity).showErrorDialog(Please_enter_valid);

        }

        return validation;

    }


    /**
     * method to validate the password string
     *
     * @param edt_password reference to a EditText to which value to be validate
     * @return true if password string is valid
     */
    public static boolean isValidLoginPassword(EditText edt_password
    ) {
        boolean validation = true;
        String password = edt_password.getText().toString();
        if (password.length() <= 0) {
            validation = false;

            /**
             * show error dialog in case of error
             */
            ValidationErrorDialog.getValidationUtilsObj(mActivity).showErrorDialog(mActivity.getResources().getString(R.string.error_passwordBlank));
        }
        /*else if (password.length() <= 5) {
            validation = false;
            *//**
         * show error dialog in case of error
         *//*
            ValidationErrorDialog.getInstance(mActivity).showErrorDialog(mActivity.getResources().getString(R.string.error_password_length_min));
        }
        else if (password.length() >=20) {
            validation = false;
            *//**
         * show error dialog in case of error
         *//*
            ValidationErrorDialog.getInstance(mActivity).showErrorDialog(mActivity.getResources().getString(R.string.error_password_length_max));

        }*/
        return validation;
    }
    /**
     *
     *
     * @return true if Edit profile fields have no issue
     */
    public   boolean validateEditProfileForm(EditText editTextFirstName,
                                             EditText editTextLastName,
                                             EditText editTextPassword,
                                             EditText editTextCPassword) {


        if (    isValidEditField(editTextFirstName)
                && isValidEditField(editTextFirstName)
                && isValidEditField(editTextLastName)
                && isValidLoginPassword(editTextPassword)
                && isValidLoginPassword(editTextCPassword)
                && isValidConfirmPwd(editTextPassword, editTextCPassword)){

            return true;

        } else{

            return false;
        }
    }

}
