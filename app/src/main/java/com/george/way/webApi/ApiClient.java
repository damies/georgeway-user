package com.george.way.webApi;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hemendrag on 1/30/2017.
 * this class contains the web api of vac admin app
 */

public class ApiClient {

    /**
     * create singleton for accessing variables
     */
    public static ApiClient mApiClient;
    public static Retrofit retrofit;

    public static ApiClient getClient() {

        if (mApiClient == null) {
            mApiClient = new ApiClient();
        }
        return mApiClient;
    }


    /**
     * get api client request
     *
     * @return
     */
    public Retrofit getRetrofitInstance() {

        if (retrofit == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(120000, TimeUnit.SECONDS)
                    .addInterceptor(logging)
                    .readTimeout(120000,TimeUnit.SECONDS).build();

            retrofit = new Retrofit.Builder().baseUrl(ApiClient.BASE_URL)

                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofit;
    }

    /*
   * Local URL
   * */
    //public static String BASE_URL = "http://demo.karnielectrical.com/";

    /*
   * Live URL
   * */
    public static String BASE_URL = "https://georgewayglobal.com/";
    public static String NOTIFICATION_URL = "https://us-central1-gw-tutor.cloudfunctions.net/sendNotification";


    public  static  final String PHP = ".php?";
//    http://georgewayglobal.com/current-plan.php?userid=3

    /**
     * declare here the web api of project
     */
    public static final String LOGIN_API = "login";
    public static final String FORGOT_PASSWORD_API = "forget_password";
    public static final String REGISTRATION_API = "user_registration";
    public static final String CATEGORY_FETCH_API = "fetch_category.php";
    public static final String FETCH_QUESTION_OFTHEDAY = "fetch_question_oftheday.php";
    public static final String FETCH_QUESTIONS_API = "fetch_questions";
    public static final String TEST_PREPARATION = "test_prepration.php";
    public static final String PACKAGES_API = "pakages.php?";
    public static final String SUBSCRIBE_PLAN_API = "subscribe_plan.php?";
    public static final String HOME_DATA_API = "get_HomeData_User.php?";
    public static final String GET_FEEDBACK_API = "getAppFeedback.php?";
    public static final String SUBMIT_FEEDBACK_API = "submit_user_feedback.php?";
    public static final String SAVE_CHAT_API = "save_chat";
    public static final String FETCH_CHAT_API = "fetch_user_chat";
    public static final String FETCH_SUCCESS_STORY_API = "fetch_successstory.php";
    public static final String ABOUT_US_API = "aboutus.php";
    public static final String TERM_API = "term.php";
    public static final String LOGOUT_API = "logout.php?";
    public static final String FETCH_REWARD_API = "fetch_reward.php?";
    public static final String UPDATE_PROFILE_API = "updateProfile";
    public static final String QOD_UPDATE_API = "question_log.php?";
    public static final String CHAT_DELETE_API = "chat_delete.php?";
    public static final String CURRENT_PLAN = "current-plan.php";
    public static final String UPDATE_SUBSCRIBBE_TIME = "update_subscribe_time.php";
    public static final String USER_DONATE = "doner.php?";
    public static final String DONATE_TITLE = "donor_page_title.php";


}
