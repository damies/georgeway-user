
package com.george.way.webApi;

import com.george.way.model.FetchChatResponse;
import com.george.way.model.QuestionOfTheDayResponse;
import com.george.way.model.TestPreparationResponse;
import com.george.way.model.allQuestions.QuestionsDataModel;
import com.george.way.model.category.CategoryDataModel;
import com.george.way.model.feedbacks.FeedbackDataModel;
import com.george.way.model.packages.PlanPackages;
import com.george.way.model.reward.RewardDataModel;
import com.george.way.model.successStory.SuccessStoryModel;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by hemendrag on 1/30/2017.
 * this class contains the fields and data of api request/response
 */

public interface ApiInterface {


    @GET(ApiClient.LOGIN_API + ApiClient.PHP)
    Call<ResponseBody> loginRequest(@Query("username") String username,
                                    @Query("password") String password,
                                    @Query("device_token") String device_token,
                                    @Query("device_type") String device_type,
                                    @Query("device_id") String device_id);


    @GET("/social_login.php")
    Call<ResponseBody> socialloginRequest(@Query("email") String username,
                                    @Query("social_id") String social_id,
                                    @Query("social_type") String social_type,
                                    @Query("device_token") String device_token,
                                    @Query("device_type") String device_type,
                                    @Query("device_name") String device_name);




    @GET(ApiClient.FORGOT_PASSWORD_API + ApiClient.PHP)
    Call<ResponseBody> forgetPwdRequest(@Query("email") String email);

    @Multipart
    @POST(ApiClient.REGISTRATION_API + ApiClient.PHP)
    Call<ResponseBody> registerUserRequest(@Part("first_name") RequestBody first_name,
                                           @Part("last_name") RequestBody last_name,
                                           @Part("email") RequestBody email,
                                           @Part("password") RequestBody password,
                                           @Part("address") RequestBody address,
                                           @Part("age") RequestBody age,
                                           @Part("device_type") RequestBody device_type,
                                           @Part("device_token") RequestBody device_token,
                                           @Part("device_id") RequestBody device_id,
                                           @Part("os_version") RequestBody os_version,
                                           @Part("subscription_plan") RequestBody subscription_plan,
                                           @Part MultipartBody.Part image);


    @Multipart
    @POST("/social_registration.php")
    Call<ResponseBody> socialregisterUserRequest(@Part("first_name") RequestBody first_name,
                                           @Part("last_name") RequestBody last_name,
                                           @Part("email") RequestBody email,
                                           @Part("address") RequestBody address,
                                           @Part("age") RequestBody age,
                                         /*  @Part("username") RequestBody username,*/
                                           @Part("device_type") RequestBody device_type,
                                           @Part("device_token") RequestBody device_token,
                                           @Part("social_type") RequestBody social_type,
                                           @Part("social_id") RequestBody social_id
                                         );



    @GET(ApiClient.CATEGORY_FETCH_API)
    Call<CategoryDataModel> fetchCategoryRequest();

    @GET(ApiClient.FETCH_QUESTION_OFTHEDAY)
    Call<QuestionOfTheDayResponse> FETCH_QUESTION_OFTHEDAY();


    @GET(ApiClient.FETCH_QUESTIONS_API + ApiClient.PHP)
    Call<QuestionsDataModel> fetchQuestionsRequest();

    @GET(ApiClient.TEST_PREPARATION)
    Call<TestPreparationResponse> testPreprationRequest();


    @GET(ApiClient.PACKAGES_API)
    Call<PlanPackages> getPlansRequest();


    @GET(ApiClient.SUBSCRIBE_PLAN_API)
    Call<ResponseBody> subscribePlanRequest(@Query("userid") String userid,
                                            @Query("planid") String planid,
                                            @Query("plan_period") String plan_period,
                                            @Query("amount") String amount ,
                                            @Query("plan_type") String plan_type,@Query("token") String token,
                                            @Query("subscription_time") String subscription_time );

    @GET(ApiClient.HOME_DATA_API)
    Call<ResponseBody> getHomeDataRequest(@Query("userid") String userid);


    @GET(ApiClient.USER_DONATE)
    Call<ResponseBody> userDonate(@Query("email") String email,@Query("amount") String amount,@Query("token") String token,@Query("month") String month);

    @GET(ApiClient.GET_FEEDBACK_API)
    Call<FeedbackDataModel> getFeedBacksRequest(@Query("page_num") int page_num);

    @GET(ApiClient.SUBMIT_FEEDBACK_API)
    Call<ResponseBody> submitFeedBacksRequest(@Query("userid") String userid,
                                              @Query("email") String email,
                                              @Query("feedback") String feedback
    );


    @GET(ApiClient.FETCH_CHAT_API + ApiClient.PHP)
    Call<FetchChatResponse> fetchUserChat(@Query("userid") String userId);

    @GET(ApiClient.FETCH_SUCCESS_STORY_API)
    Call<SuccessStoryModel> fetchSucessStoryRequest();

    @GET(ApiClient.QOD_UPDATE_API)
    Call<ResponseBody> updateQODRequest(@Query("userid") String userId,
                                        @Query("question_id") String question_id,
                                        @Query("question_result") String question_result);


    @GET(ApiClient.ABOUT_US_API)
    Call<ResponseBody> getAboutUsRequest();

    @GET(ApiClient.TERM_API)
    Call<ResponseBody> getTERMRequest();

    @Multipart
    @POST(ApiClient.SAVE_CHAT_API + ApiClient.PHP)
    Call<ResponseBody> saveChat_Image(@Part("userid") RequestBody userid,
                                      @Part MultipartBody.Part image);

    @GET(ApiClient.SAVE_CHAT_API + ApiClient.PHP)
    Call<ResponseBody> saveChat(@Query("userid") String userId,
                                @Query("message") String message);

    @GET(ApiClient.LOGOUT_API)
    Call<ResponseBody> logoutApiRequest(@Query("userid") String userId);
    @GET(ApiClient.FETCH_REWARD_API)
    Call<RewardDataModel> fetchRewardApiRequest(@Query("month") String month,
                                                @Query("year") String year);

    @Multipart
    @POST(ApiClient.UPDATE_PROFILE_API + ApiClient.PHP)
    Call<ResponseBody> editPRofileRequest(@Part("first_name") RequestBody first_name,
                                          @Part("last_name") RequestBody last_name,
                                          @Part("user_id") RequestBody userId,
                                          @Part("password") RequestBody username,
                                          @Part MultipartBody.Part image);

    @GET(ApiClient.CHAT_DELETE_API)
    Call<ResponseBody> deleteChat(@Query("id") String id);

    @GET(ApiClient.CURRENT_PLAN)
    Call<ResponseBody> currentPlan(@Query("userid") String userid);

    @GET(ApiClient.UPDATE_SUBSCRIBBE_TIME)
    Call<ResponseBody> updateSubscribTime(@Query("userid") String userid,@Query("subscription_time") String subscription_time,@Query("tutorid") String tutorid  );

    @GET(ApiClient.DONATE_TITLE)
    Call<ResponseBody> donateTitle();

    @GET("/tutor/school_list.php")
    Call<ResponseBody> getSchoolList();

    @GET("/tutor/course_list.php")
    Call<ResponseBody> getCourseList();

    @POST
    Call<ResponseBody> sendNotification(@Url String url,@Body HashMap data);

    @GET("get_rating.php")
    Call<ResponseBody> sendRating(@Query("user_id") String user_id,
                                  @Query("tutor_id") String tutor_id,
                                  @Query("rating") String rating);

}

