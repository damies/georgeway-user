package com.george.way.webApi;

import android.app.Activity;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;

import com.george.way.jcVideoPlayer.JCVideoPlayerStandard;

import java.util.HashMap;

public class GetThumbNailServerAsync extends AsyncTask<Void, Void, Bitmap> {

    private String videoPath;
    private ImageView imageViewVideo;
    private JCVideoPlayerStandard mJCVideoPlayerStandard;
    private Activity mActivity;

    public GetThumbNailServerAsync(Activity mActivity, String videoPath, JCVideoPlayerStandard mJCVideoPlayerStandard,
                                   ImageView imageViewVideo) {

        this.mActivity = mActivity;
        this.videoPath = videoPath;
        this.imageViewVideo = imageViewVideo;
        this.mJCVideoPlayerStandard = mJCVideoPlayerStandard;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mJCVideoPlayerStandard.pbLoading.setVisibility(View.VISIBLE);
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                // no headers included
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mJCVideoPlayerStandard.pbLoading.setVisibility(View.GONE);
                }
            });

        } finally {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mJCVideoPlayerStandard.pbLoading.setVisibility(View.GONE);
                }
            });
            if (mediaMetadataRetriever != null)
                mediaMetadataRetriever.release();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);

        mJCVideoPlayerStandard.pbLoading.setVisibility(View.GONE);

        try {
            if (bitmap != null)
                imageViewVideo.setImageBitmap(bitmap);
                imageViewVideo.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }
}