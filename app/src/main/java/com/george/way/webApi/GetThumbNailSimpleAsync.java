package com.george.way.webApi;

import android.app.Activity;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.george.way.jcVideoPlayer.JCVideoPlayerSimple;

public class GetThumbNailSimpleAsync extends AsyncTask<Void, Void, Bitmap> {

    private String videoPath;
    private ImageView imageViewVideo;
    private JCVideoPlayerSimple mJCVideoPlayerStandard;
    private Activity mActivity;

    public GetThumbNailSimpleAsync(Activity mActivity, String videoPath, JCVideoPlayerSimple mJCVideoPlayerStandard,
                                   ImageView imageViewVideo) {

        this.mActivity = mActivity;
        this.videoPath = videoPath;
        this.imageViewVideo = imageViewVideo;
        this.mJCVideoPlayerStandard = mJCVideoPlayerStandard;

    }


    @Override
    protected Bitmap doInBackground(Void... voids) {
        Bitmap bitmap = null;

            try {
                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                retriever.setDataSource(mActivity, Uri.parse(videoPath));
                  bitmap = retriever
                        .getFrameAtTime(100000,MediaMetadataRetriever.OPTION_PREVIOUS_SYNC);

                if (retriever != null)
                    retriever.release();

              } catch (Exception e) {
                e.printStackTrace();
            }

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);

        try {
            if (bitmap != null)
                imageViewVideo.setImageBitmap(bitmap);
            imageViewVideo.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }
}